/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file mcq_types.h
 * \date January 10, 2011
 * \author Edlira Nano
 * \brief This header contains all the type re-definitions and all 
 * the global variables definitions used in MassChroQ. 
 * 
 * For configuration global variable definitions see config.h.cmake file.
 */

#include <QString>

#ifndef _MCQ_TYPES_H_
#define _MCQ_TYPES_H_ 1


/************ Typedefs **************************************************/

/** \var typedef double mcq_double
    \brief A type definition for doubles
*/
typedef double mcq_double;

/** \var typedef float mcq_float
    \brief A type definition for floats
*/
typedef float mcq_float;

/** \var typedef QString mcq_xml_format
    \brief A type definition for xml formats (mzxml, mzml, ...)
*/
typedef QString mcq_xml_format;

/** \var typedef QString mcq_quanti_item_type
    \brief A type definition for quantification item types (mz, mzrt or peptides)
*/
typedef QString mcq_quanti_item_type;

/** \var typedef int mcq_slice_index_id;
    \brief A type definition for the unique ID of slices
*/
typedef int mcq_slice_index_id;

/** \var typedef QString mcq_matching_mode
    \brief A type definition for peptide to peak matching modes (mean, real_or_mean, post_matching)
*/
typedef QString mcq_matching_mode;

/** \var typedef QString mcq_xic_type
    \brief A type definition for xic types (max or sum)
*/
typedef QString mcq_xic_type;

/** \var typedef int mcq_slice_index_id;
    \brief A type definition for the unique ID of slices
*/
typedef int mcq_slice_index_id;

/** \var typedef QString mcq_filter_type
    \brief A type definition for filter types
*/
typedef QString mcq_filter_type;



/*********** Global variables definitions*********************************/

/** \def MHPLUS 1.007825
    \brief The (monoisotopic) mass of the H+ atom
*/
const mcq_double MHPLUS(1.007825);

/** \def ONEMILLION 1000000
    \brief One million integer, why not.
*/
const mcq_double ONEMILLION(1000000);

/** \def MAX_SLICE_SIZE 262144000
    \brief maximum size of an msrun slice in bytes 
    (1048576 (=1Mbyte) * 300 = 314572800, * 250 = 262144000) 
*/
const mcq_double MAX_SLICE_SIZE(262144000);

/** \def MZXML "mzxml"
    \brief mzxml xml format
*/
const mcq_xml_format MZXML("mzxml");

/** \def MZML "mzml"
    \brief mzml xml format
*/
const mcq_xml_format MZML("mzml");

/** \def TMP_FILENAME_PREFIX "masschroq_tmp_file";
    \brief template filename prefix for the temporary files generated by masschroq 
*/ 
const QString TMP_FILENAME_PREFIX("masschroq_tmp_file");

/** \def MZ_ITEM "mz"
    \brief mz quantification item type
*/
const mcq_quanti_item_type MZ_QUANTI_ITEM("mz");

/** \def MZRT_ITEM "mz-rt"
    \brief mz-rt quantification item type
*/
const mcq_quanti_item_type MZRT_QUANTI_ITEM("mz-rt");

/** \def PEPTIDE_ITEM "peptide"
    \brief peptide quantification item type
*/
const mcq_quanti_item_type PEPTIDE_QUANTI_ITEM("peptide");

/** \def MEAN_MODE "mean"
    \brief mean rt peptide to peak matching mode
*/
const mcq_matching_mode MEAN_MODE("mean");

/** \def REAL_OR_MEAN_MODE "real_or_mean"
    \brief real_or_mean rt peptide to peak matching mode
*/
const mcq_matching_mode REAL_OR_MEAN_MODE("real_or_mean");

/** \def POST_MATCHING_MODE "post_matching"
    \brief post matching rt peptide to peak matching mode
*/
const mcq_matching_mode POST_MATCHING_MODE("post_matching");

/** \def NO_MATCHING_MODE "no_matching"
    \brief no matching mode (for mz and mzrt quanti items)
*/
const mcq_matching_mode NO_MATCHING_MODE("no_matching");


/** \def MAX_XIC_TYPE "max"
    \brief max xic type
*/
const mcq_xic_type MAX_XIC_TYPE("max");

/** \def SUM_XIC_TYPE "sum"
    \brief max xic type
*/
const mcq_xic_type SUM_XIC_TYPE("sum");

/** \def NULL_SLICE_ID -1 
    \brief non valid slice index id
*/
const mcq_slice_index_id NULL_SLICE_ID(-1);

/** \def FILTER_BACKGROUND "background"
    \brief filter background type
*/
const mcq_filter_type FILTER_BACKGROUND("background");

/** \def FILTER_SPIKE "spike"
    \brief filter spike type
*/
const mcq_filter_type FILTER_SPIKE("spike");

/** \def FILTER_SMOOTHING "smoothing"
    \brief filter smoothing type
*/
const mcq_filter_type FILTER_SMOOTHING("smoothing");

/** \def FILTER_MINMAX "minmax"
    \brief filter minmax type
*/
const mcq_filter_type FILTER_MINMAX("minmax");

/** \def FILTER_MAXMIN "maxmin"
    \brief filter maxmin type
*/
const mcq_filter_type FILTER_MAXMIN("maxmin");


#endif /* _MCQ_TYPES_H_ */
