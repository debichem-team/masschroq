
/** LICENCE
	This file is part of "quantimscpp".

    quantimscpp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    quantimscpp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with quantimscpp.  If not, see <http://www.gnu.org/licenses/>.
    
    authors:
     Olivier Langella <olivier.langella@moulon.inra.fr>
     Benoit Valot <benoit.valot@moulon.inra.fr>

 */

/** \brief xicextractor
 * 
 * uses a list of mass to analyze
 * this detects and quantify peaks
 * and produces a list of detected peaks
 * 
 * \author Olivier Langella <olivier.langella@moulon.inra.fr>
 * 
  * \param xml file (cf schema) containing parameters and list of mass
 */

/*
 * 
 XML parameters file example :
 doc/xicextractor_data_file.xml
*/

#include "lib/xic_extractor.h"

//using namespace std;

int main (int argc, char **argv) {

	QString fileName(argv[1]);
	
	xicExtractor the_extractor;
	
	the_extractor.setParam(fileName);	
	the_extractor.detectAndQuantify();
	
	//the_extractor.printExtractorResults(cout);
	the_extractor.printXmlResults();
	
	return 0;
}
