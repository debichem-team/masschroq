#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QDockWidget>
#include <QApplication>
#include <QCloseEvent>
#include <QScrollArea>
#include <QDialog>
#include <QTextEdit>

#include "parameterMainWindow.h"
#include "quantificationwidget/masschroqml_selection_widget.h"
#include "quantificationwidget/xicSelectionWidget.h"
#include "quantificationwidget/peptideSelectionWidget.h"
#include "quantificationwidget/filterBackgroundWidget.h"
#include "quantificationwidget/filterSpikeWidget.h"
#include "quantificationwidget/detectionZivyWidget.h"
#include "engine/masschroq_gui_engin.h"
#include "quantificationwidget/msrun_selection_widget.h"
#include "alignementwidget/obiwarp_alignment_widget.h"
#include "alignementwidget/ms2_alignment_widget.h"
#include "../lib/consoleout.h"
#include "dom_methods/masschroqDomDocument.h"
#include "logQIODevice.h"

ParameterMainWindow::ParameterMainWindow(QWidget *parent) :
		QMainWindow(parent) {
	QFileInfo mcq_icon;
	mcq_icon.setFile(MASSCHROQ_ICON);
	if (mcq_icon.exists()) {
		setWindowIcon(QIcon(mcq_icon.filePath()));
	} else {
		QString mcq_icon_current(QApplication::applicationDirPath());
		mcq_icon_current.append("/").append(mcq_icon.fileName());
		mcq_icon.setFile(mcq_icon_current);
		if (mcq_icon.exists()) {
			setWindowIcon(QIcon(mcq_icon.filePath()));
		}
	}

	dock = 0;
	_dockLog = 0;
	_plot_area = 0;
	_plot_area_aligned = 0;
	_logQTextEdit = 0;

	//Add menus and Actions
	LoadFileMenus();

	QTextEdit * welcomeMessage = new QTextEdit(this);
	QString message;
	message.append("<p><b>Welcome to the MassChroq Studio application</b></p>");
	message.append(
			"<p>This application helps you to test parameters before starting masschroq analysis</p>");
	message.append("<p>Please select one of the workspace to evaluate :");
	message.append(
			"<ul><li>Quantification (file->workspace->quantification)</li>");
	message.append("<li>Alignment (file->workspace->Alignment)</li></ul></p>");
	welcomeMessage->setReadOnly(true);
	welcomeMessage->setText(message);
	this->setCentralWidget(welcomeMessage);

	this->setAttribute(Qt::WA_AlwaysShowToolTips, true);

	setWindowTitle(tr("MassChroQ Studio"));
	// cannot resize the main window to less than 200x200
	setMinimumSize(200, 200);
	// size at creation is 480x320
	resize(480, 320);
}

void ParameterMainWindow::ResetWorkspace() {
	qDebug() << "ResetWorkspace begin";
	//Remove older menu
	menuBar()->clear();
	if (_plot_area != 0) {
		delete (_plot_area);
		_plot_area = 0;
	}
	if (_plot_area_aligned != 0) {
		delete (_plot_area_aligned);
		_plot_area_aligned = 0;
	}
	qDebug() << "ResetWorkspace end";
}
void ParameterMainWindow::LoadFileMenus() {
	qDebug() << "LoadFileMenus begin";
	QMenu * _fileMenu = menuBar()->addMenu(tr("&File"));

	QMenu * _workscapeMenu = _fileMenu->addMenu(tr("&Workspace"));

	QAction * _quantiAct = new QAction(tr("&Quantification"), this);
	_quantiAct->setShortcut(tr("Ctrl+Q"));

	_quantiAct->setToolTip(tr("Load quantification workspace"));
	connect(_quantiAct, SIGNAL(triggered()), this,
			SLOT(LoadQuantificationWorkspace()));
	_workscapeMenu->addAction(_quantiAct);

	QAction * _alignAct = new QAction(tr("&Alignment"), this);
	_alignAct->setShortcut(tr("Ctrl+A"));
	_alignAct->setToolTip(tr("Load quantification workspace"));
	connect(_alignAct, SIGNAL(triggered()), this,
			SLOT(LoadAlignmentWorkspace()));
	_workscapeMenu->addAction(_alignAct);

	//_fileMenu->addAction(_loadMsrunAct);
	_fileMenu->addSeparator();
	QAction * _quitAct = new QAction(tr("&Quit"), this);
	_quitAct->setShortcuts(QKeySequence::Quit);
	_quitAct->setToolTip(tr("Quit MassChroQ GUI"));
	connect(_quitAct, SIGNAL(triggered()), this, SLOT(close()));
	_fileMenu->addAction(_quitAct);

	qDebug() << "LoadFileMenus end";
}

void ParameterMainWindow::LoadQuantificationWorkspace() {
	qDebug() << "Load quantification Workspace";
	this->ResetWorkspace();
	this->LoadFileMenus();
	qDebug() << "Load quantification Workspace 1";

	QMenu * _toolsMenu = menuBar()->addMenu(tr("&Tools"));
	QAction * extractionXicAct = new QAction(tr("&MsRun data (mzXML or mzML)"),
			this);
	extractionXicAct->setToolTip(tr("Select MsRun data mode to extracted XIC"));
	connect(extractionXicAct, SIGNAL(triggered()), this,
			SLOT(extractXicWidget()));
	_toolsMenu->addAction(extractionXicAct);

	QAction * masschroqmlXicAct = new QAction(tr("&Peptide data (masschroqML)"),
			this);
	masschroqmlXicAct->setToolTip(
			tr("Select MassChroqML resources and extracted XIC from peptide"));
	connect(masschroqmlXicAct, SIGNAL(triggered()), this,
			SLOT(masschroqmlXicWidget()));
	_toolsMenu->addAction(masschroqmlXicAct);

	_toolsMenu->addSeparator();

	QMenu * _filtersMenu = _toolsMenu->addMenu(tr("&Filter XIC"));

	QAction * _filterBackgroundAct = new QAction(tr("&Background filter"),
			this);
	_filterBackgroundAct->setToolTip(
			tr("Apply a backround noise removal filter to the XIC"));
	connect(_filterBackgroundAct, SIGNAL(triggered()), this,
			SLOT(filterBackgroundWidget()));
	_filtersMenu->addAction(_filterBackgroundAct);

	QAction *_filterSpikeAct = new QAction(tr("&Spike filter"), this);
	_filterSpikeAct->setToolTip(tr("Apply a spike removal filter to the XIC"));
	connect(_filterSpikeAct, SIGNAL(triggered()), this,
			SLOT(filterSpikeWidget()));
	_filtersMenu->addAction(_filterSpikeAct);

	_toolsMenu->addSeparator();

	QMenu * _detectionsMenu = _toolsMenu->addMenu(tr("&Detect peaks"));

	QAction * _detectionZivyAct = new QAction(tr("&Zivy peak detection"), this);
	_detectionZivyAct->setToolTip(
			tr("Detect peaks on XIC with the Zivy method"));
	connect(_detectionZivyAct, SIGNAL(triggered()), this,
			SLOT(detectionZivyWidget()));
	_detectionsMenu->addAction(_detectionZivyAct);

	_toolsMenu->addSeparator();

	QAction * _exportAct = new QAction(tr("&Export method"), this);
	connect(_exportAct, SIGNAL(triggered()), this,
			SLOT(exportXmlQuantificationMethod()));
	_toolsMenu->addAction(_exportAct);

	QMenu * _helpMenu = menuBar()->addMenu(tr("&Help"));

	QAction * _aboutAct = new QAction(tr("&About"), this);
	_aboutAct->setToolTip(tr("About MassChroQ GUI"));
	connect(_aboutAct, SIGNAL(triggered()), this, SLOT(about()));
	_helpMenu->addAction(_aboutAct);

	qDebug() << "Load quantification Workspace 2";

	qDebug() << "QCoreApplication argc "
			<< QCoreApplication::instance()->argc();
	// Central Widget
	_plot_area = new Plot(this);
	this->setCentralWidget(_plot_area);
	qDebug() << "setCentralWidget after";

	qDebug() << "Load quantification Workspace 3";
	//Add dock
	this->extractXicWidget();
	qDebug() << "LoadQuantificationWorkspace end";
	this->addLogWidget();

}

void ParameterMainWindow::LoadAlignmentWorkspace() {
	qDebug() << "Load Alignement Workspace";
	this->ResetWorkspace();
	this->LoadFileMenus();

	QMenu * _toolsMenu = menuBar()->addMenu(tr("&Tools"));
	QAction * obiwarpAct = new QAction(tr("&Obiwarp alignment (MS data)"),
			this);
	obiwarpAct->setToolTip(
			tr("Select MsRun resources and aligned run based on MS data"));
	connect(obiwarpAct, SIGNAL(triggered()), this,
			SLOT(obiwarpAlignmentWidget()));
	_toolsMenu->addAction(obiwarpAct);

	QAction * ms2Act = new QAction(tr("&MS2 alignment (MS/MS data)"), this);
	ms2Act->setToolTip(
			tr(
					"Select MassChroqML resources and aligned run based on MS/MS data"));
	connect(ms2Act, SIGNAL(triggered()), this, SLOT(ms2AlignmentWidget()));
	_toolsMenu->addAction(ms2Act);

	_toolsMenu->addSeparator();

	QAction * _exportAct = new QAction(tr("&Export method"), this);
	connect(_exportAct, SIGNAL(triggered()), this,
			SLOT(exportXmlALignmentMethod()));
	_toolsMenu->addAction(_exportAct);

	QMenu * _helpMenu = menuBar()->addMenu(tr("&Help"));

	QAction * _aboutAct = new QAction(tr("&About"), this);
	_aboutAct->setToolTip(tr("About MassChroQ GUI"));
	connect(_aboutAct, SIGNAL(triggered()), this, SLOT(about()));
	_helpMenu->addAction(_aboutAct);

	// Central Widget
	_plot_area_aligned = new PlotAligned(this);
	this->setCentralWidget(_plot_area_aligned);

	this->ms2AlignmentWidget();
	qDebug() << "Load Alignement Workspace end";
	this->addLogWidget();
}

void ParameterMainWindow::closeEvent(QCloseEvent * event) {
	if (maybeSave()) {
		event->accept();
	} else {
		event->ignore();
	}
}

bool ParameterMainWindow::maybeSave() {
	QMessageBox::StandardButton ret;
	ret = QMessageBox::warning(this, tr("MassChroQ GUI"),
			tr("Do you want to quit MassChroQ Gui?"),
			QMessageBox::Ok | QMessageBox::Cancel);
	if (ret == QMessageBox::Ok)
		return true;
	else if (ret == QMessageBox::Cancel)
		return false;
	else
		return true;
}

void ParameterMainWindow::extractXicWidget() {
	qDebug() << "New extract xic widget";
	this->resetDockTools();

	//Test msRun selection
	MsrunSelectionWidget * select = new MsrunSelectionWidget(dockwidget);
	connect(select, SIGNAL(resetData()), _plot_area, SLOT(clearPlot()));
	vbox->addWidget(select);

	TreatmentBoxXicExtract * treat =
			MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicExtract();
	connect(treat, SIGNAL(createdXic(const TreatmentBoxXicExtract *)),
			_plot_area, SLOT(viewNewPlot(const TreatmentBoxXicExtract *)));
	connect(treat, SIGNAL(onDelete(TreatmentBox *)), _plot_area,
			SLOT(remove(TreatmentBox *)));
	XicSelectionWidget * xic = new XicSelectionWidget(treat, dockwidget);
	vbox->addWidget(xic);
	vbox->addStretch(0);
}

void ParameterMainWindow::masschroqmlXicWidget() {
	qDebug() << "New masschroqml xic widget";
	this->resetDockTools();

	MasschroqmlSelectionWidget * select = new MasschroqmlSelectionWidget(
			dockwidget);
	connect(select, SIGNAL(resetData()), _plot_area, SLOT(clearPlot()));
	vbox->addWidget(select);

	TreatmentBoxXicExtract * treat =
			MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicExtract();
	connect(treat, SIGNAL(createdXic(const TreatmentBoxXicExtract *)),
			_plot_area, SLOT(viewNewPlot(const TreatmentBoxXicExtract *)));
	connect(treat, SIGNAL(onDelete(TreatmentBox *)), _plot_area,
			SLOT(remove(TreatmentBox *)));
	PeptideSelectionWidget * xic = new PeptideSelectionWidget(treat,
			dockwidget);
	connect(select, SIGNAL(updateMsRunData()), xic, SLOT(updatePeptideList()));
	connect(select, SIGNAL(resetData()), xic, SLOT(clearPeptideList()));
	vbox->addWidget(xic);
	vbox->addStretch(0);

}

void ParameterMainWindow::addLogWidget() {
	qDebug() << "addLogWidget begin";
	if (_logQTextEdit == 0) {
		_logQTextEdit = new LogQTextEdit(this);
		LogQIODevice * logQIODevice = new LogQIODevice();
		ConsoleOut::setCout(new QTextStream(logQIODevice));
		ConsoleOut::setCerr(new QTextStream(logQIODevice));
		connect(logQIODevice, SIGNAL(appendLogString(QString)), _logQTextEdit, SLOT(appendLogString(QString)));
	}
	if (_dockLog == 0) {
		_dockLog = new QDockWidget(tr("Log"), this);
		_dockLog->setFeatures(
				QDockWidget::DockWidgetMovable
						| QDockWidget::DockWidgetFloatable);
		_dockLog->setWidget(_logQTextEdit);
		addDockWidget(Qt::BottomDockWidgetArea, _dockLog);
		mcqout() << "Initialising logger" << endl;
	}

	qDebug() << "addLogWidget end";
}

void ParameterMainWindow::resetDockTools() {
	if (dock != 0) {
		delete (dock);
		dock = 0;
	}
	MasschroqGuiEngin::getInstance()->getChain().removedAll();
	dock = new QDockWidget(tr("Tools"), this);
	dock->setFeatures(
			QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	QScrollArea * scrollArea = new QScrollArea();

	vbox = new QVBoxLayout;
	vbox->setSizeConstraint(QLayout::SetMinAndMaxSize);
	dockwidget = new QWidget();
	dockwidget->setLayout(vbox);

	scrollArea->setWidget(dockwidget);
	scrollArea->setWidgetResizable(true);
	dock->setWidget(scrollArea);
	addDockWidget(Qt::LeftDockWidgetArea, dock);
	qDebug() << "New Dock";
}

void ParameterMainWindow::filterBackgroundWidget() {
	TreatmentBoxXicFilter * treat =
			MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicFilter();
	connect(treat, SIGNAL(changedXic(const TreatmentBoxXicFilter *)),
			_plot_area,
			SLOT(updatedCurrentPlot(const TreatmentBoxXicFilter *)));
	connect(treat, SIGNAL(onDelete(TreatmentBox *)), _plot_area,
			SLOT(remove(TreatmentBox *)));
	FilterBackgroundWidget * backgroundFilter = new FilterBackgroundWidget(
			treat, dockwidget);
	vbox->removeItem(vbox->itemAt(vbox->count() - 1));
	vbox->addWidget(backgroundFilter);
	vbox->addStretch(0);
}

void ParameterMainWindow::filterSpikeWidget() {
	TreatmentBoxXicFilter * treat =
			MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicFilter();
	connect(treat, SIGNAL(changedXic(const TreatmentBoxXicFilter *)),
			_plot_area,
			SLOT(updatedCurrentPlot(const TreatmentBoxXicFilter *)));
	connect(treat, SIGNAL(onDelete(TreatmentBox *)), _plot_area,
			SLOT(remove(TreatmentBox *)));
	FilterSpikeWidget * spikeFilter = new FilterSpikeWidget(treat, dockwidget);
	vbox->removeItem(vbox->itemAt(vbox->count() - 1));
	vbox->addWidget(spikeFilter);
	vbox->addStretch(0);
}

void ParameterMainWindow::detectionZivyWidget() {
	TreatmentBoxXicDetect * treat =
			MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicDetect();
	connect(treat, SIGNAL(detectedPeaks(const TreatmentBoxXicDetect *)),
			_plot_area, SLOT(updatedPeaks(const TreatmentBoxXicDetect *)));
	connect(treat, SIGNAL(onDelete(TreatmentBox *)), _plot_area,
			SLOT(remove(TreatmentBox *)));
	DetectionZivyWidget * detectionZivy = new DetectionZivyWidget(treat,
			dockwidget);
	vbox->removeItem(vbox->itemAt(vbox->count() - 1));
	vbox->addWidget(detectionZivy);
	vbox->addStretch(0);
}

void ParameterMainWindow::obiwarpAlignmentWidget() {
	qDebug() << "New Obiwarp widget" << endl;
	this->resetDockTools();
	ObiwarpAlignmentWidget * obiwarp = new ObiwarpAlignmentWidget(dockwidget);
	connect(obiwarp, SIGNAL(newAlignmentCurve()), _plot_area_aligned,
			SLOT(clearPlot()));
	connect(obiwarp, SIGNAL(finishAlignment(const Msrun *)), _plot_area_aligned,
			SLOT(viewAlignedMsRun(const Msrun *)));
	vbox->addWidget(obiwarp);
	vbox->addStretch(1);
}

void ParameterMainWindow::ms2AlignmentWidget() {
	qDebug() << "New MS2 widget" << endl;
	this->resetDockTools();
	Ms2AlignmentWidget * ms2 = new Ms2AlignmentWidget(dockwidget);
	connect(ms2, SIGNAL(newAlignmentCurve()), _plot_area_aligned,
			SLOT(clearPlot()));
	connect(ms2, SIGNAL(finishAlignment(const Msrun *)), _plot_area_aligned,
			SLOT(viewAlignedMsRun(const Msrun *)));
	connect(ms2, SIGNAL(finishMonitorAlignment(const MonitorAlignmentPlot *)),
			_plot_area_aligned,
			SLOT(viewAlignedMS2Peaks(const MonitorAlignmentPlot *)));
	vbox->addWidget(ms2);
	vbox->addStretch(1);
}

void ParameterMainWindow::about() {
	QMessageBox::about(this, tr("About MassChroQ"),
			tr("This is <b>MassChroQ Studio</b> version %1").arg(MASSCHROQ_VERSION));
}

void ParameterMainWindow::exportXmlQuantificationMethod() {
	qDebug() << "Export quantification xml method";
	MasschroqDomDocument *dom = new MasschroqDomDocument();
	try {
		dom->newQuantificationMethod();
		for (int i = 0; i < (vbox->count()-1); i++) {
			QWidget * temp = vbox->itemAt(i)->widget();
			MasschroQWidget * quant = (MasschroQWidget *) temp;
			quant->writeElement(dom);
		}
		dom->verifyQuantificationMethod();
		//VIew xml on QDialog
		QDialog*  dialog = new QDialog(this);
		dialog->setModal(true);
		dialog->setWindowTitle(tr("Quantification Method on XML"));
		QVBoxLayout * item = new QVBoxLayout();
		QTextEdit * edit = new QTextEdit(dialog);
		edit->setText(dom->getXMLString());
		item->addWidget(edit);
		dialog->setLayout(item);
		dialog->setMinimumSize(400,350);
		dialog->show();
	} catch (mcqError & error) {
		QMessageBox::warning(this,
				tr("Oops! an error occurred in MassChroQ. Dont Panic :"),
				error.qwhat());
	}
	delete (dom);
}

void ParameterMainWindow::exportXmlALignmentMethod() {
	qDebug() << "Export alignment xml method";
	MasschroqDomDocument *dom = new MasschroqDomDocument();
	try {
		dom->newAlignementMethod();
		QWidget * temp = vbox->itemAt(0)->widget();
		MasschroQWidget * align = (MasschroQWidget *) temp;
		align->writeElement(dom);
		//VIew xml on QDialog
		QDialog*  dialog = new QDialog(this);
		dialog->setModal(true);
		dialog->setWindowTitle(tr("Alignment Method on XML"));
		QVBoxLayout * item = new QVBoxLayout();
		QTextEdit * edit = new QTextEdit(dialog);
		edit->setText(dom->getXMLString());
		item->addWidget(edit);
		dialog->setLayout(item);
		dialog->setMinimumSize(450,200);
		dialog->show();
	} catch (mcqError & error) {
		QMessageBox::warning(this,
				tr("Oops! an error occurred in MassChroQ. Dont Panic :"),
				error.qwhat());
	}
	delete (dom);
}
