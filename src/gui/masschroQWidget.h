/*
 * masschroQWidget.h
 *
 *  Created on: 18 sept. 2012
 *      Author: valot
 */

#ifndef MASSCHROQWIDGET_H_
#define MASSCHROQWIDGET_H_

#include "dom_methods/masschroqDomDocument.h"
#include <QWidget>

class MasschroQWidget : public QWidget{
public:
	MasschroQWidget(QWidget * parent = 0);
	virtual ~MasschroQWidget();

	virtual void writeElement(MasschroqDomDocument * domDocument) const;
};

#endif /* MASSCHROQWIDGET_H_ */
