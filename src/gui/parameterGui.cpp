#include <QApplication>
#include "parameterMainWindow.h"
#include "../lib/consoleout.h"

int main(int argc, char ** argv) {
  
  QApplication app(argc, argv);
  ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  
  ParameterMainWindow window;
  window.show();
  
  return app.exec();
}
