/*
 * launcherMainWindow.h
 *
 *  Created on: 10 sept. 2012
 *      Author: valot
 */

#ifndef LAUNCHERMAINWINDOW_H_
#define LAUNCHERMAINWINDOW_H_

#include <QMainWindow>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include "thread/masschroqRunningThread.h"
#include "../lib/mass_chroq.h"
#include "logQTextEdit.h"

class LauncherMainWindow: public QMainWindow {
	  Q_OBJECT

public:
	LauncherMainWindow(QWidget *parent=0);
	virtual ~LauncherMainWindow();

protected:

  void closeEvent(QCloseEvent *event);

private slots:
	void selectMassChroqFile();
	void selectTempDirectory();
	void startMassChroq();
	void AbordMassChroqRunning();
	void finishRunningMassChroq();
	void viewErrorDuringRunning(QString error);
	void about();
	void modifyPeptideParse(bool parse);

private:
  bool maybeSave();
  void initialiseMassChroqRunningThread();
  void addMenu();
  void addWidget();
  void addMasschroqmlSelectionGroup(QVBoxLayout * mainlayout);
  void addTempDirectorySelectionGroup(QVBoxLayout * mainlayout);
  void viewError(QString error);

  MassChroq * _masschroq;
  MasschroqRunningThread * _masschroqRunningThread;
  LogQTextEdit * _logQTextEdit;
  QPushButton * loadButton;
  QLineEdit *_masschroqml_edit;
  QLineEdit *_temp_edit;
  bool _onlyPeptideParse;
  QDateTime _dt_begin;


};

#endif /* LAUNCHERMAINWINDOW_H_ */
