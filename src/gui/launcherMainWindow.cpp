/*
 * launcherMainWindow.cpp
 *
 *  Created on: 10 sept. 2012
 *      Author: valot
 */

#include "launcherMainWindow.h"
#include "../lib/consoleout.h"
#include <QDockWidget>
#include <QCloseEvent>
#include <QMessageBox>
#include <QMenuBar>
#include <QAction>
#include <QGroupBox>
#include <QLabel>
#include <QApplication>
#include <QDir>
#include <QFileDialog>
#include <QCheckBox>
#include "../lib/share/utilities.h"
#include "logQIODevice.h"

LauncherMainWindow::LauncherMainWindow(QWidget *parent) :
		QMainWindow(parent) {
	QFileInfo mcq_icon;
	mcq_icon.setFile(MASSCHROQ_ICON);
	if (mcq_icon.exists()) {
		setWindowIcon(QIcon(mcq_icon.filePath()));
	} else {
		QString mcq_icon_current(QApplication::applicationDirPath());
		mcq_icon_current.append("/").append(mcq_icon.fileName());
		mcq_icon.setFile(mcq_icon_current);
		if (mcq_icon.exists()) {
			setWindowIcon(QIcon(mcq_icon.filePath()));
		}
	}

	_masschroqRunningThread = 0;
	_logQTextEdit=0;
	_masschroq = new MassChroq();
	_masschroq->setTmpDir(QDir::tempPath());
	this->initialiseMassChroqRunningThread();
	this->addMenu();
	this->addWidget();

	setAttribute(Qt::WA_AlwaysShowToolTips, true);

	setWindowTitle(tr("MassChroQ GUI"));
	// cannot resize the main window to less than 200x200
	setMinimumSize(200, 200);
	// size at creation is 480x320
	resize(480, 320);
}

LauncherMainWindow::~LauncherMainWindow() {
	delete (_masschroqRunningThread);
}

void LauncherMainWindow::closeEvent(QCloseEvent * event) {
	if (maybeSave()) {
		_masschroqRunningThread->canceled();
		_masschroqRunningThread->terminate();
		event->accept();
	} else {
		event->ignore();
	}
}

bool LauncherMainWindow::maybeSave() {
	QMessageBox::StandardButton ret;
	ret =
			QMessageBox::warning(this, tr("MassChroQ GUI"),
					tr(
							"Do you want to quit MassChroQ Gui and stop all current processing?"),
					QMessageBox::Ok | QMessageBox::Cancel);
	if (ret == QMessageBox::Ok)
		return true;
	else if (ret == QMessageBox::Cancel)
		return false;
	else
		return true;
}

void LauncherMainWindow::initialiseMassChroqRunningThread() {
	qDebug() << "initialiseMassChroqRunningThread";
	if (_masschroqRunningThread != 0) {
		delete (_masschroqRunningThread);
	}
	_masschroqRunningThread = new MasschroqRunningThread();
	//add connection
	_masschroqRunningThread->setMaxProgress(4);
	connect(_masschroqRunningThread, SIGNAL(finishRunning()), this,
			SLOT(finishRunningMassChroq()));
	connect(_masschroqRunningThread, SIGNAL(errorDuringRunning(QString)), this,
			SLOT(viewErrorDuringRunning(QString)));
	qDebug() << "Finish initialiseMassChroqRunningThread";
}

void LauncherMainWindow::addMenu() {
	qDebug() << "LoadFileMenus begin";
	QMenu * _fileMenu = menuBar()->addMenu(tr("&File"));

	QAction * _quitAct = new QAction(tr("&Quit"), this);
	_quitAct->setShortcuts(QKeySequence::Quit);
	_quitAct->setStatusTip(tr("Quit MassChroQ GUI"));
	connect(_quitAct, SIGNAL(triggered()), this, SLOT(close()));
	_fileMenu->addAction(_quitAct);

	QMenu * _helpMenu = menuBar()->addMenu(tr("&Help"));

	QAction * _aboutAct = new QAction(tr("&About"), this);
	_aboutAct->setToolTip(tr("About MassChroQ GUI"));
	connect(_aboutAct, SIGNAL(triggered()), this, SLOT(about()));
	_helpMenu->addAction(_aboutAct);

	qDebug() << "LoadFileMenus end";
}

void LauncherMainWindow::addWidget() {

	qDebug() << "addLogWidget begin";
	_logQTextEdit = new LogQTextEdit(this);
	LogQIODevice * logQIODevice = new LogQIODevice();
	ConsoleOut::setCout(new QTextStream(logQIODevice));
	ConsoleOut::setCerr(new QTextStream(logQIODevice));
	setCentralWidget(_logQTextEdit);
	connect(logQIODevice, SIGNAL(appendLogString(QString)), _logQTextEdit, SLOT(appendLogString(QString)));

	mcqout() << "Initialising logger" << endl;

	QDockWidget * dockLog = new QDockWidget(tr("Parameters"), this);
	dockLog->setFeatures(QDockWidget::NoDockWidgetFeatures);
	QWidget * _masschroqWidget = new QWidget(this);
	QVBoxLayout * _mainlayout = new QVBoxLayout(_masschroqWidget);

	this->addMasschroqmlSelectionGroup(_mainlayout);

	QCheckBox * only = new QCheckBox(tr("Only parse peptide file"));
	_onlyPeptideParse=false;
	only->setChecked(_onlyPeptideParse);
	only->setToolTip(tr("If the identified peptides are given to masschroq via peptide text files (defined in FILE),\n"
			"He parses them, creates a new file called parsed-peptides_FILE which contains the FILE content plus the parsed peptides information."));
	connect(only, SIGNAL(clicked(bool)), this, SLOT(modifyPeptideParse(bool)));
	_mainlayout->addWidget(only);

	this->addTempDirectorySelectionGroup(_mainlayout);

	loadButton = new QPushButton(tr("&Start MassChroQ"));
	loadButton->setDefault(true);
	QHBoxLayout * _button_box = new QHBoxLayout();
	_button_box->addWidget(new QLabel(""), 1);
	_button_box->addWidget(loadButton, 0);
	connect(loadButton, SIGNAL(clicked()), this, SLOT(startMassChroq()));
	_mainlayout->addLayout(_button_box);

	_mainlayout->addStretch(0);
	_masschroqWidget->setLayout(_mainlayout);
	dockLog->setWidget(_masschroqWidget);
	addDockWidget(Qt::LeftDockWidgetArea, dockLog);

}

void LauncherMainWindow::addMasschroqmlSelectionGroup(
		QVBoxLayout * mainlayout) {
	QGroupBox * masschroqml_group = new QGroupBox("MassChroqML file selection");

	QHBoxLayout * groupLayout = new QHBoxLayout;

	_masschroqml_edit = new QLineEdit("No file Selected");
	_masschroqml_edit->setReadOnly(true);
	groupLayout->addWidget(_masschroqml_edit, 2);

	QPushButton * extractButton = new QPushButton(tr("&Select"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this,
			SLOT(selectMassChroqFile()));
	groupLayout->addWidget(extractButton, 0);

	masschroqml_group->setLayout(groupLayout);
	mainlayout->addWidget(masschroqml_group);
}

void LauncherMainWindow::addTempDirectorySelectionGroup(
		QVBoxLayout * mainlayout) {
	QGroupBox * temp_group = new QGroupBox("Temporary directory Selection");

	QHBoxLayout * groupLayout = new QHBoxLayout;

	_temp_edit = new QLineEdit(_masschroq->getTmpDirName());
	_temp_edit->setReadOnly(true);
	groupLayout->addWidget(_temp_edit, 2);

	QPushButton * extractButton = new QPushButton(tr("&Select"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this,
			SLOT(selectTempDirectory()));
	groupLayout->addWidget(extractButton, 0);

	temp_group->setLayout(groupLayout);
	mainlayout->addWidget(temp_group);
}

void LauncherMainWindow::selectMassChroqFile() {
	QString filename = QFileDialog::getOpenFileName(this,
			tr("Open MassChroQML File"), QString::null,
			tr("masschroqML files (*.masschroqML *.xml)"));

	if (filename.isEmpty()) {
		return;
	}
	try {
		_masschroq->setXmlFilename(filename);
		QFileInfo filenameInfo(filename);
		_masschroqml_edit->setText(filenameInfo.fileName());
	} catch (mcqError& error) {
		viewError(error.qwhat());
	}
}

void LauncherMainWindow::selectTempDirectory() {
	QString fdirname = QFileDialog::getExistingDirectory(this,
			tr("Open Temporary directory"), QString::null,
			QFileDialog::ShowDirsOnly);
	if (fdirname.isEmpty()) {
		return;
	}
	try {
		_masschroq->setTmpDir(fdirname);
		_temp_edit->setText(fdirname);
	} catch (mcqError& error) {
		viewError(error.qwhat());
	}
}

void LauncherMainWindow::modifyPeptideParse(bool parse){
	_onlyPeptideParse = parse;
}

void LauncherMainWindow::startMassChroq() {
	qDebug() << "LauncherMainWindow::startMassChroq Begin";
	QString filename = _masschroq->getXmlFilename();
	if (filename.isEmpty()) {
		this->viewError(tr("No MassChroqML have been select"));
		return;
	}
	MassChroq * masschroqTmp = new MassChroq();
	try {
		masschroqTmp->setMasschroqDir(QApplication::applicationDirPath());
		masschroqTmp->setXmlFilename(filename);
		masschroqTmp->setTmpDir(_masschroq->getTmpDirName());
	} catch (mcqError& error) {
		viewError(error.qwhat());
	}
	//Button pas to stop
	loadButton->setText(tr("&Stop"));
	disconnect(loadButton, SIGNAL(clicked()), this, SLOT(startMassChroq()));
	connect(loadButton, SIGNAL(clicked()), this, SLOT(AbordMassChroqRunning()));
	//TImer
	_dt_begin = QDateTime::currentDateTime();
	//Remove Text on logger
	_logQTextEdit->setText("");
	//Start Running
	_masschroqRunningThread->onlyParsePeptide(_onlyPeptideParse);
	_masschroqRunningThread->runMassChroqML(masschroqTmp);
}

void LauncherMainWindow::AbordMassChroqRunning() {
	if (this->close()) {
		//Stop Thread
		qDebug() << "AbordMassChroqRunning";
		_masschroqRunningThread->canceled();
		_masschroqRunningThread->terminate();
		qDebug() << "Finish AbordMassChroqRunning";
	}
}
void LauncherMainWindow::finishRunningMassChroq() {
	//Add reinitialisation of button
	loadButton->setText(tr("&Start MassChroQ"));
	disconnect(loadButton, SIGNAL(clicked()), this,
			SLOT(AbordMassChroqRunning()));
	connect(loadButton, SIGNAL(clicked()), this, SLOT(startMassChroq()));
	this->initialiseMassChroqRunningThread();
	//Add Time
	QDateTime dt_end = QDateTime::currentDateTime();

	Duration dur = Utilities::getDurationFromDates(_dt_begin, dt_end);

	mcqout() << "MassChroQ's has finish"<<endl;
	mcqout() << "MassChroQ's execution time was : "
		 << Utilities::getDaysFromDuration(dur) << " days, "
		 << Utilities::getHoursFromDuration(dur) << " hours, "
		 << Utilities::getMinutesFromDuration(dur) << " minutes, "
		 << Utilities::getSecondsFromDuration(dur) << " seconds."
		 << endl<< endl;
}
void LauncherMainWindow::viewErrorDuringRunning(QString error) {
	//Add reinitialisation of button
	loadButton->setText(tr("&Start MassChroQ"));
	disconnect(loadButton, SIGNAL(clicked()), this,
			SLOT(AbordMassChroqRunning()));
	connect(loadButton, SIGNAL(clicked()), this, SLOT(startMassChroq()));
	this->initialiseMassChroqRunningThread();
	//view Error
	this->viewError(error);
}
void LauncherMainWindow::viewError(QString error) {
	QMessageBox::warning(this,
			tr("Oops! an error occurred in MassChroQ. Dont Panic :"), error);
}

void LauncherMainWindow::about() {
	QMessageBox::about(this, tr("About MassChroQ"),
			tr("This is <b>MassChroQ Gui</b> version %1").arg(
					MASSCHROQ_VERSION));
}
