/*
 * runningQLabel.h
 *
 *  Created on: 1 août 2012
 *      Author: valot
 */

#ifndef RUNNINGQLABEL_H_
#define RUNNINGQLABEL_H_

#include <QLabel>
#include <QTimer>

class RunningQLabel: public QLabel {

Q_OBJECT

public:
	RunningQLabel(QWidget * parent = 0);
	virtual ~RunningQLabel();

	void startLoading(QString message);

	void stopLoading(QString message);

private slots:
	void updateRunning();

private :

	QTimer * _timer;

	int _countTime;

	QString _runningLabel;


};

#endif /* RUNNINGQLABEL_H_ */
