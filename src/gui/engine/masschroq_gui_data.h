/*
 * masschroq_gui_data.h
 *
 *  Created on: 26 juil. 2012
 *      Author: valot
 */

#ifndef MASSCHROQ_GUI_DATA_H_
#define MASSCHROQ_GUI_DATA_H_

#include <QString>
#include "../../lib/peptides/peptide_list.h"
#include "../../lib/peptides/isotope_label.h"

class MasschroqGuiData {
public:
	MasschroqGuiData(QString xmlfilename);
	virtual ~MasschroqGuiData();

	void addPeptide(Peptide * pep);

	void addIsotopeLabel(IsotopeLabel * p_isotope_label);

	void addMsrunFilename(QString & id, QString & filename, mcq_xml_format & format);

	QString getMsrunIdToNumber(unsigned int index) const;

	QString getMsrunFilenameToNumber(unsigned int index) const;

	const PeptideList& getPeptideList();

	const msRunHashGroup * getMsrunHashGroup () const{
		return _msRunGroup;
	}


	const std::vector<QString>& getMsrunFilenameList() const{
		return(_msrunFilenameList);
	}

	Msrun * getMsrunToId(QString id) const;

private:
	std::vector<QString> _idList;
	std::vector<QString> _msrunFilenameList;

	msRunHashGroup * _msRunGroup;

	PeptideList _peptideList;
	PeptideList _peptideIsotopeList;
	std::map<QString, const IsotopeLabel *> _map_p_isotope_labels;

	QString _xmlfilename;
	const QString _idGroup;

};

#endif /* MASSCHROQ_GUI_DATA_H_ */
