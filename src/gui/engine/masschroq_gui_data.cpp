/*
 * masschroq_gui_data.cpp
 *
 *  Created on: 26 juil. 2012
 *      Author: valot
 */

#include "masschroq_gui_data.h"
#include "../../lib/msrun/msrun_classic.h"
#include "../../lib/msrun/ms_run_hash_group.h"
#include "../../lib/peptides/peptide_isotope.h"

MasschroqGuiData::MasschroqGuiData(QString xmlfilename) :
	_xmlfilename(xmlfilename), _idGroup("G1") {
	_msRunGroup = new msRunHashGroup(_idGroup);

}

MasschroqGuiData::~MasschroqGuiData() {
	_peptideList.free();
	_msRunGroup->free();
	delete (_msRunGroup);
}

void MasschroqGuiData::addPeptide(Peptide * pep) {
	_peptideList.push_back(pep);
}

const PeptideList& MasschroqGuiData::getPeptideList() {
	if (_map_p_isotope_labels.size() == 0) {

		qDebug()
				<< "MasschroqGuiData::getPeptideList() _map_p_isotope_labels.size() == 0";
		qDebug() << "MasschroqGuiData::getPeptideList() _peptideList.size() "
				<< _peptideList.size();
		return (_peptideList);
		//_map_p_isotope_labels
	}
	_peptideIsotopeList.resize(0);
	PeptideList::const_iterator it;
	const IsotopeLabel * isotope_label;
	for (it = _peptideList.begin(); it != _peptideList.end(); ++it) {

		std::map<QString, const IsotopeLabel *>::const_iterator it_label;
		for (it_label = _map_p_isotope_labels.begin(); it_label
				!= _map_p_isotope_labels.end(); ++it_label) {
			qDebug() << "MasschroqGuiData::getPeptideList() _map_p_isotope_labels "
									+ it_label->first;
			isotope_label = it_label->second;
			PeptideIsotope * isotope = new PeptideIsotope(*it, isotope_label);
			if (isotope->getMass() != (*it)->getMass()) {
				qDebug() << "MasschroqGuiData::getPeptideList() new isotope "
						+ isotope->getSequence() + " " + isotope->getMods()
						+ " " + isotope->getIsotopeLabel()->getXmlId();
				_peptideIsotopeList.push_back(isotope);
			} else {
				delete (isotope);
			}
		}

	}
	qDebug()
			<< "MasschroqGuiData::getPeptideList() _peptideIsotopeList.size() "
			<< _peptideIsotopeList.size();

	return (_peptideIsotopeList);
}

void MasschroqGuiData::addIsotopeLabel(IsotopeLabel * p_isotope_label) {
	_map_p_isotope_labels[p_isotope_label->getXmlId()] = p_isotope_label;
}

void MasschroqGuiData::addMsrunFilename(QString & id, QString & filename,
		mcq_xml_format & format) {
	_idList.push_back(id);
	_msrunFilenameList.push_back(filename);
	MsrunClassic * msrun = new MsrunClassic(id);
	msrun->setXmlFile(filename);
	msrun->setXmlFileFormat(format);
	_msRunGroup->setMsRun(msrun);
}

QString MasschroqGuiData::getMsrunIdToNumber(unsigned int index) const {
	QString id = "";
	if ((index >= 0) & (index < _idList.size())) {
		id = _idList.at(index);
	}
	return (id);
}

QString MasschroqGuiData::getMsrunFilenameToNumber(unsigned int index) const {
	QString filename = "";
	if ((index >= 0) & (index < _msrunFilenameList.size())) {
		filename = _msrunFilenameList.at(index);
	}
	return (filename);
}

Msrun * MasschroqGuiData::getMsrunToId(QString id) const {
	return (_msRunGroup->getMsRun(id));
}
