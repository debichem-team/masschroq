/*
 * masschroq_gui_engin.h
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#ifndef MASSCHROQ_GUI_ENGIN_H_
#define MASSCHROQ_GUI_ENGIN_H_

#include "../../lib/msrun/msrun.h"
#include "../thread/msrunLoaderThread.h"
#include "../treatment/treatment_chain.h"
#include "masschroq_gui_data.h"

class MasschroqGuiEngin {

private:
	MasschroqGuiEngin();
	virtual ~MasschroqGuiEngin();
	MasschroqGuiEngin(const MasschroqGuiEngin&);
	void operator=(const MasschroqGuiEngin&);

public:
	// Fonctions de création et destruction du singleton
	static MasschroqGuiEngin *getInstance() {
		if (_singleton == 0) {
			qDebug() << "creating MassChroqGuiEngin";
			_singleton = new MasschroqGuiEngin;
		} else {
			qDebug() << "MassChroqGuiEngin already exist";
		}
		return _singleton;
	}

	static void kill() {
		if (_singleton != 0) {
			delete _singleton;
			_singleton = 0;
		}
	}

	TreatmentChain& getChain() {
		return _chain;
	}

	Msrun* getCurrentLoadedMsrun() const {
		return _current_loaded_msrun;
	}

	MasschroqGuiData * getMasschroqGuiData() const {
		return _masschroqdata;
	}

//	void LoadedMsrun(const QString & filename, MsrunLoaderThread * thread,
//			const QString & id = "");
	Msrun * getMsrun(const QString & filename, const QString & id = "", const QString &format="");

	void addMsrun(Msrun * msrun);

	void setMassChroqGuiData(MasschroqGuiData * data);

	void deleteMsrun() {
		if (_current_loaded_msrun != 0)
			delete (_current_loaded_msrun);
		_current_loaded_msrun = 0;
		qDebug() << "Current MsRun deleted";
	}

	void deleteMassChroqGuiData() {
		if (_masschroqdata != 0)
			delete (_masschroqdata);
		_masschroqdata = 0;
		qDebug() << "Current MassChroqGuiData deleted";
	}

private:

	Msrun * _current_loaded_msrun;

	MasschroqGuiData * _masschroqdata;

	TreatmentChain _chain;

	static MasschroqGuiEngin *_singleton;

};

#endif /* MASSCHROQ_GUI_ENGIN_H_ */
