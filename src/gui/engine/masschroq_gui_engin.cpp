/*
 * masschroq_gui_engin.cpp
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#include "masschroq_gui_engin.h"
#include "../../lib/mcq_error.h"
#include "../../lib/msrun/msrun_classic.h"
#include "../../lib/msrun/msrun_sliced.h"
#include <cmath>
#include "../../lib/consoleout.h"

// Initialisation du singleton à NULL
MasschroqGuiEngin *MasschroqGuiEngin::_singleton = 0;

MasschroqGuiEngin::MasschroqGuiEngin() :
		_chain() {
	_current_loaded_msrun = 0;
	_masschroqdata = 0;
}

MasschroqGuiEngin::~MasschroqGuiEngin() {
	if (_current_loaded_msrun != 0)
		delete _current_loaded_msrun;
	_current_loaded_msrun = 0;
	if (_masschroqdata != 0)
		delete (_masschroqdata);
	_masschroqdata = 0;
}

Msrun * MasschroqGuiEngin::getMsrun(const QString & filename,
		const QString & id, const QString &format) {

	Msrun * msrun = 0;
	QFileInfo filenameInfo(filename);

	mcq_xml_format formatOK;
	if (format.isEmpty()) {
		QString suffix = filenameInfo.suffix();
		if (suffix == "mzXML") {
			formatOK = MZXML;
		} else if (suffix == "mzML") {
			formatOK = MZML;
		} else {
			throw mcqError(
					QObject::tr(
							"You have chosen a file in '.%1' format.\nMS run files must be in '.mzXML' or '.mzML' formats.\nPlease, choose an MS run file in one of these formats.").arg(
							suffix));
			return(msrun);
		}
	}else{
		formatOK = format;
	}

	QString idXml;
	if (id.isEmpty())
		idXml = filename;
	else
		idXml = id;

	mcq_double size_in_bytes = filenameInfo.size();
	if (size_in_bytes < MAX_SLICE_SIZE) {
		msrun = new MsrunClassic(idXml);
		mcqout() << "MS run classic '" << filename
				<< "' : parsing begin" << endl;
	} else {
		mcq_double nb_of_slices_double = ceil(size_in_bytes / MAX_SLICE_SIZE);
		int nb_of_slices = static_cast<int>(nb_of_slices_double);
		msrun = new MsrunSliced(idXml, nb_of_slices);
		mcqout() << "MS run sliced '" << filename
				<< "', number of slices " << nb_of_slices << ", : parsing begin"
				<< endl;
	}

	msrun->setXmlFile(filename);
	msrun->setXmlFileFormat(formatOK);

	return (msrun);
}

//void MasschroqGuiEngin::LoadedMsrun(const QString & filename,
//		MsrunLoaderThread * thread, const QString & id) {
//
//	QFileInfo filenameInfo(filename);
//
//	QString suffix = filenameInfo.suffix();
//	mcq_xml_format format;
//	if (suffix == "mzXML") {
//		format = MZXML;
//	} else if (suffix == "mzML") {
//		format = MZML;
//	} else {
//		throw mcqError(
//				QObject::tr(
//						"You have chosen a file in '.%1' format.\nMS run files must be in '.mzXML' or '.mzML' formats.\nPlease, choose an MS run file in one of these formats.").arg(
//						suffix));
//		return;
//	}
//
//	mcq_double size_in_bytes = filenameInfo.size();
//	Msrun * msrun;
//	QString idXml;
//	if (id.isEmpty())
//		QString idXml = filename;
//	else
//		idXml = id;
//
//	if (size_in_bytes < MAX_SLICE_SIZE) {
//		msrun = new MsrunClassic(idXml);
//		cout << "MS run classic '" << filename.toStdString()
//				<< "' : parsing begin" << endl;
//	} else {
//		mcq_double nb_of_slices_double = ceil(size_in_bytes / MAX_SLICE_SIZE);
//		int nb_of_slices = static_cast<int>(nb_of_slices_double);
//		msrun = new MsrunSliced(idXml, nb_of_slices);
//		cout << "MS run sliced '" << filename.toStdString()
//				<< "', number of slices " << nb_of_slices << ", : parsing begin"
//				<< endl;
//	}
//
//	thread->loadMsrun(msrun, filename, format);
//}

void MasschroqGuiEngin::addMsrun(Msrun * msrun) {
	if (_current_loaded_msrun != 0)
		delete (_current_loaded_msrun);
	_current_loaded_msrun = msrun;
}

void MasschroqGuiEngin::setMassChroqGuiData(MasschroqGuiData * data) {
	if (_masschroqdata != 0)
		delete (_masschroqdata);
	_masschroqdata = data;
}
