/**
 * \file msrunLoaderThread.cpp
 * \date December 09, 2011
 * \author Edlira Nano
 */

#include "msrunLoaderThread.h"
#include "../../lib/consoleout.h"

MsrunLoaderThread::MsrunLoaderThread(QObject * parent,
		const unsigned int maxProgress) :
		McqThread(parent, maxProgress) {
}

MsrunLoaderThread::~MsrunLoaderThread() {
	_mutex.lock();
	_abort = true;
	_condition.wakeOne();
	_mutex.unlock();
	wait();
}

void MsrunLoaderThread::loadMsrun(Msrun * msrun) {
	setProgressValue(1);
	_mutex.lock();
	_current_msrun = msrun;
	_msrun_filename = msrun->getXmlFileInfo().absoluteFilePath();
	_msrun_format = msrun->getXmlFileFormat();
	_abort = false;
	_mutex.unlock();

	if (!isRunning()) {
		start();
	} else {
		_restart=true;
		_condition.wakeOne();
	}
}

void MsrunLoaderThread::run() {
	Msrun * msrun = 0;
	QString filename;
	mcq_xml_format format;
	bool read_time_values = false;
	QString time_dir("");
	try {
		int current = 0;
		while (current < 3) {
			if (current == 0) {
				qDebug()<<"Intialisation Loading";
				//initialisation
				_mutex.lock();
				msrun = this->_current_msrun;
				filename = this->_msrun_filename;
				format = this->_msrun_format;
				_mutex.unlock();

			} else if (current == 1) {
				qDebug()<<"Load Scan number";
				msrun->set_from_xml(filename, format, read_time_values, time_dir);
			} else if (current == 2) {
				qDebug()<<"Load Spectra";
				msrun->prepareSpectraForQuantification(NULL);
			} else {
				break;
			}

			//Add progress
			current++;
			incrementProgressValue();

			if (this->getAbord()) {
				//Loading is stop
				if (msrun != 0)
					delete (msrun);
				msrun = 0;
				break;
			}
			if (this->getRestart()) {
				//Loading is restart with new param
				if (msrun != 0)
					delete (msrun);
				msrun = 0;
				current = 0;
				_mutex.lock();
				_restart = false;
				_mutex.unlock();
			}
		}

	} catch (mcqError &error) {
		mcqerr() << "Oops! an error occurred in MassChroQ. Dont Panic :" << endl;
		mcqerr() << error.qwhat() << endl;
		emit errorDuringLoading(error.qwhat());
		return;
	}

	//If all ok, emit loading finish
	if (!this->getRestart() & !this->getAbord())
		emit loadedMsrun(msrun);

//	_mutex.lock();
//	if (!_restart)
//		_condition.wait(&_mutex);
//	_restart = false;
//	_mutex.unlock();
}
