/*
 * obiwarp_thread.h
 *
 *  Created on: 31 juil. 2012
 *      Author: valot
 */

#ifndef ALIGNMENT_THREAD_H_
#define ALIGNMENT_THREAD_H_ 1

#include "mcqThread.h"
#include "../../lib/msrun/msrun.h"
#include "../../lib/alignments/alignment_base.h"
#include <QWaitCondition>

class AlignmentThread: public McqThread {
Q_OBJECT

public:
	AlignmentThread(QObject * parent = 0, const unsigned int maxProgress = 0);
	virtual ~AlignmentThread();

	void performedAlignment(AlignmentBase * alignment, Msrun * p_msrun_ref,
			Msrun * p_msrun);

	void run();

signals:

	void finishAlignment();

	void errorDuringAlignment(QString error);

private:
	AlignmentBase * _alignment;
	Msrun * _p_msrun_ref;
	Msrun * _p_msrun;
	QMutex _mutex;
	QWaitCondition _condition;
};

#endif /* ALIGNMENT_THREAD_H_ */
