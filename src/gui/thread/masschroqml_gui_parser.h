/*
 * masschroqml_gui_parser.h
 *
 *  Created on: 26 juil. 2012
 *      Author: valot
 */

#ifndef MASSCHROQML_GUI_PARSER_H_
#define MASSCHROQML_GUI_PARSER_H_

#include <QXmlDefaultHandler>
#include <vector>
#include "../../lib/peptides/peptide.h"
#include "../../lib/peptides/isotope_label.h"
#include "../engine/masschroq_gui_data.h"

class MasschroqmlGuiParser: public QXmlDefaultHandler {
public:
	MasschroqmlGuiParser(MasschroqGuiData * masschroqGuiData);
	virtual ~MasschroqmlGuiParser();

	bool startElement(const QString &namespaceURI, const QString &localName,
			const QString &qName, const QXmlAttributes &attributes);

	bool endElement(const QString &namespaceURI, const QString &localName,
			const QString &qName);

	bool endDocument();

	bool characters(const QString &str);

	bool fatalError(const QXmlParseException &exception);

	QString errorString() const;

private:

	bool startElement_data_file(const QXmlAttributes & attributes);

	bool startElement_peptide(const QXmlAttributes &attributes);

	bool startElement_observed_in(const QXmlAttributes &attributes);
	bool startElement_isotope_label(const QXmlAttributes &attributes);
	bool
	startElement_mod(const QXmlAttributes &attributes);

	bool endElement_peptide();
	bool endElement_isotope_label();

	QString _errorStr;

	QString _currentText;

	std::vector<QString> _tag_stack;

	Peptide * _p_current_peptide;

	IsotopeLabel * _p_isotope_label;

	MasschroqGuiData * _masschroqGuiData;
};

#endif /* MASSCHROQML_GUI_PARSER_H_ */
