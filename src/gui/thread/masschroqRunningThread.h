/*
 * masschroqRunningThread.h
 *
 *  Created on: 10 sept. 2012
 *      Author: valot
 */

#ifndef MASSCHROQRUNNINGTHREAD_H_
#define MASSCHROQRUNNINGTHREAD_H_

#include <QWaitCondition>
#include "mcqThread.h"
#include "../../lib/mass_chroq.h"

class MasschroqRunningThread: public McqThread  {
	Q_OBJECT

public:
	MasschroqRunningThread(QObject * parent = 0,
			const unsigned int maxProgress = 0);
	virtual ~MasschroqRunningThread();

	void runMassChroqML(MassChroq * masschroq);

	void onlyParsePeptide(bool parse);

	void run();

signals:

	void finishRunning();

	void errorDuringRunning(QString error);

private:
	MassChroq * _masschroq;
	QMutex _mutex;
	QWaitCondition _condition;
	bool _onlyParsePeptide;
};

#endif /* MASSCHROQRUNNINGTHREAD_H_ */
