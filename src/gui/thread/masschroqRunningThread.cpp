/*
 * masschroqRunningThread.cpp
 *
 *  Created on: 10 sept. 2012
 *      Author: valot
 */

#include "masschroqRunningThread.h"
#include "../../lib/consoleout.h"

MasschroqRunningThread::MasschroqRunningThread(QObject * parent,
		const unsigned int maxProgress) :
		McqThread(parent, maxProgress) {
	_onlyParsePeptide=false;
	_masschroq=0;
}

MasschroqRunningThread::~MasschroqRunningThread() {
	qDebug()<<"Delete MasschroqRunningThread";
	_mutex.lock();
	_abort = true;
	_condition.wakeOne();
	if(_masschroq!=0)
		delete(_masschroq);
	_masschroq=0;
	_mutex.unlock();
	wait();
}

void MasschroqRunningThread::onlyParsePeptide(bool parse){
	_mutex.lock();
	_onlyParsePeptide = parse;
	_mutex.unlock();
}

void MasschroqRunningThread::runMassChroqML(MassChroq * masschroq) {
	setProgressValue(1);
	_mutex.lock();
	_masschroq = masschroq;
	_abort = false;
	_mutex.unlock();

	if (!isRunning()) {
		start();
	} else {
		_restart = true;
		_condition.wakeOne();
	}
}

void MasschroqRunningThread::run() {
	MassChroq * masschroq = 0;
	try {
		int current = 0;
		while (current < 3) {
			if (current == 0) {
				qDebug() << "Intialisation Loading";
				//initialisation
				_mutex.lock();
				masschroq = this->_masschroq;
				_mutex.unlock();

			} else if (current == 1) {
				//Verifie file link MassChroqML file
				qDebug() << "Verifie dom link and parse peptide";
				masschroq->runDomParser();
			} else if (current == 2) {
				qDebug() << "Start running";
				if(_onlyParsePeptide==false)
					masschroq->runXmlFile();
				qDebug() << "Finish running";
			} else {
				break;
			}

			//Add progress
			current++;
			incrementProgressValue();

			if (this->getAbord()) {
				//Loading is stop
//				if (masschroq != 0)
//					delete (masschroq);
//				masschroq = 0;
				break;
			}
			if (this->getRestart()) {
				//Loading is restart with new param
//				if (masschroq != 0)
//					delete (masschroq);
//				masschroq = 0;
				current = 0;
				_mutex.lock();
				_restart = false;
				_mutex.unlock();
			}
		}

	} catch (mcqError &error) {
		mcqerr() << "Oops! an error occurred in MassChroQ. Dont Panic :" << endl;
		mcqerr() << error.qwhat() << endl;
		emit errorDuringRunning(error.qwhat());
		return;
	}

	//If all ok, emit loading finish
	if (!this->getRestart() & !this->getAbord()){
		emit finishRunning();
	}

}
