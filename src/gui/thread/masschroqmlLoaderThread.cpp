/**
 * \file msrunLoaderThread.cpp
 * \date December 09, 2011
 * \author Edlira Nano
 */

#include "masschroqmlLoaderThread.h"
#include "masschroqml_gui_parser.h"
#include <QXmlSimpleReader>
#include <QXmlInputSource>
#include "../../lib/mass_chroq.h"
#include "../../lib/consoleout.h"
#include <QApplication>

MasschroqmlLoaderThread::MasschroqmlLoaderThread(QObject * parent,
		const unsigned int maxProgress) :
		McqThread(parent, maxProgress) {
}

MasschroqmlLoaderThread::~MasschroqmlLoaderThread() {
	_mutex.lock();
	_abort = true;
	_condition.wakeOne();
	_mutex.unlock();
	wait();
}

void MasschroqmlLoaderThread::loadMasschroqml(MasschroqGuiData * masschroqData,
		const QString & filename) {
	setProgressValue(1);
	_mutex.lock();
	_current_masschroqData = masschroqData;
	_masschroqData_filename = filename;
	_abort = false;
	_mutex.unlock();

	if (!isRunning()) {
		start();
	} else {
		_restart = true;
		_condition.wakeOne();
	}
}

void MasschroqmlLoaderThread::run() {
	MasschroqGuiData * masschroqData = 0;
	QString filename;
	try {
		int current = 0;
		while (current < 3) {
			if (current == 0) {
				qDebug() << "Intialisation Loading";
				//initialisation
				_mutex.lock();
				masschroqData = this->_current_masschroqData;
				filename = this->_masschroqData_filename;
				_mutex.unlock();

			} else if (current == 1) {
				//Test validity of XML file
				MassChroq masschroq;
				masschroq.setMasschroqDir(QApplication::applicationDirPath());
				masschroq.setXmlFilename(filename);
				masschroq.validateXmlFile();
			} else if (current == 2) {
				//Parse MassChroqML file
				qDebug() << "Start reading";
				QFile file(filename);
				MasschroqmlGuiParser parser(masschroqData);

				QXmlSimpleReader reader;
				reader.setContentHandler(&parser);
				reader.setErrorHandler(&parser);

				QXmlInputSource xmlInputSource(&file);

				if (reader.parse(xmlInputSource))
				{
					file.close();
				}
				else
				{
					file.close();
					throw mcqError(QObject::tr("error reading masschroqML input :\n").append(parser.errorString()));
				}
				qDebug() << "Finish reading";
			} else {
				break;
			}

			//Add progress
			current++;
			incrementProgressValue();

			if (this->getAbord()) {
				//Loading is stop
				if (masschroqData != 0)
					delete (masschroqData);
				masschroqData = 0;
				break;
			}
			if (this->getRestart()) {
				//Loading is restart with new param
				if (masschroqData != 0)
					delete (masschroqData);
				masschroqData = 0;
				current = 0;
				_mutex.lock();
				_restart = false;
				_mutex.unlock();
			}
		}

	} catch (mcqError &error) {
		mcqerr() << "Oops! an error occurred in MassChroQ. Dont Panic :" << endl;
		mcqerr() << error.qwhat() << endl;
		emit errorDuringLoading(error.qwhat());
		return;
	}

	//If all ok, emit loading finish
	if (!this->getRestart() & !this->getAbord())
		emit loadedMasschroqml(masschroqData);

//	_mutex.lock();
//	if (!_restart)
//		_condition.wait(&_mutex);
//	_restart = false;
//	_mutex.unlock();
}
