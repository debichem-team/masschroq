/**
 * \file mcqThread.cpp
 * \date December 09, 2011
 * \author Edlira Nano
 */

#include "mcqThread.h"

McqThread::McqThread(QObject * parent, const unsigned int maxProgress) :
		QThread(parent), _abort(false),_restart(false) {
	setMaxProgress(maxProgress);
}

McqThread::~McqThread() {
}

void McqThread::setMaxProgress(const unsigned int value) {
	_progressMutex.lock();
	_maxProgressValue = value;
	_progressMutex.unlock();
}

const unsigned int McqThread::getProgressValue() {
	_progressMutex.lock();
	unsigned int ret = _progressValue;
	_progressMutex.unlock();
	return ret;
}

void McqThread::incrementProgressValue() {
	_progressMutex.lock();
	_progressValue++;
	_progressMutex.unlock();
}

void McqThread::setProgressValue(const unsigned int val) {
	_progressMutex.lock();
	_progressValue = val;
	_progressMutex.unlock();
}

const unsigned int McqThread::getMaxProgress() {
	_progressMutex.lock();
	unsigned int ret = _maxProgressValue;
	_progressMutex.unlock();
	return ret;
}

const bool McqThread::getAbord() {
	_progressMutex.lock();
	bool abort = _abort;
	_progressMutex.unlock();
	return abort;
}

const bool McqThread::getRestart(){
	_progressMutex.lock();
	bool restart = _restart;
	_progressMutex.unlock();
	return restart;
}
