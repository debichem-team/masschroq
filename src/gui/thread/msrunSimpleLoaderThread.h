/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope thatx it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file msrunLoaderThread.h
 * \date December 09, 2011
 * \author Edlira Nano
 */

#ifndef MSRUN_SIMPLE_LOADER_THREAD_H_
#define MSRUN_SIMPLE_LOADER_THREAD_H_ 1

#include <QWaitCondition>

#include "mcqThread.h"
#include "../../lib/mcq_error.h"
#include "../../lib/msrun/msrun.h"

/**
 * \class MsrunLoaderThread
 * \brief The thread that loads MSrun files in the MassChroq GUI.
 */

class MsrunSimpleLoaderThread: public McqThread {
Q_OBJECT

public:

	MsrunSimpleLoaderThread(QObject * parent = 0, const unsigned int maxProgress = 0);
	~MsrunSimpleLoaderThread();

	void loadMsrun(Msrun * msrun);

signals:

	void loadedMsrun(Msrun * msrun);

	void errorDuringLoading(QString error);

protected:

	virtual void run();

private:

	QMutex _mutex;
	QWaitCondition _condition;

	Msrun * _current_msrun;
	QString _msrun_filename;
	mcq_xml_format _msrun_format;
};

#endif /* MSRUN_SIMPLE_LOADER_THREAD_H_ */
