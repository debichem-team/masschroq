/**
 * \file runningBarr.cpp
 * \date December 19, 2011
 * \author Edlira Nano
 */


#include "runningBarr.h"

#include <QTimer>
#include <QProgressDialog>

RunningBarr::RunningBarr(QWidget * parent, McqThread * thread,
			 QString & progressTitle) 
  : 
  QWidget(parent) 
{
  _thread = thread;
  _minProgress = 0;
  _maxProgress = _thread->getMaxProgress();
  _progressDialog = new QProgressDialog(progressTitle, QObject::tr("Cancel"), 
					_minProgress, _maxProgress, parent);
  _progressDialog->setValue(_minProgress);
  _progressTimer = new QTimer(parent);
  _progressDialog->show();
  connect(_progressDialog,SIGNAL(canceled()),_thread,SLOT(canceled()));
}




RunningBarr::~RunningBarr() {
  delete _progressTimer;
  delete _progressDialog;
}

void
RunningBarr::setTimer(const int milliseconds) {
  connect(_progressTimer, SIGNAL(timeout()), this, SLOT(updateProgress()));
  _progressTimer->setInterval(milliseconds);
  _progressTimer->start();
}

void
RunningBarr::updateProgress() {
  if (! _thread->isFinished()) {
    unsigned int progress = _thread->getProgressValue();
    _progressDialog->setValue(progress);
  } else {
    _progressDialog->setValue(_maxProgress);
    _progressTimer->stop();
  }
}

