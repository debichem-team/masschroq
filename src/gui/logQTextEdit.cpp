/*
 * logQTextEdit.cpp
 *
 *  Created on: 30 août 2012
 *      Author: valot
 */

#include "logQTextEdit.h"

LogQTextEdit::LogQTextEdit(QWidget * parent) :
		QTextEdit(parent) {
	this->setReadOnly(true);
}

LogQTextEdit::~LogQTextEdit() {

}

void LogQTextEdit::appendLogString(QString log)
{
	this->moveCursor(QTextCursor::End);
    this->insertPlainText(log);
}
