/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope thatx it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file runningBarr.h
 * \date December 19, 2011
 * \author Edlira Nano
 */

#ifndef RUNNING_BARR_H_
#define RUNNING_BARR_H_ 1

#include "thread/mcqThread.h"

#include <QWidget>

class QProgressDialog;
class QTimer;

class RunningBarr : public QWidget {
  Q_OBJECT
    
    public :
  
  RunningBarr(QWidget * parent, McqThread * thread, 
	      QString & progress_message);
  
  ~RunningBarr();
  
  void setTimer(const int milliseconds = 1000);
 
  private slots:
  
  void updateProgress();

  
 private :
  
  McqThread * _thread;
  unsigned int _minProgress;
  unsigned int _maxProgress;
  QProgressDialog * _progressDialog;
  QTimer * _progressTimer;
  

  
};

#endif /* RUNNING_BARR_H_ */
