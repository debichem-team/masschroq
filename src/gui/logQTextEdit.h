/*
 * logQTextEdit.h
 *
 *  Created on: 30 août 2012
 *      Author: valot
 */

#ifndef LOGQTEXTEDIT_H_
#define LOGQTEXTEDIT_H_

#include <QTextEdit>

class LogQTextEdit : public QTextEdit{
	Q_OBJECT

public:
	LogQTextEdit(QWidget * parent=0);
	virtual ~LogQTextEdit();

public slots:
	void appendLogString(QString log);

};

#endif /* LOGQTEXTEDIT_H_ */
