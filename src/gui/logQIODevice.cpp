/*
 * logQIODevice.cpp
 *
 *  Created on: 27 sept. 2012
 *      Author: valot
 */

#include "logQIODevice.h"

LogQIODevice::LogQIODevice(QObject * parent) :
		QIODevice(parent) {
	open(QIODevice::WriteOnly|QIODevice::Text);
}

LogQIODevice::~LogQIODevice() {
	// TODO Auto-generated destructor stub
}

qint64 LogQIODevice::writeData(const char *data, qint64 maxSize) {
	QString temp(data);
	emit appendLogString(temp);
	return maxSize;
}
