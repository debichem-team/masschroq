/*
 * masschroqDomDocument.cpp
 *
 *  Created on: 18 sept. 2012
 *      Author: valot
 */

#include "masschroqDomDocument.h"

MasschroqDomDocument::MasschroqDomDocument() {
	// TODO Auto-generated constructor stub

}

MasschroqDomDocument::~MasschroqDomDocument() {
	// TODO Auto-generated destructor stub
}

void MasschroqDomDocument::newQuantificationMethod() {
	_dom.clear();
	_quantimethod = _dom.createElement("quantification_method");
	_quantimethod.setAttribute("id","quant1");
	_dom.appendChild(_quantimethod);
}

void MasschroqDomDocument::addPpmRangeExtractionMethod(const mcq_xic_type type, const mcq_double min,const mcq_double max ){
	if (_quantimethod.isNull()) {
		throw mcqError("Quantification_method element have not been initialised");
		return;
	}
	if (_quantimethod.childNodes().size()>0){
		throw mcqError("Quantification_method element is not empty");
		return;
	}
	QDomElement xic_extraction = _dom.createElement("xic_extraction");
	if(type == SUM_XIC_TYPE){
		xic_extraction.setAttribute("xic_type", "sum");
	}else{
		xic_extraction.setAttribute("xic_type", "max");
	}
	_quantimethod.appendChild(xic_extraction);
	QDomElement ppm_range = _dom.createElement("ppm_range");
	ppm_range.setAttribute("min", QString::number(min, 'f', 0));
	ppm_range.setAttribute("max", QString::number(max, 'f', 0));
	xic_extraction.appendChild(ppm_range);
}

void MasschroqDomDocument::addMzRangeExtractionMethod(const mcq_xic_type type, const mcq_double min,const mcq_double max ){
	if (_quantimethod.isNull()) {
		throw mcqError("Quantification_method element have not been initialised");
		return;
	}
	if (_quantimethod.childNodes().size()>0){
		throw mcqError("Quantification_method element is not empty");
		return;
	}
	QDomElement xic_extraction = _dom.createElement("xic_extraction");
	if(type == SUM_XIC_TYPE){
		xic_extraction.setAttribute("xic_type", "sum");
	}else{
		xic_extraction.setAttribute("xic_type", "max");
	}
	_quantimethod.appendChild(xic_extraction);
	QDomElement mz_range = _dom.createElement("mz_range");
	mz_range.setAttribute("min", QString::number(min, 'f', 2));
	mz_range.setAttribute("max", QString::number(max, 'f', 2));
	xic_extraction.appendChild(mz_range);
}

bool MasschroqDomDocument::verifyQuantificationMethod() {
	if (_quantimethod.isNull()) {
		throw mcqError("Quantification_method element have not been initialised");
		return false;
	}
	if(_quantimethod.elementsByTagName("peak_detection").size()==0){
		throw mcqError("You must have a peak detection method\n Please correct your method");
		return false;
	}
	if(_quantimethod.elementsByTagName("xic_extraction").size()==0){
		throw mcqError("You must have a xic extraction method\n Please correct your method");
		return false;
	}
	return true;
}

void MasschroqDomDocument::addBackgroundFilterMethod(
		const FilterBackground & filter) {
	if (_quantimethod.isNull()) {
		throw mcqError("Quantification_method element have not been initialised");
		return;
	}
	if(_quantimethod.elementsByTagName("peak_detection").size()>0){
		throw mcqError("You can not add a xic filters after peak detection\n Please correct your method");
		return;
	}
	QDomNodeList filters = _quantimethod.elementsByTagName("xic_filters");
	QDomElement xic_filters;
	if(filters.size()==0){
		xic_filters =  _dom.createElement("xic_filters");
		_quantimethod.appendChild(xic_filters);
	}else{
		xic_filters=filters.at(0).toElement();
	}
	QDomElement background =  _dom.createElement("background");
	background.setAttribute("half_mediane",QString::number(filter.get_half_median_window_length()));
	background.setAttribute("half_min_max",QString::number(filter.get_half_min_max_window_length()));
	xic_filters.appendChild(background);
}

void MasschroqDomDocument::addSpikeFilterMethod(const FilterSpike & filter) {
	if (_quantimethod.isNull()) {
		throw mcqError("Quantification_method element have not been initialised");
		return;
	}
	if(_quantimethod.elementsByTagName("peak_detection").size()>0){
		throw mcqError("You can not add a xic filters after peak detection method\n Please correct your method");
		return;
	}
	QDomNodeList filters = _quantimethod.elementsByTagName("xic_filters");
	QDomElement xic_filters;
	if(filters.size()==0){
		xic_filters =  _dom.createElement("xic_filters");
		_quantimethod.appendChild(xic_filters);
	}else{
		xic_filters=filters.at(0).toElement();
	}
	QDomElement anti_spike =  _dom.createElement("anti_spike");
	anti_spike.setAttribute("half",QString::number(filter.get_half_window_length()));
	xic_filters.appendChild(anti_spike);
}

void MasschroqDomDocument::addDetectionZivyMethod(
		const PeakDetectionZivy & detect) {
	if (_quantimethod.isNull()) {
		throw mcqError("Quantification_method element have not been initialised");
		return;
	}
	if(_quantimethod.elementsByTagName("peak_detection").size()>0){
		throw mcqError("You can not add 2 peak detection methods\n Please correct your method");
		return;
	}
	QDomElement peak_detection = _dom.createElement("peak_detection");
	_quantimethod.appendChild(peak_detection);
	QDomElement detection_zivy = _dom.createElement("detection_zivy");
	peak_detection.appendChild(detection_zivy);

	QDomElement mean_filter_half_edge = _dom.createElement("mean_filter_half_edge");
	detection_zivy.appendChild(mean_filter_half_edge);
	mean_filter_half_edge.appendChild(_dom.createTextNode(
			QString::number(detect.get_mean_filter_half_edge())));

	QDomElement minmax_half_edge = _dom.createElement("minmax_half_edge");
	detection_zivy.appendChild(minmax_half_edge);
	minmax_half_edge.appendChild(_dom.createTextNode(
			QString::number(detect.get_minmax_half_edge())));

	QDomElement maxmin_half_edge = _dom.createElement("maxmin_half_edge");
	detection_zivy.appendChild(maxmin_half_edge);
	maxmin_half_edge.appendChild(_dom.createTextNode(
			QString::number(detect.get_maxmin_half_edge())));

	QDomElement detection_threshold_on_max = _dom.createElement("detection_threshold_on_max");
	detection_zivy.appendChild(detection_threshold_on_max);
	detection_threshold_on_max.appendChild(_dom.createTextNode(
			QString::number(detect.get_detection_threshold_on_max(),'f',0)));

	QDomElement detection_threshold_on_min = _dom.createElement("detection_threshold_on_min");
	detection_zivy.appendChild(detection_threshold_on_min);
	detection_threshold_on_min.appendChild(_dom.createTextNode(
			QString::number(detect.get_detection_threshold_on_min(),'f',0)));
}

void MasschroqDomDocument::newAlignementMethod() {
	_dom.clear();
	_alignmethod = _dom.createElement("alignment_method");
	_alignmethod.setAttribute("id", "align1");
	_dom.appendChild(_alignmethod);
}

void MasschroqDomDocument::addObiwarpMethod(const AlignmentObiwarp & align) {
	if (_alignmethod.isNull()) {
		throw mcqError("alignment_method element have not been initialised");
		return;
	}
	if (_alignmethod.childNodes().size()>0){
		throw mcqError("alignment_method element is not empty");
		return;
	}
	QDomElement obiwarp = _dom.createElement("obiwarp");
	_alignmethod.appendChild(obiwarp);
	QDomElement lmat_precision = _dom.createElement("lmat_precision");
	lmat_precision.appendChild(_dom.createTextNode(QString::number(align.getLmatPrecision(), 'f', 1)));
	obiwarp.appendChild(lmat_precision);
	QDomElement mz_start = _dom.createElement("mz_start");
	mz_start.appendChild(_dom.createTextNode(QString::number(align.getMassStart(), 'f', 2)));
	obiwarp.appendChild(mz_start);
	QDomElement mz_stop = _dom.createElement("mz_stop");
	mz_stop.appendChild(_dom.createTextNode(QString::number(align.getMassEnd(), 'f', 2)));
	obiwarp.appendChild(mz_stop);
}

void MasschroqDomDocument::addMs2AlignmentMethod(const AlignmentMs2 & align) {
	if (_alignmethod.isNull()) {
		throw mcqError("alignment_method element have not been initialised");
		return;
	}
	if (_alignmethod.childNodes().size()>0){
		throw mcqError("alignment_method element is not empty");
		return;
	}
	QDomElement ms2 = _dom.createElement("ms2");
	_alignmethod.appendChild(ms2);
	QDomElement ms2_tendency_halfwindow = _dom.createElement(
			"ms2_tendency_halfwindow");
	ms2_tendency_halfwindow.appendChild(_dom.createTextNode(
			QString::number(align.getMs2TendencyWindow())));
	ms2.appendChild(ms2_tendency_halfwindow);
	QDomElement ms2_smoothing_halfwindow = _dom.createElement(
			"ms2_smoothing_halfwindow");
	ms2_smoothing_halfwindow.appendChild(_dom.createTextNode(
			QString::number(align.getMs2SmoothingWindow())));
	ms2.appendChild(ms2_smoothing_halfwindow);
	QDomElement ms1_smoothing_halfwindow = _dom.createElement(
			"ms1_smoothing_halfwindow");
	ms1_smoothing_halfwindow.appendChild(_dom.createTextNode(
			QString::number(align.getMs1SmoothingWindow())));
	ms2.appendChild(ms1_smoothing_halfwindow);
}

QString MasschroqDomDocument::getXMLString() const{
	return(	_dom.toString(1));
}
