/*
 * masschroqDomDocument.h
 *
 *  Created on: 18 sept. 2012
 *      Author: valot
 */

#ifndef MASSCHROQDOMDOCUMENT_H_
#define MASSCHROQDOMDOCUMENT_H_

#include <QDomDocument>
#include <QDomElement>
#include "../../lib/filters/filter_background.h"
#include "../../lib/filters/filter_spike.h"
#include "../../lib/detections/peak_detection_zivy.h"
#include "../../lib/alignments/alignment_ms2.h"
#include "../../lib/alignments/alignment_obiwarp.h"

class MasschroqDomDocument {
public:
	MasschroqDomDocument();
	virtual ~MasschroqDomDocument();

	void newQuantificationMethod();
	void addPpmRangeExtractionMethod(const mcq_xic_type, const mcq_double min,const mcq_double max );
	void addMzRangeExtractionMethod(const mcq_xic_type, const mcq_double min,const mcq_double max );
	void addBackgroundFilterMethod(const FilterBackground & filter);
	void addSpikeFilterMethod(const FilterSpike & filter);
	void addDetectionZivyMethod(const PeakDetectionZivy & detect);
	bool verifyQuantificationMethod();

	void newAlignementMethod();
	void addObiwarpMethod(const AlignmentObiwarp & align);
	void addMs2AlignmentMethod(const AlignmentMs2 & align);

	QString getXMLString() const;

private:
	QDomDocument _dom;
	QDomElement _quantimethod;
	QDomElement _alignmethod;

};

#endif /* MASSCHROQDOMDOCUMENT_H_ */
