/*
 * alignment_widget.cpp
 *
 *  Created on: 1 août 2012
 *      Author: valot
 */

#include "alignment_widget.h"
#include <QMessageBox>

AlignmentWidget::AlignmentWidget(QWidget * parent):
MasschroQWidget(parent){

	_loadingThreadRef = new MsrunSimpleLoaderThread();
	_loadingThreadRef->setMaxProgress(2);
	connect(_loadingThreadRef, SIGNAL(loadedMsrun(Msrun *)), this,
			SLOT(doneLoadindMsrunRef(Msrun *)));
	connect(_loadingThreadRef, SIGNAL(errorDuringLoading(QString)), this,
			SLOT(errorLoadindMsrunRef(QString)));

	_loadingThreadToAligned = new MsrunSimpleLoaderThread();
	_loadingThreadToAligned->setMaxProgress(2);
	connect(_loadingThreadToAligned, SIGNAL(loadedMsrun(Msrun *)), this,
			SLOT(doneLoadindMsrunAligned(Msrun *)));
	connect(_loadingThreadToAligned, SIGNAL(errorDuringLoading(QString)), this,
			SLOT(errortLoadindMsrunAligned(QString)));

	_alignment_thread = new AlignmentThread();
	_alignment_thread->setMaxProgress(2);
	connect(_alignment_thread, SIGNAL(finishAlignment()), this,
			SLOT(doneAlignment()));
	connect(_alignment_thread, SIGNAL(errorDuringAlignment(QString)), this,
			SLOT(errorAlignment(QString)));
}

AlignmentWidget::~AlignmentWidget() {

}

void AlignmentWidget::doneLoadindMsrunRef(Msrun * ref){
	_msrunRef = ref;
	if(_msrunRef==0){
		this->viewError("Msrun Ref are null?");
		_loadingMsrunRefLabel->stopLoading("");
	}else{
		_loadingMsrunRefLabel->stopLoading("MsRun correctly load");
		this->completDataToMsrun(_msrunRef);
	}
}

void AlignmentWidget::doneLoadindMsrunAligned(Msrun * aligned){
	_msunToAligned = aligned;
	if(_msunToAligned==0){
		this->viewError("Msrun aligned are null?");
		_loadingMsruntoALignedLabel->stopLoading("");
	}else{
		_loadingMsruntoALignedLabel->stopLoading("MsRun correctly load");
		this->completDataToMsrun(_msunToAligned);
	}
}

void AlignmentWidget::viewError(QString error) {
	QMessageBox::warning(this, tr("Oops! an error occurred in MassChroQ. Dont Panic :"),
			error);
}

QString AlignmentWidget::strippedFilename(const QString & fullFilename) {
	return QFileInfo(fullFilename).fileName();
}

void AlignmentWidget::errorAlignment(QString error){
	_runningQLabel->stopLoading("");
	this->viewError(error);
}

void AlignmentWidget::errorLoadindMsrunRef(QString error){
	_loadingMsrunRefLabel->stopLoading("Problems");
	_msrunRef=0;
	this->viewError(error);
}

void AlignmentWidget::errortLoadindMsrunAligned(QString error){
	_loadingMsruntoALignedLabel->stopLoading("Problems");
	_msunToAligned=0;
	this->viewError(error);
}

void AlignmentWidget::deleteMsrun(Msrun * msrun){
	if(msrun!=0){
		delete(msrun);
		msrun=0;
	}
}

void AlignmentWidget::completDataToMsrun(Msrun * msrun){
	qDebug()<<"Complete data to Msrun";
}

void AlignmentWidget::doneAlignment() {
	_runningQLabel->stopLoading("");
	this->emitSignalDoneAlignement();
}

void AlignmentWidget::startAlignment() {
	if (_msunToAligned == 0) {
		this->viewError(
				tr(
						"No MSrun file to Aligned have been select.\n Please choose one!"));
		return;
	} else if (_msrunRef == 0) {
		this->viewError(
				tr(
						"No MSrun file to Reference have been select.\n Please choose one!"));
		return;
	}
	_runningQLabel->startLoading("Alignment in progress");
	_alignment_thread->performedAlignment(_alignmentBase, _msrunRef,
			_msunToAligned);
}

void AlignmentWidget::deleteLoadingThread(){
	qDebug()<<"delete Loading thread";
	delete(_loadingThreadRef);
	delete(_loadingThreadToAligned);
	delete(_alignment_thread);
}
