/*
 * ms2_alignment_widget.cpp
 *
 *  Created on: 2 août 2012
 *      Author: valot
 */

#include "ms2_alignment_widget.h"
#include <QGroupBox>
#include <QPushButton>
#include <QFormLayout>
#include <QDoubleSpinBox>
#include <QFileDialog>

Ms2AlignmentWidget::Ms2AlignmentWidget(QWidget * parent) :
		AlignmentWidget(parent), MS2_TENDENCY(10), MS2_SMOOTHING(15), MS1_SMOOTHING(
				0) {
	//kill including widget
	setAttribute(Qt::WA_DeleteOnClose);

	_mainLayout = new QVBoxLayout;
	this->addMasschroqmlSelectionGroup();
	this->addMsRunSelectionGroup();
	this->addParametersGroup();
	this->addAlignButton();

	this->setLayout(_mainLayout);

	this->initializeMs2Method();

	_masschroqml_loader_thread.setMaxProgress(3);
	connect(&_masschroqml_loader_thread,
			SIGNAL(loadedMasschroqml(MasschroqGuiData *)), this,
			SLOT(doneLoadingMasschroqml(MasschroqGuiData *)));
	connect(&_masschroqml_loader_thread, SIGNAL(errorDuringLoading(QString)),
			this, SLOT(ErrorLoadingMasschroqml(QString)));
}

Ms2AlignmentWidget::~Ms2AlignmentWidget() {
	qDebug() << "delete Ms2alignmentWidget";
	this->deleteLoadingThread();
	this->deleteData();
	delete (_monitor);
}

void Ms2AlignmentWidget::addMasschroqmlSelectionGroup() {
	QGroupBox * masschroqml_group = new QGroupBox("MassChroqML selection");

	QVBoxLayout * groupLayout = new QVBoxLayout;

	_masschroqml_edit = new QLineEdit("No MassChroqML Selected");
	_masschroqml_edit->setReadOnly(true);
	groupLayout->addWidget(_masschroqml_edit);

	QHBoxLayout *layout = new QHBoxLayout;
	_masschroqml_message = new RunningQLabel(this);
	layout->addWidget(_masschroqml_message, 2);
	QPushButton * extractButton = new QPushButton(tr("&Load"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this,
			SLOT(startLoadingMasschroqml()));
	layout->addWidget(extractButton, 0);

	groupLayout->addLayout(layout);

	masschroqml_group->setLayout(groupLayout);
	_mainLayout->addWidget(masschroqml_group);
}

void Ms2AlignmentWidget::addMsRunSelectionGroup() {
	QGroupBox * msrun_group = new QGroupBox("MsRun selection");

	QVBoxLayout * groupLayout = new QVBoxLayout;
	_msrun_select_ref = new QComboBox();
	connect(_msrun_select_ref, SIGNAL(activated(int)), this,
			SLOT(startLoadindMsrunRef(int)));

	groupLayout->addWidget(_msrun_select_ref);
	_loadingMsrunRefLabel = new RunningQLabel(this);
	groupLayout->addWidget(_loadingMsrunRefLabel);

	_msrun_select_align = new QComboBox();
	connect(_msrun_select_align, SIGNAL(activated(int)), this,
			SLOT(startLoadindMsrunAligned(int)));

	groupLayout->addWidget(_msrun_select_align);
	_loadingMsruntoALignedLabel = new RunningQLabel(this);
	groupLayout->addWidget(_loadingMsruntoALignedLabel);

	msrun_group->setLayout(groupLayout);
	_mainLayout->addWidget(msrun_group);
}

void Ms2AlignmentWidget::addParametersGroup() {
	QGroupBox * _parameter_group = new QGroupBox("MS2 Parameters");
	QFormLayout * layout = new QFormLayout;

	QDoubleSpinBox * ms2_tendency_box = new QDoubleSpinBox;
	ms2_tendency_box->setMaximum(1000);
	ms2_tendency_box->setWrapping(true);
	ms2_tendency_box->setSingleStep(1);
	ms2_tendency_box->setDecimals(0);
	ms2_tendency_box->setValue(MS2_TENDENCY);
	layout->addRow(tr("MS2 tendency half window"), ms2_tendency_box);

	QDoubleSpinBox * ms2_smoothing_box = new QDoubleSpinBox;
	ms2_smoothing_box->setMaximum(1000);
	ms2_smoothing_box->setWrapping(true);
	ms2_smoothing_box->setSingleStep(1);
	ms2_smoothing_box->setDecimals(0);
	ms2_smoothing_box->setValue(MS2_SMOOTHING);
	layout->addRow(tr("MS2 smoothing half window"), ms2_smoothing_box);

	QDoubleSpinBox * ms1_smoothing_box = new QDoubleSpinBox;
	ms1_smoothing_box->setMaximum(1000);
	ms1_smoothing_box->setWrapping(true);
	ms1_smoothing_box->setSingleStep(1);
	ms1_smoothing_box->setDecimals(0);
	ms1_smoothing_box->setValue(MS1_SMOOTHING);
	layout->addRow(tr("MS1 smoothing half window"), ms1_smoothing_box);

	_parameter_group->setLayout(layout);

	connect(ms2_tendency_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMs2Tendency(double)));

	connect(ms2_smoothing_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMs2Smoothing(double)));

	connect(ms1_smoothing_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMs1Smoothing(double)));

	_mainLayout->addWidget(_parameter_group);
}

void Ms2AlignmentWidget::addAlignButton() {
	QHBoxLayout * layout = new QHBoxLayout;
	_runningQLabel = new RunningQLabel(this);
	layout->addWidget(_runningQLabel, 2);
	QPushButton * extractButton = new QPushButton(tr("&Align"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this, SLOT(startAlignment()));
	layout->addWidget(extractButton, 0);
	_mainLayout->addLayout(layout);
}

void Ms2AlignmentWidget::initializeMs2Method() {
	_monitor = new MonitorAlignmentPlot();
	_alignmentBase = new AlignmentMs2(_monitor);
	AlignmentMs2 * align((AlignmentMs2 *) _alignmentBase);
	align->setMs2TendencyWindow(MS2_TENDENCY);
	align->setMs2SmoothingWindow(MS2_SMOOTHING);
	align->setMs1SmoothingWindow(MS1_SMOOTHING);
	_masschroqGuidata = 0;
	_msunToAligned = 0;
	_msrunRef = 0;
}

void Ms2AlignmentWidget::setMs2Tendency(double win) {
	AlignmentMs2 * align((AlignmentMs2 *) _alignmentBase);
	align->setMs2TendencyWindow(win);
}

void Ms2AlignmentWidget::setMs2Smoothing(double win) {
	AlignmentMs2 * align((AlignmentMs2 *) _alignmentBase);
	align->setMs2SmoothingWindow(win);
}

void Ms2AlignmentWidget::setMs1Smoothing(double win) {
	AlignmentMs2 * align((AlignmentMs2 *) _alignmentBase);
	align->setMs1SmoothingWindow(win);
}

void Ms2AlignmentWidget::startLoadindMsrunRef(int index) {
	if (_alignment_thread->isRunning())
		return;
	qDebug() << "Load MsRun Ref index : " << index;
	QString id = _masschroqGuidata->getMsrunIdToNumber(index);
	Msrun * msrun = _masschroqGuidata->getMsrunHashGroup()->getMsRun(id);

	if (msrun != 0) {
		_loadingMsrunRefLabel->startLoading("Loading");
		_loadingThreadRef->loadMsrun(msrun);
		this->_msrunRef = 0;
	}
}

void Ms2AlignmentWidget::startLoadindMsrunAligned(int index) {
	if (_alignment_thread->isRunning())
		return;
	qDebug() << "Load MsRun Align index : " << index;
	QString id = _masschroqGuidata->getMsrunIdToNumber(index);
	Msrun * msrun = _masschroqGuidata->getMsrunHashGroup()->getMsRun(id);

	if (msrun != 0) {
		_loadingMsruntoALignedLabel->startLoading("Loading");
		_loadingThreadToAligned->loadMsrun(msrun);
		_msunToAligned = 0;
	}
}

void Ms2AlignmentWidget::startLoadingMasschroqml() {
	if (_alignment_thread->isRunning())
		return;
	qDebug() << "Begin MassChroqML Loading";
	//TODO parse masschroml and return a masschroqdata
	QString filename = QFileDialog::getOpenFileName(this,
			tr("Open MasschroqML File"), QString::null,
			tr("masschroqML files (*.masschroqML &  *.xml)"));
	if (filename.isEmpty()) {
		return;
	}

	QFileInfo filenameInfo(filename);

	if (!filenameInfo.exists()) {
		this->viewError(
				tr(
						"The chosen MasschroqML file '%1', does not exist..\nPlease, change the read permissions on this file or load another one. ").arg(
						filename));
		return;
	} else if (!filenameInfo.isReadable()) {
		this->viewError(
				tr(
						"The chosen MasschroqML file '%1', is not readable.\nPlease, change the read permissions on this file or load another one. ").arg(
						filename));
		return;
	}
	_masschroqml_message->startLoading(tr("Loading"));
	_masschroqml_edit->setText(this->strippedFilename(filename));
	_loadingMsrunRefLabel->setText("");
	_loadingMsruntoALignedLabel->setText("");

	this->deleteData();
	this->_msrun_select_ref->clear();
	this->_msrun_select_align->clear();
	//emit resetData();

	MasschroqGuiData * masschroqdata = new MasschroqGuiData(filename);
	QDir::setCurrent(filenameInfo.absolutePath());
	_masschroqml_loader_thread.loadMasschroqml(masschroqdata, filename);

}

void Ms2AlignmentWidget::doneLoadingMasschroqml(MasschroqGuiData * guiData) {
	_masschroqGuidata = guiData;
	_masschroqml_message->stopLoading(tr("MasschroqML correctly loaded"));

//add msrun selection
	const std::vector<QString> FilenameList =
			_masschroqGuidata->getMsrunFilenameList();
	std::vector<QString>::const_iterator it;

	for (it = FilenameList.begin(); it < FilenameList.end(); it++) {
		this->_msrun_select_ref->addItem(this->strippedFilename(*it));
		this->_msrun_select_align->addItem(this->strippedFilename(*it));
	}
//emit updateMasschroqData();
}

void Ms2AlignmentWidget::ErrorLoadingMasschroqml(QString error) {
	_masschroqml_edit->setText(tr("No MassChroqML selected"));
	_masschroqml_message->stopLoading(tr(""));

	this->viewError(error);
}

void Ms2AlignmentWidget::completDataToMsrun(Msrun * msrun) {
	qDebug() << "Complete data to Msrun MS2 Alignment";
	msrun->setPeptideList(
			_masschroqGuidata->getPeptideList().newPeptideListObservedInMsrun(
					msrun));
}

void Ms2AlignmentWidget::emitSignalDoneAlignement() {
	emit newAlignmentCurve();
	MonitorAlignmentPlot * monitor = (MonitorAlignmentPlot *) _monitor;
	emit finishMonitorAlignment(monitor);
	emit finishAlignment(_msunToAligned);
}

void Ms2AlignmentWidget::deleteData() {
	if (_masschroqGuidata != 0) {
		delete (_masschroqGuidata);
		_masschroqGuidata = 0;
	}
}

void Ms2AlignmentWidget::writeElement(
		MasschroqDomDocument * domDocument) const {
	AlignmentMs2 * align((AlignmentMs2 *) _alignmentBase);
	domDocument->addMs2AlignmentMethod(*align);
}
