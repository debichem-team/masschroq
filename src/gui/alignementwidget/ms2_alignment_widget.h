/*
 * ms2_alignment_widget.h
 *
 *  Created on: 2 août 2012
 *      Author: valot
 */

#ifndef MS2_ALIGNMENT_WIDGET_H_
#define MS2_ALIGNMENT_WIDGET_H_

#include "alignment_widget.h"
#include "../../lib/alignments/alignment_ms2.h"
#include "../../lib/alignments/monitors/monitor_alignment_plot.h"
#include "../engine/masschroq_gui_data.h"
#include "../thread/masschroqmlLoaderThread.h"
#include <QVBoxLayout>
#include <QLineEdit>
#include <QComboBox>


class Ms2AlignmentWidget: public AlignmentWidget {

	Q_OBJECT
public:
	Ms2AlignmentWidget(QWidget * parent = 0);
	virtual ~Ms2AlignmentWidget();

	void writeElement(MasschroqDomDocument * domDocument) const;

signals :
	void finishMonitorAlignment(const MonitorAlignmentPlot * monitor);

private slots :
	void setMs2Tendency(double precision);
	void setMs2Smoothing(double mz);
	void setMs1Smoothing(double mz);
	void startLoadindMsrunRef(int mum);
	void startLoadindMsrunAligned(int num);
	void startLoadingMasschroqml();
	void doneLoadingMasschroqml(MasschroqGuiData *);
	void ErrorLoadingMasschroqml(QString);

protected:
	void completDataToMsrun(Msrun * msrun);
	void emitSignalDoneAlignement();

private:
	void addMasschroqmlSelectionGroup();
	void addMsRunSelectionGroup();
	void addParametersGroup();
	void addAlignButton();
	void initializeMs2Method();
	void deleteData();

	QVBoxLayout * _mainLayout;
	QLineEdit * _masschroqml_edit;
	RunningQLabel * _masschroqml_message;
	QComboBox * _msrun_select_ref;
	QComboBox * _msrun_select_align;
	MasschroqmlLoaderThread _masschroqml_loader_thread;
	MasschroqGuiData * _masschroqGuidata;

	const mcq_double MS2_TENDENCY;
	const mcq_double MS2_SMOOTHING;
	const mcq_double MS1_SMOOTHING;
};

#endif /* MS2_ALIGNMENT_WIDGET_H_ */
