/*
 * obiwarp_alignment_widget.h
 *
 *  Created on: 31 juil. 2012
 *      Author: valot
 */

#ifndef OBIWARP_ALIGNMENT_WIDGET_H_
#define OBIWARP_ALIGNMENT_WIDGET_H_ 1

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include "../../lib/alignments/alignment_obiwarp.h"
#include "alignment_widget.h"

class ObiwarpAlignmentWidget: public AlignmentWidget {

Q_OBJECT

public:
	ObiwarpAlignmentWidget(QWidget * parent = 0);
	virtual ~ObiwarpAlignmentWidget();

	void writeElement(MasschroqDomDocument * domDocument) const;

private slots :
	void setLmatPrecision(double precision);
	void setMzStart(double mz);
	void setMzStop(double mz);
	void startLoadindMsrunRef();
	void startLoadindMsrunAligned();

protected:
	void completDataToMsrun(Msrun * msrun);
	void emitSignalDoneAlignement();

private:
	void initializeObiwarpMethod();
	void addSelectionGroup();
	void addParameterGroup();
	void addAlignButton();
	QString getMsrunFilename();

	QVBoxLayout * _mainLayout;
	QLineEdit * _text_edit_ref;
	QLineEdit * _text_edit_toAligned;

	const mcq_double LMAT_PRECISION_WINDOW;
	const mcq_double MZ_START;
	const mcq_double MZ_STOP;
};

#endif /* OBIWARP_ALIGNMENT_WIDGET_H_ */
