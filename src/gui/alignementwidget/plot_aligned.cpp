/**
 * \file plot.cpp
 * \date November 23, 2011
 * \author Edlira Nano
 */

#include "plot_aligned.h"
#include <qwt_legend.h>
#include <qwt_symbol.h>

PlotAligned::PlotAligned(QWidget *parent) :
		QwtPlot(parent) {

	setAutoReplot(false);

	setTitle("");

	// legend
	QwtLegend * legend = new QwtLegend;
	insertLegend(legend, QwtPlot::BottomLegend);

	setAxisTitle(QwtPlot::xBottom, "Retention time (s)");
	setAxisTitle(QwtPlot::yLeft, "Delta RT (s)");
	// enable zooming
	_zoomer = 0;
}

PlotAligned::~PlotAligned() {
	this->clear();
	if (_zoomer != 0) {
		delete _zoomer;
		_zoomer = 0;
	}
}

void PlotAligned::viewAlignedMsRun(const Msrun * msrun) {
	qDebug() << "View MsRun aligned";
	const std::vector<mcq_double> originalRt =
			msrun->getOriginalRetentionTimes();
	const std::vector<mcq_double> alignedRt = msrun->getAlignedRetentionTimes();
	if (originalRt.size() == alignedRt.size()) {
		std::vector<mcq_double>::const_iterator itOriginal = originalRt.begin();
		std::vector<mcq_double>::const_iterator itAligned = alignedRt.begin();
		QwtPlotCurve * curve;
		curve = new QwtPlotCurve("Aligned RT");
		curve->setStyle(QwtPlotCurve::Lines);
		curve->setRenderHint(QwtPlotItem::RenderAntialiased);
		curve->setPen(QPen(getNewColors()));
		_alignedPlots.push_back(curve);

		unsigned int plotsize(alignedRt.size());
		mcq_double x1[plotsize], y1[plotsize];
		int i = 0;
		for (; itOriginal != originalRt.end(); ++itOriginal, ++itAligned) {
			y1[i] = (*itOriginal) - (*itAligned);
			x1[i] = (*itOriginal);
			i++;
		}
		curve->setData(x1, y1, plotsize);

		curve->attach(this);

		this->setTitle(
				tr("Alignment of '%1'").arg(
						msrun->getXmlFileInfo().fileName()));
		this->replot();
		initZoomer();
	}
}

void PlotAligned::viewAlignedMS2Peaks(const MonitorAlignmentPlot * monitorAlignment) {
	if(monitorAlignment==0)
		return;

	qDebug() << "View Ms2 point";
	QwtPlotCurve * curve = new QwtPlotCurve("Common identification");
	curve->setStyle(QwtPlotCurve::Dots);
	curve->setRenderHint(QwtPlotItem::RenderAntialiased);
	const QwtSymbol symbol(QwtSymbol::Star1, getNewColors(), getNewColors(),
			QSize(9, 9));
	curve->setSymbol(symbol);
	_alignedPlots.push_back(curve);

	const std::map<mcq_double,mcq_double>& mapDeltaPeaks =	monitorAlignment->getMs2CommonPeak();

	unsigned int plotsize(mapDeltaPeaks.size());
	mcq_double x1[plotsize], y1[plotsize];
	std::map<mcq_double,mcq_double>::const_iterator it;
	int i = 0;
	for (it = mapDeltaPeaks.begin(); it != mapDeltaPeaks.end(); ++it) {
		y1[i] = it->second;
		x1[i] =  it->first;
		i++;
	}
	curve->setData(x1, y1, plotsize);

	curve->attach(this);
	this->replot();
	initZoomer();
}

void PlotAligned::clear() {
	std::vector<QwtPlotCurve *>::iterator it2;
	for (it2 = _alignedPlots.begin(); it2 != _alignedPlots.end(); ++it2) {
		if ((*it2) != 0) {
			delete (*it2);
			(*it2) = 0;
		}
	}
	_alignedPlots.clear();

	setAxisAutoScale(QwtPlot::xBottom);
	setAxisAutoScale(QwtPlot::yLeft);
	this->setTitle("");
	replot();
	initZoomer();

	qDebug() << "Clear to plot Aligned";
}

void PlotAligned::clearPlot() {
	this->clear();
}

const QColor PlotAligned::getNewColors() {
	QColor color;
	switch (_alignedPlots.size()) {
	case 0:
		return (QColor(Qt::black));
	case 1:
		return (QColor(Qt::blue));
	case 2:
		return (QColor(Qt::red));
	case 3:
		return (QColor(Qt::green));
	case 4:
		return (QColor(Qt::yellow));
	default:
		return (QColor(Qt::black));
	}
	return color;
}

void PlotAligned::initZoomer() {
	// LeftButton for the zooming
	// MidButton for the panning
	// RightButton: zoom out by 1
	// Ctrl+RighButton: zoom out to full size

	if (_zoomer != 0)
		delete _zoomer;

	_zoomer = new QwtPlotZoomer(canvas());
	_zoomer->setResizeMode(QwtPicker::KeepSize);
	_zoomer->setTrackerMode(QwtPicker::AlwaysOn);
	_zoomer->setMousePattern(QwtEventPattern::MouseSelect2, Qt::RightButton,
			Qt::ControlModifier);
	_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::RightButton);

	const QColor c(Qt::darkBlue);
	_zoomer->setRubberBandPen(c);
	_zoomer->setTrackerPen(c);

}
