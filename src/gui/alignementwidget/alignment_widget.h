/*
 * alignment_widget.h
 *
 *  Created on: 1 août 2012
 *      Author: valot
 */

#ifndef ALIGNMENT_WIDGET_H_
#define ALIGNMENT_WIDGET_H_

#include <QWidget>
#include "../runningQLabel.h"
#include "../../lib/msrun/msrun.h"
#include "../thread/msrunSimpleLoaderThread.h"
#include "../thread/alignment_thread.h"
#include "../../lib/alignments/alignment_base.h"
#include "../masschroQWidget.h"

class AlignmentWidget: public MasschroQWidget {
Q_OBJECT

public:
	AlignmentWidget(QWidget * parent = 0);
	virtual ~AlignmentWidget();

signals :
	void newAlignmentCurve();
	void finishAlignment(const Msrun * aligned);

protected slots:
	void startAlignment();
	void doneAlignment();
	void errorAlignment(QString error);
	void errorLoadindMsrunRef(QString error);
	void errortLoadindMsrunAligned(QString error);
	void doneLoadindMsrunRef(Msrun * ref);
	void doneLoadindMsrunAligned(Msrun * align);

protected:
	QString strippedFilename(const QString & fullFilename);
	void viewError(QString error);
	void deleteMsrun(Msrun * msrun);
	virtual void completDataToMsrun(Msrun * msrun)=0;
	virtual void emitSignalDoneAlignement() = 0;
	void deleteLoadingThread();

	Msrun * _msrunRef;
	Msrun * _msunToAligned;
	RunningQLabel * _loadingMsrunRefLabel;
	RunningQLabel * _loadingMsruntoALignedLabel;
	RunningQLabel * _runningQLabel;
	MsrunSimpleLoaderThread * _loadingThreadRef;
	MsrunSimpleLoaderThread * _loadingThreadToAligned;
	AlignmentThread * _alignment_thread;
	AlignmentBase * _alignmentBase;
	MonitorAlignmentBase * _monitor;
};

#endif /* ALIGNMENT_WIDGET_H_ */
