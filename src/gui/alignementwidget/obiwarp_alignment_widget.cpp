/*
 * obiwarp_alignment_widget.cpp
 *
 *  Created on: 31 juil. 2012
 *      Author: valot
 */

#include "obiwarp_alignment_widget.h"
#include "../engine/masschroq_gui_engin.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QMessageBox>
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QFormLayout>

ObiwarpAlignmentWidget::ObiwarpAlignmentWidget(QWidget * parent) :
		AlignmentWidget(parent), LMAT_PRECISION_WINDOW(1), MZ_START(500), MZ_STOP(
				1200) {
	//kill including widget
	setAttribute(Qt::WA_DeleteOnClose);

	_mainLayout = new QVBoxLayout;
	this->addSelectionGroup();
	this->addParameterGroup();
	this->addAlignButton();

	this->setLayout(_mainLayout);

	this->initializeObiwarpMethod();
}

ObiwarpAlignmentWidget::~ObiwarpAlignmentWidget() {
	qDebug() << "Delete Obiwarp widget";
	this->deleteLoadingThread();
	delete (_alignmentBase);
	this->deleteMsrun(_msrunRef);
	this->deleteMsrun(_msunToAligned);
	delete (_monitor);
}

void ObiwarpAlignmentWidget::addSelectionGroup() {
	QGroupBox * _selection_group = new QGroupBox("MsRun selection");
	QVBoxLayout * groupLayout = new QVBoxLayout;

	_text_edit_ref = new QLineEdit("No MsRun Ref Selected");
	_text_edit_ref->setReadOnly(true);
	groupLayout->addWidget(_text_edit_ref);

	QHBoxLayout *layout = new QHBoxLayout;
	_loadingMsrunRefLabel = new RunningQLabel(this);
	layout->addWidget(_loadingMsrunRefLabel, 2);
	QPushButton * extractButton1 = new QPushButton(tr("&Load"));
	extractButton1->setDefault(true);
	connect(extractButton1, SIGNAL(clicked()), this,
			SLOT(startLoadindMsrunRef()));
	layout->addWidget(extractButton1, 0);
	groupLayout->addLayout(layout);

	_text_edit_toAligned = new QLineEdit("No MsRun to Aligned Selected");
	_text_edit_toAligned->setReadOnly(true);
	groupLayout->addWidget(_text_edit_toAligned);

	QHBoxLayout *layout2 = new QHBoxLayout;
	_loadingMsruntoALignedLabel = new RunningQLabel(this);
	layout2->addWidget(_loadingMsruntoALignedLabel, 2);
	QPushButton * extractButton2 = new QPushButton(tr("&Load"));
	extractButton2->setDefault(true);
	connect(extractButton2, SIGNAL(clicked()), this,
			SLOT(startLoadindMsrunAligned()));
	layout2->addWidget(extractButton2, 0);
	groupLayout->addLayout(layout2);

	_selection_group->setLayout(groupLayout);
	_mainLayout->addWidget(_selection_group);
}

void ObiwarpAlignmentWidget::addParameterGroup() {
	QGroupBox * _parameter_group = new QGroupBox("Obiwarp Parameters");
	QFormLayout * layout = new QFormLayout;

	QDoubleSpinBox * lmat_box = new QDoubleSpinBox;
	lmat_box->setMaximum(20);
	lmat_box->setWrapping(true);
	lmat_box->setSingleStep(1);
	lmat_box->setDecimals(0);
	lmat_box->setValue(LMAT_PRECISION_WINDOW);
	layout->addRow(tr("Lmat precision window (Th)"), lmat_box);

	QDoubleSpinBox * mz_start_box = new QDoubleSpinBox;
	mz_start_box->setMaximum(10000);
	mz_start_box->setWrapping(true);
	mz_start_box->setSingleStep(1);
	mz_start_box->setDecimals(2);
	mz_start_box->setValue(MZ_START);
	layout->addRow(tr("m/z Start"), mz_start_box);

	QDoubleSpinBox * mz_stop_box = new QDoubleSpinBox;
	mz_stop_box->setMaximum(10000);
	mz_stop_box->setWrapping(true);
	mz_stop_box->setSingleStep(1);
	mz_stop_box->setDecimals(2);
	mz_stop_box->setValue(MZ_STOP);
	layout->addRow(tr("m/z stop"), mz_stop_box);

	_parameter_group->setLayout(layout);

	connect(lmat_box, SIGNAL(valueChanged(double)), this,
			SLOT(setLmatPrecision(double)));

	connect(mz_start_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMzStart(double)));

	connect(mz_stop_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMzStop(double)));

	_mainLayout->addWidget(_parameter_group);
}

void ObiwarpAlignmentWidget::addAlignButton() {
	QHBoxLayout * layout = new QHBoxLayout;
	_runningQLabel = new RunningQLabel(this);
	layout->addWidget(_runningQLabel, 2);
	QPushButton * extractButton = new QPushButton(tr("&Align"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this, SLOT(startAlignment()));
	layout->addWidget(extractButton, 0);
	_mainLayout->addLayout(layout);
}

void ObiwarpAlignmentWidget::initializeObiwarpMethod() {
	_monitor = new MonitorAlignmentBase();
	_alignmentBase = new AlignmentObiwarp(_monitor);
	AlignmentObiwarp * align((AlignmentObiwarp *) _alignmentBase);
	align->setLmatPrecision(LMAT_PRECISION_WINDOW);
	align->setMassStart(MZ_START);
	align->setMassEnd(MZ_STOP);
	_msunToAligned = 0;
	_msrunRef = 0;
}

void ObiwarpAlignmentWidget::startLoadindMsrunRef() {
	if (_alignment_thread->isRunning())
		return;

	QString filename = this->getMsrunFilename();
	if (filename.isEmpty())
		return;

	Msrun * msrun = MasschroqGuiEngin::getInstance()->getMsrun(filename);
	if (msrun != 0) {
		_text_edit_ref->setText(msrun->getXmlFileInfo().fileName());
		_loadingMsrunRefLabel->startLoading("Loading");
		_loadingThreadRef->loadMsrun(msrun);
		this->deleteMsrun(_msrunRef);
	} else {
		_text_edit_ref->setText("No MsRun ref Selected");
	}
}

void ObiwarpAlignmentWidget::startLoadindMsrunAligned() {
	if (_alignment_thread->isRunning())
		return;
	QString filename = this->getMsrunFilename();

	if (filename.isEmpty())
		return;

	Msrun * msrun = MasschroqGuiEngin::getInstance()->getMsrun(filename);
	if (msrun != 0) {
		_text_edit_toAligned->setText(msrun->getXmlFileInfo().fileName());
		_loadingMsruntoALignedLabel->startLoading("Loading");
		_loadingThreadToAligned->loadMsrun(msrun);
		this->deleteMsrun(_msunToAligned);
	} else {
		_text_edit_toAligned->setText("No MsRun to Aligned Selected");
	}

}

QString ObiwarpAlignmentWidget::getMsrunFilename() {
	QString filename = QFileDialog::getOpenFileName(this, tr("Open Msrun File"),
			QString::null, tr("mzXML or mzML files (*.mzXML *.mzML)"));

	if (!filename.isEmpty()) {

		QFileInfo filenameInfo(filename);

		if (!filenameInfo.exists()) {
			this->viewError(
					tr(
							"The chosen MS run file '%1', does not exist..\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return ("");
		} else if (!filenameInfo.isReadable()) {
			this->viewError(
					tr(
							"The chosen MS run file '%1', is not readable.\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return ("");
		}
	}
	return (filename);
}

void ObiwarpAlignmentWidget::setLmatPrecision(double precision) {
	AlignmentObiwarp * align((AlignmentObiwarp *) _alignmentBase);
	align->setLmatPrecision(precision);
}

void ObiwarpAlignmentWidget::setMzStart(double mz) {
	AlignmentObiwarp * align((AlignmentObiwarp *) _alignmentBase);
	align->setMassStart(mz);
}

void ObiwarpAlignmentWidget::completDataToMsrun(Msrun * msrun){
	qDebug()<<"Complete data to Msrun";
}

void ObiwarpAlignmentWidget::emitSignalDoneAlignement() {
	emit newAlignmentCurve();
	emit finishAlignment(_msunToAligned);
}

void ObiwarpAlignmentWidget::setMzStop(double mz) {
	AlignmentObiwarp * align((AlignmentObiwarp *) _alignmentBase);
	align->setMassEnd(mz);
}

void ObiwarpAlignmentWidget::writeElement(MasschroqDomDocument * domDocument) const{
	AlignmentObiwarp * align((AlignmentObiwarp *) _alignmentBase);
	domDocument->addObiwarpMethod(* align);
}
