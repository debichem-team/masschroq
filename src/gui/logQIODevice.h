/*
 * logQIODevice.h
 *
 *  Created on: 27 sept. 2012
 *      Author: valot
 */

#ifndef LOGQIODEVICE_H_
#define LOGQIODEVICE_H_

#include <QIODevice>

class LogQIODevice: public QIODevice {

	Q_OBJECT

public:
	LogQIODevice(QObject * parent=0);
	virtual ~LogQIODevice();

signals:
	void appendLogString(QString log);

protected:
    qint64 readData(char *data, qint64 maxSize) { return 0; }
    qint64 writeData(const char *data, qint64 maxSize);
};

#endif /* LOGQIODEVICE_H_ */
