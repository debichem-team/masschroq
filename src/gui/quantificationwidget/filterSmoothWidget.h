/*
 *
 *  File filterSmoothWidget.h in 
 *  MassChroQ: Mass Chromatogram Quantification software. 
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
  * \file filterSmoothWidget.h
  * \date February 21, 2012
  * \author Edlira Nano
*/

#ifndef FILTERSMOOTHWIDGET_H
#define FILTERSMOOTHWIDGET_H

#include "../../lib/filters/filter_smoothing.h"
#include "../../mcq_types.h"

#include <QWidget>
class QDialogButtonBox;
class QVBoxLayout;
class QButtonGroup;
class QGroupBox;


/**
 * \class FilterSmoothWidget
 * \brief Smoothing XIC filter Widget
 * 
 * 
 **/

class FilterSmoothWidget : public QWidget
{
	
	Q_OBJECT
    
public :
  
	FilterSmoothWidget(QWidget * parent = 0);
  
	virtual ~FilterSmoothWidget();
	
private slots :

	void filterXic();
	void setHalfWindow(int i);
		
signals :
  
	void filter(FilterSmoothing * filter);
	
protected :

	int _half_window;
		
	virtual void setWidgetParameters();

private :
	
	FilterSmoothing * _filter_smooth;
	QDialogButtonBox * _button_box;
	QGroupBox * _half_window_group;

	const int HALF_WINDOW_DEFAULT;
	const int getHalfWindow() const;
	
};

#endif /* FILTERSMOOTHWIDGET_H */
