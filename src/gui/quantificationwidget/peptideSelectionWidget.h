/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file xicSelectionWidget.h
 * \date November 07, 2011
 * \author Edlira Nano
 */

#ifndef PEPTIDE_SELECTION_WIDGET_H_
#define PEPTIDE_SELECTION_WIDGET_H_ 1

#include "../../mcq_types.h"
#include "xicSelectionWidget.h"
#include <QComboBox>
#include <QLineEdit>
#include "../../lib/peptides/peptide_list.h"

/**
 * \class XicSelectionWidget
 * \brief Xic Selection Widget
 * 
 * 
 */

class PeptideSelectionWidget: public XicSelectionWidget {

Q_OBJECT

public:

PeptideSelectionWidget(TreatmentBoxXicExtract * treatment,
			QWidget * parent = 0);

	virtual ~PeptideSelectionWidget();

private slots :

	void setPeptideToExtract(int comboIndex);
	void setChargeToExtract(QString Charge);

public slots :
	void clearPeptideList();
	void updatePeptideList();

protected:

	void create_mz_group();

private:

	PeptideList * _peptideList;
	QComboBox * _peptideComboBox;
	QComboBox * _zComboBox;
	QLineEdit * _peptide_message;

};

#endif /* PEPTIDE_SELECTION_WIDGET_H_ */
