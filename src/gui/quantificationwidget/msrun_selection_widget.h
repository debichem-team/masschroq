/*
 * msrun_selection_widget.h
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#ifndef MSRUN_SELECTION_WIDGET_H_
#define MSRUN_SELECTION_WIDGET_H_

#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include "../thread/msrunLoaderThread.h"
#include "../../lib/msrun/msrun.h"
#include "../runningQLabel.h"
#include "../masschroQWidget.h"

class MsrunSelectionWidget: public MasschroQWidget {

Q_OBJECT

public:
	MsrunSelectionWidget(QWidget * parent = 0);
	virtual ~MsrunSelectionWidget();

signals :
	void resetData();

private slots :
	void loadMsrun();
	void doneLoading(Msrun * msrun);
	void ErrorLoading(QString error);

private:
	QString strippedFilename(const QString & fullFilename);

	QGroupBox * _selection_group;
	RunningQLabel * _text_message;
	QLineEdit * _text_edit;
	MsrunLoaderThread _msrun_loader_thread;
};

#endif /* MSRUN_SELECTION_WIDGET_H_ */
