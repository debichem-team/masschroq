/*
 *
 *  File filterMaxMinWidget.h in 
 *  MassChroQ: Mass Chromatogram Quantification software. 
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
  * \file filterMaxMinWidget.h
  * \date February 29, 2012
  * \author Edlira Nano
*/

#ifndef FILTERMAXMINWIDGET_H
#define FILTERMAXMINWIDGET_H

#include "../../lib/filters/filter_max_min.h"
//#include "../mcq_types.h"

#include <QWidget>
class QDialogButtonBox;
class QVBoxLayout;
class QButtonGroup;
class QGroupBox;


/**
 * \class FilterMaxMinWidget 
 * \brief MaxMin Open XIC filter Widget
 * 
 **/

class FilterMaxMinWidget : public QWidget
{
	
	Q_OBJECT
    
public :
  
	FilterMaxMinWidget(QWidget * parent = 0);
  
	virtual ~FilterMaxMinWidget();
	
private slots :

	void filterXic();
	void setHalfWindow(int i);
		
signals :
  
	void filter(FilterMaxMin * filter);
	
protected :

	int _half_window;
		
	virtual void setWidgetParameters();

private :
	
	FilterMaxMin * _filter_maxmin;
	QDialogButtonBox * _button_box;
	QGroupBox * _half_window_group;

	const int HALF_WINDOW_DEFAULT; 
	const int getHalfWindow() const;
	
};


#endif /* FILTERMAXMINWIDGET_H */
