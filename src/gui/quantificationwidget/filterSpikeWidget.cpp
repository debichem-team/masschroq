/**
  * \file filterSpikeWidget.cpp
  * \date February 15, 2012 
  * \author Edlira Nano
*/

#include "filterSpikeWidget.h"
#include <QtGui>
#include "../../lib/consoleout.h"

FilterSpikeWidget::FilterSpikeWidget(TreatmentBoxXicFilter * treatmentBox, QWidget * parent)
	:
MasschroQWidget(parent),
	_p_treatmentBox(treatmentBox),
	HALF_WINDOW_DEFAULT(5)
{
	// initialize filter
	_filter_spike = new FilterSpike();
	_half_window = HALF_WINDOW_DEFAULT;
	_filter_spike->set_half_window_length(_half_window);

//	QPushButton * filterButton = new QPushButton(tr("&Filter"));
//	filterButton->setDefault(true);
//	_button_box = new QDialogButtonBox(Qt::Vertical);
//	_button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);
//
//	connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));
	
	QVBoxLayout * mainLayout = new QVBoxLayout;
	setWidgetParameters();
	mainLayout->addWidget(_half_window_group);
	//mainLayout->addWidget(_button_box);
	setLayout(mainLayout);
	setWindowTitle(tr("Spike noise filter"));

	//Trigger filter
	this->filterXic();
}

FilterSpikeWidget::~FilterSpikeWidget()
{
	mcqout()<<"delete filter spike"<<endl;
	if (_filter_spike != 0)
	{
		delete _filter_spike;
		_filter_spike = 0;
	}
}

void
FilterSpikeWidget::setWidgetParameters()
{
  
	_half_window_group = new QGroupBox("Spike Filter");
	QGridLayout *layout = new QGridLayout;
  
	QLabel * half_label = new QLabel("Spike filtering half window");
	QSpinBox * half_box = new QSpinBox;
  
	connect(half_box, SIGNAL(valueChanged(int)), this, SLOT(setHalfWindow(int)));

	half_box->setMaximum(30);
	half_box->setWrapping(true);
	half_box->setSingleStep(1);
	half_box->setValue(HALF_WINDOW_DEFAULT);
	layout->addWidget(half_label, 0, 0);
	layout->addWidget(half_box, 0, 1);
	_half_window_group->setLayout(layout);
}

void
FilterSpikeWidget::setHalfWindow(int i)
{
	_half_window = i;
	_filter_spike->set_half_window_length(i);
	this->filterXic();
}

const int 
FilterSpikeWidget::getHalfWindow() const
{
	return _half_window;
} 

void
FilterSpikeWidget::filterXic() {
	_p_treatmentBox->setFilter(_filter_spike);
}

void FilterSpikeWidget::writeElement(MasschroqDomDocument * domDocument) const{
	domDocument->addSpikeFilterMethod(* _filter_spike);
}
