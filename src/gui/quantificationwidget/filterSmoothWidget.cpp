/**
  * \file filterSmoothWidget.cpp
  * \date February 21, 2012
  * \author Edlira Nano
*/

#include "filterSmoothWidget.h"
#include <QtGui>

FilterSmoothWidget::FilterSmoothWidget(QWidget * parent) 
	:
	QWidget(parent),
	HALF_WINDOW_DEFAULT(1)
{
	// initialize filter
	_filter_smooth = new FilterSmoothing();
	_half_window = HALF_WINDOW_DEFAULT;
	_filter_smooth->set_smoothing_half_window_length(_half_window);
	
	QPushButton * filterButton = new QPushButton(tr("&Filter"));
	filterButton->setDefault(true);
	_button_box = new QDialogButtonBox(Qt::Vertical);
	_button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);
	
	connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));
	
	QVBoxLayout * mainLayout = new QVBoxLayout;
	setWidgetParameters();
	mainLayout->addWidget(_half_window_group);
	mainLayout->addWidget(_button_box);
	setLayout(mainLayout);
	setWindowTitle(tr("Smoothing filter"));

}

FilterSmoothWidget::~FilterSmoothWidget()
{
	if (_filter_smooth != 0)
	{
		delete _filter_smooth;
		_filter_smooth = 0;
	}
}

void
FilterSmoothWidget::setWidgetParameters()
{
  
	_half_window_group = new QGroupBox;
	QGridLayout *layout = new QGridLayout;
  
	QLabel * half_label = new QLabel("Smoothing filter half window");
	QSpinBox * half_box = new QSpinBox;
  
	connect(half_box, SIGNAL(valueChanged(int)),
			this, SLOT(setHalfWindow(int)));

	half_box->setMaximum(100);
	half_box->setWrapping(true);
	half_box->setSingleStep(1);
	half_box->setValue(HALF_WINDOW_DEFAULT);
	layout->addWidget(half_label, 0, 0);
	layout->addWidget(half_box, 0, 1);
	_half_window_group->setLayout(layout);
}

void
FilterSmoothWidget::setHalfWindow(int i)
{
	_half_window = i;
	_filter_smooth->set_smoothing_half_window_length(i);
}

const int 
FilterSmoothWidget::getHalfWindow() const
{
	return _half_window;
} 

void
FilterSmoothWidget::filterXic() {
	emit filter(_filter_smooth);
}
