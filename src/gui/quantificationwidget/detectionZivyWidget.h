/*
 *
 *  File detectionWidget.h in 
 *  MassChroQ: Mass Chromatogram Quantification software. 
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
 * \file detectionWidget.h
 * \date February 29, 2012
 * \author Edlira Nano
 */

#ifndef DETECTION_ZIVY_WIDGET_H
#define DETECTION_ZIVY_WIDGET_H

#include "../../lib/detections/peak_detection_zivy.h"
#include "../treatment/treatment_box_xic_detect.h"
#include "../masschroQWidget.h"
#include <QWidget>

class QDialogButtonBox;
class QVBoxLayout;
class QButtonGroup;
class QGroupBox;
class QSpinBox;

/**
 * \class DetectionZivyWidget
 * \brief Zivy peak detection method Widget
 * 
 * 
 **/

class DetectionZivyWidget: public MasschroQWidget {

Q_OBJECT

public:

	DetectionZivyWidget(TreatmentBoxXicDetect * treatmentBox, QWidget * parent =
			0);

	virtual ~DetectionZivyWidget();

	void writeElement(MasschroqDomDocument * domDocument) const;

private slots :

	void detectPeaks();

	void setSmoothingFilterHalfWindow(int i);
	void setMinMaxFilterHalfWindow(int i);
	void setMaxMinFilterHalfWindow(int i);
	void setThresholdOnMax(double d);
	void setThresholdOnMin(double d);

protected:

	PeakDetectionZivy * _detection_method;
	int _smoothing_filter_half_window;
	int _minmax_filter_half_window;
	int _maxmin_filter_half_window;
	double _threshold_on_max;
	double _threshold_on_min;

	virtual void setWidgetParameters();

private:

	QGroupBox * _parameter_group;
	QDialogButtonBox * _button_box;
	TreatmentBoxXicDetect * _p_treatmentBox;

	void initializeDetectionMethod();
	const int SMOOTH_HALF_WINDOW;
	const int MINMAX_HALF_WINDOW;
	const int MAXMIN_HALF_WINDOW;
	const double THRESHOLD_ON_MAX;
	const double THRESHOLD_ON_MIN;

	const int getSmoothingHalfWindow() const;
	const int getMinMaxHalfWindow() const;
	const int getMaxMinHalfWindow() const;
	const double getThresholdOnMax() const;
	const double getThresholdOnMin() const;

};

#endif /* DETECTION_ZIVY_WIDGET_H */
