/*
 * msrun_selection_widget.cpp
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#include "msrun_selection_widget.h"
#include "../engine/masschroq_gui_engin.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QMessageBox>


MsrunSelectionWidget::MsrunSelectionWidget(QWidget * parent)
	:MasschroQWidget(parent)
{
	//kill including widget
	setAttribute(Qt::WA_DeleteOnClose);

	QVBoxLayout * mainLayout = new QVBoxLayout;
	_selection_group = new QGroupBox("MsRun selection");

	QVBoxLayout * groupLayout = new QVBoxLayout;

	_text_edit = new QLineEdit("No MsRun Selected");
	_text_edit->setReadOnly(true);
	groupLayout->addWidget(_text_edit);

	QHBoxLayout *layout = new QHBoxLayout;
	_text_message = new RunningQLabel(this);
	layout->addWidget(_text_message,2);
	QPushButton * extractButton = new QPushButton(tr("&Load"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this,
			SLOT(loadMsrun()));
	layout->addWidget(extractButton,0);

	groupLayout->addLayout(layout);

	_selection_group->setLayout(groupLayout);
	mainLayout->addWidget(_selection_group);
	this->setLayout(mainLayout);

	_msrun_loader_thread.setMaxProgress(3);
	connect(&_msrun_loader_thread, SIGNAL(loadedMsrun(Msrun *)), this,
			SLOT(doneLoading(Msrun *)));
	connect(&_msrun_loader_thread, SIGNAL(errorDuringLoading(QString)), this,
			SLOT(ErrorLoading(QString)));
}

MsrunSelectionWidget::~MsrunSelectionWidget() {
	qDebug()<<"Delete Msrun Selection widget";
	MasschroqGuiEngin::getInstance()->deleteMsrun();
}

void MsrunSelectionWidget::doneLoading(Msrun * msrun) {
	QString filename = (msrun->getXmlFileInfo()).filePath();
	MasschroqGuiEngin::getInstance()->addMsrun(msrun);
	QString sfilename = strippedFilename(filename);
	//this->setPlotAreaTitle(filename);
	//delete _runningBarr;
	_text_edit->setText(sfilename);
	_text_message->stopLoading("MsRun correctly loaded");
}

void MsrunSelectionWidget::loadMsrun() {
	QString filename = QFileDialog::getOpenFileName(this, tr("Open Msrun File"),
			QString::null, tr("mzXML or mzML files (*.mzXML *.mzML)"));

	if (!filename.isEmpty()) {

		QFileInfo filenameInfo(filename);

		if (!filenameInfo.exists()) {
			this->ErrorLoading(tr(
							"The chosen MS run file '%1', does not exist..\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return;
		} else if (!filenameInfo.isReadable()) {
			this->ErrorLoading(tr(
							"The chosen MS run file '%1', is not readable.\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return;
		}

		MasschroqGuiEngin::getInstance()->deleteMsrun();
		emit resetData();

		_text_edit->setText(strippedFilename(filename));
		_text_message->startLoading("Loading");

		Msrun * msrun = MasschroqGuiEngin::getInstance()->getMsrun(filename);
		_msrun_loader_thread.loadMsrun(msrun);
	} else {
	}
}

void MsrunSelectionWidget::ErrorLoading(QString error) {
	_text_edit->setText(tr("No Msrun selected"));
	_text_message->stopLoading("");
	QMessageBox::warning(this, tr("Oops! an error occurred in MassChroQ. Dont Panic :"),
			error);
}

QString MsrunSelectionWidget::strippedFilename(const QString & fullFilename) {
	return QFileInfo(fullFilename).fileName();
}
