/**
 * \file plot.cpp
 * \date November 23, 2011
 * \author Edlira Nano
 */

#include "plot.h"
#include <qwt_legend.h>
#include <qwt_symbol.h>

Plot::Plot(QWidget *parent) :
		QwtPlot(parent) {

		  qDebug() << "Plot(QWidget *parent) begin";
	setAutoReplot(false);

	setTitle("");
	qDebug() << "Plot(QWidget *parent) begin 2";
	
	// legend
	QwtLegend * legend = new QwtLegend;
	insertLegend(legend, QwtPlot::BottomLegend);

	setAxisTitle(QwtPlot::xBottom, "Retention time (s)");
	setAxisTitle(QwtPlot::yLeft, "Intensity");
	// enable zooming
	_zoomer = 0;
	qDebug() << "Plot(QWidget *parent) end";
		}

Plot::~Plot() {
	this->clear();

	if (_zoomer != 0) {
		delete _zoomer;
		_zoomer = 0;
	}
}

void Plot::viewNewPlot(const TreatmentBoxXicExtract * p_treatment) {
	this->clear();
	qDebug() << "Created new xic plot";
	const xicBase * p_xic = p_treatment->getCurrentXic();
	if (p_xic != 0) {
		QwtPlotCurve * curve = new QwtPlotCurve("Raw Xic");
		curve->setStyle(QwtPlotCurve::Lines);
		curve->setRenderHint(QwtPlotItem::RenderAntialiased);
		curve->setPen(QPen(getNewColors()));
		_xicPlots.insert(
				std::pair<const TreatmentBox*, QwtPlotCurve *>(p_treatment,
						curve));

		unsigned int plotsize(p_xic->size());
		mcq_double x1[plotsize], y1[plotsize];
		p_xic->fill_data_array(x1, y1, plotsize);
		curve->setData(x1, y1, plotsize);

		curve->attach(this);
		this->replot();
		initZoomer();

		//Add New Title
		QString title;
		title.append(p_xic->getMsRun()->getXmlFileInfo().fileName());
		title.append(" : ");
		title.append(QString().setNum(p_xic->getQuantiItem()->get_mz(),'f',4));
		this->setTitle(title);
	}
}
void Plot::updatedCurrentPlot(const TreatmentBoxXicFilter * p_treatment) {
	qDebug() << "Add/update xic plot";
	const xicBase * p_xic = p_treatment->getCurrentXic();
	if (p_xic != 0) {
		QwtPlotCurve * curve;
		if (_xicPlots.count(p_treatment) > 0) {
			curve = _xicPlots.find(p_treatment)->second;
		} else {
			curve = new QwtPlotCurve("Filter Xic");
			curve->setStyle(QwtPlotCurve::Lines);
			curve->setRenderHint(QwtPlotItem::RenderAntialiased);
			curve->setPen(QPen(getNewColors()));
			_xicPlots.insert(
					std::pair<const TreatmentBox*, QwtPlotCurve *>(p_treatment,
							curve));
		}

		unsigned int plotsize(p_xic->size());
		mcq_double x1[plotsize], y1[plotsize];
		p_xic->fill_data_array(x1, y1, plotsize);
		curve->setData(x1, y1, plotsize);

		curve->attach(this);
		this->replot();
		initZoomer();
	}
}
void Plot::updatedPeaks(const TreatmentBoxXicDetect * p_treatment) {
	const std::vector<xicPeak *> * p_peaks = p_treatment->getAllXicPeak();
	if (p_peaks != 0) {
		QwtPlotCurve * curve;
		if (_xicPlots.count(p_treatment) > 0) {
			curve = _xicPlots.find(p_treatment)->second;
		} else {
			curve = new QwtPlotCurve("Detected Peaks");
			curve->setStyle(QwtPlotCurve::Dots);
			curve->setRenderHint(QwtPlotItem::RenderAntialiased);
			const QwtSymbol symbol(QwtSymbol::Star1, getNewColors(),
					getNewColors(), QSize(9, 9));
			curve->setSymbol(symbol);
			_xicPlots.insert(
					std::pair<const TreatmentBox*, QwtPlotCurve *>(p_treatment,
							curve));
		}

		unsigned int plotsize(p_peaks->size());
		mcq_double x1[plotsize], y1[plotsize];
		std::vector<xicPeak *>::const_iterator it;
		int i = 0;
		for (it = p_peaks->begin(); it != p_peaks->end(); ++it) {
			y1[i] = (*it)->get_max_intensity();
			x1[i] = (*it)->get_max_rt();
			i++;
		}
		curve->setData(x1, y1, plotsize);

		curve->attach(this);
		this->replot();
		initZoomer();
	}

}

void Plot::remove(TreatmentBox * box) {
	if (_xicPlots.count(box) > 0) {
		QwtPlotCurve * curve = _xicPlots.find(box)->second;
		delete (curve);
		_xicPlots.erase(_xicPlots.find(box));
		this->replot();
		initZoomer();
	}
}

void Plot::clear() {
	std::map<const TreatmentBox*, QwtPlotCurve *>::iterator it2;
	for (it2 = _xicPlots.begin(); it2 != _xicPlots.end(); ++it2) {
		if ((*it2).second != 0) {
			delete ((*it2).second);
			(*it2).second = 0;
		}
	}
	_xicPlots.clear();

	setAxisAutoScale(QwtPlot::xBottom);
	setAxisAutoScale(QwtPlot::yLeft);
	this->setTitle("");
	replot();
	initZoomer();

	qDebug() << "Clear to plot";
}

void Plot::clearPlot(){
	this->clear();
}

const QColor Plot::getNewColors() {
	QColor color;
	switch (_xicPlots.size()) {
	case 0:
		return (QColor(Qt::black));
	case 1:
		return (QColor(Qt::blue));
	case 2:
		return (QColor(Qt::red));
	case 3:
		return (QColor(Qt::green));
	case 4:
		return (QColor(Qt::yellow));
	default:
		return (QColor(Qt::black));
	}
	return color;
}

void Plot::initZoomer() {
	// LeftButton for the zooming
	// MidButton for the panning
	// RightButton: zoom out by 1
	// Ctrl+RighButton: zoom out to full size

	if (_zoomer != 0)
		delete _zoomer;

	_zoomer = new QwtPlotZoomer(canvas());
	_zoomer->setResizeMode(QwtPicker::KeepSize);
	_zoomer->setTrackerMode(QwtPicker::AlwaysOn);
	_zoomer->setMousePattern(QwtEventPattern::MouseSelect2, Qt::RightButton,
			Qt::ControlModifier);
	_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::RightButton);

	const QColor c(Qt::darkBlue);
	_zoomer->setRubberBandPen(c);
	_zoomer->setTrackerPen(c);

}
