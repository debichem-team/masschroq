/**
 * \file xicSelectionWidget.cpp
 * \date November 15, 2011
 * \author Edlira Nano
 */

#include "xicSelectionWidget.h"

#include <QtGui>
#include "../../lib/mcq_error.h"
#include "../../lib/xicExtractionMethods/xicExtractionMethodMzRange.h"
#include "../../lib/xicExtractionMethods/xicExtractionMethodPpmRange.h"
#include "../../lib/quanti_items/quantiItemMzRt.h"
#include "../engine/masschroq_gui_engin.h"

XicSelectionWidget::XicSelectionWidget(QWidget * parent) :
MasschroQWidget(parent),
		MIN_MZ_RANGE(0.3),
		MAX_MZ_RANGE(0.3),
		MIN_PPM_RANGE(10),
		MAX_PPM_RANGE(10),
		PPM_TYPE("PPM_TYPE"),
		MZ_TYPE("MZ_TYPE")
{
}

XicSelectionWidget::XicSelectionWidget(TreatmentBoxXicExtract * treatment,
		QWidget * parent) :
		MasschroQWidget(parent),
		MIN_MZ_RANGE(0.3),
		MAX_MZ_RANGE(0.3),
		MIN_PPM_RANGE(10),
		MAX_PPM_RANGE(10),
		PPM_TYPE("PPM_TYPE"),
		MZ_TYPE("MZ_TYPE")
{
  qDebug() << "XicSelectionWidget::XicSelectionWidget begin";
	_treatmentBoxXicExtract= treatment;
	//close action
	setAttribute(Qt::WA_DeleteOnClose);

	// extraction method initialization
	// by default in ppm range
	initializeExtractionMethod();

	_xic_selection = new QGroupBox("XIC Extraction");
	create_xic_type_group();
	create_xic_range_group();
	create_mz_group();

	QPushButton * extractButton = new QPushButton(tr("&Extract"));
	extractButton->setDefault(true);
	QDialogButtonBox * _button_box = new QDialogButtonBox(Qt::Horizontal);
	_button_box->addButton(extractButton, QDialogButtonBox::AcceptRole);
	//_button_box->addButton(QDialogButtonBox::Cancel);
	connect(_button_box, SIGNAL(accepted()), this, SLOT(extractXic()));
	//connect(_button_box, SIGNAL(rejected()), this, SLOT(cancel()));

	QVBoxLayout * mainLayout = new QVBoxLayout;
	mainLayout->addWidget(_xic_selection);
	QVBoxLayout * selectLayout = new QVBoxLayout;
	selectLayout->addWidget(_mz_group);
	selectLayout->addWidget(_xic_type_group);
	selectLayout->addWidget(_xic_range_group);
	selectLayout->addWidget(_button_box);
	_xic_selection->setLayout(selectLayout);
	setLayout(mainLayout);
	setWindowTitle(tr("XIC extraction"));
	qDebug() << "XicSelectionWidget::XicSelectionWidget end";
}

XicSelectionWidget::~XicSelectionWidget() {
	qDebug() << "delete xic selection" ;
	if (_xic_extraction_method != 0) {
		delete (_xic_extraction_method);
		_xic_extraction_method=0;
	}
}

void XicSelectionWidget::initializeExtractionMethod() {
	_xic_extraction_method = new XicExtractionMethodPpmRange();
	_min_range = MIN_PPM_RANGE;
	_xic_extraction_method->set_min_range(_min_range);
	_max_range = MAX_PPM_RANGE;
	_xic_extraction_method->set_max_range(_max_range);
	_xic_type = MAX_XIC_TYPE;
	_range_type = PPM_TYPE;
	_mz_to_extract = 421.756;
	_z_to_extract = 1;
	_type_to_extract = "m/z";
}

void XicSelectionWidget::create_xic_type_group() {
	_xic_type_group = new QGroupBox(tr("XIC type"));
	QHBoxLayout *layout = new QHBoxLayout;

	QCheckBox * xic_type_max = new QCheckBox(tr("max xic"));
	xic_type_max->setChecked(true);
	QCheckBox * xic_type_sum = new QCheckBox(tr("sum xic"));

	QButtonGroup * xic_type_buttons = new QButtonGroup();
	xic_type_buttons->setExclusive(true);
	xic_type_buttons->addButton(xic_type_max);
	xic_type_buttons->addButton(xic_type_sum);

	layout->addWidget(xic_type_max);
	layout->addWidget(xic_type_sum);

	_xic_type_group->setLayout(layout);

	connect(xic_type_buttons, SIGNAL(buttonClicked(QAbstractButton *)), this,
			SLOT(setXicType(QAbstractButton *)));
}

void XicSelectionWidget::create_xic_range_group() {
	_xic_range_group = new QGroupBox(tr("XIC extraction range"));
	QGridLayout *layout = new QGridLayout;

	QCheckBox * ppm_range = new QCheckBox(tr("ppm range"));
	ppm_range->setChecked(true);

	QCheckBox * mz_range = new QCheckBox(tr("mz range"));

	QButtonGroup * xic_range_buttons = new QButtonGroup();
	xic_range_buttons->setExclusive(true);
	xic_range_buttons->addButton(ppm_range);
	xic_range_buttons->addButton(mz_range);

	layout->addWidget(ppm_range, 0, 0);
	layout->addWidget(mz_range, 0, 1);

	QLabel * min_label = new QLabel("minimum range");
	_min_range_box = new QDoubleSpinBox;
	_min_range_box->setMaximum(5000.00);
	_min_range_box->setWrapping(true);
	_min_range_box->setSingleStep(1.0);
	_min_range_box->setValue(MIN_PPM_RANGE);
	layout->addWidget(min_label, 1, 0);
	layout->addWidget(_min_range_box, 1, 1);

	QLabel * max_label = new QLabel("maximum range");
	_max_range_box = new QDoubleSpinBox;
	_max_range_box->setMaximum(5000.00);
	_max_range_box->setWrapping(true);
	_max_range_box->setSingleStep(1.0);
	_max_range_box->setValue(MAX_PPM_RANGE);
	layout->addWidget(max_label, 2, 0);
	layout->addWidget(_max_range_box, 2, 1);
	_xic_range_group->setLayout(layout);

	connect(xic_range_buttons, SIGNAL(buttonClicked(QAbstractButton * )), this,
			SLOT(setXicExtractionMethod(QAbstractButton *)));

	connect(_min_range_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMinRange(double)));
	connect(_max_range_box, SIGNAL(valueChanged(double)), this,
			SLOT(setMaxRange(double)));
}

void XicSelectionWidget::create_mz_group() {
  qDebug()<<"XicSelectionWidget::create_mz_group() begin xiSelectionWidget m/z group";
	_mz_group = new QGroupBox(tr("m/z value selection"));
	QGridLayout * layout = new QGridLayout;

	QComboBox * mz_type = new QComboBox;
	mz_type->addItem("m/z");
	mz_type->addItem("Mr");
	mz_type->addItem("MH+");
	mz_type->setEditable(false);

	layout->addWidget(mz_type, 0, 0);

	QDoubleSpinBox * mz_value = new QDoubleSpinBox;
	mz_value->setDecimals(4);
	mz_value->setMaximum(5000.0000);
	mz_value->setSingleStep(10.0000);
	mz_value->setValue(_mz_to_extract);

	layout->addWidget(mz_value, 0, 1);

	QDoubleSpinBox * z_value = new QDoubleSpinBox;
	z_value->setDecimals(0);
	z_value->setMaximum(10);
	z_value->setMinimum(1);
	z_value->setSingleStep(1);
	z_value->setValue(_z_to_extract);

	layout->addWidget(new QLabel("Charge (z)"), 1, 0);
	layout->addWidget(z_value, 1, 1);

	_mz_group->setLayout(layout);

	connect(mz_value, SIGNAL(valueChanged(double)), this,
			SLOT(setMzToExtract(double)));
	connect(z_value, SIGNAL(valueChanged(double)), this,
			SLOT(setZToExtract(double)));
	connect(mz_type, SIGNAL(currentIndexChanged(const QString &)), this,
			SLOT(setTypeToExtract(const QString &)));
	qDebug()<<"XicSelectionWidget::create_mz_group() end";
}

void XicSelectionWidget::setXicType(QAbstractButton * button) {
	QString button_text = button->text();
	mcq_xic_type xic_type;
	if (button_text == "max xic") {
		xic_type = MAX_XIC_TYPE;
	} else if (button_text == "sum xic") {
		xic_type = SUM_XIC_TYPE;
	} else {
		throw mcqError(
				QObject::tr(
						"XicSelectionWidget::setXicType(QAbstractButton *) : Invalid xic type button"));
	}
	_xic_type = xic_type;
}

void XicSelectionWidget::setXicExtractionMethod(QAbstractButton * button) {
	QString button_text = button->text();
	XicExtractionMethodBase * extraction_method;
	if (button_text == "ppm range") {
		extraction_method = new XicExtractionMethodPpmRange();
		_min_range = MIN_PPM_RANGE;
		_max_range = MAX_PPM_RANGE;
		_range_type = PPM_TYPE;
	} else if (button_text == "mz range") {
		extraction_method = new XicExtractionMethodMzRange();
		_min_range = MIN_MZ_RANGE;
		_max_range = MAX_MZ_RANGE;
		_range_type = MZ_TYPE;
	} else {
		throw mcqError(
				QObject::tr(
						"XicSelectionWidget::setXicExtractionMethod(QAbstractButton *) : Invalid range extraction type"));
	}

	if (_xic_extraction_method != 0) {
		delete _xic_extraction_method;
		_xic_extraction_method = 0;
	}

	_xic_extraction_method = extraction_method;
	_min_range_box->setValue(_min_range);
	_max_range_box->setValue(_max_range);
}

void XicSelectionWidget::setMinRange(double min) {
	_min_range = min;
	_xic_extraction_method->set_min_range(_min_range);
}

void XicSelectionWidget::setMaxRange(double max) {
	_max_range = max;
	_xic_extraction_method->set_max_range(_max_range);
}

void XicSelectionWidget::setMzToExtract(double mz) {
	_mz_to_extract = mz;
}

void XicSelectionWidget::setZToExtract(double z) {
	_z_to_extract = z;
}

void XicSelectionWidget::setTypeToExtract(const QString & text){
	_type_to_extract = text;
}

void XicSelectionWidget::extractXic() {
	mcq_double mz;
	if( _type_to_extract == "MH+"){
		mz = (_mz_to_extract+((_z_to_extract-1) * MHPLUS))/_z_to_extract;
	}else if (_type_to_extract == "Mr"){
		mz = (_mz_to_extract+(_z_to_extract * MHPLUS))/_z_to_extract;
	}else{
		mz = _mz_to_extract;
	}
	_xic_extraction_method->setMz(mz);
	QuantiItemBase * quanti_item;
	//if (_rt_to_extract != 0) {
	//	quanti_item = new QuantiItemMzRt(*_xic_extraction_method,
	//			_rt_to_extract);
	//} else {
		quanti_item = new QuantiItemBase(*_xic_extraction_method);
	//}
	_treatmentBoxXicExtract->setXicType(_xic_type);
	_treatmentBoxXicExtract->setQuantiItem(quanti_item);
}

XicExtractionMethodBase *
XicSelectionWidget::getXicExtractionMethod() const {
	return _xic_extraction_method;
}

const mcq_xic_type XicSelectionWidget::getXicType() const {
	return _xic_type;
}

const mcq_double XicSelectionWidget::getMzToExtract() const {
	return _mz_to_extract;
}

void XicSelectionWidget::writeElement(MasschroqDomDocument * domDocument) const{
	if(_range_type == PPM_TYPE)
		domDocument->addPpmRangeExtractionMethod(_xic_type,_min_range,_max_range);
	else if(_range_type == MZ_TYPE)
		domDocument->addMzRangeExtractionMethod(_xic_type,_min_range,_max_range);
	else throw mcqError(tr("This range type %1 are not defined").arg(_range_type));
}
