/**
 * \file filterBackgroundWidget.cpp
 * \date November 24, 2011
 * \author Edlira Nano
 */

#include "filterBackgroundWidget.h"
#include <QtGui>
#include "../../lib/consoleout.h"

FilterBackgroundWidget::FilterBackgroundWidget(
		TreatmentBoxXicFilter * treatmentBox, QWidget * parent) :
		MasschroQWidget(parent), _p_treatmentBox(treatmentBox), MEDIAN_HALF_WINDOW(
				5), MINMAX_HALF_WINDOW(20) {
	setAttribute(Qt::WA_DeleteOnClose);
	// initialize filter
	initializeFilter();

//	QPushButton * filterButton = new QPushButton(tr("&Filter"));
//	filterButton->setDefault(true);
//	_button_box = new QDialogButtonBox(Qt::Vertical);
//	_button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);
//	connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));
//	QPushButton * removeButton = new QPushButton(tr("&Remove"));
//	_button_box->addButton(removeButton, QDialogButtonBox::DestructiveRole);
//	connect(_button_box, SIGNAL(clicked()), this, SLOT(removeFilter()));

	QVBoxLayout * mainLayout = new QVBoxLayout;
	setWidgetParameters();
	mainLayout->addWidget(_parameter_group);
	//mainLayout->addWidget(_button_box);
	setLayout(mainLayout);
	setWindowTitle(tr("Background noise filter"));

	//Trigger filter
	this->filterXic();
}

FilterBackgroundWidget::~FilterBackgroundWidget() {
	mcqout() << "delete filter background" << endl;
	if (_filter_background != 0) {
		delete _filter_background;
		_filter_background = 0;
	}
}

void FilterBackgroundWidget::initializeFilter() {
	_filter_background = new FilterBackground();
	_median_half_window = MEDIAN_HALF_WINDOW;
	_filter_background->set_half_median_window_length(_median_half_window);
	_min_max_half_window = MINMAX_HALF_WINDOW;
	_filter_background->set_half_min_max_window_length(MINMAX_HALF_WINDOW);
}

void FilterBackgroundWidget::setWidgetParameters() {

	_parameter_group = new QGroupBox("Background Filter");
	QGridLayout *layout = new QGridLayout;

	QLabel * median_label = new QLabel("Median filtering half window");
	QSpinBox * median_box = new QSpinBox;
	median_box->setMaximum(100);
	median_box->setWrapping(true);
	median_box->setSingleStep(1);
	median_box->setValue(MEDIAN_HALF_WINDOW);
	layout->addWidget(median_label, 0, 0);
	layout->addWidget(median_box, 0, 1);

	QLabel * minmax_label = new QLabel("MinMax filtering half window");
	QSpinBox * minmax_box = new QSpinBox;
	minmax_box->setMaximum(100);
	minmax_box->setWrapping(true);
	minmax_box->setSingleStep(1);
	minmax_box->setValue(MINMAX_HALF_WINDOW);
	layout->addWidget(minmax_label, 1, 0);
	layout->addWidget(minmax_box, 1, 1);

	_parameter_group->setLayout(layout);

	connect(median_box, SIGNAL(valueChanged(int)), this,
			SLOT(setMedianHalfWindow(int)));
	connect(minmax_box, SIGNAL(valueChanged(int)), this,
			SLOT(setMinMaxHalfWindow(int)));
}

void FilterBackgroundWidget::setMedianHalfWindow(int i) {
	_median_half_window = i;
	_filter_background->set_half_median_window_length(_median_half_window);
	this->filterXic();
}

void FilterBackgroundWidget::setMinMaxHalfWindow(int i) {
	_min_max_half_window = i;
	_filter_background->set_half_min_max_window_length(_min_max_half_window);
	this->filterXic();
}

const int FilterBackgroundWidget::getMedianHalfWindow() const {
	return _median_half_window;
}

const int FilterBackgroundWidget::getMinMaxHalfWindow() const {
	return _min_max_half_window;
}

void FilterBackgroundWidget::filterXic() {
	_p_treatmentBox->setFilter(_filter_background);
}

void FilterBackgroundWidget::writeElement(
		MasschroqDomDocument * domDocument) const {
	domDocument->addBackgroundFilterMethod(* _filter_background);
}
