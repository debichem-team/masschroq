/*
 * msrun_selection_widget.h
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#ifndef MASSCHROQML_SELECTION_WIDGET_H_
#define MASSCHROQML_SELECTION_WIDGET_H_

#include <QWidget>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLineEdit>
#include "../thread/msrunLoaderThread.h"
#include "../../lib/msrun/msrun.h"
#include "../thread/masschroqmlLoaderThread.h"
#include "../engine/masschroq_gui_data.h"
#include "../runningQLabel.h"
#include "../masschroQWidget.h"

class MasschroqmlSelectionWidget: public MasschroQWidget {

Q_OBJECT

public:
	MasschroqmlSelectionWidget(QWidget * parent = 0);
	virtual ~MasschroqmlSelectionWidget();

signals :
	void resetData();
	void updateMasschroqData();
	void updateMsRunData();

private slots :
	void loadMasschroqml();
	void loadMsrun(int index);
	void doneLoadingMsRun(Msrun * msrun);
	void doneLoadingMasschroqml(MasschroqGuiData *);
	void ErrorLoadingMasschroqml(QString error);
	void ErrorLoadingMsrun(QString error);

private:
	QString strippedFilename(const QString & fullFilename);
	void addMasschroqmlSelectionGroup(QVBoxLayout *layout);
	void addMsRunSelectionGroup(QVBoxLayout *layout);
	void viewErrorMessage(QString error);

	RunningQLabel * _masschroqml_message;
	QLineEdit * _masschroqml_edit;
	QComboBox * _msrun_select;
	RunningQLabel * _msrun_message;
	MsrunLoaderThread _msrun_loader_thread;
	MasschroqmlLoaderThread _masschroqml_loader_thread;
};

#endif /* MASSCHROQML_SELECTION_WIDGET_H_ */
