/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file plot.h
 * \date November 23, 2011
 * \author Edlira Nano
 */

#ifndef _PLOT_H_
#define _PLOT_H_ 1

#include <qwt_plot.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_curve.h>
#include "../treatment/treatment_box_xic_extract.h"
#include "../treatment/treatment_box_xic_detect.h"
#include "../treatment/treatment_box_xic_filter.h"
#include "../treatment/treatment_box.h"

class Plot : public QwtPlot {
  Q_OBJECT

public:
  
  Plot(QWidget * parent);
  
  virtual ~Plot();

  void clear();

  void initZoomer();

public slots :
	void viewNewPlot(const TreatmentBoxXicExtract *);
	void updatedCurrentPlot(const TreatmentBoxXicFilter *);
	void updatedPeaks(const TreatmentBoxXicDetect *);
	void remove(TreatmentBox *);
	void clearPlot();
  

private:
	const QColor getNewColors();

  std::map<const TreatmentBox *, QwtPlotCurve *> _xicPlots;
  QwtPlotZoomer * _zoomer;
};


#endif // _PLOT_H_
