/*
 * msrun_selection_widget.cpp
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#include "masschroqml_selection_widget.h"
#include "../engine/masschroq_gui_engin.h"
#include <vector>
#include <QVBoxLayout>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QMessageBox>

MasschroqmlSelectionWidget::MasschroqmlSelectionWidget(QWidget * parent) :
MasschroQWidget(parent) {
	//kill including widget
	setAttribute(Qt::WA_DeleteOnClose);

	QVBoxLayout * mainLayout = new QVBoxLayout;
	this->addMasschroqmlSelectionGroup(mainLayout);
	this->addMsRunSelectionGroup(mainLayout);

	this->setLayout(mainLayout);

	_msrun_loader_thread.setMaxProgress(3);
	connect(&_msrun_loader_thread, SIGNAL(loadedMsrun(Msrun *)), this,
			SLOT(doneLoadingMsRun(Msrun *)));
	connect(&_msrun_loader_thread, SIGNAL(errorDuringLoading(QString)), this,
			SLOT(ErrorLoadingMsrun(QString)));

	_masschroqml_loader_thread.setMaxProgress(3);
	connect(&_masschroqml_loader_thread,
			SIGNAL(loadedMasschroqml(MasschroqGuiData *)), this,
			SLOT(doneLoadingMasschroqml(MasschroqGuiData *)));
	connect(&_masschroqml_loader_thread, SIGNAL(errorDuringLoading(QString)),
			this, SLOT(ErrorLoadingMasschroqml(QString)));
}

MasschroqmlSelectionWidget::~MasschroqmlSelectionWidget() {
	qDebug() << "Delete Msrun Selection widget";
	MasschroqGuiEngin::getInstance()->deleteMsrun();
	MasschroqGuiEngin::getInstance()->deleteMassChroqGuiData();
}

void MasschroqmlSelectionWidget::addMasschroqmlSelectionGroup(
		QVBoxLayout * mainlayout) {
	QGroupBox * masschroqml_group = new QGroupBox("MassChroqML selection");

	QVBoxLayout * groupLayout = new QVBoxLayout;

	_masschroqml_edit = new QLineEdit("No MassChroqML Selected");
	_masschroqml_edit->setReadOnly(true);
	groupLayout->addWidget(_masschroqml_edit);

	QHBoxLayout *layout = new QHBoxLayout;
	_masschroqml_message = new RunningQLabel(this);
	layout->addWidget(_masschroqml_message, 2);
	QPushButton * extractButton = new QPushButton(tr("&Load"));
	extractButton->setDefault(true);
	connect(extractButton, SIGNAL(clicked()), this, SLOT(loadMasschroqml()));
	layout->addWidget(extractButton, 0);

	groupLayout->addLayout(layout);

	masschroqml_group->setLayout(groupLayout);
	mainlayout->addWidget(masschroqml_group);
}

void MasschroqmlSelectionWidget::addMsRunSelectionGroup(
		QVBoxLayout * mainlayout) {
	QGroupBox * msrun_group = new QGroupBox("MsRun selection");

	QVBoxLayout * groupLayout = new QVBoxLayout;
	_msrun_select = new QComboBox();
	connect(_msrun_select, SIGNAL(activated(int)), this, SLOT(loadMsrun(int)));

	groupLayout->addWidget(_msrun_select);
	_msrun_message = new RunningQLabel(this);
	groupLayout->addWidget(_msrun_message);

	msrun_group->setLayout(groupLayout);
	mainlayout->addWidget(msrun_group);
}

void MasschroqmlSelectionWidget::doneLoadingMsRun(Msrun * msrun) {
	QString filename = (msrun->getXmlFileInfo()).filePath();
	MasschroqGuiEngin::getInstance()->addMsrun(msrun);

	_msrun_message->stopLoading(tr("MsRun correctly loaded"));
	emit updateMsRunData();
}

void MasschroqmlSelectionWidget::doneLoadingMasschroqml(
		MasschroqGuiData * masschroqdata) {
	MasschroqGuiEngin::getInstance()->setMassChroqGuiData(masschroqdata);
	_masschroqml_message->stopLoading(tr("MasschroqML correctly loaded"));

	//add msrun selection
	const std::vector<QString> FilenameList =
			masschroqdata->getMsrunFilenameList();
	std::vector<QString>::const_iterator it;

	for (it = FilenameList.begin(); it < FilenameList.end(); it++)
		this->_msrun_select->addItem(this->strippedFilename(*it));

	emit updateMasschroqData();
}

void MasschroqmlSelectionWidget::loadMasschroqml() {
	qDebug() << "Begin MassChroqML Loading";
	//TODO parse masschroml and return a masschroqdata
	QString filename = QFileDialog::getOpenFileName(this,
			tr("Open MasschroqML File"), QString::null,
			tr("masschroqML files (*.masschroqML &  *.xml)"));
	if (!filename.isEmpty()) {

		QFileInfo filenameInfo(filename);

		if (!filenameInfo.exists()) {
			this->ErrorLoadingMsrun(
					tr(
							"The chosen MasschroqML file '%1', does not exist..\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return;
		} else if (!filenameInfo.isReadable()) {
			this->ErrorLoadingMsrun(
					tr(
							"The chosen MasschroqML file '%1', is not readable.\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return;
		}
		_masschroqml_message->startLoading(tr("Loading"));
		_masschroqml_edit->setText(this->strippedFilename(filename));
		_msrun_message->setText("");

		MasschroqGuiEngin::getInstance()->deleteMassChroqGuiData();
		MasschroqGuiEngin::getInstance()->deleteMsrun();
		this->_msrun_select->clear();
		emit resetData();

		MasschroqGuiData * masschroqdata = new MasschroqGuiData(filename);
		_masschroqml_loader_thread.loadMasschroqml(masschroqdata, filename);
	}
}

void MasschroqmlSelectionWidget::loadMsrun(int index) {
	qDebug() << "Load MsRun index : " << index;
	QString id =
			MasschroqGuiEngin::getInstance()->getMasschroqGuiData()->getMsrunIdToNumber(
					index);
	Msrun * current =
			MasschroqGuiEngin::getInstance()->getMasschroqGuiData()->getMsrunHashGroup()->getMsRun(
					id);
	if (current == 0) {
		this->ErrorLoadingMsrun(
				tr("There is no MsRun corresponded to id '%1'").arg(id));
		return;
	}

	QString filename = current->getXmlFileInfo().absoluteFilePath();
	QString format = current->getXmlFileFormat();
	if (!filename.isEmpty()) {

		QFileInfo filenameInfo(filename);

		if (!filenameInfo.exists()) {
			this->ErrorLoadingMsrun(
					tr(
							"The chosen MS run file '%1', does not exist..\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return;
		} else if (!filenameInfo.isReadable()) {
			this->ErrorLoadingMsrun(
					tr(
							"The chosen MS run file '%1', is not readable.\nPlease, change the read permissions on this file or load another one. ").arg(
							filename));
			return;
		}

		MasschroqGuiEngin::getInstance()->deleteMsrun();
		emit resetData();

		_msrun_message->startLoading(tr("Loading"));

		Msrun * msrun = MasschroqGuiEngin::getInstance()->getMsrun(filename, id,
				format);
		_msrun_loader_thread.loadMsrun(msrun);
	} else {
	}
}

void MasschroqmlSelectionWidget::ErrorLoadingMsrun(QString error) {
	_msrun_message->stopLoading("");
	this->viewErrorMessage(error);
}

void MasschroqmlSelectionWidget::ErrorLoadingMasschroqml(QString error) {

	_masschroqml_edit->setText(tr("No MassChroqML selected"));
	_masschroqml_message->stopLoading(tr(""));

	this->viewErrorMessage(error);
}

void MasschroqmlSelectionWidget::viewErrorMessage(QString error) {
	QMessageBox::warning(this,
			tr("Oops! an error occurred in MassChroQ. Dont Panic :"), error);
}

QString MasschroqmlSelectionWidget::strippedFilename(
		const QString & fullFilename) {
	return QFileInfo(fullFilename).fileName();
}
