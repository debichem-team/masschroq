/**
  * \file filterMaxMinWidget.cpp
  * \date February 29, 2012 
  * \author Edlira Nano
*/

#include "filterMaxMinWidget.h"
#include <QtGui>

FilterMaxMinWidget::FilterMaxMinWidget(QWidget * parent) 
	:
	QWidget(parent),
	HALF_WINDOW_DEFAULT(2)
{
	// initialize filter
	_filter_maxmin = new FilterMaxMin();
	_half_window = HALF_WINDOW_DEFAULT;
	_filter_maxmin->set_max_min_half_window_length(_half_window);

	QPushButton * filterButton = new QPushButton(tr("&Filter"));
	filterButton->setDefault(true);
	_button_box = new QDialogButtonBox(Qt::Vertical);
	_button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);
	
	connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));
	
	QVBoxLayout * mainLayout = new QVBoxLayout;
	setWidgetParameters();
	mainLayout->addWidget(_half_window_group);
	mainLayout->addWidget(_button_box);
	setLayout(mainLayout);
	setWindowTitle(tr("MaxMin (Open) filter"));

}

FilterMaxMinWidget::~FilterMaxMinWidget()
{
	if (_filter_maxmin != 0)
	{
		delete _filter_maxmin;
		_filter_maxmin = 0;
	}
}

void
FilterMaxMinWidget::setWidgetParameters()
{
  
	_half_window_group = new QGroupBox;
	QGridLayout *layout = new QGridLayout;
  
	QLabel * half_label = new QLabel("MaxMin/Open filtering half window");
	QSpinBox * half_box = new QSpinBox;
  
	connect(half_box, SIGNAL(valueChanged(int)),
			this, SLOT(setHalfWindow(int)));

	half_box->setMaximum(50);
	half_box->setWrapping(true);
	half_box->setSingleStep(1);
	half_box->setValue(HALF_WINDOW_DEFAULT);
	layout->addWidget(half_label, 0, 0);
	layout->addWidget(half_box, 0, 1);
	_half_window_group->setLayout(layout);
}

void
FilterMaxMinWidget::setHalfWindow(int i)
{
	_half_window = i;
	_filter_maxmin->set_max_min_half_window_length(_half_window);
}

const int 
FilterMaxMinWidget::getHalfWindow() const
{
	return _half_window;
} 

void
FilterMaxMinWidget::filterXic() {
	emit filter(_filter_maxmin);
}
