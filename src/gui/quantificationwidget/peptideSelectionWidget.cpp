/**
 * \file xicSelectionWidget.cpp
 * \date November 15, 2011
 * \author Edlira Nano
 */

#include "peptideSelectionWidget.h"

#include <QtGui>
#include "../../lib/mcq_error.h"
#include "../../lib/xicExtractionMethods/xicExtractionMethodMzRange.h"
#include "../../lib/xicExtractionMethods/xicExtractionMethodPpmRange.h"
#include "../../lib/quanti_items/quantiItemMzRt.h"
#include "../engine/masschroq_gui_engin.h"
#include "../../lib/peptides/peptide_isotope.h"

PeptideSelectionWidget::PeptideSelectionWidget(
		TreatmentBoxXicExtract * treatment, QWidget * parent) :
	XicSelectionWidget(parent) {
	_treatmentBoxXicExtract = treatment;
	//close action
	setAttribute(Qt::WA_DeleteOnClose);

	// extraction method initialization
	// by default in ppm range
	initializeExtractionMethod();

	_xic_selection = new QGroupBox("XIC Extraction");
	create_xic_type_group();
	create_xic_range_group();
	create_mz_group();

	QPushButton * extractButton = new QPushButton(tr("&Extract"));
	extractButton->setDefault(true);
	QDialogButtonBox * _button_box = new QDialogButtonBox(Qt::Horizontal);
	_button_box->addButton(extractButton, QDialogButtonBox::AcceptRole);
	//_button_box->addButton(QDialogButtonBox::Cancel);
	connect(_button_box, SIGNAL(accepted()), this, SLOT(extractXic()));
	//connect(_button_box, SIGNAL(rejected()), this, SLOT(cancel()));

	QVBoxLayout * mainLayout = new QVBoxLayout;
	mainLayout->addWidget(_xic_selection);
	QVBoxLayout * selectLayout = new QVBoxLayout;
	selectLayout->addWidget(_mz_group);
	selectLayout->addWidget(_xic_type_group);
	selectLayout->addWidget(_xic_range_group);
	selectLayout->addWidget(_button_box);
	_xic_selection->setLayout(selectLayout);
	setLayout(mainLayout);
	setWindowTitle(tr("XIC extraction"));
	_type_to_extract = "Mr";
}

PeptideSelectionWidget::~PeptideSelectionWidget() {
	qDebug() << "delete xic selection";
	if (_xic_extraction_method != 0) {
		delete (_xic_extraction_method);
		_xic_extraction_method = 0;
	}
}

void PeptideSelectionWidget::create_mz_group() {
	qDebug() << "peptideSelectionWidget peptide group";
	_mz_group = new QGroupBox(tr("Peptide selection"));
	QVBoxLayout * groupLayout = new QVBoxLayout;
	QHBoxLayout * layout = new QHBoxLayout;

	_peptideComboBox = new QComboBox;
	_peptideComboBox->setEditable(false);

	layout->addWidget(_peptideComboBox, 2);

	_zComboBox = new QComboBox;
	_zComboBox->setEditable(false);
	layout->addWidget(_zComboBox, 0);

	groupLayout->addLayout(layout);
	_peptide_message = new QLineEdit();
	_peptide_message->setReadOnly(true);
	groupLayout->addWidget(_peptide_message);

	_mz_group->setLayout(groupLayout);

connect(_zComboBox, SIGNAL(currentIndexChanged(const QString &)), this,
		SLOT(setChargeToExtract(const QString &)));
connect(_peptideComboBox, SIGNAL(currentIndexChanged(int)),
		this, SLOT(setPeptideToExtract(int)));
}

void PeptideSelectionWidget::setChargeToExtract(QString Charge) {
	_z_to_extract = Charge.toInt();
}

void PeptideSelectionWidget::updatePeptideList() {
	MasschroqGuiData * data =
			MasschroqGuiEngin::getInstance()->getMasschroqGuiData();
	if (data != 0) {
		PeptideList list = data->getPeptideList();
		qDebug() << "PeptideSelectionWidget::updatePeptideList() list.size() "
				<< list.size();
		PeptideList::iterator it;
		_peptideComboBox->clear();
		_peptide_message->setText("");
		if (MasschroqGuiEngin::getInstance()->getCurrentLoadedMsrun() != 0) {
			QString
					msRunID =
							MasschroqGuiEngin::getInstance()->getCurrentLoadedMsrun()->getXmlId();
			qDebug() << "Select Peptide to msRUn id : " << msRunID;
			int i;
			for ( i = 0, it = list.begin(); it != list.end(); it++, i++) {
				if ((*it)->getIsotopeLabel() == NULL) {
					if ((*it)->isObservedIn(msRunID)) {
						_peptideComboBox->addItem((*it)->getXmlId(), QVariant(i));
					}
				} else {
					PeptideIsotope * p_peptideIsotope = (PeptideIsotope *) *it;
					if (p_peptideIsotope->isObservedIn(msRunID)) {
						_peptideComboBox->addItem(
								p_peptideIsotope->getXmlId() + " "
										+ p_peptideIsotope->getIsotopeLabel()->getXmlId(), QVariant(i));
						qDebug()
								<< "PeptideSelectionWidget::updatePeptideList() isotope "
								<< p_peptideIsotope->getSequence() << " "
								<< p_peptideIsotope->getMods() << " "
								<< p_peptideIsotope->getIsotopeLabel()->getXmlId()
								<< " IS observed in msRun";
					} else {
					}
				}
			}
		}
	}
}

void PeptideSelectionWidget::clearPeptideList() {
	_peptideComboBox->clear();
	_zComboBox->clear();
	_peptide_message->setText("");
}

void PeptideSelectionWidget::setPeptideToExtract(int index) {
	qDebug()
			<< "PeptideSelectionWidget::setPeptideToExtract() index "
			<< index << "  _peptideComboBox->currentIndex() " << _peptideComboBox->currentIndex();
	MasschroqGuiData * data =
			MasschroqGuiEngin::getInstance()->getMasschroqGuiData();
	if (data != 0) {
		int peptideIndex = _peptideComboBox->itemData(_peptideComboBox->currentIndex()).toInt();
		const Peptide * pep = data->getPeptideList()[peptideIndex];
		if (pep != NULL) {
			_mz_to_extract = pep->getMass();
			//Reload charge selector
			std::set<unsigned int> * charges = pep->getCharges(
					*data->getMsrunHashGroup());
			qDebug() << "Peptide Selection : " << pep->getXmlId()
					<< " with different charge " << charges->size();
			std::set<unsigned int>::const_iterator it;
			_zComboBox->clear();
			for (it = charges->begin(); it != charges->end(); it++) {
				_zComboBox->addItem(QString().setNum(*it));
			}
			if (pep->getIsotopeLabel() == NULL) {
				_peptide_message->setText(pep->getSequence() + " "
						+ pep->getMods());

			} else {
				qDebug()
						<< "PeptideSelectionWidget::updatePeptideList() isotope "
						<< pep->getSequence() << " " << pep->getMods() << " "
						<< pep->getIsotopeLabel()->getXmlId()
						<< " not observed in msRun";
				_peptide_message->setText(pep->getSequence() + " "
						+ pep->getMods() + " "
						+ pep->getIsotopeLabel()->getXmlId());
			}
		}
	}
}
