/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file xicSelectionWidget.h
 * \date November 07, 2011
 * \author Edlira Nano
 */

#ifndef XIC_SELECTION_WIDGET_H_
#define XIC_SELECTION_WIDGET_H_ 1

#include "../../mcq_types.h"
#include "../treatment/treatment_box_xic_extract.h"
#include "../masschroQWidget.h"
#include <QWidget>

class QButtonGroup;
class QGroupBox;
class QDialogButtonBox;
class QAbstractButton;
class QDoubleSpinBox;
class XicExtractionMethodBase;

/**
 * \class XicSelectionWidget
 * \brief Xic Selection Widget
 * 
 * 
 */

class XicSelectionWidget : public MasschroQWidget {

  Q_OBJECT
    
 public :
  
  XicSelectionWidget(TreatmentBoxXicExtract * treatment, QWidget * parent = 0);
  XicSelectionWidget(QWidget * parent= 0);
  
  virtual ~XicSelectionWidget();
  
  XicExtractionMethodBase * getXicExtractionMethod() const;
  
  const mcq_xic_type getXicType() const;

  const mcq_double getMzToExtract() const;

  const mcq_double getRtToExtract() const;
  
  void writeElement(MasschroqDomDocument * domDocument) const;

  private slots :
  
  void setXicType(QAbstractButton *);
  void setXicExtractionMethod(QAbstractButton *);
  void setMinRange(double min);
  void setMaxRange(double max);
  void setMzToExtract(double mz);
  void setZToExtract(double z);
  void setTypeToExtract(const QString & text);
  void extractXic();

 protected :

  XicExtractionMethodBase * _xic_extraction_method;
  mcq_xic_type _xic_type;
  mcq_double _min_range;
  mcq_double _max_range;
  mcq_double _mz_to_extract;
  mcq_double _z_to_extract;
  QString _type_to_extract;
  QString _range_type;
  QGroupBox * _mz_group;
  
  virtual void create_mz_group();
  void create_xic_type_group();
  void create_xic_range_group();
  void initializeExtractionMethod();
  
  QGroupBox * _xic_type_group;
  QGroupBox * _xic_range_group;
  QGroupBox * _xic_selection;
  QDoubleSpinBox * _min_range_box; 
  QDoubleSpinBox * _max_range_box;
  const mcq_double MIN_MZ_RANGE;
  const mcq_double MAX_MZ_RANGE;
  const mcq_double MIN_PPM_RANGE;
  const mcq_double MAX_PPM_RANGE;
  const QString PPM_TYPE;
  const QString MZ_TYPE;

  TreatmentBoxXicExtract * _treatmentBoxXicExtract;
   
};


#endif /* XIC_SELECTION_WIDGET_H_ */
