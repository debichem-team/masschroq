/**
 * \file detectionZivyWidget.cpp
 * \date February 29, 2012
 * \author Edlira Nano
 */

#include "detectionZivyWidget.h"
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <limits>
#include "../../lib/consoleout.h"

DetectionZivyWidget::DetectionZivyWidget(TreatmentBoxXicDetect * treatmentBox,
		QWidget * parent) :
		MasschroQWidget(parent), _p_treatmentBox(treatmentBox), SMOOTH_HALF_WINDOW(
				1), MINMAX_HALF_WINDOW(3), MAXMIN_HALF_WINDOW(2), THRESHOLD_ON_MAX(
				2000), THRESHOLD_ON_MIN(1000) {
	// initialize detection
	initializeDetectionMethod();

	QVBoxLayout * mainLayout = new QVBoxLayout;
	setWidgetParameters();
	mainLayout->addWidget(_parameter_group);
	//mainLayout->addWidget(_button_box);
	setLayout(mainLayout);
	setWindowTitle(tr("Zivy peak detection"));

	//Trigger Detect
	detectPeaks();
}

DetectionZivyWidget::~DetectionZivyWidget() {
	mcqout() << "delete detection widget" << endl;
	if (_detection_method != 0) {
		delete (_detection_method);
		_detection_method = 0;
	}
}

void DetectionZivyWidget::initializeDetectionMethod() {
	_detection_method = new PeakDetectionZivy();
	_smoothing_filter_half_window = SMOOTH_HALF_WINDOW;
	_detection_method->set_mean_filter_half_edge(_smoothing_filter_half_window);
	_minmax_filter_half_window = MINMAX_HALF_WINDOW;
	_detection_method->set_minmax_half_edge(_minmax_filter_half_window);
	_maxmin_filter_half_window = MAXMIN_HALF_WINDOW;
	_detection_method->set_maxmin_half_edge(_maxmin_filter_half_window);
	_threshold_on_max = THRESHOLD_ON_MAX;
	_detection_method->set_detection_threshold_on_max(_threshold_on_max);
	_threshold_on_min = THRESHOLD_ON_MIN;
	_detection_method->set_detection_threshold_on_min(_threshold_on_min);
}

void DetectionZivyWidget::setWidgetParameters() {
	_parameter_group = new QGroupBox("Peak detection");
	QFormLayout * layout = new QFormLayout;

	QSpinBox * smooth_box = new QSpinBox;
	smooth_box->setMaximum(20);
	smooth_box->setWrapping(true);
	smooth_box->setSingleStep(1);
	smooth_box->setValue(SMOOTH_HALF_WINDOW);
	layout->addRow(tr("Smoothing filter half window"), smooth_box);

	QSpinBox * minmax_box = new QSpinBox;
	minmax_box->setMaximum(20);
	minmax_box->setWrapping(true);
	minmax_box->setSingleStep(1);
	minmax_box->setValue(MINMAX_HALF_WINDOW);
	layout->addRow(tr("MinMax close filter half window"), minmax_box);

	QSpinBox * maxmin_box = new QSpinBox;
	maxmin_box->setMaximum(20);
	maxmin_box->setWrapping(true);
	maxmin_box->setSingleStep(1);
	maxmin_box->setValue(MAXMIN_HALF_WINDOW);
	layout->addRow(tr("MaxMin open filter half window"), maxmin_box);

	QDoubleSpinBox * thresholdmax_box = new QDoubleSpinBox;
	thresholdmax_box->setMaximum(1E9);
	thresholdmax_box->setDecimals(0);
	thresholdmax_box->setWrapping(true);
	thresholdmax_box->setSingleStep(1000);
	thresholdmax_box->setValue(THRESHOLD_ON_MAX);
	layout->addRow(tr("Threshold on Max"), thresholdmax_box);

	QDoubleSpinBox * thresholdmin_box = new QDoubleSpinBox;
	thresholdmin_box->setMaximum(1E9);
	thresholdmin_box->setDecimals(0);
	thresholdmin_box->setWrapping(true);
	thresholdmin_box->setSingleStep(1000);
	thresholdmin_box->setValue(THRESHOLD_ON_MIN);
	layout->addRow(tr("Threshold on Min"), thresholdmin_box);

	_parameter_group->setLayout(layout);

	connect(smooth_box, SIGNAL(valueChanged(int)), this,
			SLOT(setSmoothingFilterHalfWindow(int)));

	connect(minmax_box, SIGNAL(valueChanged(int)), this,
			SLOT(setMinMaxFilterHalfWindow(int)));

	connect(maxmin_box, SIGNAL(valueChanged(int)), this,
			SLOT(setMaxMinFilterHalfWindow(int)));

	connect(thresholdmax_box, SIGNAL(valueChanged(double)), this,
			SLOT(setThresholdOnMax(double)));

	connect(thresholdmin_box, SIGNAL(valueChanged(double)), this,
			SLOT(setThresholdOnMin(double)));

}

void DetectionZivyWidget::setSmoothingFilterHalfWindow(int i) {
	_smoothing_filter_half_window = i;
	_detection_method->set_mean_filter_half_edge(_smoothing_filter_half_window);
	detectPeaks();
}

void DetectionZivyWidget::setMinMaxFilterHalfWindow(int i) {
	_minmax_filter_half_window = i;
	_detection_method->set_minmax_half_edge(_minmax_filter_half_window);
	detectPeaks();
}

void DetectionZivyWidget::setMaxMinFilterHalfWindow(int i) {
	_maxmin_filter_half_window = i;
	_detection_method->set_maxmin_half_edge(_maxmin_filter_half_window);
	detectPeaks();
}

void DetectionZivyWidget::setThresholdOnMin(double d) {
	_threshold_on_min = d;
	_detection_method->set_detection_threshold_on_min(_threshold_on_min);
	detectPeaks();
}

void DetectionZivyWidget::setThresholdOnMax(double d) {
	_threshold_on_max = d;
	_detection_method->set_detection_threshold_on_max(_threshold_on_max);
	detectPeaks();
}

void DetectionZivyWidget::detectPeaks() {
	_p_treatmentBox->setPeakDetectionBase(_detection_method);
}

void DetectionZivyWidget::writeElement(
		MasschroqDomDocument * domDocument) const {
	domDocument->addDetectionZivyMethod(*_detection_method);
}
