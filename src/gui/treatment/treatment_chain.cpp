/*
 * treatment_chain.cpp
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#include "treatment_chain.h"
#include "treatment_box.h"

TreatmentChain::TreatmentChain() :
		_chain() {

}

TreatmentChain::~TreatmentChain() {
}

void TreatmentChain::updatedTreamentChain() {
	std::list<TreatmentBox*>::iterator it = _chain.begin();
	TreatmentBox * prev = 0;
	TreatmentBox * next = 0;
	for (; it != _chain.end(); it++) {
		if (it != _chain.begin()) {
			prev->setNext(*it);
		}
		(*it)->setPrevious(prev);
		prev = *it;
	}
	prev->setNext(next);
}

void TreatmentChain::removedTreamentBox(TreatmentBox* toRemoved) {
	qDebug() << "removed treatmentBox" << endl;
	TreatmentBox* next = toRemoved->getNext();
	if (toRemoved != 0) {
		_chain.remove(toRemoved);
		delete (toRemoved);
		toRemoved = 0;
	}
	this->updatedTreamentChain();
	next->updatedAndEmitChanged();
}

void TreatmentChain::removedAll() {
  qDebug()  << "removed all treatmentBox" << endl;
	std::list<TreatmentBox*>::iterator it = _chain.begin();
	for (; it != _chain.end(); it++) {
		delete (*it);
		(*it) = 0;
	}
	_chain.clear();
}

TreatmentBoxXicExtract * TreatmentChain::addNewTreatmentBoxXicExtract() {
	TreatmentBoxXicExtract * extract = new TreatmentBoxXicExtract(*this);
	_chain.push_back(extract);
	qDebug()  << "Add TreatmentBox Xic Extract to chain" << endl;
	this->updatedTreamentChain();
	return (extract);
}

TreatmentBoxXicFilter * TreatmentChain::addNewTreatmentBoxXicFilter() {
	TreatmentBoxXicFilter * filter = new TreatmentBoxXicFilter(*this);
	_chain.push_back(filter);
	qDebug()  << "Add TreatmentBox Xic filter to chain" << endl;
	this->updatedTreamentChain();
	return (filter);
}

TreatmentBoxXicDetect * TreatmentChain::addNewTreatmentBoxXicDetect() {
	TreatmentBoxXicDetect * detect = new TreatmentBoxXicDetect(*this);
	_chain.push_back(detect);
	qDebug()  << "Add TreatmentBox Xic Detect to chain" << endl;
	this->updatedTreamentChain();
	return (detect);
}
