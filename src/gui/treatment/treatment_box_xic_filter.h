/*
 * treatment_box_xic_filter.h
 *
 *  Created on: 21 mai 2012
 *      Author: valot
 */

#ifndef TREATMENT_BOX_XIC_FILTER_H_
#define TREATMENT_BOX_XIC_FILTER_H_

#include "treatment_box.h"

class TreatmentBoxXicFilter: public TreatmentBox {

	Q_OBJECT

	friend class TreatmentChain;

public:

	void setFilter(FilterBase * filter);

signals :
	void changedXic(const TreatmentBoxXicFilter *);

protected:
	TreatmentBoxXicFilter(const TreatmentChain &chain);
	virtual ~TreatmentBoxXicFilter();

private:
	void doTreatment();

	FilterBase * _p_filter;
};

#endif /* TREATMENT_BOX_XIC_FILTER_H_ */
