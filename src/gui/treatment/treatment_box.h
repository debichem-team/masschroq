/*
 * treatment_box_base.h
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#ifndef TREATMENT_BOX_H_
#define TREATMENT_BOX_H_

#include <qobject.h>
#include "../../lib/xic/xic_base.h"

class TreatmentChain;

class TreatmentBox: public QObject {

	Q_OBJECT

	friend class TreatmentChain;

public:
	const xicBase* getCurrentXic() const {
		return _currentXic;
	}

signals :
	void onDelete(TreatmentBox *);
//	void createdXic(const TreatmentBox *) const;
//	void detectedPeaks(const vector<xicPeak *> *) const;

protected:

	TreatmentBox(const TreatmentChain &chain);
	virtual ~TreatmentBox();

	void setNext(TreatmentBox* next) {
		_next = next;
	}

	void setPrevious(TreatmentBox* previous) {
		_previous = previous;
	}


	TreatmentBox* getNext() const {
		return _next;
	}

	TreatmentBox* getPrevious() const {
		return _previous;
	}

	void updatedAndEmitChanged();

	xicBase * _currentXic;

private:

	virtual void doTreatment()=0;

	TreatmentBox* _previous;
	TreatmentBox* _next;
	const TreatmentChain& _chain;

};

#endif /* TREATMENT_BOX_H_ */
