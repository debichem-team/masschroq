/*
 * treatment_box_xic_extract.h
 *
 *  Created on: 16 mai 2012
 *      Author: valot
 */

#ifndef TREATMENT_BOX_XIC_EXTRACT_H_
#define TREATMENT_BOX_XIC_EXTRACT_H_

#include "treatment_box.h"

class TreatmentBoxXicExtract : public TreatmentBox {

	Q_OBJECT

	friend class TreatmentChain;

public:
	void setQuantiItem(QuantiItemBase * quantiItem);

	void setXicType(mcq_xic_type xicType) {
		xic_type = xicType;
	}

signals :
	void createdXic(const TreatmentBoxXicExtract *);

protected:
	TreatmentBoxXicExtract(const TreatmentChain &chain);
	virtual ~TreatmentBoxXicExtract();

private:
	void doTreatment();

	QuantiItemBase * _quantiItem;
	mcq_xic_type xic_type;
};

#endif /* TREATMENT_BOX_XIC_EXTRACT_H_ */
