/*
 * treatment_box_xic_detection.cpp
 *
 *  Created on: 19 July 2012
 *      Author: valot
 */

#include "treatment_box_xic_detect.h"
#include "../../lib/xic/xic_factory.h"
#include "../../lib/peak/peak_extractor.h"

TreatmentBoxXicDetect::TreatmentBoxXicDetect(const TreatmentChain &chain) :
		TreatmentBox(chain) {
	_p_detect = 0;
	_all_peaks_list=0;
}

TreatmentBoxXicDetect::~TreatmentBoxXicDetect() {
}

void TreatmentBoxXicDetect::setPeakDetectionBase(PeakDetectionBase * detect) {
	if (detect != 0) {
		_p_detect = detect;
		this->updatedAndEmitChanged();
	}
}

void TreatmentBoxXicDetect::doTreatment() {
	qDebug() << "Do Detect treatment";
	if ((this->getPrevious() != 0)
			&& (this->getPrevious()->getCurrentXic() != 0)) {
		XicFactory factory;
		_currentXic = factory.newXic(*(this->getPrevious()->getCurrentXic()));
		if (_p_detect != 0) {
			const PeakExtractor * peak_extractor = new PeakExtractor(_p_detect);
			_all_peaks_list =
					peak_extractor->newAllPeaksList(
							_currentXic->getQuantiItem(),
							_currentXic->getMsRun(),
							_currentXic->getConstRetentionTimes(),
							_currentXic->getConstIntensities());
			//ToDo test impression peaks
//			vector<xicPeak *>::const_iterator itp;
//			for (itp = _all_peaks_list->begin(); itp != _all_peaks_list->end(); ++itp) {
//				qDebug()<< (*itp)->get_max_intensity() <<" "<< (*itp)->get_aligned_max_rt();
//			}

			emit detectedPeaks(this);
		}
	}
}
