/*
 * treatment_box_xic_detect.h
 *
 *  Created on: 19 July 2012
 *      Author: valot
 */

#ifndef TREATMENT_BOX_XIC_DETECT_H_
#define TREATMENT_BOX_XIC_DETECT_H_

#include "treatment_box.h"

class TreatmentBoxXicDetect: public TreatmentBox {

	Q_OBJECT

	friend class TreatmentChain;

public:

	void setPeakDetectionBase (PeakDetectionBase * detect);
	const std::vector<xicPeak *> * getAllXicPeak() const{
		return(_all_peaks_list);
	}

signals :
	void detectedPeaks(const TreatmentBoxXicDetect *);

protected:
	TreatmentBoxXicDetect(const TreatmentChain &chain);
	virtual ~TreatmentBoxXicDetect();

private:
	void doTreatment();

	PeakDetectionBase * _p_detect;

	std::vector<xicPeak *> * _all_peaks_list;
};

#endif /* TREATMENT_BOX_XIC_DETECT_H_ */
