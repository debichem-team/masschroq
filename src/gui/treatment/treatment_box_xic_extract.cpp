/*
 * treatment_box_xic_extract.cpp
 *
 *  Created on: 16 mai 2012
 *      Author: valot
 */

#include "treatment_box_xic_extract.h"
#include "../engine/masschroq_gui_engin.h"

TreatmentBoxXicExtract::TreatmentBoxXicExtract(const TreatmentChain &chain) :
		TreatmentBox(chain), xic_type(MAX_XIC_TYPE) {
	_quantiItem = 0;
}

TreatmentBoxXicExtract::~TreatmentBoxXicExtract() {
	if (_quantiItem != 0) {
		delete (_quantiItem);
	}
}

void TreatmentBoxXicExtract::setQuantiItem(QuantiItemBase * quantiItem) {
	if (quantiItem != 0) {
		if (_quantiItem != 0) {
			delete (_quantiItem);
		}
		_quantiItem = quantiItem;
		this->updatedAndEmitChanged();
	}
}

void TreatmentBoxXicExtract::doTreatment() {
	qDebug() << "Do extract treatment";
	if (_quantiItem != 0) {
		// extract XIC
		Msrun * ms_run =
				MasschroqGuiEngin::getInstance()->getCurrentLoadedMsrun();
		if (ms_run != 0) {
			_currentXic = ms_run->extractXic(xic_type, _quantiItem);
			if (_currentXic != 0) {
				//_currentXic->toStream(std::cout);
				emit createdXic(this);
			}
		}
	}
}
