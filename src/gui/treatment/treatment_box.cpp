/*
 * treatment_box_base.cpp
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#include "treatment_box.h"

TreatmentBox::TreatmentBox(const TreatmentChain &chain) :
		_chain(chain){
	_next = 0;
	_previous = 0;
	_currentXic=0;
}

TreatmentBox::~TreatmentBox() {
  qDebug() <<"Delete treatment box"<<endl;
	emit onDelete(this);
}

void TreatmentBox::updatedAndEmitChanged() {
	this->doTreatment();
	if (_next != 0)
		_next->updatedAndEmitChanged();
}
