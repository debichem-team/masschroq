/*
 * treatment_box_xic_filter.cpp
 *
 *  Created on: 21 mai 2012
 *      Author: valot
 */

#include "treatment_box_xic_filter.h"
#include "../../lib/xic/xic_factory.h"

TreatmentBoxXicFilter::TreatmentBoxXicFilter(const TreatmentChain &chain) :
		TreatmentBox(chain) {
	_p_filter = 0;
}

TreatmentBoxXicFilter::~TreatmentBoxXicFilter() {
}

void TreatmentBoxXicFilter::setFilter(FilterBase * filter) {
	if (filter != 0) {
		_p_filter = filter;
		this->updatedAndEmitChanged();
	}
}

void TreatmentBoxXicFilter::doTreatment() {
	qDebug() << "Do filter treatment";
	if ((this->getPrevious() != 0) && (this->getPrevious()->getCurrentXic()!=0)) {
		XicFactory factory;
		_currentXic = factory.newXic(*(this->getPrevious()->getCurrentXic()));
		if (_p_filter != 0) {
			_currentXic->applyFilter(*_p_filter);
			//_currentXic->toStream(cout);
			emit changedXic(this);
		}
	}
}
