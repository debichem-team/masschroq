/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope thatx it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file mainwindow.h
 * \date November 10, 2011
 * \author Edlira Nano
 */

#ifndef PARAMETERMAINWINDOW_H_
#define PARAMETERMAINWINDOW_H_ 1

#include <QMainWindow>
#include <QVBoxLayout>
#include <QTextEdit>

#include "logQTextEdit.h"
#include "quantificationwidget/plot.h"
#include "alignementwidget/plot_aligned.h"


class ParameterMainWindow : public QMainWindow
{ 
  Q_OBJECT
    
public:
  
  ParameterMainWindow(QWidget * parent = 0);
   
private slots:
  void LoadQuantificationWorkspace();
  void LoadAlignmentWorkspace();
  void extractXicWidget();
  void masschroqmlXicWidget();
  void filterBackgroundWidget();
  void filterSpikeWidget();
  void detectionZivyWidget();
  void obiwarpAlignmentWidget();
  void ms2AlignmentWidget();
  void about();
  void exportXmlQuantificationMethod();
  void exportXmlALignmentMethod();
  
protected:
  
  void closeEvent(QCloseEvent *event);
  

private:
  void ResetWorkspace();
  void LoadFileMenus();
  void resetDockTools();
  void addLogWidget();
  
  bool maybeSave();
  
  Plot * _plot_area;
  PlotAligned * _plot_area_aligned;
  
  QDockWidget * dock;
  QWidget * dockwidget;
  QVBoxLayout *vbox;

  LogQTextEdit * _logQTextEdit;
  QDockWidget * _dockLog;

};

#endif /* PARAMETERMAINWINDOW_H_ */
