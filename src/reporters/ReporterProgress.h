/*
 *
 *  File ReporterProgress.h in 
 *  MassChroQ: Mass Chromatogram Quantification software. 
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
  * \file ReporterProgress.h
  * \date February 10, 2012
  * \author Edlira Nano
*/

#ifndef REPORTER_PROGRESS_H
#define REPORTER_PROGRESS_H

#include "ReporterBase.h"

/**
   \class ReporterProgress
   \brief Reporter object that writes progress messages to stdout
 */

class ReporterProgress : public ReporterBase {
  
 public :

	ReporterProgress();
	virtual ~ReporterProgress();
	virtual void write(const QString & message);
	
};
#endif /* REPORTER_PROGRESS_H */
