/*
 *
 *  File ReporterLog.h in 
 *  MassChroQ: Mass Chromatogram Quantification software. 
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
  * \file ReporterLog.h
  * \date 
  * \author Edlira Nano
*/

#ifndef REPORTERLOG_H
#define REPORTERLOG_H

/**
 * \file ReporterConsole.h
 * \date January 24, 2011
 * \author Edlira Nano
 */

#include "ReporterBase.h"

/**
   \class ReporterLog
   \brief Reporter object that writes log messages to a file.
 */

class ReporterLog : public ReporterBase {
	
public : 
	
	ReporterLog();
	virtual ~ReporterLog();
	virtual void write(const QString & message);

private :
	QFile * _log_file;
};


#endif /* REPORTERLOG_H */
