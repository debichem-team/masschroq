/**
  * \file ReporterProgress.cpp
  * \date February 10, 2012
  * \author Edlira Nano
*/

#include "ReporterProgress.h"

ReporterProgress::ReporterProgress() {
  _out_stream = new QTextStream(stdout, QIODevice::WriteOnly);
}

ReporterProgress::~ReporterProgress() {
  delete (_out_stream);
}

void
ReporterProgress::write(const QString & message) {
  std::cout << message.toStdString() << std::endl;
}
