/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifndef REPORTER_BASE_H
#define REPORTER_BASE_H

/**
 * \file ReporterBase.h
 * \date January 24, 2011
 * \author Edlira Nano
 */

#include <QTextStream>

/**
   \class ReporterBase
   \brief Base abstract class that represents a logging object in MassChroQ 
 */

class ReporterBase {
  
 public : 
  
  virtual ~ReporterBase() {};
  virtual void write(const QString & message) = 0;
  
  template <typename T>
    ReporterBase& operator<<(const T& message){
    (*_out_stream) << message << endl;
    return *this;
  }
 protected :
  
  QTextStream * _out_stream;

};

#endif /* REPORTER_BASE_H */
