/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifndef REPORTER_CONSOLE_H
#define REPORTER_CONSOLE_H

/**
 * \file ReporterConsole.h
 * \date January 24, 2011
 * \author Edlira Nano
 */

#include "ReporterBase.h"

/**
   \class ReporterConsole
   \brief Reporter object that writes log messages to console.
 */

class ReporterConsole : public ReporterBase {
	
public : 
	
	ReporterConsole();
	virtual ~ReporterConsole();
	virtual void write(const QString & message);
	
};

#endif /* REPORTER_CONSOLE_H */
