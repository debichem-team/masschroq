/**
 * \file ReporterConsole.cpp
 * \date January 24, 2011
 * \author Edlira Nano
 */

#include "ReporterConsole.h"
#include <iostream>

ReporterConsole::ReporterConsole() {
  _out_stream = new QTextStream(stdout, QIODevice::WriteOnly);
}

ReporterConsole::~ReporterConsole() {
  delete (_out_stream);
}

void
ReporterConsole::write(const QString & message) {
  std::cout << message.toStdString() << std::endl;
}
