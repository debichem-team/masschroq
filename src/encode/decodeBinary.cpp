/**
 * \file decodeBinary.cpp
 * \date November 22, 2010
 * \author Edlira Nano
 */

#include "decodeBinary.h"
//#include <iostream>
#include <QString>
#include <QDebug>

static const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz"
		"0123456789+/";

static inline bool is_base64(unsigned char c) {
	return (isalnum(c) || (c == '+') || (c == '/'));
}

/// sets a float's value in little endian
void setLittleEndianFloat(unsigned char * toFloat,
		const unsigned char * bytes) {
	toFloat[0] = bytes[0];
	toFloat[1] = bytes[1];
	toFloat[2] = bytes[2];
	toFloat[3] = bytes[3];
}

/// sets a double's value in little endian
void setLittleEndianDouble(unsigned char * toDouble,
		const unsigned char * bytes) {
	toDouble[0] = bytes[0];
	toDouble[1] = bytes[1];
	toDouble[2] = bytes[2];
	toDouble[3] = bytes[3];
	toDouble[4] = bytes[4];
	toDouble[5] = bytes[5];
	toDouble[6] = bytes[6];
	toDouble[7] = bytes[7];
}

/// sets a float's value in big endian
void setBigEndianFloat(unsigned char * toFloat, const unsigned char * bytes) {
	toFloat[0] = bytes[3];
	toFloat[1] = bytes[2];
	toFloat[2] = bytes[1];
	toFloat[3] = bytes[0];
}

/// sets a double's value in big endian
void setBigEndianDouble(unsigned char * toDouble, const unsigned char * bytes)

{
	toDouble[0] = bytes[7];
	toDouble[1] = bytes[6];
	toDouble[2] = bytes[5];
	toDouble[3] = bytes[4];
	toDouble[4] = bytes[3];
	toDouble[5] = bytes[2];
	toDouble[6] = bytes[1];
	toDouble[7] = bytes[0];
}

DecodeBinary::DecodeBinary() {
}

DecodeBinary::~DecodeBinary() {
}

/// decode a QByteArray's content in base 64
const QByteArray DecodeBinary::base64_decode(const QByteArray & encoded_data) {
	return QByteArray::fromBase64(encoded_data);
}

/// encode a QByteArray's content in base 64
const QByteArray DecodeBinary::base64_encode(const QByteArray & data) {
	return data.toBase64();
}

/** qUncompress does not work for 2 reasons :
 - qUncompress needs the size of the decompressed data to be entered in
 big endian in the first four bytes of the data. Neither mzXml, nor mzMl
 give this information.
 - tested with a known decompressed data size, it gives a data corrupted
 error (testing data came from msconvert which uses the boost zlib library
 for compression.
 **/
// const QByteArray
// DecodeBinary::zlib_uncompress(const QByteArray &encoded_data, 
// 			      const unsigned int length) {
/* QT Note: If you want to use this function to uncompress external data
 compressed using zlib, you first need to prepend four bytes to the
 byte array that contain the expected length (as an unsigned integer)
 of the uncompressed data encoded in big-endian order
 (most significant byte first). */

//qDebug() << "data : " << encoded_data;
//   QByteArray dataPlusSize;
//   dataPlusSize.push_back((char)((length >> 24) & 0xFF));
//   dataPlusSize.push_back((char)((length >> 16) & 0xFF));
//   dataPlusSize.push_back((char)((length >> 8) & 0xFF));
//   dataPlusSize.push_back((char)((length >> 0) & 0xFF));
//   dataPlusSize.push_back(encoded_data);
//   //qDebug() << "data plus size : "<< dataPlusSize;
//   QByteArray uncompressed = qUncompress(dataPlusSize);
//   //qDebug() << uncompressed;
//   return uncompressed;
// }
std::vector<mcq_double> DecodeBinary::getDoubleFromBinary32(
		const QByteArray &decoded_data, const bool bigendian) {

	const unsigned char * decoded_char =
			(const unsigned char *) decoded_data.constData();

	mcq_float value;
	std::vector<mcq_double> ret;
	ret.resize(decoded_data.size() / 4);

	unsigned char * pointer = (unsigned char *) decoded_char;
	for (unsigned int i = 0; i < ret.size(); ++i) {
		if (bigendian) {
			setBigEndianFloat((unsigned char *) &value, pointer);
		} else {
			setLittleEndianFloat((unsigned char *) &value, pointer);
		}
		ret[i] = static_cast<mcq_double>(value);
		pointer += 4;
	}

	return ret;

	/** other version, for little endian only**/
	// float * floatBuf =(float *) decoded_data.data();
	// for( int size = decoded_data.size() / (sizeof(float)); size; --size){
	//   float f = *floatBuf++;
	//   ret.push_back(f);
	// }
}

std::vector<mcq_double> DecodeBinary::getDoubleFromBinary64(
		const QByteArray &decoded_data, const bool bigendian) {

	const unsigned char * decoded_char =
			(const unsigned char *) decoded_data.constData();

	mcq_double value;
	std::vector<mcq_double> ret;
	ret.resize(decoded_data.size() / 8);

	unsigned char * pointer = (unsigned char *) decoded_char;
	for (unsigned int i = 0; i < ret.size(); ++i) {
		if (bigendian) {
			setBigEndianDouble((unsigned char *) &value, pointer);
		} else {
			setLittleEndianDouble((unsigned char *) &value, pointer);
		}
		ret[i] = value;
		pointer += 8;
	}
	return ret;

	/** other version, for little endian only **/
	// double * doubleBuf = (double *) decoded_data.data();
	// for( int size = decoded_data.size() / (sizeof(double)); size; --size){
	//   double d = *doubleBuf++;
	//   mcq_double f = static_cast<mcq_double>(d);
	//   ret.push_back(f);
	// }
}
const QByteArray *
DecodeBinary::putDoublesInBinary64(const std::vector<mcq_double> & doubles) {
	QByteArray * ret_array = new QByteArray();
	unsigned int size_of_double = sizeof(mcq_double);
	ret_array->reserve(size_of_double * (doubles.size()));

	mcq_double value;
	std::vector<mcq_double>::const_iterator it;
	for (it = doubles.begin(); it != doubles.end(); ++it) {
		value = *it;
		ret_array->append((const char*) &value, size_of_double);
	}
	return ret_array;
}

/// decode 
std::vector<mcq_double> DecodeBinary::base64_decode_binary(
		const QByteArray &data,
		//const int binary_length,
		//const bool compression,
		const int precision, const bool bigendian) {

	// if (compression) {
	//   if (binary_length == -1) {
	//     //throw mcqError(...
	//   }

	//   QByteArray uncompressed_data =
	//     binary_zlibUncompress(data, binary_length);
	// }

	QByteArray decoded_data = base64_decode(data);
	std::vector<mcq_double> result;

	if (precision == 32)
		result = getDoubleFromBinary32(decoded_data, bigendian);

	else if (precision == 64)
		result = getDoubleFromBinary64(decoded_data, bigendian);

	return result;
}

const QByteArray DecodeBinary::base64_encode_doubles(
		const std::vector<mcq_double> & doubles) {
	//const int binary_length,
	//const bool compression,
	//const int precision=64
	//const bool bigendian=false

	const QByteArray * tmp_array = putDoublesInBinary64(doubles);
	QByteArray ret = base64_encode(*tmp_array);
	delete tmp_array;
	return ret;
}

/** @brief specialized function : decode base64 array containing peaks (pair of values, mz/int or int/mz)
 *
 * the values could be compressed with zlib, we can check consistency between expected compressed
 * length and awaited compressed length if it is mentionned in the file of origin (mzXml mzml...)
 *
 *
 * @param encoded_data
 * @param precision
 * @param bigendian
 * @param expected_number_of_values
 * @param zlibEncoded
 * @param compressed_len the awaited length of data after base63 decode and before zlib decompression.
 * @return
 */
std::vector<mcq_double> DecodeBinary::base64_decode_peaks(
		const QByteArray & encoded_data, const int precision,
		const bool bigendian, const unsigned int expected_number_of_peaks,
		const bool zlibEncoded, const unsigned int compressed_len) {

	QByteArray decoded_data = base64_decode(encoded_data);

	if (zlibEncoded) {
		if ((compressed_len >0) && (compressed_len != (unsigned int) decoded_data.length())) {
			qDebug() << "compressed_len = " << compressed_len
					<< " decoded_data.length() = " << decoded_data.length();
		}
		unsigned int uncompress_length = (expected_number_of_peaks * 2
				* precision) / (8);
//		qDebug() << "expected_number_of_peaks = " << expected_number_of_peaks
//				<< "compressed_len = " << compressed_len
//				<< " uncompress_length = " << uncompress_length;
		//prepand expected uncompressed size, last 4 byte in dat 0x0e = 14
		QByteArray dataPlusSize;

		dataPlusSize.append((unsigned int) ((uncompress_length >> 24) & 0xFF));
		dataPlusSize.append((unsigned int) ((uncompress_length >> 16) & 0xFF));
		dataPlusSize.append((unsigned int) ((uncompress_length >> 8) & 0xFF));
		dataPlusSize.append((unsigned int) ((uncompress_length >> 0) & 0xFF));
		dataPlusSize.append(decoded_data);

		decoded_data = qUncompress(dataPlusSize);
//		qDebug() << "decoded_data OK  decoded_data.length() "
//				<< decoded_data.length();
	}

	std::vector<mcq_double> result;

	if (precision == 32)
		result = getDoubleFromBinary32(decoded_data, bigendian);

	else if (precision == 64)
		result = getDoubleFromBinary64(decoded_data, bigendian);

//	qDebug() << "expected_number_of_peaks = " << expected_number_of_peaks
//			<< " result.size() = " << result.size();

	return result;
}

std::vector<mcq_double> DecodeBinary::base64_decode_peaks_without_compressed_length_check(
		const QByteArray & encoded_data, const int precision,
		const bool bigendian, const unsigned int expected_number_of_peaks,
		const bool zlibEncoded) {

	// return base64_decode_peaks(encoded_data, precision,
	// 		bigendian, expected_number_of_peaks,
	// 		zlibEncoded,0);

QByteArray decoded_data = base64_decode(encoded_data);

	if (zlibEncoded) {

		unsigned int uncompress_length = (expected_number_of_peaks
				* precision) / (8);
//		qDebug() << "expected_number_of_peaks = " << expected_number_of_peaks
//			 << " uncompress_length = " << uncompress_length;
		//prepand expected uncompressed size, last 4 byte in dat 0x0e = 14
		QByteArray dataPlusSize;

		dataPlusSize.append((unsigned int) ((uncompress_length >> 24) & 0xFF));
		dataPlusSize.append((unsigned int) ((uncompress_length >> 16) & 0xFF));
		dataPlusSize.append((unsigned int) ((uncompress_length >> 8) & 0xFF));
		dataPlusSize.append((unsigned int) ((uncompress_length >> 0) & 0xFF));
		dataPlusSize.append(decoded_data);

		decoded_data = qUncompress(dataPlusSize);
//		qDebug() << "decoded_data OK  decoded_data.length() "
//				<< decoded_data.length();
	}

	std::vector<mcq_double> result;

	if (precision == 32)
		result = getDoubleFromBinary32(decoded_data, bigendian);

	else if (precision == 64)
		result = getDoubleFromBinary64(decoded_data, bigendian);

//	qDebug() << "expected_number_of_peaks = " << expected_number_of_peaks
//			<< " result.size() = " << result.size();

	return result;

}
