/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file decodeBinary.h
 * \date November 22, 2010
 * \author Edlira Nano
 */

#ifndef DECODE_BINARY_H_
#define DECODE_BINARY_H_ 1

#include <string>
#include <vector>
#include <QByteArray>
#include "../config.h"

/// sets a float's value in little endian
void setLittleEndianFloat(unsigned char * toFloat, const unsigned char *bytes);

/// sets a double's value in little endian
void setLittleEndianDouble(unsigned char * toDouble,
		const unsigned char *bytes);

/// sets a float's value in big endian
void setBigEndianFloat(unsigned char * toFloat, const unsigned char *bytes);

/// sets a double's value in big endian
void setBigEndianDouble(unsigned char * toDouble, const unsigned char *bytes);

class DecodeBinary {
public:

	DecodeBinary();
	~DecodeBinary();

	/// decode a QByteArray's content in base 64
	static const QByteArray base64_decode(const QByteArray & encoded_data);

	/// encode a QByteArray's content in base 64
	static const QByteArray base64_encode(const QByteArray & data);

	/// get the doubles (in a vector) coded in a QByteArray in
	/// big or little endian in precision 32
	static std::vector<mcq_double>
	getDoubleFromBinary32(const QByteArray & decoded_data,
			const bool bigendian);

	/// get the doubles (in a vector) coded in a QByteArray in
	/// big or little endian in precision 32
	static std::vector<mcq_double>
	getDoubleFromBinary64(const QByteArray & decoded_data,
			const bool bigendian);

	/// put a vector of doubles in a QByteArray in little endian (precision 64)
	static const QByteArray *
	putDoublesInBinary64(const std::vector<mcq_double> & doubles);

	/// decode base64 encoded_data (wih precision and big or littleendian)
	/// into a vector of doubles
	static std::vector<mcq_double>
	base64_decode_binary(const QByteArray & encoded_data, const int precision,
			const bool bigendian);

	/// encode the doubles in base64 with a precision of 64 bytes into the
	/// data QByteArray
	static const QByteArray
	base64_encode_doubles(const std::vector<mcq_double> & doubles);

	/// decode base64 encoded_peaks (wih precision and big or littleendian)
	/// into a vector of doubles
	static std::vector<mcq_double>
	base64_decode_peaks(const QByteArray & encoded_data, const int precision,
			const bool bigendian, const unsigned int expected_number_of_peaks,
			const bool zlibEncoded, const unsigned int compressed_len);

	static std::vector<mcq_double>
	base64_decode_peaks_without_compressed_length_check(const QByteArray & encoded_data, const int precision,
			const bool bigendian, const unsigned int expected_number_of_peaks,
			const bool zlibEncoded);


};

#endif /* DECODE_BINARY_H_ */
