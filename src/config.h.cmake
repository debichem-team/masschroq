#ifndef _CONFIG_H
#define _CONFIG_H

#cmakedefine MASSCHROQ_VERSION "@MASSCHROQ_VERSION@"
#cmakedefine MASSCHROQ_XSD "@MASSCHROQ_XSD@"
#cmakedefine MASSCHROQ_SCHEMA_VERSION "@MASSCHROQ_SCHEMA_VERSION@"
#cmakedefine MASSCHROQ_SCHEMA_FILE "@MASSCHROQ_SCHEMA_FILE@"
#cmakedefine MASSCHROQ_ICON "@MASSCHROQ_ICON@"
#define QT_V_4_5  0x040500
#define QT_V_4_6  0x040600


#include <QDebug>
#include "mcq_types.h"




#endif /* _CONFIG_H */
