/**
 * \file xic_base.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include <algorithm>


#include "xic_base.h"
#include "../filters/filter_background.h"

xicBase::xicBase(const Msrun * p_ms_run,
				 QuantiItemBase * quanti_item)
	:
	_p_quanti_item(quanti_item),
	_p_ms_run(p_ms_run)
{
	_mz_start = _p_quanti_item->get_min_mz();
	_mz_stop = _p_quanti_item->get_max_mz();

	_p_v_rt = new std::vector<mcq_double>;
	_p_v_intensity = new std::vector<mcq_double>;
	_p_monitor = 0;
}

xicBase::xicBase(const xicBase & xic_base):
			_p_quanti_item(xic_base._p_quanti_item),
			_p_ms_run(xic_base._p_ms_run)
{
	_mz_start = xic_base._mz_start;
	_mz_stop =  xic_base._mz_stop;
	_p_v_rt = new std::vector<mcq_double>(*xic_base._p_v_rt);
	_p_v_intensity = new std::vector<mcq_double>(*xic_base._p_v_intensity);
	_p_monitor = xic_base._p_monitor;
}

xicBase::~xicBase() {
	delete _p_v_rt;
	delete _p_v_intensity;
	_p_v_rt = 0;
	_p_v_intensity = 0;
}

const Msrun *
xicBase::getMsRun() const {
	return (_p_ms_run);
}

QuantiItemBase *
xicBase::getQuantiItem() const {
	return (_p_quanti_item);
}

const std::vector<mcq_double> *
xicBase::getConstIntensities() const {
	return (_p_v_intensity);
}

std::vector<mcq_double> *
xicBase::getIntensities() const {
	return (_p_v_intensity);
}

const std::vector<mcq_double> *
xicBase::getConstRetentionTimes() const {
	return (_p_v_rt);
}

std::vector<mcq_double> *
xicBase::getRetentionTimes() const {
	return (_p_v_rt);
}

std::vector<mcq_double>
xicBase::getAlignedRetentionTimes() const {
	std::vector<mcq_double> p_v_aligned_rt;
	std::vector<mcq_double>::const_iterator it;
	for (it = _p_v_rt->begin(); it != _p_v_rt->end(); ++it) {
		p_v_aligned_rt.push_back(_p_ms_run->getAlignedRtByOriginalRt(*it));
	}
	return (p_v_aligned_rt);
}

void
xicBase::applyFilter(const FilterBase & filter) {
	filter.treatSignal(_p_v_rt, &_p_v_intensity);
}

void
xicBase::applyFilters(const std::vector<const FilterBase *> v_filters) {
	std::vector<const FilterBase *>::const_iterator it;
	unsigned int i;
	for (it = v_filters.begin(), i = 1;
		 it != v_filters.end();
		 ++it, i++) {
		//qDebug() << "applying xic filter number " << i;
		//qDebug() << (*it)->printInfos();
		this->applyFilter(**it);

		if(_p_monitor!=0){
			const mcq_filter_type filterType = (*it)->getFilterType();
			_p_monitor->setFilteredXic(this, filterType, i);
		}
	}
}

void
xicBase::toStream(QTextStream & to_stream) const {

	std::vector<mcq_double>::const_iterator it;
	for (it = _p_v_rt->begin(); it != _p_v_rt->end(); ++it) {
		to_stream << *it << " ";
	}
	to_stream << endl;

	for (it = _p_v_intensity->begin(); it != _p_v_intensity->end(); ++it) {
		to_stream << *it << " ";
	}
	to_stream << endl;
}


bool
xicBase::peak_contains_rt(const xicPeak & peak, mcq_double rt) const {
	return (peak.contains_rt(rt));
}

void
xicBase::applyFilterBackground(unsigned int median_window_length,
							   unsigned int min_max_window_length) {

	FilterBackground my_filter_background;
	my_filter_background.set_half_median_window_length(median_window_length
													   / 2);
	my_filter_background.set_half_min_max_window_length(min_max_window_length
														/ 2);
	my_filter_background.treatSignal(_p_v_rt, &_p_v_intensity);
}

void
xicBase::fill_data_array(double * xdata,
						 double * ydata,
						 unsigned int plotsize) const {

	std::vector<mcq_double>::const_iterator itrt;
	std::vector<mcq_double>::const_iterator itintensity;
	unsigned int i;
	/// _p_v_rt and _p_v_intensity have the same size
	for (itrt = _p_v_rt->begin(),
			 itintensity = _p_v_intensity->begin(),
			 i = 0;
		 itrt != _p_v_rt->end() ||
			 itintensity != _p_v_intensity->end();
		 ++itrt, ++itintensity, ++i) {
		xdata[i] = *itrt;
		ydata[i] = *itintensity;
	}
}

void
xicBase::setMonitor(MonitorBase & monitor) {
	_p_monitor = &monitor;
}
