/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xic_factory.h
 * \date 25 sept. 2009
 * \author Olivier Langella
 */

#ifndef XIC_FACTORY_H_
#define XIC_FACTORY_H_ 1

#include "xic_base.h"
#include "../quanti_items/quantiItemBase.h"

/**
 * \class XicFactory
 * \brief A class that fabricates a xic from the xic's type, the xic's msrun
 * and the xic's item to be quantified 
 */
class XicFactory {

 public:
  
  XicFactory();
  virtual ~XicFactory();

  xicBase * newXic(const QString & xic_type, 
		   Msrun * ms_run,
		   QuantiItemBase * quanti_item) const;

  xicBase * newXic(const xicBase & xicbase) const;

};

#endif /* XIC_FACTORY_H_ */
