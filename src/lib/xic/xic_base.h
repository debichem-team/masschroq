/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xic_base.h
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#ifndef XIC_BASE_H_
#define XIC_BASE_H_ 1

#include <iostream>
#include <vector>


#include "../msrun/msrun.h"
#include "../peak/xic_peak.h"
#include "../filters/filter_base.h"
#include "../detections/peak_detection_base.h"
#include "../monitors/monitorBase.h"


/**
 * \class xicBase
 * \brief Virtual base class representing a XIC 
 * (eXtracted Ion Chromatogram : retention times / intesity ) 

 * A Xic is associated to :
 * - the msrun it belongs to,
 * - the selection method used for its extraction
 * It contains : 
 * - the vector of its rt-s (the x-axis)
 * - the vector of its intesities (the y-axis)
 */

class xicBase {

 public:
  
  xicBase(const Msrun * p_ms_run, QuantiItemBase * quanti_item);
  xicBase(const xicBase & xic_base);

  virtual ~xicBase();
  
  const Msrun * getMsRun() const;
  
  QuantiItemBase * getQuantiItem() const;

  virtual const std::vector<mcq_double> * getConstIntensities() const;
  virtual const std::vector<mcq_double> * getConstRetentionTimes() const;
  
  virtual std::vector<mcq_double> * getIntensities() const;
  virtual std::vector<mcq_double> * getRetentionTimes() const;
  
  std::vector<mcq_double> getAlignedRetentionTimes() const;
    
  /// method that applies a filter to this Xic
  void applyFilter(const FilterBase & filter);

  /// method that applies a vector of filters to this Xic
  void applyFilters(const std::vector<const FilterBase *> v_filters);
  
  /// method that applies a background filter to this Xic
  void applyFilterBackground(unsigned int median_window_length,
			     unsigned int min_max_window_length);
  
  /// print the p_v_rt and p_v_intensity space-separated values
  /// into a stream
  void toStream(QTextStream & to_stream) const;

  /// size of the rt/intensity vectors (they have the same size)
  virtual unsigned int size() const {
    return _p_v_rt->size();
  }
  
  /// fills the xdata and the ydata arrays with the values of 
  /// p_v_rt and p_v_intensity
  virtual void fill_data_array(double * xdata, 
			       double * ydata, 
			       unsigned int plotsize) const;
  
  /// returns true if peak contains rt
  bool peak_contains_rt(const xicPeak & peak, mcq_double rt) const;
  
  void setMonitor(MonitorBase & monitor);
  
  virtual mcq_double getIntensityFromSpectrum(const spectrum & the_spectrum) const = 0;

  virtual const QString getType() const = 0;

 protected:

  /// the selection method for this Xic-s extraction  
  QuantiItemBase * _p_quanti_item;

  /// the MSrun this Xic belongs to 
  const Msrun * _p_ms_run;
  
  mcq_double _mz_start;
  mcq_double _mz_stop;
  
  /// Xic's retention times (x-axis)
  std::vector<mcq_double> * _p_v_rt;
  
  /// Xic's intensities (y-axis)
  std::vector<mcq_double> * _p_v_intensity;
  
  /// Xic's monitor
  MonitorBase * _p_monitor;
 
};

#endif /*XIC_BASE_H_*/
