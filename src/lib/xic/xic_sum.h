/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file xic_sum.h
 * \date June 11, 2011
 * \author Edlira Nano
 */

#ifndef XIC_SUM_H_
#define XIC_SUM_H_ 1

#include "xic_base.h"

/**
 * \class xicSum 
 * \brief Represents a Xic of type "sum", derives from xicBase
 */
class xicSum : public xicBase {

public:
  
	xicSum(const Msrun * ms_run, QuantiItemBase * quanti_item);
	xicSum(const xicSum & xic_sum):xicBase(xic_sum){}
	virtual ~xicSum();
	virtual const QString getType() const;

protected:
  
	virtual mcq_double getIntensityFromSpectrum(const spectrum & the_spectrum) const;
  
};

#endif /*XIC_SUM_H_*/
