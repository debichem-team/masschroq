/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xic.h
 * \date June 11, 2009
 * \author Edlira Nano
 */


#ifndef XIC_MAX_H_
#define XIC_MAX_H_ 1


#include "xic_base.h"

class xicMax : public xicBase {

 public:
 
  xicMax(const Msrun * ms_run, QuantiItemBase * quanti_item);
  xicMax(const xicMax & xic_max):xicBase(xic_max){}
  virtual ~xicMax();
  virtual const QString getType() const;
    
 protected:
  
  virtual mcq_double getIntensityFromSpectrum(const spectrum & the_spectrum) const;
  
};

#endif /*XIC_MAX_H_*/
