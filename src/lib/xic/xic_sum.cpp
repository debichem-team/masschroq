/**
 * \file xic_sum.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */


#include "xic_sum.h"
#include <limits>
#include <math.h>


xicSum::xicSum(const Msrun * ms_run, QuantiItemBase * quanti_item) 
	:
	xicBase(ms_run, quanti_item) 
{
}  
 
xicSum::~xicSum() {
}

mcq_double 
xicSum::getIntensityFromSpectrum(const spectrum & the_spectrum) const {
	return (the_spectrum.getSumIntensity(_mz_start, _mz_stop));
}

const QString 
xicSum::getType() const {
	QString xic_type("sum");
	return xic_type;
}
