/**
 * \file xic.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include "xic_max.h"
#include <limits>
#include <math.h>

xicMax::xicMax(const Msrun * ms_run, QuantiItemBase * quanti_item) 
	:
	xicBase(ms_run, quanti_item) 
{  
}

xicMax::~xicMax() {
}

mcq_double 
xicMax::getIntensityFromSpectrum(const spectrum & the_spectrum) const {
	return (the_spectrum.getMaxIntensity(_mz_start, _mz_stop));
}

const QString 
xicMax::getType() const {
	QString xic_type("max");
	return xic_type;
}
