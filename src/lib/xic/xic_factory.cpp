/**
 * \file xic_factory.cpp
 * \date 25 sept. 2009
 * \author Olivier Langella
 */

#include "xic_factory.h"
#include "xic_max.h"
#include "xic_sum.h"

XicFactory::XicFactory() {
}

XicFactory::~XicFactory() {
}

xicBase * XicFactory::newXic(const QString & xic_type, Msrun * ms_run,
							 QuantiItemBase * quanti_item) const {
	/// extract XIC
	xicBase * ptr_on_xic;
	if (xic_type == "max") {
		ptr_on_xic = new xicMax(ms_run, quanti_item);
	} else if (xic_type == "sum") {
		ptr_on_xic = new xicSum(ms_run, quanti_item);
	} else {
		throw mcqError(
				QObject::tr(
						"error in XicFactory::newXic :\n xic type can be 'sum' or 'max', but not %1").arg(
						xic_type));
	}

	if (ptr_on_xic->size() == 0) {
		delete ptr_on_xic;
		ptr_on_xic = NULL;
	}
	return (ptr_on_xic);
}

xicBase * XicFactory::newXic(const xicBase & xicbase) const {
	xicBase * ptr_on_xic = 0;
	if (xicbase.getType() == "max") {
		ptr_on_xic = new xicMax((const xicMax &)xicbase);
	} else if (xicbase.getType() == "sum") {
		ptr_on_xic = new xicSum((const xicSum &)xicbase);
	} else {
		throw mcqError(
				QObject::tr(
						"error in XicFactory::newXic :\n xic type can be 'sum' or 'max', but not %1").arg(
								xicbase.getType()));
	}
	return(ptr_on_xic);
}

