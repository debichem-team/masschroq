/**
 * \file quantificator.cpp
 * \date August 03, 2011
 * \author Edlira Nano
 */

#include "quantificator.h"
#include "./consoleout.h"
#include "quanti_items/quantiItemPeptide.h"
#include "quanti_items/quantiItemMzRt.h"
#include "quantifications/quantificationMethod.h"
#include "peak/peak_extractor.h"
#include "monitors/monitorBase.h"

#include <QStringList>
#include <algorithm>

Quantificator::Quantificator(const QString & xml_id,
		const msRunHashGroup * quanti_group,
		QuantificationMethod * quanti_method) :
		_xml_id(xml_id) {
	set_quanti_group(quanti_group);
	set_quanti_method(quanti_method);
	_quanti_items = new std::vector<QuantiItemBase *>;
	const mcq_double coeff_match_range =
			(_quanti_method->getDetectionMethod())->get_coeff_detect_range();
	_peak_matcher = new PeakMatcher(coeff_match_range);
}

Quantificator::~Quantificator() {
	std::vector<QuantiItemBase *>::iterator it;
	for (it = _quanti_items->begin(); it != _quanti_items->end(); ++it) {
		if (*it != 0) {
			delete (*it);
			*it = 0;
		}
	}

	if (_quanti_items != 0) {
		delete _quanti_items;
		_quanti_items = 0;
	}

	if (_peak_matcher != 0) {
		delete _peak_matcher;
		_peak_matcher = 0;
	}

}

void Quantificator::set_quanti_group(const msRunHashGroup * quanti_group) {
	_group_to_quantify = quanti_group;
}

void Quantificator::set_quanti_method(QuantificationMethod * quanti_method) {
	_quanti_method = quanti_method;
}

void Quantificator::set_matching_mode(const mcq_matching_mode & match_mode) {
	_peak_matcher->set_matching_mode(match_mode);
}

const QString &
Quantificator::get_quanti_group_id() const {
	return (_group_to_quantify->getXmlId());
}

void Quantificator::add_peptide_quanti_items(
		std::vector<const Peptide *> & isotope_peptides) {

	XicExtractionMethodBase * extraction_method =
			_quanti_method->getXicExtractionMethod();

	QuantiItemBase * quanti_item;
	std::vector<const Peptide *>::const_iterator itpep;
	/// for each peptide (or isotope in the vector), we will create a peptide quanti item
	for (itpep = isotope_peptides.begin(); itpep != isotope_peptides.end();
			++itpep) {

		/// get the set of charges z the peptide is observed in the current group,
		/// and construct a quantification item for each peptide/z

		std::set<unsigned int> * s_charges = (*itpep)->getCharges(
				*_group_to_quantify);

		// for each z do

		std::set<unsigned int>::const_iterator itz;
		for (itz = s_charges->begin(); itz != s_charges->end(); ++itz) {
			/// calculate the mean bestRt for this peptide/z   
			mcq_double rt((*itpep)->getMeanBestRt(*_group_to_quantify));
			/// this test is not necessary here
			if (rt != -1) {
				extraction_method->setMz((*itpep)->getMz(*itz));
				quanti_item = new QuantiItemPeptide(*extraction_method, *itpep,
						*itz, *_group_to_quantify,
						_peak_matcher->get_match_mode());
				_quanti_items->push_back(quanti_item);
			}
		}
		delete (s_charges);
	}
}

void Quantificator::add_mz_quanti_items(const QStringList & mz_list) {

	XicExtractionMethodBase * extraction_method =
			_quanti_method->getXicExtractionMethod();

	QuantiItemBase * quanti_item;

	QStringList::const_iterator it;

	for (it = mz_list.begin(); it != mz_list.end(); ++it) {
		extraction_method->setMz(it->toDouble());
		quanti_item = new QuantiItemBase(*extraction_method);
		_quanti_items->push_back(quanti_item);
	}
}

void Quantificator::add_mzrt_quanti_item(const QString & mz,
		const QString & rt) {

	XicExtractionMethodBase * extraction_method =
			_quanti_method->getXicExtractionMethod();

	extraction_method->setMz(mz.toDouble());

	QuantiItemBase * quanti_item = new QuantiItemMzRt(*extraction_method,
			rt.toDouble());
	_quanti_items->push_back(quanti_item);
}

void Quantificator::setMonitor(MonitorBase * monitor) {
	_monitor = monitor;
}

struct CmpQuantiItems {
	bool operator()(const QuantiItemBase * l1, const QuantiItemBase * l2) {
		const mcq_double mz1 = l1->get_mz();
		const mcq_double mz2 = l2->get_mz();
		return (mz1 < mz2);
	}
} QuantiItemsCmp;

void Quantificator::sortQuantiItems() {
	sort(_quanti_items->begin(), _quanti_items->end(), QuantiItemsCmp);
}

void Quantificator::quantify() {

	sortQuantiItems();

	QString group_id = _group_to_quantify->getXmlId();
	QString quanti_method_id = _quanti_method->getXmlId();
	XicExtractionMethodBase * xic_extraction_method =
			_quanti_method->getXicExtractionMethod();
	const mcq_xic_type xic_type = _quanti_method->getXicType();
	const std::vector<const FilterBase *> xic_filters =
			_quanti_method->getXicFilters();
	PeakDetectionBase * detection_method = _quanti_method->getDetectionMethod();
	const PeakExtractor * peak_extractor = new PeakExtractor(detection_method);

	mcqout() << "MS run group '" << group_id << "': quantification method '"
			<< quanti_method_id << "': quantification begin" << endl;
	_quanti_method->printInfos(mcqout());

	_monitor->setCurrentQuantify(_xml_id);
	_monitor->setCurrentGroup(_group_to_quantify);

	// launch quantification in group
	Msrun * current_msrun;
	msRunHashGroup::const_iterator itmsrun;
	unsigned int count_msruns(1);
	QuantiItemBase * current_mz;
	std::vector<QuantiItemBase *>::const_iterator itmz;
	xicBase * current_xic;
	unsigned int quanti_items_size = _quanti_items->size();

	for (itmsrun = _group_to_quantify->begin();
			itmsrun != _group_to_quantify->end(); ++itmsrun, ++count_msruns) {
		current_msrun = itmsrun->second;
		mcqout() << "\tQuantifying in MS run '" << (current_msrun->getXmlId())
				<< "' : " << count_msruns << "/" << _group_to_quantify->size()
				<< " in group '" << group_id << "'" << endl;

		_monitor->setCurrentMsrun(current_msrun);
		current_msrun->prepareSpectraForQuantification(xic_extraction_method);

		//Indicated advance of quantification by a passphrase
		QString phrase("MassChroQ-eXtract-Ion-Current");
		int count_xic(1);
		int count_phrase(0);
		int count(0);
		for (itmz = _quanti_items->begin(); itmz != _quanti_items->end();
				++itmz, ++count_xic, ++count) {
			//Print advance of quantification
			if (count == 100) {
				count=0;
				if(count_phrase==0){
					mcqout() <<"\t";
				}
				if (count_phrase<phrase.size()) {
					mcqout() << phrase.at(count_phrase);
					mcqout().flush();
					count_phrase++;
				} else {
					mcqout() << " " << count_xic << "/" << quanti_items_size<<endl;
					mcqout().flush();
					count_phrase=0;
				}
			}
			//mcqout() << "\r\t\textracting XIC and detecting peaks on XIC "
			//	 << count_xic << "/" << quanti_items_size;

			current_mz = *itmz;
			_monitor->setCurrentSearchItem(current_mz);

			current_xic = current_msrun->extractXic(xic_type, current_mz);

			if (current_xic == 0)
				qDebug() << "detectAndQuantify null xic ";

			current_xic->setMonitor(*_monitor);
			_monitor->setXic(current_xic);
			// apply filters to the extracted Xic
			// monitor is passed to Xic class which calls
			// monitor.setFilteredXics() method
			current_xic->applyFilters(xic_filters);

			// extract all peaks from xic
			std::vector<xicPeak *> * all_peaks_list =
					peak_extractor->newAllPeaksList(current_xic);
			_monitor->setAllPeaks(all_peaks_list);
			delete (current_xic);

			// match the peaks
			std::vector<xicPeak *> * matched_peak_list =
					_peak_matcher->newMatchedPeaks(current_mz, all_peaks_list,
							current_msrun);

			_monitor->setMatchedPeaks(matched_peak_list);
			_monitor->setEndCurrentSearchItem();

			// clean up
			if (matched_peak_list != 0) {
				delete matched_peak_list;
				matched_peak_list = 0;
			}
		}
		mcqout() << endl;
		current_msrun->doneWithSpectra();
		_monitor->setEndCurrentMsrun();
	}
	delete (peak_extractor);

	mcqout() << "MS run group '" << group_id << "': quantification method '"
			<< quanti_method_id << "': quantification finished" << endl;

	performPostMatching();
	_monitor->setEndCurrentGroup();

}

void Quantificator::performPostMatching() {
	if (_peak_matcher->isPostMatchingRequired()) {
		QString group_id = _group_to_quantify->getXmlId();
		mcqout() << "MS run group '" << group_id << "': post matching begin"
				<< endl;

		_monitor->setPostMatchingOn();
		std::vector<QuantiItemBase *>::const_iterator itmz;
		Msrun * current_msrun;
		unsigned int count_msruns(1);
		msRunHashGroup::const_iterator itmsrun;
		for (itmsrun = _group_to_quantify->begin();
				itmsrun != _group_to_quantify->end();
				++itmsrun, ++count_msruns) {
			current_msrun = itmsrun->second;
			mcqout() << "\tPost matching in MS run '"
					<< (current_msrun->getXmlId()) << "' : " << count_msruns
					<< "/" << _group_to_quantify->size() << " in group '"
					<< group_id << "'" << endl;

			_monitor->setCurrentMsrun(current_msrun);
			for (itmz = _quanti_items->begin(); itmz != _quanti_items->end();
					++itmz) {
				std::vector<xicPeak *> * post_matched_peaks =
						_peak_matcher->newPostMatchedPeaks(*itmz,
								current_msrun);
				_monitor->setPostMatchedPeaks(*itmz, post_matched_peaks);
				delete (post_matched_peaks);
			}
			_monitor->setEndCurrentMsrun();
		}
		_monitor->setPostMatchingOff();
		mcqout() << "MS run group '" << group_id << "': post matching end"
				<< endl;

	}
}
