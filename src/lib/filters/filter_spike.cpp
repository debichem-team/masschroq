/**
 * \file filter_spike.cpp
 * \date July 22, 2009
 * \author Olivier Langella
 */

#include "filter_spike.h"
#include "../share/utilities.h"
#include <map>

FilterSpike::FilterSpike()
{
	_filter_type = FILTER_SPIKE;
}

FilterSpike::~FilterSpike()
{
}

void 
FilterSpike::printInfos(QTextStream & out) const
{
	out << "\t filter '"<<_filter_type << "' with parameters :\n"
		<< "\t _window_length = " << _half_window_length << endl;
}

void 
FilterSpike::set_half_window_length(unsigned int half_length)
{
	_half_window_length = half_length;
}

void 
FilterSpike::privTreatSignal(std::vector<mcq_double> * psignalx, 
							 std::vector<mcq_double> ** ppsignaly) const
{
	
	std::vector<mcq_double> * p_v_spike = 
		Utilities::newVectorByApplyingWindowsOperation(*ppsignaly,
													   _half_window_length, &get_spike_to_zero);
	delete *ppsignaly;
	*ppsignaly = p_v_spike;
}
