/**
 * \file filter_smoothing.cpp
 * \date November 9, 2009
 * \author Olivier Langella
 */

#include "filter_smoothing.h"
#include "../share/utilities.h"

FilterSmoothing::FilterSmoothing()
{
	_filter_type = FILTER_SMOOTHING;
}

FilterSmoothing::~FilterSmoothing()
{
}

void
FilterSmoothing::set_smoothing_half_window_length(unsigned int length)
{
	_smoothing_half_window_length = length;
}


void 
FilterSmoothing::printInfos(QTextStream & out) const
{
	out << "\t filter '"<<_filter_type << "' with parameters :\n"
		<< "_smoothing_half_window_length = " << _smoothing_half_window_length
		<< endl;
}

void FilterSmoothing::privTreatSignal(
	std::vector<mcq_double> * psignalx,
	std::vector<mcq_double> ** psignaly) const
{
	std::vector<mcq_double> * new_psignaly =
		Utilities::newVectorByApplyingWindowsOperation(
			*psignaly,
			_smoothing_half_window_length, &get_average);
	delete (*psignaly);
	*psignaly = new_psignaly;
}
