/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file filter_base.h
 * \date 22 juil. 2009
 * \author Olivier Langella
 */

#ifndef FILTER_BASE_H_
#define FILTER_BASE_H_ 1

#include <QDebug>
#include <QString>
#include <QTextStream>
#include "../../config.h"

//using namespace std;
/**
   \class FilterBase
   \brief Abstract base class representing a signal's filter
*/ 
class FilterBase {
public:
  
	FilterBase();
	virtual ~FilterBase();
  
	void treatSignal(std::vector<mcq_double> * psignalx,
					 std::vector<mcq_double> ** ppsignaly) const;
  
	const QString printInfos() const;
  
	virtual void printInfos(QTextStream & out) const = 0;
  
	virtual const mcq_filter_type & getFilterType() const;
  
protected:
  
	mcq_filter_type _filter_type;
  
	virtual void privTreatSignal(std::vector<mcq_double> * psignalx, 
			std::vector<mcq_double> ** ppsignaly) const = 0;
  
};

#endif /* FILTER_BASE_H_ */
