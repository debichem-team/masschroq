/**
 * \file filter_base.cpp
 * \date 22 juil. 2009
 * \author Olivier Langella
 */

#include "filter_base.h"
#include "../mcq_error.h"
#include <iostream>

FilterBase::FilterBase()
{
}

FilterBase::~FilterBase()
{
}

const QString 
FilterBase::printInfos() const
{
	QString infos;
	QTextStream info_stream(&infos, QIODevice::WriteOnly);
	printInfos(info_stream);
	return (infos);
}

const mcq_filter_type & 
FilterBase::getFilterType() const
{
	return _filter_type;
}


void 
FilterBase::treatSignal(std::vector<mcq_double> * psignalx,
						std::vector<mcq_double> ** ppsignaly) const
{
	if (psignalx->size() != (*ppsignaly)->size())
	{
		throw mcqError("error in filterBase::treatSignal :\n signal x and signal y vectors must have the same size.");
	}
	
	this->privTreatSignal(psignalx, ppsignaly);
  
	if (psignalx->size() != (*ppsignaly)->size())
	{
		qDebug() << "error in filterBase::treatSignal" << psignalx->size()
				 << " " << (*ppsignaly)->size() << endl;
		qDebug() << printInfos();
    
		throw mcqError("error in filterBase::treatSignal :\n signal x and signal y vectors have different sizes after treatment ");
	}
}
