/**
 * \file filter_background.cpp
 * \date July, 22 2009
 * \author Olivier Langella
 */

#include "filter_background.h"
#include <iostream>
#include "filter_max_min.h"
#include "../share/utilities.h"

FilterBackground::FilterBackground()
{
	_filter_type = FILTER_BACKGROUND;
}

FilterBackground::~FilterBackground()
{
}

void
FilterBackground::set_half_median_window_length(unsigned int length)
{
	_half_median_window_length = length;
}

void
FilterBackground::set_half_min_max_window_length(unsigned int length)
{
	_half_min_max_window_length = length;
}

void 
FilterBackground::printInfos(QTextStream & out) const
{
	out << "\t filter '"<<_filter_type << "' with parameters :\n"
		<< "\t _half_median_window_length = " 
		<< _half_median_window_length << endl;
	out << "\t _half_min_max_window_length = " 
		<< _half_min_max_window_length << endl;
  
}

void 
FilterBackground::privTreatSignal(
	std::vector<mcq_double> * psignalx,
	std::vector<mcq_double> ** ppsignaly) const
{
  
	std::vector<mcq_double>::iterator itsignaly;
  
	// applying a median filter

	std::vector<mcq_double> * p_v_median =
		Utilities::newVectorByApplyingWindowsOperation(
			*ppsignaly,
			_half_median_window_length, &get_median);
  
	// applying a max/min (opening) filter
	FilterMaxMin f_maxmin;
	f_maxmin.set_max_min_half_window_length(_half_min_max_window_length);
	f_maxmin.treatSignal(psignalx, &p_v_median);
  
	constvdoubleit it;
	for (it = p_v_median->begin(), itsignaly = (*ppsignaly)->begin(); itsignaly
			 != (*ppsignaly)->end(); ++it, ++itsignaly) {
    
		*itsignaly -= *it;
		if (*itsignaly < 0) {
			*itsignaly = 0;
		}
	}
	delete p_v_median;
}
