/**
 * \file filter_max_min.cpp
 * \date November 3, 2009
 * \author Olivier Langella
 */

#include "filter_max_min.h"
#include "../share/utilities.h"

FilterMaxMin::FilterMaxMin()
{
	_max_min_half_window_length = 2;
	_filter_type = FILTER_MAXMIN;
}

FilterMaxMin::~FilterMaxMin()
{
}

void
FilterMaxMin::set_max_min_half_window_length(unsigned int length)
{
	_max_min_half_window_length = length;
}


void 
FilterMaxMin::printInfos(QTextStream & out) const
{
	out << "\t filter '"<<_filter_type << "' with parameters :\n"
		<< "_max_min_half_window_length = " << _max_min_half_window_length
		<< endl;
}

void 
FilterMaxMin::privTreatSignal(
	std::vector<mcq_double> * psignalx, 
	std::vector<mcq_double> ** ppsignaly) const
{
  
	//compute erosion (min)
	std::vector<mcq_double> * p_v_min = 
		Utilities::newVectorByApplyingWindowsOperation(
			*ppsignaly,
			_max_min_half_window_length, &get_min_element);
	delete *ppsignaly;
  
	//then compute delation (max) on the eroded signal
	*ppsignaly = 
		Utilities::newVectorByApplyingWindowsOperation(
			p_v_min,
			_max_min_half_window_length, &get_max_element);
	delete p_v_min;
}
