/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file filter_background.h
 * \date July, 22 2009
 * \author Olivier Langella
 */

#ifndef FILTER_BACKGROUND_H_
#define FILTER_BACKGROUND_H_ 1

#include "filter_base.h"

/**
 * \class FilterBackground
 * \brief The background filter is designed to eliminate the signal's 
 background noise. This filter contains to fitering methods : a median filter 
 followed by an opening (Max-Min) filter.
*/


class FilterBackground : public FilterBase {
public:
  
	FilterBackground();
	virtual ~FilterBackground();

	virtual void printInfos(QTextStream & out) const;
  
	void set_half_median_window_length(unsigned int length);
  
	void set_half_min_max_window_length(unsigned int length);
	
	int get_half_median_window_length() const{
		return(_half_median_window_length);
	}

	int get_half_min_max_window_length() const{
		return(_half_min_max_window_length);
	}

protected:
	
	virtual void privTreatSignal(
		std::vector<mcq_double> * psignalx, 
		std::vector<mcq_double> ** psignaly) const;
	
private:
  
	unsigned int _half_median_window_length;
	unsigned int _half_min_max_window_length;
  
};

#endif /* FILTER_BACKGROUND_H_ */
