/**
 * \file filter_min_max.cpp
 * \date November 3, 2009
 * \author Olivier Langella
 */

#include "filter_min_max.h"
#include "../share/utilities.h"

FilterMinMax::FilterMinMax()
{
	_min_max_half_window_length = 2;
	_filter_type = FILTER_MINMAX;
}

FilterMinMax::~FilterMinMax()
{
}

void
FilterMinMax::set_min_max_half_window_length(unsigned int length)
{
    _min_max_half_window_length = length;
}

void 
FilterMinMax::printInfos(QTextStream & out) const
{
	out << "\t filter '"<<_filter_type << "' with parameters :\n"
		<< "\t _min_max_half_window_length = " << _min_max_half_window_length
		<< endl;
}

void 
FilterMinMax::privTreatSignal(
	std::vector<mcq_double> * psignalx, 
	std::vector<mcq_double> ** psignaly) const
{
	//compute delation (max)
	std::vector<mcq_double> * p_v_max = 
		Utilities::newVectorByApplyingWindowsOperation(
			*psignaly,
			_min_max_half_window_length, &get_max_element);
	delete (*psignaly);
	
	//then compute erosion (min) on the delated signal
	*psignaly = 
		Utilities::newVectorByApplyingWindowsOperation(
			p_v_max,
			_min_max_half_window_length,
			&get_min_element);
	delete (p_v_max);
}
