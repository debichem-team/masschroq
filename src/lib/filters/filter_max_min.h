/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file filter_max_min.h
 * \date November 3, 2009
 * \author Olivier Langella
 */

#ifndef FILTER_MAX_MIN_H_
#define FILTER_MAX_MIN_H_ 1

#include "filter_base.h"

/**
   \class FilterMaxMin
   \brief This is an opening filter (Max-Min filter): it computes a delation
   (Max) of an erosion (Min) on the signal.
*/

class FilterMaxMin : public FilterBase
{
public:

	FilterMaxMin();
	virtual ~FilterMaxMin();
  
	virtual void printInfos(QTextStream & out) const;

	void set_max_min_half_window_length(unsigned int length);
  
protected:
  
	virtual void privTreatSignal(std::vector<mcq_double> * psignalx, 
			std::vector<mcq_double> ** psignaly) const;
  
private:

	unsigned int _max_min_half_window_length;

};

#endif /* FILTER_MAX_MIN_H_ */
