#ifndef XIC_EXTRACTOR_H_
#define XIC_EXTRACTOR_H_ 1

#include <QString>
#include <vector>

#include "msrun/msrun.h"
#include "peak/xic_peak.h"
#include "peak/peak_extractor.h"
#include "peak/peak_extractor_mz_rt.h"

#include "params/background_filter.h"
#include "params/xic_peak_detection.h"
#include "params/peak_search_mz.h"
#include "xic/mz_selection_base.h"
#include "params/peak_search_mz_rt.h"

class xicExtractor
{
public:
	xicExtractor();
	virtual ~xicExtractor();
	
	void set_result_file_info(const QFileInfo & result_file);
	
	void setParam(QString fileName);
	void set_msrun(msRun * the_ms_run);
	/*void set_filter_background(bool ok) {
		_filter_background = ok;
	};*/
	/*void set_half_mediane_window_size(unsigned int size) {
		_half_mediane_window_size = size;
	};*/
	/*void set_half_min_max_window_size(unsigned int size) {
		_half_min_max_window_size = size;
	};*/
	void set_xic_mz_selection(mzSelectionBase * xic_mz_selection) {
		_xic_mz_selection = xic_mz_selection;
	};
	void set_xic_type(string xic_type) {
		_xic_type = xic_type;
	};
	
	void set_p_xic_peak_detection(xicPeakDetection * p_xic_peak_detection) {
		_p_xic_peak_detection = p_xic_peak_detection;
	};
	void set_p_background_filter(backgroundFilter * p_background_filter) {
		_p_background_filter = p_background_filter;
	};
	/*
	void set_mass_min(float mass_min) {
		_mass_min = mass_min;
	};
	void set_mass_max(float mass_max) {
		_mass_max = mass_max;
	};
	*/
	void push_mass(float mass);
	void push_mass(const peakSearchMzRt & search_mzrt);

	void detectAndQuantify();

	void printExtractorResults(std::ostream & out) const;
	void printXmlResults(std::ostream & out) const;
	void printXmlResults() const;

private:

	QFileInfo _param_file_info;
	QFileInfo _result_file_info;

	void detectAndQuantifyPeaksOnMass(const peakSearchMz mz);

	mzSelectionBase * _xic_mz_selection;

	msRun * _ms_run;
	
	string _xic_type;
	
	backgroundFilter * _p_background_filter;
	
//	bool _filter_background;
//	unsigned int _half_mediane_window_size;
//	unsigned int _half_min_max_window_size;
	
	xicPeakDetection * _p_xic_peak_detection;
	//float _mass_min;
	//float _mass_max;
	
	std::vector<peakExtractor *> _peak_extractor_list;
	//std::vector<peakSearchMz> _mass_list;
	//std::map<const peakSearchMz, std::vector<xicPeak *> *> _map_mass2detected_peaks;
	
};

#endif /*XIC_EXTRACTOR_H_*/
