/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifndef MCQERROR_H_
#define MCQERROR_H_ 1

#include <exception>
#include <string>
#include <QString>

class mcqError: public std::exception {
public:
  mcqError(QString message) throw () {
    _message = message;
  }
  
  /*mcqError(std::string message = "") throw () :
    _message(message.c_str()) {
    }*/
  
   
  virtual const QString & qwhat() const throw () {
    return _message;
  }
  
  virtual ~mcqError() throw () {
  }
  
 private:
  QString _message; //Description de l'erreur
};

#endif /*MCQERROR_H_*/
