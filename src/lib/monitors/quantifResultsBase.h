/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantifResultsBase.h
 * \date October 05, 2010  
 * \author Edlira Nano
 */

#ifndef QUANTIF_RESULTS_BASE_H_
#define QUANTIF_RESULTS_BASE_H_ 1

#include "monitorBase.h"
#include <QStringList>
#include <QTime>

/**
 * \class QuantifResultsBase
 * \brief Absract class representing the quantification results produced 
 * by MassChroQ
 */

class QuantifResultsBase : public MonitorBase {
 
public :  

	QuantifResultsBase();
	QuantifResultsBase(const QString & filename);
	virtual ~QuantifResultsBase();
  
	virtual void setCurrentQuantify(const QString & quantify_id);
  
	virtual void setCurrentGroup(const msRunHashGroup * group); 
	virtual void setEndCurrentGroup(); 

	virtual void setCurrentMsrun(const Msrun * msrun); 
	virtual void setEndCurrentMsrun();   

	virtual void setCurrentSearchItem(QuantiItemBase * quanti_item);
	virtual void setEndCurrentSearchItem();

	virtual void setXic(const xicBase * xic);
  
	virtual void setFilteredXic(const xicBase * xic, 
								const mcq_filter_type & filterType,
								const unsigned int filterNum);
  
	virtual void setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list);

	virtual void setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list);

	virtual void setPostMatchingOn();
    
	virtual void setPostMatchedPeaks(QuantiItemBase * quanti_item, 
									 const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual void setPostMatchingOff();
  
	virtual void debriefing();
  
protected :

	/// some accessors
  
	virtual const QString & getCurrentGroupId() const {
		return _current_group_id;
	}
  
	virtual const QString & getCurrentMsrunId() const {
		return _current_msrun_id;
	}

	virtual const QString & getCurrentMsrunFile() const {
		return _current_msrun_file;
	}

	virtual const mcq_double getCurrentMz() const {
		return (_current_quanti_item->get_mz());
	}
  
	virtual const unsigned int getCurrentZ() const {
		return *(_current_quanti_item->getZ());
	}
 
	virtual void setOutputFilesAndStreams(const QString & filename) = 0; 
  
	virtual void setLocaleAndPrecisionForAllStreams() = 0;
  
	virtual void printLine(const QStringList & list) = 0;
  
	/// some printing and formating methods

	virtual void printHeaders() = 0;
  
	virtual const QStringList getPepHeaders() const;

	virtual const QStringList getProtHeaders() const;

	virtual const QString & formatCell(const mcq_double f);
  
	virtual const QString & formatCell(const QString & cell);
  
	const QTime seconds2DateTime(const mcq_double s) const;

	/// temporary variables needed for data formating
	QString _tmp_element;
	QTextStream _tmp_element_stream;
	QString _sep;
	QString _endl;

	QString _current_quantify_id;
	QString _current_group_id;
	QString _current_msrun_id;
	QString _current_msrun_file;
	QuantiItemBase * _current_quanti_item;

	/// hash map : peptide id -> map[protein id -> protein description]
	std::map< QString, std::map<QString, QString> >_map_peptide_proteins;
};

#endif /* QUANTIF_RESULTS_BASE_H_ */
