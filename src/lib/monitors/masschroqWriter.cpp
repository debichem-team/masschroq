
/**
 * \file masschroqWriter.cpp
 * \date January 03, 2011
 * \author Edlira Nano
 */

#include "masschroqWriter.h"
#include "../msrun/ms_run_hash_group.h"
#include "../peptides/peptide_isotope.h"
#include "../xic/xic_base.h"
#include "../../encode/decodeBinary.h"
#include "../mass_chroq.h"
#include "../share/utilities.h"
#include "../consoleout.h"

#include <QXmlStreamReader>


MasschroqWriter::MasschroqWriter(
	const QString & in_filename,
	const QString & out_filename, 
	const bool with_traces) 
	:
	_quanti_item_counter(0),
	_input_filename(in_filename),
    _xic_traces(with_traces),
    _toBeTraced(with_traces),
    _enc_precision(64),
    _enc_little_endian("true")
{  
    QFileInfo outFileInfo(out_filename);
	QString outDir(outFileInfo.path());
	QFileInfo outDirInfo(outDir);
	if (! outDirInfo.exists()) {
		throw mcqError(QObject::tr("cannot find the output directory '%1' of output file '%2' \n").arg(outDir, out_filename));
	}
  
	QString complete_out_filename = out_filename;
	complete_out_filename.append(".xml");
	_output_file = new QFile(complete_out_filename);
	if (_output_file->exists())
	{
		mcqout() << "WARNING: XML output file '"
			 << complete_out_filename
			 << "' already exists, it will be overwrited." 
			 << endl;
	}
  
	if (_output_file->open(QIODevice::WriteOnly))
	{
		_output_stream = new MCQXmlStreamWriter();
		_output_stream->setDevice(_output_file);
	} else
	{
		throw mcqError(QObject::tr("error : cannot open the XML output file : %1\n").arg(out_filename));
	}

	_output_stream->setAutoFormatting(true);
	writeInputParameters();
}

MasschroqWriter::~MasschroqWriter()
{
	if (_output_stream != 0)
	{
		_output_stream->writeEndDocument();
		delete _output_stream;
		_output_stream = 0;
	}

	if (_output_file != 0)
	{
		_output_file->close();
		delete _output_file;
		_output_file = 0;
	}
}

bool 
MasschroqWriter::isToBeTraced() const
{
	return _toBeTraced;
}

void 
MasschroqWriter::setToBeTraced(const bool b)
{
	_toBeTraced = b;
}

void
MasschroqWriter::writeInputParameters()
{
	// parse MassChroQ's input XML file
	QFile infile(_input_filename);
  
	if (!infile.open(QIODevice::ReadOnly))
	{
		throw mcqError(QObject::tr("MasschroqWriter : error reading MassChroQ XML input file '%1'\n").arg(_input_filename));
	}
  
	QXmlStreamReader reader(&infile);
	while (!reader.atEnd() && !reader.hasError())
	{
		QXmlStreamReader::TokenType token = reader.readNext();
		if (token == QXmlStreamReader::StartDocument)
		{
			_output_stream->writeStartDocument();
		}
		else if (token == QXmlStreamReader::StartElement)
		{
			QString name = reader.name().toString();
			_output_stream->writeStartElement(reader.namespaceUri().toString(), name);
			_output_stream->writeAttributes(reader.attributes());
			if (name =="masschroq")
			{
				QString mcq_xml_version(MASSCHROQ_SCHEMA_VERSION);
				_output_stream->writeAttribute("version", mcq_xml_version);
				QString type("output");
				_output_stream->writeAttribute("type", type);
			}
		}
		else if (token == QXmlStreamReader::Comment)
		{
			_output_stream->writeComment(reader.text().toString());
		}
		else if (token == QXmlStreamReader::Characters)
		{
			_output_stream->writeCharacters(reader.text().toString());
		}
		else if (token == QXmlStreamReader::EndElement &&
				 reader.name() != "masschroq")
		{
			_output_stream->writeEndElement();
		}
	}
	
	if (reader.hasError())
	{
		QString line, col;
		line.setNum(reader.lineNumber());
		col.setNum(reader.columnNumber());
		throw mcqError(QObject::tr("MasschroqWriter : error reading MassChroQ XML input file '%1':\n at line %2, column %3 : ").arg(_input_filename, line, col, reader.errorString()));
	}
  
	if (reader.atEnd()) {
		initialiseOutput();
	}
}

void 
MasschroqWriter::initialiseOutput()
{
	_output_stream->writeStartElement("results");
	QDateTime date_time(QDateTime::currentDateTime());
	_output_stream->writeAttribute("date", date_time);
	_output_stream->writeAttribute("input_file", _input_filename);
}

void
MasschroqWriter::printAlignmentResults(const Msrun * msrun)
{
	
	_output_stream->writeStartElement("alignment_result");
	// get the time values for this msrun and encode them
	std::vector<mcq_double> v_time = msrun->getVectorOfTimeValues();
	QByteArray enc_time_array = DecodeBinary::base64_encode_doubles(v_time); 
	const QString enc_time = QString::fromAscii(enc_time_array.constData());
	_output_stream->writeAttribute("precision", _enc_precision);
	_output_stream->writeAttribute("little_endian", _enc_little_endian);
	_output_stream->writeCharacters(enc_time);
	_output_stream->writeEndElement(); // </alignment_result>
}

void
MasschroqWriter::setCurrentQuantify(const QString & quantify_id)
{
	_current_quantify_id = quantify_id;
	_output_stream->writeStartElement("result");
	_output_stream->writeAttribute("quantify_id", quantify_id);
}

void
MasschroqWriter::setCurrentGroup(const msRunHashGroup * group)
{
	_current_group_id = group->getXmlId();
}

void
MasschroqWriter::setEndCurrentGroup()
{
	_current_group_id.clear();
	_output_stream->writeEndElement(); // </result>
}

void 
MasschroqWriter::setCurrentMsrun(const Msrun * msrun)
{
	_current_msrun_id = msrun->getXmlId();
	_output_stream->writeStartElement("data");
	_output_stream->writeAttribute("id_ref", _current_msrun_id);
	if (msrun->hasBeenAligned())
	{
		printAlignmentResults(msrun);
	}
				  
	if (isToBeTraced())
	{
		// get the rt values for this msrun and encode them
		std::vector<mcq_double> v_rt = msrun->getAlignedRetentionTimes();
		QByteArray enc_rt_array = DecodeBinary::base64_encode_doubles(v_rt); 
		const QString enc_rt = QString::fromAscii(enc_rt_array.constData());
        
		/** Debug region **/
		// const QByteArray encoded_data = enc_rt.toAscii();
		// std::vector<mcq_double> decoded = DecodeBinary::base64_decode_binary(encoded_data, _enc_precision, false);
		// qDebug() << "Encoded Decoded rt vector: " << endl;
		// std::vector<mcq_double>::iterator it_rt, it_rt2;
		// for (it_rt = v_rt.begin(), it_rt2 = decoded.begin();
		// 	  it_rt2 != decoded.end();
		// 	  ++it_rt, ++it_rt2) {
		//    qDebug() << *it_rt << " " << *it_rt2 << endl;
		//  }
   
		_output_stream->writeStartElement("data_rt");
		_current_rt_id = 
			QString("rt_").append(_current_msrun_id).append(_current_quantify_id);
		_output_stream->writeAttribute("id", _current_rt_id);
		_output_stream->writeAttribute("precision", _enc_precision);
		_output_stream->writeAttribute("little_endian", _enc_little_endian);
		_output_stream->writeCharacters(enc_rt);
		_output_stream->writeEndElement(); // </data_rt>;
	}
}


void 
MasschroqWriter::setEndCurrentMsrun()
{
	_current_msrun_id.clear();
	_current_rt_id.clear();
	_output_stream->writeEndElement(); // </data>
}

void 
MasschroqWriter::setCurrentSearchItem(QuantiItemBase * quanti_item)
{
	_current_quanti_item = quanti_item;
	_output_stream->writeStartElement("quanti_item");
  
	QString quanti_item_id("quanti_item"), item_counter;
	item_counter.setNum(_quanti_item_counter);
	quanti_item_id.append(item_counter);
	_output_stream->writeAttribute("id", quanti_item_id);
  
	QString quanti_item_type = quanti_item->getType();
	if (quanti_item_type == PEPTIDE_QUANTI_ITEM)
	{
		QString item_id = _current_quanti_item->getQuantiItemId();
		unsigned int z = *(_current_quanti_item->getZ());
		_output_stream->writeAttribute("item_id_ref", item_id);
		_output_stream->writeAttribute("z", z);
	}
	else if (quanti_item_type == MZ_QUANTI_ITEM)
	{
		mcq_double mz = _current_quanti_item->get_mz();
		_output_stream->writeAttribute("mz", mz);
	}
	else if (quanti_item_type == MZRT_QUANTI_ITEM)
	{
		mcq_double mz = _current_quanti_item->get_mz();
		mcq_double rt = _current_quanti_item->getRt();
		_output_stream->writeAttribute("mz", mz);
		_output_stream->writeAttribute("rt", rt);
	}
  
	_quanti_item_counter++;
	// if il y a des xics à tracer
	// writeStartElement("xic_traces");
}

void 
MasschroqWriter::setEndCurrentSearchItem()
{
	_output_stream->writeEndElement(); // </search>
	_current_xic_all_peaks_map.clear();
}

void 
MasschroqWriter::setXic(const xicBase * xic)
{
	if ( isToBeTraced() )
	{
		_output_stream->writeStartElement("xic_traces");
    
		std::vector<mcq_double> xic_v_int = *(xic->getConstIntensities());
		// encode intensity values in base64 in little endian order        
 
		QByteArray enc_int_array = DecodeBinary::base64_encode_doubles(xic_v_int); 
		const QString enc_int = QString::fromAscii(enc_int_array.constData());
    
		/* Debug region */
		// const QByteArray encoded_data_int = enc_int.toAscii();
		// std::vector<mcq_double> decoded_int = DecodeBinary::base64_decode_binary(encoded_data_int, _enc_precision, false);
    
		// qDebug() << "Encoded Decoded int vector: " << endl;
		// std::vector<mcq_double>::iterator it_int, it_int2;
		//  for (it_int = xic_v_int->begin(), it_int2 = decoded_int.begin();
		// 	  it_int2 != decoded_int.end();
		// 	  ++it_int, ++it_int2) {
		//    qDebug() << *it_int << " " << *it_int2 << endl;
		//  }
    
		_output_stream->writeStartElement("raw_xic");
		// reference the base64 encoded rt values
		_output_stream->writeStartElement("rt");
		_output_stream->writeAttribute("data_rt_ref", _current_rt_id);
		_output_stream->writeEndElement(); // </rt>);
		// write the base64 encoded intensity values
		_output_stream->writeStartElement("intensity");
		_output_stream->writeAttribute("precision", _enc_precision);
		_output_stream->writeAttribute("little_endian", _enc_little_endian);
		_output_stream->writeCharacters(enc_int);
		_output_stream->writeEndElement(); // </intensity>;
		_output_stream->writeEndElement(); // </raw_xic>;
	}
}

void 
MasschroqWriter::setFilteredXic(const xicBase * xic, 
								const mcq_filter_type & filterType, 
								const unsigned int num)
{
	if ( isToBeTraced() )
	{
		_output_stream->writeStartElement("filtered_xic_intensity");
		_output_stream->writeAttribute("num", num);
		_output_stream->writeAttribute("precision", _enc_precision);
		_output_stream->writeAttribute("little_endian", _enc_little_endian);
		std::vector<mcq_double> filtered_int = *(xic->getConstIntensities());  
		QByteArray enc_f_int_array = 
			DecodeBinary::base64_encode_doubles(filtered_int); 
		const QString enc_f_int = QString::fromAscii(enc_f_int_array.constData());
		_output_stream->writeCharacters(enc_f_int);
		_output_stream->writeEndElement(); // </filtered_xic_intensity>
	}
}
  
void 
MasschroqWriter::setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list)
{
	if ( isToBeTraced() )
	{
	  std::vector<xicPeak *>::const_iterator it_peaks;
		mcq_double max_int, rt, area;
		for (it_peaks = p_v_peak_list->begin();
			 it_peaks != p_v_peak_list->end();
			 ++it_peaks) {
			rt = (*it_peaks)->get_aligned_max_rt();
			max_int = (*it_peaks)->get_max_intensity();
			area = (*it_peaks)->get_area();
			_current_xic_all_peaks_map[rt] = 
				std::pair<mcq_double, mcq_double>(max_int, area);
		}
	}
}
  
void 
MasschroqWriter::setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list)
{
	// finish printing xic_traces
	// print xic_peaks traces (all deteceted peaks + matched ones)
	if ( isToBeTraced() )
	{
		mcq_double rt_peak, max_int, area_peak;
		std::map<mcq_double, std::pair<mcq_double, mcq_double> >::const_iterator 
			it_all_peaks;
			std::vector<xicPeak *>::const_iterator it_sel_peaks;
		_output_stream->writeStartElement("xic_peaks");
		for (it_all_peaks = _current_xic_all_peaks_map.begin();
			 it_all_peaks != _current_xic_all_peaks_map.end();
			 ++it_all_peaks) 
		{
			_output_stream->writeStartElement("peak");
			rt_peak = it_all_peaks->first;
			max_int = (it_all_peaks->second).first;
			area_peak = (it_all_peaks->second).second;
			_output_stream->writeAttribute("rt", rt_peak);
			_output_stream->writeAttribute("intensity", max_int);
			_output_stream->writeAttribute("area", area_peak);
			for (it_sel_peaks = p_v_peak_list->begin();
				 it_sel_peaks != p_v_peak_list->end(); 
				 ++it_sel_peaks) 
			{
				xicPeak & ref_peak(*(*it_sel_peaks));
				if (rt_peak == ref_peak.get_aligned_max_rt()) {
					_output_stream->writeAttribute("matched", QString("true"));
				}
				break;
			}
			_output_stream->writeEndElement(); // </peak>
		}
		_output_stream->writeEndElement(); // </xic_peaks>
		_output_stream->writeEndElement(); // </xic_traces>
	}

	// print quantification data
	printQuantificationData(p_v_peak_list);
 
}

void
MasschroqWriter::printQuantificationData(const std::vector<xicPeak *> * p_v_peak_list)
{
	mcq_double currentMz = _current_quanti_item->get_mz();
	const Peptide * current_peptide(_current_quanti_item->getPeptide());
	QString pepId, isotopeLabel;
	unsigned int currentZ;
  
	mcq_double rt, max_intensity, area, rt_begin, rt_end;
	std::vector<xicPeak *>::const_iterator it_post_peaks;
	for (it_post_peaks = p_v_peak_list->begin(); 
		 it_post_peaks != p_v_peak_list->end(); 
		 ++it_post_peaks)
	{
		rt = (*it_post_peaks)->get_aligned_max_rt();
		max_intensity = (*it_post_peaks)->get_max_intensity();
		area = (*it_post_peaks)->get_area();
		rt_begin = (*it_post_peaks)->get_aligned_first_rtime();
		rt_end = (*it_post_peaks)->get_aligned_last_rtime();

		_output_stream->writeStartElement("quantification_data");
    
		_output_stream->writeAttribute("mz", currentMz);  
		_output_stream->writeAttribute("rt", rt);
		_output_stream->writeAttribute("max_intensity", max_intensity);
		_output_stream->writeAttribute("area", area);
		_output_stream->writeAttribute("rt_begin", rt_begin);
		_output_stream->writeAttribute("rt_end", rt_end);
    
		if  (current_peptide != 0)
		{
			pepId = current_peptide->getXmlId();
			_output_stream->writeAttribute("peptide", pepId);
      
			if (current_peptide->getIsotopeLabel() != 0)
			{
				isotopeLabel = current_peptide->getIsotopeLabel()->getXmlId();
				_output_stream->writeAttribute("isotope", isotopeLabel);
			}
      
			currentZ = *_current_quanti_item->getZ();
			_output_stream->writeAttribute("z", currentZ);
		}
		_output_stream->writeEndElement(); // "</quantification_data>"
	}
}

void
MasschroqWriter::setPostMatchingOn()
{
	_output_stream->writeStartElement("post_matching_data");
	setToBeTraced(false);
}

void 
MasschroqWriter::setPostMatchedPeaks(
	QuantiItemBase * quanti_item, 
	const std::vector<xicPeak *> * p_v_peak_list)
{
	if (! p_v_peak_list->empty())
	{
		setCurrentSearchItem(quanti_item);
		printQuantificationData(p_v_peak_list);
		setEndCurrentSearchItem();
	}
}

void
MasschroqWriter::setPostMatchingOff()
{
	_output_stream->writeEndElement(); //</post_matching_data>;
	setToBeTraced(_xic_traces);
}

void 
MasschroqWriter::debriefing()
{
	// write protein results

	_output_stream->writeStartElement("summary");
	QString mcq_version(MASSCHROQ_VERSION);
	_output_stream->writeAttribute("masschroq_version", mcq_version);
	_output_stream->writeStartElement("execution_time");

	QDateTime dt_end_xml = QDateTime::currentDateTime();
	const unsigned int secs_elapsed = MassChroq::begin_date_time.secsTo(dt_end_xml);
	_output_stream->writeAttribute("seconds", secs_elapsed);
	const Duration dur =
		Utilities::getDurationFromDates(MassChroq::begin_date_time,
				dt_end_xml);
	QString days, hours, minutes, secs;
	days.setNum(Utilities::getDaysFromDuration(dur));
	hours.setNum(Utilities::getHoursFromDuration(dur));
	minutes.setNum(Utilities::getMinutesFromDuration(dur));
	secs.setNum(Utilities::getSecondsFromDuration(dur));
	QString xml_duration_format = QString("P0Y0M");
	xml_duration_format.append(days).append(QString("DT"));
	xml_duration_format.append(hours).append(QString("H"));
	xml_duration_format.append(minutes).append(QString("M"));
	xml_duration_format.append(secs).append(QString("S"));
	_output_stream->writeAttribute("duration", xml_duration_format);
	_output_stream->writeEndElement(); // </execution_time>
	_output_stream->writeEndElement(); // </summary>
	_output_stream->writeEndDocument();
	_output_file->close();
}
