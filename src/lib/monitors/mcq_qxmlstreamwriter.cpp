/**
 * \file mcq_qxmlstreamwriter.cpp
 * \date January 19, 2011  
 * \author Edlira Nano
 */

#include "mcq_qxmlstreamwriter.h"
#include "../mcq_error.h"

MCQXmlStreamWriter::MCQXmlStreamWriter()
	:
	QXmlStreamWriter(),
	_format('g'),
	_precision(8)
{
}

MCQXmlStreamWriter::MCQXmlStreamWriter(const char format, 
									   const int precision) 
	:
	QXmlStreamWriter()
{
	setFormat(format);
	setPrecision(precision);
}

MCQXmlStreamWriter::~MCQXmlStreamWriter()
{
}

void 
MCQXmlStreamWriter::setFormat(const char format)
{ 
	if (format != 'g' && format != 'e' && format != 'E' &&
		format != 'f' && format != 'G')
	{
		throw mcqError(QObject::tr("in MCQXmlStreamWriter::setFormatAndPrecision: the given format value '%1' is not a valid value.\nValid values are 'g', 'e', 'E', 'f' and 'G'. See QString documentation for details.\n").arg(format));
	}
	_format = format;
}

void 
MCQXmlStreamWriter::setPrecision(const int precision)
{
	_precision = precision; 
}

void 
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const QString & value)
{
	QXmlStreamWriter::writeAttribute(qualifiedName, value);
}

void 
MCQXmlStreamWriter::writeAttribute(const QString & namespaceUri, 
								   const QString & name, 
								   const QString & value)
{
	QXmlStreamWriter::writeAttribute(namespaceUri, name, value);
}

void 
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const mcq_double value)
{
	const QString str_value = QString::number(value, _format, _precision);
	writeAttribute(qualifiedName, str_value);
}

void
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const int value)
{
	const QString str_value = QString::number(value);
	writeAttribute(qualifiedName, str_value);
}

void
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const unsigned int value)
{
	const QString str_value = QString::number(value);
	writeAttribute(qualifiedName, str_value);
}

void 
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const QDate & value)
{
	const QString str_value = value.toString(Qt::ISODate);
	writeAttribute(qualifiedName, str_value);
}

void 
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const QTime & value)
{
	const QString str_value = value.toString(Qt::ISODate);
	writeAttribute(qualifiedName, str_value);
}


void
MCQXmlStreamWriter::writeAttribute(const QString & qualifiedName, 
								   const QDateTime & value)
{
	const QString str_value = value.toString(Qt::ISODate);
	writeAttribute(qualifiedName, str_value);
}
