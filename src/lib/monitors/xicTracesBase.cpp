/**
 * \file xicTracesBase.cpp
 * \date Spetember 28, 2010  
 * \author Edlira Nano
 */

#include "xicTracesBase.h"
#include "../msrun/ms_run_hash_group.h"
#include "../xic/xic_base.h"
#include "../consoleout.h"

XicTracesBase::XicTracesBase(const QString & output_dir) 
	:
	_tmp_element_stream(&_tmp_element),
	_sep("\t")
{
	/// verify and set the output directory
	QFileInfo output_dir_info(output_dir);
	if (! output_dir_info.exists()) {
		QDir current_dir = QDir::current();
		if (! current_dir.mkdir(output_dir)) {
			QString new_name("xic_traces");
			current_dir.mkdir(new_name);
			output_dir_info.setFile(new_name);
		}
    
		mcqout() << "WARNING : quantification traces directory '" 
			 << output_dir
			 << "' does not exist, creating directory '"
			 << output_dir_info.absoluteFilePath() << "'" << endl;
	}
   
	_output_dir_name = output_dir_info.absoluteFilePath();
  
	_current_stream = new QTextStream();
	setLocaleAndPrecisionForAllStreams();
	mcqout() << "Writing quantification traces in directory '"
		 << (output_dir_info.absoluteFilePath()) 
		 << "'" << endl;
}

XicTracesBase::~XicTracesBase(){
	if (_current_stream != 0) {
		_current_stream->flush();
		delete _current_stream;
		_current_stream = 0;
	}
}

bool 
XicTracesBase::isToBeTraced() const {
	return _toBeTraced;
}

void 
XicTracesBase::setToBeTraced(const bool b) {
	_toBeTraced = b;
}

const QString 
XicTracesBase::setTraceFileName(QuantiItemBase * quanti_item) {
	QString prefix(_current_quantify_id);
	prefix.append("_").append(_current_group_id).append("_");
	prefix.append(_current_msrun->getXmlId()).append("_");
	prefix.append(quanti_item->getSimpleName(_current_msrun));
	QString current_filename = _output_dir_name;
	current_filename.append("/").append(prefix).append(".tsv");
	return current_filename;
}

void
XicTracesBase::setLocaleAndPrecisionForStream(QTextStream * stream) {
#if QT_VERSION >= QT_V_4_5 
	stream->setLocale(QLocale::c());
#endif 
	stream->setRealNumberPrecision(8); 
	stream->setRealNumberNotation(QTextStream::SmartNotation);
}

void
XicTracesBase::setLocaleAndPrecisionForAllStreams() {
	setLocaleAndPrecisionForStream(&_tmp_element_stream);
	setLocaleAndPrecisionForStream(_current_stream);
}

void 
XicTracesBase::setCurrentQuantify(const QString & quantify_id) {
	_current_quantify_id.clear();
	_current_quantify_id = quantify_id;
}

void 
XicTracesBase::setCurrentGroup(const msRunHashGroup * group) { 
	_current_group_id.clear();
	_current_group_id = group->getXmlId();
}

void 
XicTracesBase::setEndCurrentGroup() { 
	_current_group_id.clear();
}

void 
XicTracesBase::setCurrentMsrun(const Msrun * msrun) {
	_current_msrun = msrun;
}

void 
XicTracesBase::setEndCurrentMsrun() {
	_current_msrun = 0;
}

void 
XicTracesBase::setCurrentSearchItem(QuantiItemBase * quanti_item) {
	setTraceItem(quanti_item);
	if ( isToBeTraced() ) {
		const QString filename = setTraceFileName(quanti_item);
		_current_file.setFileName(filename);
    
		if (!_current_file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			_current_file.close();
			delete _current_stream;
			throw mcqError(QObject::tr("Cannot create file : '%1'.").arg(_current_file.fileName()));
		}
    
		_current_stream->setDevice(&_current_file);
		printCurrentTitle(quanti_item);
		_current_header << "rt" << "intensities";
	}
}

void
XicTracesBase::setEndCurrentSearchItem() {
	if ( isToBeTraced() ) {
		_current_header.clear();
		_current_file.close();
		_current_xic_v_int.clear();
		_current_xic_v_rt.clear();
		_current_filters_v_int.clear();
		_current_allpeaks_v_maxintensity.clear();
		privateUnsetTraceItem();
	}
}

void 
XicTracesBase::setXic(const xicBase * xic) {
	if ( isToBeTraced() ) {
		_current_xic_v_rt = xic->getAlignedRetentionTimes();
		_current_xic_v_int = *(xic->getConstIntensities());
	}
}

void 
XicTracesBase::setFilteredXic(const xicBase * xic,
							  const mcq_filter_type & filterType,
							  const unsigned int filterNum) {
	if ( isToBeTraced() ) {
		QString filterInfo = QString("filter_%1").arg(filterType);
		_current_header << filterInfo;
		_current_filters_v_int.push_back(*(xic->getConstIntensities()));
	}
}

void 
XicTracesBase::setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list) {
	if ( isToBeTraced() ) {
		QString allpeaks = QString("all_peaks_max_intensity");    
		_current_header << allpeaks;
		std::vector<mcq_double>::const_iterator it_xic_rt;
		std::vector<xicPeak *>::const_iterator it_peaks;
		for (it_xic_rt = _current_xic_v_rt.begin();
			 it_xic_rt != _current_xic_v_rt.end();
			 ++it_xic_rt) {
			bool found(false); 
     
			for (it_peaks = p_v_peak_list->begin();
				 it_peaks != p_v_peak_list->end();
				 ++it_peaks) {
				if ( *it_xic_rt == (*it_peaks)->get_aligned_max_rt() ) {
					found = true;
					mcq_double max_int = (*it_peaks)->get_max_intensity();
					_current_allpeaks_v_maxintensity.push_back(max_int); 
				}
			}
			if (! found) {
				_current_allpeaks_v_maxintensity.push_back(-1);
			}
		}
	}
}
 
void 
XicTracesBase::setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list) {
	if ( isToBeTraced() ) {
		///finish and print header
		QString sel_peaks = QString("matched_peaks_max_intensity");    
		_current_header << sel_peaks;
		printHeader();
		printTraces(p_v_peak_list);
	}
}

void 
XicTracesBase::setPostMatchingOn() {
}

void 
XicTracesBase::setPostMatchingOff() {
}

void 
XicTracesBase::setPostMatchedPeaks(
	QuantiItemBase * quanti_item, 
	const std::vector<xicPeak *> * p_v_peak_list)
{
	privateUnsetTraceItem();
	if (! p_v_peak_list->empty())
	{
		// looks if the item is to be traced or not and sets it if yes
		setTraceItem(quanti_item);
		if ( isToBeTraced() )
		{
			// get the file corresponding to this quanti_item (we will read and copy/modify it)
			QString filename = setTraceFileName(quanti_item);
			//setCurrentSearchItem(quanti_item);
			std::vector<xicPeak *>::const_iterator it_peaks;
			QFile old_file(filename);
			int columns_number(0);
			if (!old_file.open(QIODevice::ReadOnly))
			{
				old_file.close();
			} else
			{
				QTextStream read_stream(&old_file);
				read_stream.readLine();
				QString line = read_stream.readLine();
				columns_number = line.count("\t");
				old_file.close();
			}
      
			if (!old_file.open(QIODevice::Append))
			{
				old_file.close();
				return;
			}
			else
			{
				QTextStream append_stream(&old_file);
				setLocaleAndPrecisionForStream(&append_stream);
				std::vector<xicPeak *>::const_iterator it_peaks;
				for (it_peaks = p_v_peak_list->begin();
					 it_peaks != p_v_peak_list->end();
					 ++it_peaks)
				{
					mcq_double rt = (*it_peaks)->get_aligned_max_rt();
					QString rt_str;
					rt_str.setNum(rt);
					mcq_double max_int = (*it_peaks)->get_max_intensity();
					append_stream << rt_str;
					do
					{
						append_stream << _sep;
						columns_number--;
					} while (columns_number > 0);
					append_stream << max_int << endl;
				}
				old_file.close();      
			}
		}
	}
	privateUnsetTraceItem();
}

const QString & 
XicTracesBase::formatCell(const mcq_double d) {
	_tmp_element = "";
	_tmp_element_stream << d;
	return (this->formatCell(_tmp_element));
}

const QString & 
XicTracesBase::formatCell(const QString & cell) {
	return (cell);
}

void 
XicTracesBase::printLine(const QStringList & list) {
	*_current_stream << list.join(_sep) << endl;
}

void 
XicTracesBase::printHeader() {
	printLine(_current_header);
}

bool
XicTracesBase::isHeaderLine(const QString & line) const{
	return (line.contains("intensities") &&  
			line.contains("rt"));
}

void
XicTracesBase::printCurrentTitle(QuantiItemBase * quanti_item) { 
	printLine(quanti_item->getTracesTitle(_current_msrun));
}

void
XicTracesBase::printTraces(const std::vector<xicPeak *> * p_v_peak_list) {
	for (unsigned int i = 0; i < _current_xic_v_rt.size(); ++i) {
		// print xic's rt and intensities
		*_current_stream << _current_xic_v_rt[i] << _sep; 
		*_current_stream << _current_xic_v_int[i] << _sep;
    
		// print xic's filtered intensities
		for (unsigned int j = 0; j < _current_filters_v_int.size(); ++j) {
			*_current_stream << _current_filters_v_int[j][i] << _sep;
		}
		// print all peaks (max intensity)
		mcq_double peak_int = _current_allpeaks_v_maxintensity[i];
		if (peak_int != -1) {
			*_current_stream << peak_int;
		}
		*_current_stream << _sep;
		// print matched peaks (max intensity)
		for (unsigned int k = 0; k < p_v_peak_list->size(); ++k) {
			xicPeak & ref_peak(*(p_v_peak_list->at(k)));
			if (_current_xic_v_rt[i] == ref_peak.get_aligned_max_rt()) {
				*_current_stream << ref_peak.get_max_intensity();
			}
			break;
		}
		*_current_stream << endl;
	}
}


void 
XicTracesBase::debriefing() {
}
