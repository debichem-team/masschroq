/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file masschroqWriter.h
 * \date January 03, 2011  
 * \author Edlira Nano
 */

#ifndef MASSCHROQ_WRITER_H_
#define MASSCHROQ_WRITER_H_ 1

#include "monitorBase.h"
#include "mcq_qxmlstreamwriter.h"


/**
 * \class MasschroqWriter
 * \brief masschroqML type of output containing quantification results as well
 * as xic traces

 * This type of result produces a masschroqML result file that contains the XML input file 
 * used to generate these results plus the quantification results and the 
 * xic traces produced if any. The output masschroqML file follows the same schema as 
 * the input masschroqML file.
 */


class MasschroqWriter : public MonitorBase {

public :
  
	MasschroqWriter(const QString & in_filename,
					const QString & out_filename,
					const bool with_traces);
	virtual ~MasschroqWriter();
  
	virtual void setCurrentQuantify(const QString & quantify_id);
  
	virtual void setCurrentGroup(const msRunHashGroup * group); 
	virtual void setEndCurrentGroup(); 
  
	virtual void setCurrentMsrun(const Msrun * msrun); 
	virtual void setEndCurrentMsrun();   

	virtual void setCurrentSearchItem(QuantiItemBase * quanti_item); 
	virtual void setEndCurrentSearchItem();

	virtual void setXic(const xicBase * xic);
  
	virtual void setFilteredXic(const xicBase * xic, 
								const mcq_filter_type & filterType, 
								const unsigned int filterNum);
  
	virtual void setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual void setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list);

	virtual void setPostMatchingOn();

	virtual void setPostMatchedPeaks(QuantiItemBase * quanti_item, 
									 const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual void setPostMatchingOff();

	virtual void debriefing();

protected :

	/// set the _toBeTraced flag's value to b
	void setToBeTraced(const bool b);
  
private :
  
	void writeInputParameters();
  
	void initialiseOutput();

	void printQuantificationData(const std::vector<xicPeak *> * p_v_peak_list);

	void printAlignmentResults(const Msrun * msrun);

	/// counter for the search items, used to generate unique search xml ids
	unsigned int _quanti_item_counter; 
  
	QString _input_filename;
  
	QFile * _output_file;
  
	MCQXmlStreamWriter * _output_stream;
  
	QString _current_group_id;
	QString _current_quantify_id;
	QString _current_msrun_id;
	QString _current_rt_id;
  
	QuantiItemBase * _current_quanti_item;
  
	/// flag representing the boolean masschroqML attribute xic_traces 
	const bool _xic_traces;
  
	/// flag telling if the current quantification item is to be traced or not
	bool _toBeTraced;
  
	/// returns the _toBeTraced flag's value
	bool isToBeTraced() const;

	const unsigned int _enc_precision;
	const QString _enc_little_endian;

	/// hash map : (rt -> max intensity) of all the peaks of the current xic 
	std::map<mcq_double, std::pair<mcq_double, mcq_double> > _current_xic_all_peaks_map;
};

#endif /* MASSCHROQ_WRITER_H_ */
