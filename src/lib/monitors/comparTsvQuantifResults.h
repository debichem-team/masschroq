/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file comparTsvQuantifResults.h
 * \date January 25, 2012
 * \author Benoit Valot
 */

#ifndef COMPAR_TSV_QUANTIF_RESULTS_H_
#define COMPAR_TSV_QUANTIF_RESULTS_H_ 1

#include "../msrun/ms_run_hash_group.h"
#include "../peptides/peptide_isotope.h"
#include "quantifResultsBase.h"

#include <iostream>
#include <QStringList>
#include <QFile>
#include <QTextStream>
//#include <QTime>

/**
 * \class ComparTsvQuantifResults
 * \brief Tab Separated Values type of peptide quantification results well suited for result
 * comparison
 */

class ComparTsvQuantifResults : public QuantifResultsBase {

public:

	ComparTsvQuantifResults();
	ComparTsvQuantifResults(const QString & filename);

	virtual ~ComparTsvQuantifResults();

	virtual void debriefing();

protected:

	/// pure virtual methods
	virtual void setOutputFilesAndStreams(const QString & filename);
	virtual void setLocaleAndPrecisionForAllStreams();

	
	/// override methods on QuantifResultsBase
	virtual void setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list);

	/// some printing and formating methods
	virtual void printHeaders();
	virtual void printLine(const QStringList & list);

	/// Output file for compar results
	QFile * _compar_output_file;
	
	//stored data
	/// hash map : peptide-z id -> map[sample id -> area]
	std::map< QString, std::map<QString, QString> > _map_peptide_quantification;
	/// hash map : peptide-z id -> protein id's separated by a space 
	std::map<QString, QStringList> _map_peptide_description;
	/// list of all the sample id-s
	QStringList _sampleIds;

private :

	/// The streams corresponding to the output file
	QTextStream * _compar_output_stream;

	/// Unique key identifying the sample (sample id)
	QString _current_sample_id;
};

#endif /* COMPAR_TSV_QUANTIF_RESULTS_H_ */
