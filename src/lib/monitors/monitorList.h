/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file monitorList.h
 * \date September 21, 2010  
 * \author Edlira Nano
 */

#ifndef MONITORLIST_H_
#define MONITORLIST_H_ 1

#include "monitorBase.h"

/**
 * \class MonitorList
 * \brief class representing a list of results for MassChroQ, derives from 
 * MonitorBase
 *
 * A private vector _p_v_monitors contains the MonitorBase results 
 * wanted for QuantiMsCpp. For ex. if the user wants quantification results 
 * both in gnumeric form, rdataset form and xic traces to validate them, he 
 * will put in the XML input file the following sequence :
 * <xic_extractor_result>
 * <tsv_all_xics output_dir="xics_traces"/>
 * <rdataset output_file="test_scan_align.rdataset"/>
 * <gnumeric output_file="test_scan_align.gnumeric"/>
 * </xic_extractor_result>
 * The private vector _p_v_monitors will then contain a TsvAllXics object, 
 * followed by a quantifResultsRdataset object, followed by a 
 * quantifResultsGnumeric object. All the calculus : xics extraction, 
 * peaks detection, quantification ... will be done only once for all 
 * these results (see how the virtual methods set...() are implemented here).  
 */


class MonitorList : public MonitorBase {

public :
  
	MonitorList();
  
	virtual ~MonitorList();
  
	virtual void setCurrentQuantify(const QString & quantify_id);
  
	virtual void setCurrentGroup(const msRunHashGroup * group); 
	virtual void setEndCurrentGroup(); 

	virtual void setCurrentMsrun(const Msrun * msrun); 
	virtual void setEndCurrentMsrun();   

	virtual void setCurrentSearchItem(QuantiItemBase * quanti_item); 
	virtual void setEndCurrentSearchItem();

	virtual void setXic(const xicBase * xic);
  
	virtual void setFilteredXic(const xicBase * xic, 
								const mcq_filter_type & filterType,
								const unsigned int filterNum);
      
	virtual void setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual void setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list);
 
	virtual void setPostMatchingOn();
  
	virtual void setPostMatchingOff();
  
	virtual void setPostMatchedPeaks(QuantiItemBase * quanti_item, 
									 const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual void debriefing();

	void addMonitor(MonitorBase * monitor);

private :
  
	std::vector<MonitorBase * > _p_v_monitors;
  
};

#endif /* MONITORLIST_H_ */
