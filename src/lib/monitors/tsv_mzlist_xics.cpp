/**
 * \file tsv_mzlist_xics.cpp
 * \date September 29, 2010 
 * \author Edlira Nano
 */

#include "tsv_mzlist_xics.h"

TsvMzListXics::TsvMzListXics(const QString output_dir)
	: 
	XicTracesBase(output_dir)
{
	setToBeTraced(false);
}

TsvMzListXics::~TsvMzListXics()
{
}

void
TsvMzListXics::addMz(const mcq_double mz)
{
	_v_mz_values.push_back(mz);
}

void 
TsvMzListXics::setTraceItem(QuantiItemBase * quanti_item)
{
	if (quanti_item->getType() == MZ_QUANTI_ITEM)
	{
		mcq_double current_mz(quanti_item->get_mz());
		std::vector<mcq_double>::const_iterator it;
		for (it = _v_mz_values.begin();
			 it != _v_mz_values.end();
			 ++it)
		{
			if (*it == current_mz)
			{
				/// tracing flag becomes true
				setToBeTraced(true);
				return;
			}
		}
	}
}


void 
TsvMzListXics::privateUnsetTraceItem()
{
	setToBeTraced(false);
}
