/**
 * \file comparTsvQuantifResults.h
 * \date January 25, 2012
 * \author Benoit Valot
 */

#include "comparTsvQuantifResults.h"
#include <QDebug>
#include "../consoleout.h"

ComparTsvQuantifResults::ComparTsvQuantifResults()
	:
	QuantifResultsBase()
	
{
	_compar_output_stream = 0;
}

ComparTsvQuantifResults::ComparTsvQuantifResults(const QString & filename)
	: QuantifResultsBase(filename)
{
	setOutputFilesAndStreams(filename);
}

ComparTsvQuantifResults::~ComparTsvQuantifResults()
{
	if (_compar_output_stream != 0)
	{
		delete _compar_output_stream;
		_compar_output_stream = 0;
	}

	if (_compar_output_file != 0)
	{
		_compar_output_file->close();
		delete _compar_output_file;
		_compar_output_file = 0;
	}
}

void
ComparTsvQuantifResults::setOutputFilesAndStreams(const QString & filename)
{
	QString compar_filename = filename;
	compar_filename.append("_compar").append(".tsv");

	_compar_output_file = new QFile(compar_filename);
	const QString compar_file = _compar_output_file->fileName();


	if (_compar_output_file->exists())
	{
		mcqout() << "WARNING : Compar TSV output file '"
			 << compar_file
			 << "' already exists, it will be overwrited."
			 << endl;
	}

	if (_compar_output_file->open(QIODevice::WriteOnly))
	{
		_compar_output_stream = new QTextStream(_compar_output_file);
	}
	else
	{
		throw mcqError(QObject::tr("cannot open the compar output file '%1' \n").arg(compar_file));
	}


	mcqout() << "Writing comparision quantification results in TSV output file : '"
		 << compar_file		 << "'" << endl;

	setLocaleAndPrecisionForAllStreams();
}

void
ComparTsvQuantifResults::setLocaleAndPrecisionForAllStreams()
{
#if QT_VERSION >= QT_V_4_5
	_tmp_element_stream.setLocale(QLocale::system());
	_compar_output_stream->setLocale(QLocale::system());
#endif
	_tmp_element_stream.setRealNumberPrecision(8);
	_tmp_element_stream.setRealNumberNotation(QTextStream::SmartNotation);
	_compar_output_stream->setRealNumberPrecision(8);
	_compar_output_stream->setRealNumberNotation(QTextStream::SmartNotation);
}

void
ComparTsvQuantifResults::setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list)
{
	_current_sample_id.clear();
	const Peptide * p_peptide(_current_quanti_item->getPeptide());
	// we only print peptide information in the compar file
	if  (p_peptide != NULL)
	{		
	  std::vector<xicPeak *>::const_iterator itp;
		QString pepId, pepSequence, tempArea, currentZ, protIds, pepMods;
		
		currentZ.setNum(getCurrentZ());
		pepSequence = p_peptide->getSequence();
		pepMods = p_peptide->getMods();

		// get the current sample id and add it to the _sampleIds list 
		_current_sample_id = _current_quantify_id;
		_current_sample_id.append("_").append(_current_group_id);
		_current_sample_id.append("_").append(_current_msrun_id);
		const IsotopeLabel * isotope = p_peptide->getIsotopeLabel();
		if (isotope != NULL)
		{
			QString isotope_label = isotope->getXmlId();
			_current_sample_id.append("_").append(isotope_label);
		}
		if ( !_sampleIds.contains(_current_sample_id) )
		{
			_sampleIds << _current_sample_id;
		}
		
		// construct the pepId (key of _map_peptide_quantification)
		pepId = p_peptide->getXmlId();
		pepId.append("_").append(currentZ);

		// construct the print information for every detected peak
		for (itp = p_v_peak_list->begin(); itp != p_v_peak_list->end();	 ++itp)
		{
			// first get the area and add it to the _map_peptide_quantification
			// if no value is found for current sample ID, put an empty string (very important)
			// if more than one values are found, append them
			
			tempArea = _map_peptide_quantification[pepId][_current_sample_id];
			mcq_double area((*itp)->get_area());
			if ( ! tempArea.isEmpty())
			{
				tempArea.append("|").append(formatCell(area));
			}
			else
			{
				tempArea = formatCell(area);
			}
			_map_peptide_quantification[pepId][_current_sample_id] = tempArea;

			// print information
			QStringList list;
			list << formatCell(p_peptide->getXmlId());
			list << formatCell(_current_quanti_item->get_mz());
			list << formatCell(currentZ);
			list << formatCell(pepSequence);
			list << formatCell(pepMods);
			
			std::vector <const Protein *> prots = p_peptide->getProteinList();
			std::vector <const Protein *>::iterator it;
			for (it = prots.begin(); it != prots.end();	++it)
			{
				protIds.append((*it)->getXmlId()).append(" ");
			}
			
			list << formatCell(protIds);
			_map_peptide_description[pepId] = list;
		}
	}
}


void
ComparTsvQuantifResults::debriefing()
{
	/// print results
	this->printHeaders();

	std::map<QString, std::map<QString, QString> >::const_iterator itpep;
	std::map<QString, QStringList>::const_iterator itfind;
	
	for (itpep = _map_peptide_quantification.begin();
		 itpep != _map_peptide_quantification.end();
		 ++itpep)
	{
		QStringList list;
		QString pepId = itpep->first;
		
		itfind =  _map_peptide_description.find(pepId);
		if (itfind != _map_peptide_description.end())
		{
			list << itfind->second;	
		}	

		QStringList::const_iterator sampleIterator;
		for (sampleIterator = _sampleIds.constBegin();
			 sampleIterator != _sampleIds.constEnd();
			 ++sampleIterator)
		{
			QString sampId = (*sampleIterator);
			QString tmpArea = _map_peptide_quantification[pepId][sampId];
			list << formatCell(tmpArea);
		}
		
		printLine(list);
	}
}

void
ComparTsvQuantifResults::printHeaders()
{
	QStringList list;
	list << formatCell("peptide");
	list << formatCell("m/z");
	list << formatCell("z");
	list << formatCell("sequence");
	list << formatCell("mods");
	list << formatCell("proteins");
	list << _sampleIds;
	printLine(list);
}

void
ComparTsvQuantifResults::printLine(const QStringList & list)
{
	*_compar_output_stream << list.join(_sep) << _endl;
}
