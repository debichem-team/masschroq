/**
 * \file quantif_results_tsv.cpp
 * \date Octover 03, 2010
 * \author Edlira Nano
 */

#include "tsv_quantif_results.h"
#include <QDir>
#include "../consoleout.h"


/** 
    Constructor that takes the output filename as  parameter.
    The filename is verified.
*/

TsvQuantifResults::TsvQuantifResults()
	: QuantifResultsBase()
  
{
	_pep_output_stream = 0;
	_prot_output_stream = 0;
}

TsvQuantifResults::TsvQuantifResults(const QString & filename)
	: QuantifResultsBase(filename)
{
	setOutputFilesAndStreams(filename);
}

TsvQuantifResults::~TsvQuantifResults()
{
	if (_pep_output_stream != 0)
	{
		delete _pep_output_stream;
		_pep_output_stream = 0;
	}

	if (_prot_output_stream != 0)
	{
		delete _prot_output_stream;
		_prot_output_stream = 0;
	}
  
	if (_pep_output_file != 0)
	{
		_pep_output_file->close();
		delete _pep_output_file;
		_pep_output_file = 0;
	}
  
	if (_prot_output_file != 0)
	{
		_prot_output_file->close();
		delete _prot_output_file;
		_prot_output_file = 0;
	}
}

void
TsvQuantifResults::setOutputFilesAndStreams(const QString & filename)
{
	QString pep_filename = filename, prot_filename = filename;
	pep_filename.append("_pep").append(".tsv");
	prot_filename.append("_prot").append(".tsv");
  
	_pep_output_file = new QFile(pep_filename);
	_prot_output_file = new QFile(prot_filename);
  
	const QString pep_file = _pep_output_file->fileName();
	const QString prot_file = _prot_output_file->fileName();
  

	if (_pep_output_file->exists())
	{
		mcqout() << "WARNING : Peptides TSV output file '"
			 << pep_file
			 << "' already exists, it will be overwrited." 
			 << endl;
	}
	if (_prot_output_file->exists())
	{
		mcqout() << "WARNING : Proteins TSV output file '"
			 << prot_file
			 << "' already exists, it will be overwrited." 
			 << endl;
	}
	if (_pep_output_file->open(QIODevice::WriteOnly))
	{
		_pep_output_stream = new QTextStream(_pep_output_file);
	}
	else
	{
		throw mcqError(QObject::tr("cannot open the peptides output file '%1' \n").arg(pep_file));
	}
  
	if (_prot_output_file->open(QIODevice::WriteOnly))
	{
		_prot_output_stream = new QTextStream(_prot_output_file);
	} else
	{
		throw mcqError(QObject::tr("cannot open the proteins output file '%1' \n").arg(prot_file));
	}
  
  
	mcqout() << "Writing quantification results in TSV output files : '" 
		 << pep_file << "' and '" << prot_file 
		 << "'" << endl;
 
  
	setLocaleAndPrecisionForAllStreams();
  	printHeaders();
}

void
TsvQuantifResults::setLocaleAndPrecisionForAllStreams()
{
#if QT_VERSION >= QT_V_4_5 
	_tmp_element_stream.setLocale(QLocale::c());
	_pep_output_stream->setLocale(QLocale::c());
	_prot_output_stream->setLocale(QLocale::c());
#endif   
	_tmp_element_stream.setRealNumberPrecision(8); 
	_tmp_element_stream.setRealNumberNotation(QTextStream::SmartNotation);
	_pep_output_stream->setRealNumberPrecision(8);
	_pep_output_stream->setRealNumberNotation(QTextStream::SmartNotation);
	_prot_output_stream->setRealNumberPrecision(8);
	_prot_output_stream->setRealNumberNotation(QTextStream::SmartNotation);
}

void
TsvQuantifResults::printHeaders()
{
	QStringList list;
  
	// print peptides' file header
	foreach (QString str, getPepHeaders())
	{
		list << formatCell(str);
	}
	printPepLine(list);
  
	// print proteins' file header
	list.clear();
	foreach (QString str, getProtHeaders())
	{
		list << formatCell(str);
	}
	printProtLine(list);
}


void 
TsvQuantifResults::printPepLine(const QStringList & list)
{
	*_pep_output_stream << list.join(_sep) << _endl;
}

void
TsvQuantifResults::printLine(const QStringList & list)
{
	this->printPepLine(list);
}

void 
TsvQuantifResults::printProtLine(const QStringList & list)
{
	*_prot_output_stream << list.join(_sep) << _endl;
}

void 
TsvQuantifResults::debriefing()
{
	// print proteins
	QStringList list;
	std::map<QString, std::map<QString, QString> >::const_iterator itpep;
	for (itpep = _map_peptide_proteins.begin(); 
		 itpep != _map_peptide_proteins.end(); 
		 ++itpep)
	{
		std::map<QString, QString>::const_iterator itprot;
		for (itprot = itpep->second.begin(); 
			 itprot != itpep->second.end(); 
			 ++itprot)
		{
			list.clear();
			list << formatCell(itpep->first);
			list << formatCell(itprot->first);
			list << formatCell(itprot->second);
			printProtLine(list);
		}
	}
}


