/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file tsv_quantif_results.h
 * \date October 03, 2010
 * \author Edlira Nano
 */

#ifndef TSV_QUANTIF_RESULTS_H_
#define TSV_QUANTIF_RESULTS_H_ 1

#include "../msrun/ms_run_hash_group.h"
#include "../peptides/peptide_isotope.h"
#include "quantifResultsBase.h"

#include <iostream>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QTime>

/**
 * \class TsvQuantifResults
 * \brief Tab Separated Values type of quantification results 
 */

class TsvQuantifResults : public QuantifResultsBase {

 public:

  TsvQuantifResults();
  TsvQuantifResults(const QString & filename);

  virtual ~TsvQuantifResults();
  
  virtual void debriefing();
  
 protected:
  
  virtual void setOutputFilesAndStreams(const QString & filename); 
  
  virtual void setLocaleAndPrecisionForAllStreams();

  /// some printing and formating methods

  virtual void printHeaders();
  
  virtual void printPepLine(const QStringList & list);
  
  virtual void printProtLine(const QStringList & list);

  virtual void printLine(const QStringList & list);
  
  /// Two output files will be created, one for the quantified peptide results
  /// and one for the quantified protein results
  QFile * _pep_output_file;
  QFile * _prot_output_file;
 
 private :

  /// The streams corresponding to each output file
  QTextStream * _pep_output_stream;
  QTextStream * _prot_output_stream;

};

#endif /* TSV_QUANTIF_RESULTS_H_ */
