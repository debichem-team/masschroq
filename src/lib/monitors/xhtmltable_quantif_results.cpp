/**
 * \file quantif_results_xhtmltable.cpp
 * \date 16 janv. 2010
 * \author Olivier Langella
 */

#include "xhtmltable_quantif_results.h"
#include "../consoleout.h"


/** 
    Constructor that takes the _output_file filename as  parameter.
    Calls the base quantifResultsRdataset default constructor.
    The filename is verified.
 */
XhtmltableQuantifResults::XhtmltableQuantifResults(const QString & filename)
:
  TsvQuantifResults()
{
  QFileInfo outFileInfo(filename);
  QString outDir(outFileInfo.path());
  QFileInfo outDirInfo(outDir);
  if (! outDirInfo.exists()) {
    throw mcqError(QObject::tr("cannot find the output directory '%1' of output file '%2' \n").arg(outDir, filename));
  } 
  
  setOutputFilesAndStreams(filename);
}

void
XhtmltableQuantifResults::setOutputFilesAndStreams(const QString & filename) {
  
  QString pep_filename = filename, prot_filename = filename;
  pep_filename.append("_pep").append(".xhtmltable");
  prot_filename.append("_prot").append(".xhtmltable");
  
  _pep_output_file = new QFile(pep_filename);
  _prot_output_file = new QFile(prot_filename);
  
  const QString pep_file = _pep_output_file->fileName();
  const QString prot_file = _prot_output_file->fileName();
  
  if (_pep_output_file->exists()) {
    mcqout() << "WARNING : Peptides XHTML output file '"
	 << pep_file
	 << "' already exists, it will be overwrited." 
	 << endl;
  }
  if (_prot_output_file->exists()) {
    mcqout() << "WARNING : Proteins XHTML output file '"
	 << prot_file
	 << "' already exists, it will be overwrited." 
	 << endl;
  }
  if (_pep_output_file->open(QIODevice::WriteOnly)) {
    _pep_xml_stream = new QXmlStreamWriter(_pep_output_file);
    
  } else {
    throw mcqError(QObject::tr("cannot open the peptides output file '%1' \n").arg(pep_file));
  }
  if (_prot_output_file->open(QIODevice::WriteOnly)) {
    _prot_xml_stream = new QXmlStreamWriter(_prot_output_file);
  } else {
    throw mcqError(QObject::tr("cannot open the proteins output file '%1' \n").arg(prot_file));
  }

  mcqout() << "Writing quantification results in XHTML output files : '" 
       << pep_file << "' and '" << prot_file 
       << "'" << endl;
 
  setLocaleAndPrecisionForAllStreams();
  
  printHeaders();
}

void
XhtmltableQuantifResults::setLocaleAndPrecisionForAllStreams() {
#if QT_VERSION >= QT_V_4_5 
  _tmp_element_stream.setLocale(QLocale::c());
#endif   
  _tmp_element_stream.setRealNumberPrecision(8); 
  _tmp_element_stream.setRealNumberNotation(QTextStream::SmartNotation);
}

XhtmltableQuantifResults::~XhtmltableQuantifResults() {

  if (_pep_xml_stream != 0) {
    _pep_xml_stream->writeEndDocument();
    delete _pep_xml_stream;
    _pep_xml_stream = 0;
  }
  
  if (_pep_output_file != 0) {
    _pep_output_file->close();
    delete _pep_output_file;
    _pep_output_file = 0;
  }
  
  if (_prot_xml_stream != 0) {
    _prot_xml_stream->writeEndDocument();
    delete _prot_xml_stream;
    _prot_xml_stream = 0;
  }
  
  if (_prot_output_file != 0) {
    _prot_output_file->close();
    delete _prot_output_file;
    _prot_output_file = 0;
  }
}

void 
XhtmltableQuantifResults::printHeaders() {
  printXmlHeaders();
 
  QStringList list;
  foreach (QString str, getPepHeaders()) {
    list << formatCell(str);
  }
  printPepLine(list);

  list.clear();
  foreach (QString str, getProtHeaders()){
    list << formatCell(str);
  }
  printProtLine(list);
}

void 
XhtmltableQuantifResults::printXmlHeaders() {
  _pep_xml_stream->setAutoFormatting(true);
  _pep_xml_stream->writeStartDocument();
  _pep_xml_stream->writeStartElement("xhtml");
  _pep_xml_stream->writeDefaultNamespace("http://www.w3.org/1999/xhtml");
  _pep_xml_stream->writeAttribute("xml", "lang", "en");
  _pep_xml_stream->writeStartElement("body");
  _pep_xml_stream->writeStartElement("table");
  _pep_xml_stream->writeStartElement("tbody");

  _prot_xml_stream->setAutoFormatting(true);
  _prot_xml_stream->writeStartDocument();
  _prot_xml_stream->writeStartElement("xhtml");
  _prot_xml_stream->writeDefaultNamespace("http://www.w3.org/1999/xhtml");
  _prot_xml_stream->writeAttribute("xml", "lang", "en");
  _prot_xml_stream->writeStartElement("body");
  _prot_xml_stream->writeStartElement("table");
  _prot_xml_stream->writeStartElement("tbody");
}

void 
XhtmltableQuantifResults::printPepLine(const QStringList & list) {
  _pep_xml_stream->writeStartElement("tr");
  foreach (QString str, list) {
    _pep_xml_stream->writeTextElement("td", str );
  }
  _pep_xml_stream->writeEndElement();
}

void 
XhtmltableQuantifResults::printProtLine(const QStringList & list) {
  _prot_xml_stream->writeStartElement("tr");
  foreach (QString str, list) {
    _prot_xml_stream->writeTextElement ( "td", str );
  }
  _prot_xml_stream->writeEndElement();
}

void
XhtmltableQuantifResults::printLine(const QStringList & list) {
  this->printPepLine(list);
}

void 
XhtmltableQuantifResults::debriefing() {
  // finalize the peptides file
  _pep_xml_stream->writeEndDocument();
  _pep_output_file->close();
  // print proteins
  QStringList list;
  std::map<QString, std::map<QString, QString> >::const_iterator itpep;
  for (itpep = _map_peptide_proteins.begin(); 
       itpep != _map_peptide_proteins.end(); 
       ++itpep) {
    
    std::map<QString, QString>::const_iterator itprot;
    for (itprot = itpep->second.begin(); 
	 itprot != itpep->second.end(); 
	 ++itprot) {
      list.clear();
      list << formatCell(itpep->first);
      list << formatCell(itprot->first);
      list << formatCell(itprot->second);
      printProtLine(list);
    }
  }
    _prot_xml_stream->writeEndDocument();
    _prot_output_file->close();
}
