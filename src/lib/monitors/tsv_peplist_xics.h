/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file tsv_peplist_xics.h
 * \date September 23, 2010 
 * \author Edlira Nano
 */

#ifndef TSV_PEPLIST_XICS_H_
#define TSV_PEPLIST_XICS_H_ 1

#include "xicTracesBase.h"

class PeptideList;

/**
   \class TsvPepListXics
   \brief Type of result containing tab separated values of
   the xics for a given peptide list.

   The results will be put in files created in the _output_dir member. 
   One file per peptide will be created.
*/

class TsvPepListXics : public XicTracesBase {
  
 public:
  
  TsvPepListXics(const QString output_dir);
  
  virtual ~TsvPepListXics();

  void addPeptide(const QString & pepId);    

 protected : 

  virtual void setTraceItem(QuantiItemBase * quanti_item);
  
  virtual void privateUnsetTraceItem();
    
 private:

   std::vector<QString> _peptide_list;
  
  bool containsPeptide(const QString & pepId) const;

  QString _current_pep_id;

};

#endif /* TSV_PEPLIST_XICS_H_ */
