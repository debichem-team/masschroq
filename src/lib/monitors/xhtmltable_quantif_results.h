/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xhtmltable_quantif_results.h
 * \date 16 janv. 2010
 * \author Olivier Langella
 */

#ifndef XHTMLTABLE_QUANTIF_RESULTS_H_
#define XHTMLTABLE_QUANTIF_RESULTS_H_ 1

#include "tsv_quantif_results.h"
#include <QXmlStreamWriter>

/**
 * \class XhtmltableQuantifResults
 * \brief Xhtml table type of quantification results
 */

class XhtmltableQuantifResults : public TsvQuantifResults {
  
 public:
  
  XhtmltableQuantifResults(const QString & filename);
  
  virtual ~XhtmltableQuantifResults();
  
  //  virtual void debriefing();
  
 protected:

  virtual void setOutputFilesAndStreams(const QString & filename); 
  
  virtual void setLocaleAndPrecisionForAllStreams();
  
  virtual void printHeaders();
  
  virtual void printPepLine(const QStringList & list);
  
  virtual void printProtLine(const QStringList & list);
    
  virtual void printLine(const QStringList & list);
  
  virtual void debriefing();

 private :
 
  virtual void printXmlHeaders();
  
  QXmlStreamWriter * _pep_xml_stream;
  QXmlStreamWriter * _prot_xml_stream;
  
};

#endif /* XHTMLTABLE_QUANTIF_RESULTS_H_ */
