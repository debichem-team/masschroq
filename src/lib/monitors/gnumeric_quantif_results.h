/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file gnumeric_quantif_results.h
 * \date February 1, 2010
 * \author Olivier Langella
 */

#ifndef GNUMERIC_QUANTIF_RESULTS_H_
#define GNUMERIC_QUANTIF_RESULTS_H_ 1

#include "quantifResultsBase.h"
#include "QXmlStreamWriter"

/**
 * \class GnumericQuantifResults
 * \brief Gnumeric type of quantification results
 */

class GnumericQuantifResults : public QuantifResultsBase {
  
 public:
  
  GnumericQuantifResults(const QString & filename);
  
  virtual ~GnumericQuantifResults();

  virtual void debriefing();

 protected:
  
  void setOutputFilesAndStreams(const QString & filename);

  virtual void setLocaleAndPrecisionForAllStreams();

  virtual void printHeaders();
  
  void printLine(const QStringList & list);
  
  int irow;
  
 private :

  QFile * _output_file;
  
  QXmlStreamWriter * _xml_stream;
  
};

#endif /* GNUMERIC_QUANTIF_RESULTS_H_ */
