/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file xicTracesBase.h
 * \date September 28, 2010  
 * \author Edlira Nano
 */

#ifndef XICTRACESBASE_H_
#define XICTRACESBASE_H_ 1

#include "monitorBase.h"
#include <QDir>

class msRunHashGroup;
class xicBase;

/**
 * \class XicTracesBase
 * \brief Absract class representing the xic traces produced during 
 quantification 
*/

class XicTracesBase : public MonitorBase {
  
public :
  
	XicTracesBase(const QString & output_dir);
	virtual ~XicTracesBase();

	virtual void setCurrentQuantify(const QString & quantify_id);

	virtual void setCurrentGroup(const msRunHashGroup * group); 
	virtual void setEndCurrentGroup(); 

	virtual void setCurrentMsrun(const Msrun * msrun); 
	virtual void setEndCurrentMsrun();   

	virtual void setCurrentSearchItem(QuantiItemBase * quanti_utem);
	virtual void setEndCurrentSearchItem();

	virtual void setXic(const xicBase * xic);
  
	virtual void setFilteredXic(const xicBase * xic, 
								const mcq_filter_type & filterType,
								const unsigned int filterNum);
  
	virtual void setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list);

	virtual void setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list);

	virtual void setPostMatchingOn();

	virtual void setPostMatchedPeaks(QuantiItemBase * quanti_item, 
									 const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual void setPostMatchingOff();

	virtual void debriefing();

	virtual void cleanUp(){};
  
protected :

	/// flag telling if the current quanti item is to be traced or not
	bool _toBeTraced;
  
	/// returns the _toBeTraced flag's value
	bool isToBeTraced() const;
  
	/// set the _toBeTraced flag's value to b
	void setToBeTraced(const bool b);
  
	virtual void setLocaleAndPrecisionForAllStreams();

	virtual void setLocaleAndPrecisionForStream(QTextStream * stream);

	/// pure virtual method that set's the items to be traced 
	/// in each derived class (peptides, masses ou mzrt items) 
	virtual void setTraceItem(QuantiItemBase * quanti_item) = 0;
  

	/// set the filename of the current traces file
	virtual const QString setTraceFileName(QuantiItemBase * quanti_item);
  
	/// set and print the title of columns/rows of the traces file
	virtual void printCurrentTitle(QuantiItemBase * quanti_item);
 
	virtual void privateUnsetTraceItem() = 0;
  
	/// prints the xics' and peaks' information on the trace file
	void printTraces(const std::vector<xicPeak *> * p_v_peak_list);
  
	virtual const QString & formatCell(const mcq_double f);
  
	virtual const QString & formatCell(const QString & cell);
  
	/// given a list of QStrings format and print it on the _current_stream
	virtual void printLine(const QStringList & list);

	virtual bool isHeaderLine(const QString & line) const;
  
	/// prints a _current_header on _current_stream
	void printHeader();
  
	/// temporary variables needed for data formating
	QString _tmp_element;
	QTextStream _tmp_element_stream;
	QString _sep;  
  
	/// the traces output directory name
	QString _output_dir_name;
  
	/// the current output file
	QFile _current_file;
  
	/// the current stream associated to the current output file
	QTextStream * _current_stream;
  
	/// current group
	QString _current_quantify_id;
  
	/// current group
	QString _current_group_id;
  
	/// current msrun
	const Msrun * _current_msrun;
  
	/// the header of the current output file 
	QStringList _current_header; 
  
	/// the current xic's retention times and intensities
	std::vector<mcq_double> _current_xic_v_rt;
	std::vector<mcq_double> _current_xic_v_int;
  
	/// the current xic's intensities after filter application, placed
	/// in a vector in the order of filters' application
	std::vector< std::vector<mcq_double> > _current_filters_v_int; 

	std::vector<mcq_double> _current_allpeaks_v_maxintensity;

};
#endif /* XICTRACESBASE_H_ */
