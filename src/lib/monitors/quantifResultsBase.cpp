/**
 * \file quantifResultsBase.cpp
 * \date October 05, 2010
 * \author Edlira Nano
 */

#include "quantifResultsBase.h"
#include "../msrun/ms_run_hash_group.h"
#include "../peptides/peptide_isotope.h"


QuantifResultsBase::QuantifResultsBase()
	: 
	_tmp_element_stream(&_tmp_element),
	_sep("\t"),
	_endl("\n")
{
}
  

QuantifResultsBase::QuantifResultsBase(const QString & filename) 
	: 
	_tmp_element_stream(&_tmp_element),
	_sep ("\t"),
	_endl("\n")
{
	QFileInfo outFileInfo(filename);
	QString outDir(outFileInfo.path());
	QFileInfo outDirInfo(outDir);
	if (! outDirInfo.exists())
	{
		throw mcqError(QObject::tr("cannot find the output directory '%1' of output file '%2' \n").arg(outDir, filename));
	} 
}

QuantifResultsBase::~QuantifResultsBase()
{
}

const QStringList 
QuantifResultsBase::getPepHeaders() const
{
	QStringList tmp_list;
	tmp_list << "group";
	tmp_list << "msrun";
	tmp_list << "msrunfile";
	tmp_list << "mz" << "rt" << "maxintensity" 
			 << "area" << "rtbegin" << "rtend";
	tmp_list << "peptide";
	tmp_list << "isotope";
	tmp_list << "sequence" ;
	tmp_list  << "z";
	tmp_list  << "mods";
	return (tmp_list);
}

const QStringList
QuantifResultsBase::getProtHeaders() const
{
	QStringList tmp_list;
	tmp_list << "peptide";
	tmp_list << "protein";
	tmp_list << "protein_description";
	return (tmp_list);
}

const QString & 
QuantifResultsBase::formatCell(const mcq_double d)
{
	_tmp_element = "";
	_tmp_element_stream << d;
	return (this->formatCell(_tmp_element));
}


const QString & 
QuantifResultsBase::formatCell(const QString & cell)
{
	return (cell);
}

const QTime 
QuantifResultsBase::seconds2DateTime(const mcq_double s) const
{
	QTime res(0, 0, 0);
	int msecs(s * 1000);
	res = res.addMSecs(msecs);
	return res;
}

void
QuantifResultsBase::setCurrentQuantify(const QString & quantify_id) {
	_current_quantify_id = quantify_id;
}

void 
QuantifResultsBase::setCurrentGroup(const msRunHashGroup * group)
{
	_current_group_id = group->getXmlId();
}

void 
QuantifResultsBase::setEndCurrentGroup()
{
	_current_group_id.clear();
}

void 
QuantifResultsBase::setCurrentMsrun(const Msrun * msrun)
{
	_current_msrun_id = msrun->getXmlId();
	_current_msrun_file = (msrun->getXmlFileInfo()).baseName();
}

void 
QuantifResultsBase::setEndCurrentMsrun()
{
	_current_msrun_id.clear();
	_current_msrun_file.clear();
}

void 
QuantifResultsBase::setCurrentSearchItem(QuantiItemBase * quanti_item)
{
	_current_quanti_item = quanti_item;
}

void 
QuantifResultsBase::setEndCurrentSearchItem()
{
}

void
QuantifResultsBase::setXic(const xicBase * xic)
{
	// nothing to do here
}

void 
QuantifResultsBase::setFilteredXic(	const xicBase * xic,
									const mcq_filter_type & filterType,
									const unsigned int filterNum)
{
	// nothing to do here
}

void 
QuantifResultsBase::setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list)
{
	// nothing to do here
}

// print results
void 
QuantifResultsBase::setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list)
{
 
    // print one quantification result line per matched peak
    std::vector<xicPeak *>::const_iterator itp;
    for (itp = p_v_peak_list->begin();
		 itp != p_v_peak_list->end();
		 ++itp)
	{
		QStringList list;
		list << formatCell(_current_group_id);
		list << formatCell(((*itp)->getMsrun())->getXmlId());
		list << formatCell((((*itp)->getMsrun())->getXmlFileInfo()).baseName());
		mcq_double currentMz = _current_quanti_item->get_mz();  
		list << formatCell(currentMz);
		list << formatCell((*itp)->get_aligned_max_rt());
		list << formatCell((*itp)->get_max_intensity());
		list << formatCell((*itp)->get_area());
		list << formatCell((*itp)->get_aligned_first_rtime());
		list << formatCell((*itp)->get_aligned_last_rtime());
            
      
		const Peptide * p_peptide(_current_quanti_item->getPeptide());
		QString pepId(""), isotope_label(""), pepSequence(""), currentZ, pepMods;
   
		if  (p_peptide != NULL)
		{
			pepId = p_peptide->getXmlId();
			const IsotopeLabel * isotope = p_peptide->getIsotopeLabel();
			if (isotope != NULL)
			{
				isotope_label = isotope->getXmlId();
			}
			pepSequence = p_peptide->getSequence();
			pepMods = p_peptide->getMods();
			currentZ.setNum(getCurrentZ());
			std::vector <const Protein *> prots = p_peptide->getProteinList();
			unsigned int prot_size = prots.size();
			//copy this peptide's proteins information in the member map
			if  (prot_size > 0)
			{
				QString protId, protDescription;
				std::vector <const Protein *>::iterator it;
				for (it = prots.begin(); 
					 it != prots.end();
					 ++it)
				{
					protId = (*it)->getXmlId();
					protDescription = (*it)->getDescription();
					_map_peptide_proteins[pepId][protId] = protDescription; 
				}
			}
		}
		list << formatCell(pepId);
		list << formatCell(isotope_label);
		list << formatCell(pepSequence);
		list << formatCell(currentZ);
		list << formatCell(pepMods);
		printLine(list);
    }
}

void 
QuantifResultsBase::setPostMatchingOn()
{
}

void 
QuantifResultsBase::setPostMatchingOff()
{
}

void 
QuantifResultsBase::setPostMatchedPeaks(
	QuantiItemBase * quanti_item, 
	const std::vector<xicPeak *> * p_v_peak_list)
{
	setCurrentSearchItem(quanti_item);
	setMatchedPeaks(p_v_peak_list);
	setEndCurrentSearchItem();
}

void
QuantifResultsBase::debriefing()
{
}
