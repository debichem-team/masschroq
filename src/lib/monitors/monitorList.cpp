/**
 * \file monitorList.cpp
 * \date September 22, 2010  
 * \author Edlira Nano
 */

#include "monitorList.h"
#include <vector>

MonitorList::MonitorList() {
	_p_v_monitors.resize(0);
}

MonitorList::~MonitorList() {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		delete *it;
		*it = 0;
	}
}

void
MonitorList::setCurrentQuantify(const QString & quantify_id) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setCurrentQuantify(quantify_id);
	}
}

void
MonitorList::setCurrentGroup(const msRunHashGroup * group) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setCurrentGroup(group);
	}
}

void
MonitorList::setEndCurrentGroup() {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setEndCurrentGroup();
	}
}
 
void 
MonitorList::setCurrentMsrun(const Msrun * msrun) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setCurrentMsrun(msrun);
	}
}

void 
MonitorList::setEndCurrentMsrun() {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setEndCurrentMsrun();
	}
}

void 
MonitorList::setCurrentSearchItem(QuantiItemBase * quanti_item) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setCurrentSearchItem(quanti_item);
	}
}

void 
MonitorList::setEndCurrentSearchItem(){
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setEndCurrentSearchItem();
	}
}

void 
MonitorList::setXic(const xicBase * xic) {
	std::vector<MonitorBase *>::const_iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setXic(xic);
	}
}

void
MonitorList::setFilteredXic(const xicBase * xic, 
							const mcq_filter_type & filterType,
							const unsigned int filterNum) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setFilteredXic(xic, filterType, filterNum);
	}
}

void 
MonitorList::setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setAllPeaks(p_v_peak_list);
	}
}

void 
MonitorList::setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setMatchedPeaks(p_v_peak_list);
	}
}

void 
MonitorList::setPostMatchingOn() { 
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setPostMatchingOn();
	}
}

void 
MonitorList::setPostMatchedPeaks(QuantiItemBase * quanti_item, 
								 const std::vector<xicPeak *> * p_v_peak_list) {
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setPostMatchedPeaks(quanti_item, p_v_peak_list);
	}
}

void 
MonitorList::setPostMatchingOff() { 
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->setPostMatchingOff();
	}
}

void 
MonitorList::debriefing(){
	std::vector<MonitorBase *>::iterator it;
	for (it = _p_v_monitors.begin(); it != _p_v_monitors.end(); ++it) {
		(*it)->debriefing();
	}
}

void 
MonitorList::addMonitor(MonitorBase * monitor) {
	_p_v_monitors.push_back(monitor);
}
