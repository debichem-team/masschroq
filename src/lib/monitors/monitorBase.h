/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file monitorBase.h
 * \date August 27, 2010  
 * \author Edlira Nano
 */

#ifndef MONITORBASE_H_
#define MONITORBASE_H_ 1

#include "../msrun/msrun.h"
#include "../msrun/ms_run_hash_group.h"
#include "../quanti_items/quantiItemBase.h"
#include "../peak/xic_peak.h"
#include "../mcq_error.h"

/**
 * \class MonitorBase
 * \brief Abstract class representing the monitoring of the results 
 * of QuantiMsCpp. All type of results derive from this class.
 */

class MonitorBase {

public :
  
	MonitorBase();
  
	virtual ~MonitorBase();

	virtual void setCurrentQuantify(const QString & quantify_id) = 0;

	virtual void setCurrentGroup(const msRunHashGroup * group) = 0; 
	virtual void setEndCurrentGroup() = 0; 

	virtual void setCurrentMsrun(const Msrun * msrun) = 0; 
	virtual void setEndCurrentMsrun() = 0; 
  
	virtual void setCurrentSearchItem(QuantiItemBase * quanti_item) = 0; 
	virtual void setEndCurrentSearchItem() = 0; 
  
	virtual void setXic(const xicBase * xic) = 0;
  
	virtual void setFilteredXic(const xicBase * xic, 
								const mcq_filter_type & filterType,
								const unsigned int filterNum) = 0;
      
	virtual void setAllPeaks(const std::vector<xicPeak *> * p_v_peak_list) = 0;

	virtual void setMatchedPeaks(const std::vector<xicPeak *> * p_v_peak_list) = 0;
  
	virtual void setPostMatchingOn() = 0;
  
	virtual void setPostMatchingOff() = 0;

	virtual void setPostMatchedPeaks(QuantiItemBase * quanti_item, 
									 const std::vector<xicPeak *> * p_v_peak_list) = 0; 
  
	virtual void debriefing() = 0;
  
};

#endif /* MONITORBASE_H_ */
