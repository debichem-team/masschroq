/**
 * \file tsv_mzlist_xics.cpp
 * \date September 29, 2010 
 * \author Edlira Nano
 */

#include "tsv_mzrt_xics.h"
#include "../quanti_items/quantiItemMzRt.h"


TsvMzRtXics::TsvMzRtXics(const QString output_dir)
	: 
	XicTracesBase(output_dir)
{
	setToBeTraced(false);
}

TsvMzRtXics::~TsvMzRtXics()
{
}

void
TsvMzRtXics::addMzRt(const mcq_double mz, const mcq_double rt)
{
  std::pair<mcq_double, mcq_double> elmt(mz, rt);
	_v_mzrt_values.push_back(elmt);
}

void 
TsvMzRtXics::setTraceItem(QuantiItemBase * quanti_item)
{
	if (quanti_item->getType() == MZRT_QUANTI_ITEM)
	{
		const mcq_double current_mz(quanti_item->get_mz());
		const mcq_double current_rt(quanti_item->getRt());
		std::vector< std::pair<mcq_double, mcq_double> >::const_iterator it;
		for (it = _v_mzrt_values.begin();
			 it != _v_mzrt_values.end();
			 ++it)
		{
			if ( (it->first == current_mz) && 
				 (it->second == current_rt) )
		{
			/// tracing flag becomes true
			setToBeTraced(true);
			return;
		}
	}
	}
}

void 
TsvMzRtXics::privateUnsetTraceItem()
{
	setToBeTraced(false);
}
