/**
 * \file tsv_peplist_xics.cpp
 * \date September 23, 2010 
 * \author Edlira Nano
 */

#include "tsv_peplist_xics.h"
#include "../peptides/peptide_list.h"
#include "algorithm"

TsvPepListXics::TsvPepListXics(const QString output_dir)
	: 
	XicTracesBase(output_dir)
{
	setToBeTraced(false);
}

TsvPepListXics::~TsvPepListXics()
{
	_peptide_list.clear();
}

void
TsvPepListXics::addPeptide(const QString & pepId)
{
	_peptide_list.push_back(pepId);
}

bool 
TsvPepListXics::containsPeptide(const QString & pepId) const
{
  std::vector<QString>::const_iterator it; 
	it = find(_peptide_list.begin(), _peptide_list.end(), pepId);
	if (it != _peptide_list.end())
		return true;
	else
		return false;
}

void 
TsvPepListXics::setTraceItem(QuantiItemBase * quanti_item)
{
	if (quanti_item->getType() == PEPTIDE_QUANTI_ITEM)
	{
		const Peptide * current_pep(quanti_item->getPeptide());
		if (current_pep != NULL)
		{
			QString current_pep_id = current_pep->getXmlId();
			if ( containsPeptide(current_pep_id) )
			{
				/// tracing flag becomes true
				setToBeTraced(true);
				_current_pep_id = current_pep_id;
			}
		}
	}
}

void
TsvPepListXics::privateUnsetTraceItem()
{
	_current_pep_id.clear();
	setToBeTraced(false);
}

