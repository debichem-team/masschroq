/**
 * \file gnumeric_quantif_results.cpp
 * \date October 24, 2009
 * \author Olivier Langella
 */

#include "gnumeric_quantif_results.h"
#include "../consoleout.h"

#include <QTime>

/** 
    Constructor that takes the _output_file filename as  parameter.
    Calls the base quantifResultsXhtmltable default constructor
    The filename is verified.
*/



GnumericQuantifResults::GnumericQuantifResults(const QString & filename)
	:
	QuantifResultsBase(filename),
	irow(0)
{
	setOutputFilesAndStreams(filename);
}

void
GnumericQuantifResults::setOutputFilesAndStreams(const QString & filename) {
  
	QString complete_filename = filename;
	complete_filename.append(".gnumeric");
	_output_file = new QFile(complete_filename);
  
	QString out_filename = _output_file->fileName();

	if (_output_file->exists()) {
		mcqout() << "WARNING : gnumeric output file '"
			 << out_filename
			 << "' already exists, it will be overwrited." 
			 << endl;
	}
  
	if (_output_file->open(QIODevice::WriteOnly)) {
		_xml_stream = new QXmlStreamWriter(_output_file);
	} else {
		throw mcqError(QObject::tr("error : cannot open the output file : %1\n").arg(out_filename));
	}
	mcqout() << "Writing quantification results in gnumeric output file : '" 
		 << out_filename << "'" << endl;
  
	setLocaleAndPrecisionForAllStreams();
	printHeaders();
}

void 
GnumericQuantifResults::setLocaleAndPrecisionForAllStreams() {
#if QT_VERSION >= QT_V_4_5 
	_tmp_element_stream.setLocale(QLocale::c());
#endif   
	_tmp_element_stream.setRealNumberPrecision(8); 
	_tmp_element_stream.setRealNumberNotation(QTextStream::SmartNotation);
	_xml_stream->setAutoFormatting(true);
}

GnumericQuantifResults::~GnumericQuantifResults() {
	if (_xml_stream != 0) {
		_xml_stream->writeEndDocument();
		delete _xml_stream;
		_xml_stream = 0;
	}
  
	if (_output_file != 0) {
		_output_file->close();
		delete _output_file;
		_output_file = 0;
	}
}

void 
GnumericQuantifResults::printHeaders() {
	QStringList list;
	foreach (QString str, getPepHeaders())
    {
		list << formatCell(str);
    }
	_xml_stream->writeStartDocument();
	_xml_stream->writeNamespace("http://www.gnumeric.org/v10.dtd", "gnm");
	_xml_stream->writeNamespace("http://www.w3.org/2001/XMLSchema-instance",
			"xsi");
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Workbook");
	_xml_stream->writeAttribute("http://www.w3.org/2001/XMLSchema-instance",
			"schemaLocation", 
			"http://www.gnumeric.org/v9.xsd");
	//<gnm:Version Epoch="1" Major="9" Minor="9" Full="1.9.9"/>
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Version");
	_xml_stream->writeAttribute("Epoch", "1");
	_xml_stream->writeAttribute("Major", "9");
	_xml_stream->writeAttribute("Minor", "9");
	_xml_stream->writeAttribute("Full", "1.9.9");
	_xml_stream->writeEndElement();
	//<gnm:Attributes>
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Attributes");
	//<gnm:Attribute>
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Attribute");
	//<gnm:type>4</gnm:type>
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "type",
			"4");
	//<gnm:name>WorkbookView::show_horizontal_scrollbar</gnm:name>
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"WorkbookView::show_horizontal_scrollbar");
	//<gnm:value>TRUE</gnm:value>
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "value",
			"TRUE");
	_xml_stream->writeEndElement();
  
	/*
	 *     <gnm:Attribute>
	 <gnm:type>4</gnm:type>
	 <gnm:name>WorkbookView::show_vertical_scrollbar</gnm:name>
	 <gnm:value>TRUE</gnm:value>
	 </gnm:Attribute>
	*/
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Attribute");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "type",
			"4");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"WorkbookView::show_vertical_scrollbar");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "value",
			"TRUE");
	_xml_stream->writeEndElement();
  
	/*
	  <gnm:Attribute>
	  <gnm:type>4</gnm:type>
	  <gnm:name>WorkbookView::show_notebook_tabs</gnm:name>
	  <gnm:value>TRUE</gnm:value>
	  </gnm:Attribute>
	*/
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Attribute");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "type",
			"4");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"WorkbookView::show_notebook_tabs");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "value",
			"TRUE");
	_xml_stream->writeEndElement();
	/*
	  <gnm:Attribute>
	  <gnm:type>4</gnm:type>
	  <gnm:name>WorkbookView::do_auto_completion</gnm:name>
	  <gnm:value>TRUE</gnm:value>
	  </gnm:Attribute>
	*/
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Attribute");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "type",
			"4");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"WorkbookView::do_auto_completion");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "value",
			"TRUE");
	_xml_stream->writeEndElement();
	/*
	  <gnm:Attribute>
	  <gnm:type>4</gnm:type>
	  <gnm:name>WorkbookView::is_protected</gnm:name>
	  <gnm:value>FALSE</gnm:value>
	  </gnm:Attribute>
	  *
	  */
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Attribute");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "type",
			"4");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"WorkbookView::is_protected");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "value",
			"FALSE");
	_xml_stream->writeEndElement();
	//gnm:Attributes
	_xml_stream->writeEndElement();
  
	/*
	 *   <office:document-meta xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
	 *   xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/"
	 *   xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
	 *   xmlns:ooo="http://openoffice.org/2004/office" office:version="1.1">
	 <office:meta/>
	 </office:document-meta>
	 *
	 */
	_xml_stream->writeNamespace
		("urn:oasis:names:tc:opendocument:xmlns:office:1.0", "office");
	_xml_stream->writeNamespace("http://www.w3.org/1999/xlink", "xlink");
	_xml_stream->writeNamespace("http://purl.org/dc/elements/1.1/", "dc");
	_xml_stream->writeNamespace
		("urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "meta");
	_xml_stream->writeNamespace("http://openoffice.org/2004/office", "ooo");
	_xml_stream->writeStartElement
		("urn:oasis:names:tc:opendocument:xmlns:office:1.0", "document-meta");
	_xml_stream->writeAttribute
		("urn:oasis:names:tc:opendocument:xmlns:office:1.0", "version", "1.1");
	_xml_stream->writeStartElement
		("urn:oasis:names:tc:opendocument:xmlns:office:1.0", "meta");
	_xml_stream->writeEndElement();
	_xml_stream->writeEndElement();
  
	//  <gnm:Calculation ManualRecalc="0" EnableIteration="1" MaxIterations="100"
	//IterationTolerance="0.001" FloatRadix="2" FloatDigits="53"/>
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Calculation");
	_xml_stream->writeAttribute("ManualRecalc", "0");
	_xml_stream->writeAttribute("EnableIteration", "1");
	_xml_stream->writeAttribute("MaxIterations", "100");
	_xml_stream->writeAttribute("IterationTolerance", "0.001");
	_xml_stream->writeAttribute("FloatRadix", "2");
	_xml_stream->writeAttribute("FloatDigits", "53");
	_xml_stream->writeEndElement();
  
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Summary");
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", "Item");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"biology softwares");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"val-string", "http://pappso.inra.fr/");
	_xml_stream->writeEndElement();
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", "Item");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "name",
			"author");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"val-string", "Olivier Langella <Olivier.Langella@moulon.inra.fr>");
	_xml_stream->writeEndElement();
	_xml_stream->writeEndElement();
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"SheetNameIndex");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"SheetName", "peptides");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"SheetName", "proteins");
	_xml_stream->writeEndElement();
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd",
			"Sheets");
  
	//QString sheetname("Matrix 1");
	//oGnumericSheet(xml_stream, sheetname);
	/*fichier << "<gmr:Sheet ";
	  fichier
	  << "DisplayFormulas=\"false\" HideZero=\"false\" HideGrid=\"false\" HideColHeader=\"false\" HideRowHeader=\"false\" DisplayOutlines=\"true\" OutlineSymbolsBelow=\"true\" OutlineSymbolsRight=\"true\">"
	  << endl;*/
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", "Sheet");
	_xml_stream->writeAttribute("DisplayFormulas", "false");
	_xml_stream->writeAttribute("HideZero", "false");
	_xml_stream->writeAttribute("HideGrid", "false");
	_xml_stream->writeAttribute("HideColHeader", "false");
	_xml_stream->writeAttribute("HideRowHeader", "false");
	_xml_stream->writeAttribute("DisplayOutlines", "true");
	_xml_stream->writeAttribute("OutlineSymbolsBelow", "true");
	_xml_stream->writeAttribute("OutlineSymbolsRight", "true");
  
	/*_xml_stream->writeAttribute("xml", "lang", "en");
	  _xml_stream->writeStartElement("body");
	  _xml_stream->writeStartElement("table");
	  _xml_stream->writeStartElement("tbody");*/
  
	// 	fichier.imbue();
	//	fichier << "<gmr:Name>" << sheetname << "</gmr:Name>" << endl;
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "Name",
			"peptides");
  
	//fichier << "<gmr:MaxCol>" << (GetNC() + 2) << "</gmr:MaxCol>" << endl;
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"MaxCol", "0");
	//fichier << "<gmr:MaxRow>" << (GetNL() + 5 + _titre.size())
	//		<< "</gmr:MaxRow>" << endl;
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"MaxRow", "0");
	//fichier << "<gmr:Zoom>1.000000</gmr:Zoom>" << endl;
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "Zoom",
			"1.000000");
  
	//fichier << "<gmr:Names/>" << endl;
	_xml_stream->writeEmptyElement("http://www.gnumeric.org/v10.dtd", "Names");
  
	//fichier << "<gmr:Cells>" << endl;
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", "Cells");
  
	printLine(list);
}

void 
GnumericQuantifResults::printLine(const QStringList & list) {
	int icol(0);
	foreach (QString str, list) {
		_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", 
				"Cell");
		_xml_stream->writeAttribute("Row", QString::number(irow));
		_xml_stream->writeAttribute("Col", QString::number(icol));

		bool ok;
		str.toDouble(&ok);
		if (ok == false) {
			QTime temps;
			temps = temps.fromString(str, "hh:mm:ss.zzz");
			if (temps.isValid()) {
				_xml_stream->writeAttribute("ValueType", "40");
				_xml_stream->writeAttribute("ValueFormat", "hh:mm:ss");
			} else {
				_xml_stream->writeAttribute("ValueType", "60");
			}
		}
		else {
			_xml_stream->writeAttribute("ValueType", "40");
		}
		//fichier << _titre[i];
		_xml_stream->writeCharacters(str);
		//fichier << "</gmr:Cell>" << endl;
		_xml_stream->writeEndElement();
		icol++;
	}
	irow++;
}

void 
GnumericQuantifResults::debriefing() {
	// close the peptides sheet
	_xml_stream->writeEndElement();// "Cells");
	_xml_stream->writeEndElement();// "Sheet");

	// open the proteins sheet
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", "Sheet");
	_xml_stream->writeAttribute("DisplayFormulas", "false");
	_xml_stream->writeAttribute("HideZero", "false");
	_xml_stream->writeAttribute("HideGrid", "false");
	_xml_stream->writeAttribute("HideColHeader", "false");
	_xml_stream->writeAttribute("HideRowHeader", "false");
	_xml_stream->writeAttribute("DisplayOutlines", "true");
	_xml_stream->writeAttribute("OutlineSymbolsBelow", "true");
	_xml_stream->writeAttribute("OutlineSymbolsRight", "true");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "Name",
			"proteins");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"MaxCol", "0");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd",
			"MaxRow", "0");
	_xml_stream->writeTextElement("http://www.gnumeric.org/v10.dtd", "Zoom",
			"1.000000");
	_xml_stream->writeEmptyElement("http://www.gnumeric.org/v10.dtd", "Names");
	_xml_stream->writeStartElement("http://www.gnumeric.org/v10.dtd", "Cells");
  
	irow = 0;
  
	// print the proteins Header
	QStringList list;
	foreach (QString str, getProtHeaders())
    {
		list << formatCell(str);
    }
	printLine(list);
  
	std::map<QString, std::map<QString, QString> >::const_iterator itpep;
	for (itpep = _map_peptide_proteins.begin(); 
		 itpep != _map_peptide_proteins.end(); 
		 ++itpep) {
    
		std::map<QString, QString>::const_iterator itprot;
		for (itprot = itpep->second.begin(); 
			 itprot != itpep->second.end(); 
			 ++itprot) {
			list.clear();
			list << formatCell(itpep->first);
			list << formatCell(itprot->first);
			list << formatCell(itprot->second);
			printLine(list);
		}
	}
	_xml_stream->writeEndDocument();
	_output_file->close();
}

