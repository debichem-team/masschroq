/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file tsv_mzlist_xics.h
 * \date September 29, 2010 
 * \author Edlira Nano
 */

#ifndef TSV_MZLIST_XICS_H_
#define TSV_MZLIST_XICS_H_ 1

#include "xicTracesBase.h"


/**
   \class TsvMzListXics
   \brief Type of result containing tab separated values of
   the xics for a given mz set of values.

   The results will be put in files created in the _output_dir member. 
   One file per mz value will be created.
*/

class TsvMzListXics : public XicTracesBase {
  
 public:
  
  TsvMzListXics(const QString output_dir);
  
  virtual ~TsvMzListXics();

  void addMz(const mcq_double mz);    

protected : 

  virtual void setTraceItem(QuantiItemBase * quanti_item);
  
  virtual void privateUnsetTraceItem();

 private:
  
   std::vector<mcq_double > _v_mz_values;

};

#endif /* TSV_MZLIST_XICS_H_ */
