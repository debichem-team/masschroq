/**
 * \file tsv_all_xics.cpp
 * \date September 01, 2010 
 * \author Edlira Nano
 */

#include "tsv_all_xics.h"

TsvAllXics::TsvAllXics(const QString output_dir)
	:
	XicTracesBase(output_dir)
{
	/// all is to be traced
	setToBeTraced(true);
}

TsvAllXics::~TsvAllXics()
{
}

void 
TsvAllXics::setTraceItem(QuantiItemBase * quanti_item)
{
	/// all is to be traced, nothing to do
}

void
TsvAllXics::privateUnsetTraceItem()
{
}
