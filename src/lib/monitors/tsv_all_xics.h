/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file tsv_all_xics.h
 * \date September 28, 2010 
 * \author Edlira Nano
 */

#ifndef TSV_ALL_XICS_H_
#define TSV_ALL_XICS_H_ 1

#include "xicTracesBase.h"
#include <QDir>

/**
   \class TsvAllXics
   \brief Class representing tsv results containing information 
   about all the xics extracted 
   
   The results will be put in directory _output_dir : one directory 
   per group and one per msrun will be created; one file per
   xic extracted by msrun that contains the csv information about this xic
 */

class TsvAllXics : public XicTracesBase {

 public:
  
  TsvAllXics(const QString output_dir);
  
  virtual ~TsvAllXics();
  
 protected :

  virtual void setTraceItem(QuantiItemBase * quanti_item);
  
  virtual void privateUnsetTraceItem();
  
};

#endif /* TSV_ALL_XICS_H_ */
