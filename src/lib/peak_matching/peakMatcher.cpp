/**
 * \file peakMatcher.cpp
 * \date August 2, 2011
 * \author Edlira Nano
 */

#include "peakMatcher.h"

PeakMatcher::PeakMatcher(const mcq_double coeff_match_range)
  : 
	_coeff_match_range(coeff_match_range)
{
	set_matching_mode(NO_MATCHING_MODE);
}

PeakMatcher::~PeakMatcher()
{
}

void 
PeakMatcher::set_matching_mode(const mcq_matching_mode & match_mode)
{
	_match_mode = match_mode;
} 

const bool
PeakMatcher::isPostMatchingRequired() const
{
	return (_match_mode == POST_MATCHING_MODE);
}

const mcq_double 
PeakMatcher::get_coeff_match_range() const
{
	return _coeff_match_range;
}

const mcq_matching_mode
PeakMatcher::get_match_mode() const
{
	return _match_mode;
}

std::vector<xicPeak *> * 
PeakMatcher::newMatchedPeaks(
	QuantiItemBase * quanti_item, 
	std::vector<xicPeak *> * all_peaks,
	const Msrun * msrun) const
{
	return (quanti_item->getMatchedPeaks(all_peaks, msrun, _coeff_match_range));
}


std::vector<xicPeak *> * 
PeakMatcher::newPostMatchedPeaks(QuantiItemBase * quanti_item, const Msrun * msrun)
{
	return (quanti_item->getPostMatchedPeaks(msrun, _coeff_match_range));
}
