/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file peakMatcher.h
 * \date August 2, 2011
 * \author Edlira Nano
 */

#ifndef PEAK_MATCHER_H_
#define PEAK_MATCHER_H_ 1

#include "../msrun/ms_run_hash_group.h"
#include "../peak/xic_peak.h"

/**
 * \class peakMatcher
 * \brief Class performing the matching of the detected peaks to an mz
 *  quantification item.
 */


class PeakMatcher {
  
 public :
  
  PeakMatcher(const mcq_double coeff_match_range);
  
  virtual ~PeakMatcher();

  void set_matching_mode(const mcq_matching_mode & match_mode);

  const mcq_matching_mode get_match_mode() const;
  
  const bool isPostMatchingRequired() const;

  const mcq_double get_coeff_match_range() const;



  std::vector<xicPeak *> * newMatchedPeaks(QuantiItemBase * quanti_item, 
					   std::vector<xicPeak *> * all_peaks,
					   const Msrun * msrun) const;

  std::vector<xicPeak *> * newPostMatchedPeaks(QuantiItemBase * quanti_item, const Msrun * msrun);
    
 private :

  const mcq_double _coeff_match_range;
  
  mcq_matching_mode _match_mode;
  
};
#endif /* PEAK_MATCHER_H_ */
