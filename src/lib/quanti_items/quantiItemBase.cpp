/**
 * \file quantiItemBase.cpp
 * \date August 01, 2011
 * \author Edira Nano
 *
 */

#include "quantiItemBase.h"
#include <QStringList>

QuantiItemBase::QuantiItemBase(const XicExtractionMethodBase & extraction_method) {
  _mz = extraction_method.get_mz();
  _low_mz = extraction_method.get_min_mz();
  _high_mz = extraction_method.get_max_mz();
  this->setType();
}

QuantiItemBase::~QuantiItemBase() {
}

mcq_double 
QuantiItemBase::get_mz() const {
  return (_mz);
}
  
mcq_double 
QuantiItemBase::get_min_mz() const {
  return (_low_mz);
}

mcq_double 
QuantiItemBase::get_max_mz() const {
  return (_high_mz);
}

const mcq_quanti_item_type 
QuantiItemBase::getType() const {
  return _type;
}

void 
QuantiItemBase::setType() {
  _type = MZ_QUANTI_ITEM;
}

bool 
QuantiItemBase::operator<(const QuantiItemBase &other) const {
  const mcq_double mz = this->get_mz();
  const mcq_double mz_other = other.get_mz();
  return (mz < mz_other); 
}


void 
QuantiItemBase::printInfos(QTextStream & out) const {
  out << "Mz quantification item :" << endl;
  out << "_mz = " << _mz << endl;
  out << "_low_mz = " << _low_mz << endl;
  out << "_high_mz = " << _high_mz << endl;
}

QString 
QuantiItemBase::getSimpleName(const Msrun * msrun) const {
  QString name("mz_"), tmp;
  tmp.setNum(get_mz());
  name.append(tmp).append("_l_");
  tmp.setNum(get_min_mz()).append("_");
  name.append(tmp).append("_h_");
  tmp.setNum(get_max_mz());
  name.append(tmp);
  name = name.replace(".", "-");
  //name.append(suffixName());
  return (name);
}

QStringList
QuantiItemBase::getTracesTitle(const Msrun * msrun) const {
  QList<QString> header;
  QString mz("mz = "), mz_value;
  mz_value.setNum(get_mz());
  mz.append(mz_value);
  header << mz;
  return QStringList(header);
}

const QString 
QuantiItemBase::getQuantiItemId() const {
  return QString("");
}

std::vector<xicPeak *> * 
QuantiItemBase::getMatchedPeaks(std::vector<xicPeak *> * all_peaks,
				const Msrun * msrun,
				const mcq_double coeff_match_range) {
  return all_peaks;
}
  

