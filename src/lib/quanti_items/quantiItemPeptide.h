/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file quantiItemPeptide.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#ifndef QUANTI_ITEM_PEPTIDE_H_ 
#define QUANTI_ITEM_PEPTIDE_H_ 1

#include "quantiItemBase.h"
#include "../peptides/peptide.h"

class xicPeak;


/**
 * \class QuantiItemPeptide
 * \brief Class representing a peptide item to be quantified.
 */

class QuantiItemPeptide : public QuantiItemBase {
  
 public:
 
  QuantiItemPeptide(XicExtractionMethodBase & extraction_method,
		    const Peptide * p_peptide, 
		    unsigned int z, 
		    const msRunHashGroup & group,
		    const mcq_matching_mode & match_mode);
  
  virtual ~QuantiItemPeptide();
  
  virtual const Peptide * getPeptide() const;
  
  virtual const unsigned int * getZ() const;

  virtual QString getSimpleName(const Msrun * msrun) const;
  
  virtual QStringList getTracesTitle(const Msrun * msrun) const;
  
  virtual const QString getQuantiItemId() const;
  
  virtual void printInfos(QTextStream & out) const;

  const mcq_matching_mode getMatchMode() const;
  
  virtual std::vector<xicPeak *> * getMatchedPeaks(std::vector<xicPeak *> * all_peaks,
					      const Msrun * msrun,
					      const mcq_double coeff_match_range);
  
					      virtual std::vector<xicPeak *> * getPostMatchedPeaks(const Msrun * msrun, 
						  const mcq_double coeff_match_range);

 protected:

  const Peptide * _p_peptide;
  const unsigned int _z;
  const msRunHashGroup & _group_to_quantify;
  
 private :

  virtual void setType();

  const mcq_double getRealRt(const Msrun * pmsrun, const msRunHashGroup & group) const;
  
  const mcq_double getMeanRt(const msRunHashGroup & group) const;

  const mcq_double getRealOrMeanRt(const Msrun * msrun, const msRunHashGroup & group) const;
 
  /// for post matching

  const mcq_double getMatchingRt(const Msrun * msrun) const;
  
  const bool isPostMatchingRequired() const;

  void add_new_post_matching_rt_from_matched_peak(const xicPeak * matched_peak);

  void add_new_post_matching_rt_when_unmatched_peak(const Msrun * msrun);
 
  const mcq_double compute_post_matching_rt() const;

  void add_mismatched_peak(xicPeak * mismatched_peak);

  std::vector<xicPeak *> * _mismatched_peaks;

  std::vector<mcq_double> _v_post_matching_rt;

  const mcq_matching_mode _match_mode;
  
  
};

#endif /* QUANTI_ITEM_PEPTIDE_H_ */
