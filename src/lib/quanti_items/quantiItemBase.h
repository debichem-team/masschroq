/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file quantiItemBase.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#ifndef QUANTI_ITEM_BASE_H_
#define QUANTI_ITEM_BASE_H_ 1

#include <iostream> 
#include "../xicExtractionMethods/xicExtractionMethodBase.h"

class xicPeak;
class Msrun;
class Peptide;

/**
 * \class QuantiItemBase
 * \brief Base class representing a given mz value to be quantified.
 * 
 * The items to be quantified can be:
 * - an mz value,
 * - a pair of mz, rt values,
 * - a peptide.
 */

class QuantiItemBase {
  
 public:
 
  QuantiItemBase(const XicExtractionMethodBase & extraction_method); 
  
  virtual ~QuantiItemBase();
 
  bool operator<(const QuantiItemBase &other) const;
   
  mcq_double get_mz() const;
  
  mcq_double get_min_mz() const;
  
  mcq_double get_max_mz() const;
  
  virtual const unsigned int * getZ() const {
    return (NULL);
  }

  virtual const Peptide * getPeptide() const {
    return (NULL);
  }

  virtual const mcq_double getRt() const {
    return -1;
  }

  virtual QString getSimpleName(const Msrun * msrun) const;

  virtual QStringList getTracesTitle(const Msrun * msrun) const;
  
  virtual const QString getQuantiItemId() const;
  
  virtual void printInfos(QTextStream & out) const;
  
  virtual const mcq_quanti_item_type getType() const;

  virtual std::vector<xicPeak *> * getMatchedPeaks(std::vector<xicPeak *> * all_peaks,
					      const Msrun * msrun,
					      const mcq_double coeff_match_range);
  
					      virtual std::vector<xicPeak *> * getPostMatchedPeaks(const Msrun * msrun,
						  const mcq_double coeff_match_range){
    return NULL;
  }
  
 protected:
  
  mcq_double _mz;
  mcq_double _low_mz;
  mcq_double _high_mz;
 
  mcq_quanti_item_type _type;

 private :
  
  virtual void setType();

};

#endif /* _QUANTI_ITEM_BASE_H_ */
