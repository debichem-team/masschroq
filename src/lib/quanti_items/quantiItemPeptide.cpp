/**
 * \file quantiItemPeptide.h
 * \date August 01, 2011
 * \author Edira Nano
 */


#include "quantiItemPeptide.h"
#include "../peak/xic_peak.h"
#include "../mcq_error.h"

#include <QStringList>

QuantiItemPeptide::QuantiItemPeptide(XicExtractionMethodBase & extraction_method,
		const Peptide * p_peptide, 
		unsigned int z,
		const msRunHashGroup & group,
		const mcq_matching_mode & match_mode)
	:
	QuantiItemBase(extraction_method),
	_p_peptide(p_peptide), 
	_z(z),
	_group_to_quantify(group),
	_match_mode(match_mode)
{
	this->setType();

	if (isPostMatchingRequired())
	{
	  _mismatched_peaks = new std::vector<xicPeak *>;
	}
	else
	{
		_mismatched_peaks = 0;
	}
}

QuantiItemPeptide::~QuantiItemPeptide()
{
  std::vector<xicPeak *>::iterator it_mis_peak;
	for (it_mis_peak = _mismatched_peaks->begin();
		 it_mis_peak != _mismatched_peaks->end();
		 ++it_mis_peak)
	{
		if (*it_mis_peak != 0)
		{
			delete (*it_mis_peak);
			*it_mis_peak = 0;
		}
	}
  
	if (_mismatched_peaks != 0)
	{
		delete (_mismatched_peaks);
		_mismatched_peaks = 0;
	}
}

void 
QuantiItemPeptide::setType()
{
	_type = PEPTIDE_QUANTI_ITEM;
}

const Peptide * 
QuantiItemPeptide::getPeptide() const
{
	return (_p_peptide);
}

const unsigned int * 
QuantiItemPeptide::getZ() const
{
	return (&_z);
}


QString 
QuantiItemPeptide::getSimpleName(const Msrun * msrun) const
{
	QString name(_p_peptide->getXmlId()), tmp;
	name.append("_mz_");
	tmp.setNum(get_mz());
	name.append(tmp).append("_rt_");
	tmp.setNum(getMatchingRt(msrun));
	name.append(tmp).append("_l_");
	tmp.setNum(get_min_mz());
	name.append(tmp).append("_h_");
	tmp.setNum(get_max_mz());
	name.append(tmp).append("_z_");
	tmp.setNum(_z);
	name.append(tmp);
	name = name.replace(".", "-");
	//  name.append(suffixName());
	return (name);
}

QStringList
QuantiItemPeptide::getTracesTitle(const Msrun * msrun) const
{
	QList<QString> header;
	QString pep("peptide = "), mz("mz = "), mz_value, rt_mode("rt_mode = "), rt("rt = "), rt_value;
	pep.append(_p_peptide->getXmlId());
	mz_value.setNum(get_mz());
	mz.append(mz_value);
	rt_mode.append(_match_mode);
	rt_value.setNum(getMatchingRt(msrun));
	rt.append(rt_value);
	header << pep << mz << rt_mode << rt;
	return QStringList(header);
}

const mcq_matching_mode 
QuantiItemPeptide::getMatchMode() const
{
	return _match_mode;
}

const bool
QuantiItemPeptide::isPostMatchingRequired() const
{
	return (_match_mode == POST_MATCHING_MODE);
}

void 
QuantiItemPeptide::printInfos(QTextStream & out) const
{
	out << "peptide quantification item :" << endl;
	_p_peptide->printInfos(out);
	out << "_z = " << _z << endl;
	out << "_mz = " << _mz << endl;
	out << "rt matching mode= " << _match_mode << endl;
	out << "_low_mz = " << _low_mz << endl;
	out << "_high_mz = " << _high_mz << endl;
}

const mcq_double
QuantiItemPeptide::getMatchingRt(const Msrun * msrun) const
{
  
	if ( _match_mode == MEAN_MODE )
	{
		return (getMeanRt(_group_to_quantify));
	} 
	else if ( _match_mode == POST_MATCHING_MODE )
	{
		return (getRealRt(msrun, _group_to_quantify));  
	}
	else if (_match_mode == REAL_OR_MEAN_MODE)
	{
		return (getRealOrMeanRt(msrun, _group_to_quantify)); 
	}
	else
	{
		throw mcqError(QObject::tr("bug in getMatchingRt: the getMatchingRt in quantification item '%1' has unknown matching mode\n").arg(this->getSimpleName(msrun)));
	} 
}

// returns -1 if peptide not observed in msrun
const mcq_double 
QuantiItemPeptide::getRealRt(const Msrun * msrun, const msRunHashGroup & group) const
{
	return (_p_peptide->getObservedBestRtForMsRun(msrun, group));
}

const mcq_double 
QuantiItemPeptide::getMeanRt(const msRunHashGroup & group) const
{
	return (_p_peptide->getMeanBestRt(group));
}

const mcq_double 
QuantiItemPeptide::getRealOrMeanRt(const Msrun * msrun, const msRunHashGroup & group) const
{
	if ( _p_peptide->isObservedIn(msrun) )
	{
		return (getRealRt(msrun, _group_to_quantify)); 
	}
	else
	{
		return getMeanRt(_group_to_quantify);
	}
}

std::vector<xicPeak *> * 
QuantiItemPeptide::getMatchedPeaks(
std::vector<xicPeak *> * all_peaks,
	const Msrun * msrun,
	const mcq_double coeff_match_range)
{
	
	bool hasBeenMatched(false);
	std::vector<xicPeak *> * matched_peak_list = new std::vector<xicPeak *>;
	std::vector<xicPeak *> tmp_mismatched_peaks;
  
	std::vector<xicPeak *>::iterator it_all_peaks;
	const mcq_double rt_to_look_for = getMatchingRt(msrun);
	
	for (it_all_peaks = all_peaks->begin();
		 it_all_peaks != all_peaks->end();
		 ++it_all_peaks)
	{
		
		if ( (*it_all_peaks)->contains_rt(rt_to_look_for, coeff_match_range) )
		{
			matched_peak_list->push_back(*it_all_peaks);
			hasBeenMatched = true;
			if (isPostMatchingRequired())
			{
				add_new_post_matching_rt_from_matched_peak(*it_all_peaks);
			}
		} else
		{
			if (isPostMatchingRequired())
			{
				tmp_mismatched_peaks.push_back(*it_all_peaks);
			}
			else
			{
				delete (*it_all_peaks);
			}
		}
	}
	
	if (isPostMatchingRequired() && (!hasBeenMatched))
	{
	  std::vector<xicPeak *>::iterator it_tmp_peaks;
		for (it_tmp_peaks = tmp_mismatched_peaks.begin();
			 it_tmp_peaks != tmp_mismatched_peaks.end();
			 ++it_tmp_peaks)
		{
			add_mismatched_peak(*it_tmp_peaks);
			add_new_post_matching_rt_when_unmatched_peak(msrun);
		}
	}
	delete all_peaks;
	return (matched_peak_list);
}   

void
QuantiItemPeptide::add_new_post_matching_rt_from_matched_peak(const xicPeak * matched_peak)
{
	mcq_double max_peak_rt = matched_peak->get_aligned_max_rt();
	_v_post_matching_rt.push_back(max_peak_rt);
}

void
QuantiItemPeptide::add_new_post_matching_rt_when_unmatched_peak(const Msrun * msrun)
{
	mcq_double real_rt = getRealRt(msrun, _group_to_quantify);
	if (real_rt != -1)
	{
		_v_post_matching_rt.push_back(real_rt);
	}
}

const mcq_double 
QuantiItemPeptide::compute_post_matching_rt() const
{
	const unsigned int size = _v_post_matching_rt.size();
	if (size == 0)
	{ 
		return -1;
	}
	else
	{ 
	  std::vector<mcq_double>::const_iterator it; 
		mcq_double sum(0);
		for (it = _v_post_matching_rt.begin();
			 it != _v_post_matching_rt.end();
			 ++it)
		{
			sum += *it;
		}
		return (sum / size);
	}
}


void
QuantiItemPeptide::add_mismatched_peak(xicPeak * mismatched_peak)
{ 
	_mismatched_peaks->push_back(mismatched_peak); 
}

std::vector<xicPeak *> * 
QuantiItemPeptide::getPostMatchedPeaks(const Msrun * msrun, const mcq_double coeff_match_range)
{
	mcq_double post_matching_rt = compute_post_matching_rt();
	std::vector<xicPeak *> * post_matched_peaks = new std::vector<xicPeak *>;
	std::vector<xicPeak * >::iterator it;
	for (it = _mismatched_peaks->begin();
		 it != _mismatched_peaks->end();
		 ++it)
	{
		if ( (*it)->contains_rt(post_matching_rt, coeff_match_range) && 
			 (*it)->getMsrun() == msrun )
		{
			post_matched_peaks->push_back(*it);
		}
	}
	return post_matched_peaks;
}

const QString 
QuantiItemPeptide::getQuantiItemId() const
{
	return (_p_peptide->getXmlId());
}
