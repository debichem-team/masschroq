/**
 * \file quantiItemMzRt.cpp
 * \date August 01, 2011
 * \author Edira Nano
 *
 */

#include "quantiItemMzRt.h"
#include "../peak/xic_peak.h"

#include <QStringList>

QuantiItemMzRt::QuantiItemMzRt(const XicExtractionMethodBase & extraction_method,
			       const mcq_double rt) :
  QuantiItemBase(extraction_method),
  _rt(rt)
{
  this->setType();
}

QuantiItemMzRt::~QuantiItemMzRt() {
}
  
const mcq_double 
QuantiItemMzRt::getRt() const {
  return (_rt);
}

void 
QuantiItemMzRt::setType() {
  _type = MZRT_QUANTI_ITEM;
}

void 
QuantiItemMzRt::printInfos(QTextStream & out) const {
  out << " Mz-Rt quantification item :" << endl;
  out << "_mz = " << _mz << endl;
  out << "_low_mz = " << _low_mz << endl;
  out << "_high_mz = " << _high_mz << endl;
  out << "_rt = " << _rt << endl;
}

QString 
QuantiItemMzRt::getSimpleName(const Msrun * msrun) const {
  QString name("mz_"), tmp;
  tmp.setNum(get_mz());
  name.append(tmp).append("_rt_");
  tmp.setNum(_rt);
  name.append(tmp).append("_l_");
  tmp.setNum(get_min_mz());
  name.append(tmp).append("_h_");
  tmp.setNum(get_max_mz());
  name.append(tmp);
  name = name.replace(".", "-");
  //  name.append(suffixName());
  return (name);
}

QStringList
QuantiItemMzRt::getTracesTitle(const Msrun * msrun) const {
  QList<QString> header;
  QString mz("mz = "), mz_value, rt("rt = "), rt_value;
  mz_value.setNum(get_mz());
  mz.append(mz_value);
  rt_value.setNum(getRt());
  rt.append(rt_value);
  header << mz << rt;
  return QStringList(header);
}

std::vector<xicPeak *> *
QuantiItemMzRt::getMatchedPeaks(std::vector<xicPeak *> * all_peaks,
				const Msrun * msrun, 
				const mcq_double coeff_match_range) {
  

  std::vector<xicPeak *> * matched_peak_list = new std::vector<xicPeak *>;
  const mcq_double rt_to_look_for = getRt();
  std::vector<xicPeak *>::iterator it_all_peaks;
  
  for (it_all_peaks = all_peaks->begin();
       it_all_peaks != all_peaks->end();
       ++it_all_peaks) {
    
    if ( (*it_all_peaks)->contains_rt(rt_to_look_for, coeff_match_range) ) {
	matched_peak_list->push_back(*it_all_peaks);
    } else {
      delete (*it_all_peaks);
    }
  }
  delete all_peaks;
  return (matched_peak_list);
}
  
