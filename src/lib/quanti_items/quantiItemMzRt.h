/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file quantiItemMzRt.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#ifndef QUANTI_ITEM_MZRT_H_
#define QUANTI_ITEM_MZRT_H_ 1

#include "quantiItemBase.h"

/**
 * \class QuantiItemMzRt
 * \brief Class representing a given pair of mz, retention time values to be quantified.
 */

class QuantiItemMzRt : public QuantiItemBase {
  
 public:
  
  QuantiItemMzRt(const XicExtractionMethodBase & extraction_method,
		 const mcq_double rt);
 
  
  virtual ~QuantiItemMzRt();
  
  virtual QString getSimpleName(const Msrun * msrun) const;
  
  virtual QStringList getTracesTitle(const Msrun * msrun) const;
  
  virtual void printInfos(QTextStream & out) const;

  virtual std::vector<xicPeak *> * getMatchedPeaks(std::vector<xicPeak *> * all_peaks,
					      const Msrun * msrun,
					      const mcq_double coeff_match_range);
						
 protected :
  
  mcq_double _rt;
  
  virtual const mcq_double getRt() const;
  
 private:

  virtual void setType();
    
};

#endif /* _QUANTI_ITEM_MZRT_H_ */
