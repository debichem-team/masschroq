/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file quantificator.h
 * \date August 03, 2011
 * \author Edlira Nano
 */

#ifndef _QUANTIFICATOR_H_
#define _QUANTIFICATOR_H_ 1

#include "msrun/ms_run_hash_group.h"
#include "peak_matching/peakMatcher.h"

class QuantificationMethod;
class MonitorBase;
class Peptide;


/**
   \class Quantificator
   \brief The class that launches the quantification of a group.
 */

class Quantificator {

 public :
  
  Quantificator(const QString & xml_id, 
		const msRunHashGroup * quanti_group, 
		QuantificationMethod * quanti_method);
  
  virtual ~Quantificator();

  const QString & getXmlId() const {
    return _xml_id;
  }

  void set_matching_mode(const mcq_matching_mode & match_mode);

  void setMonitor(MonitorBase * monitor);

  const QString & get_quanti_group_id() const;

  void add_peptide_quanti_items(std::vector <const Peptide *> & isotope_peptides);

  void add_mz_quanti_items(const QStringList & mz_list);
  
  void add_mzrt_quanti_item(const QString & mz, const QString & rt);

  void quantify();
  
  void performPostMatching();

 private:

  void set_quanti_group(const msRunHashGroup * quanti_group);
  
  void set_quanti_method(QuantificationMethod * quanti_method);
  
  const QString _xml_id;

  const msRunHashGroup * _group_to_quantify;
  
  QuantificationMethod * _quanti_method;
  
  MonitorBase * _monitor;
  
  PeakMatcher * _peak_matcher;

  std::vector<QuantiItemBase *> * _quanti_items;

  void sortQuantiItems();

};


#endif /* _QUANTIFICATOR_H_ */

