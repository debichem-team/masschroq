/**
 * \file xic_peak.cpp
 * \date September 21, 2009
 * \author Olivier Langella
 */

#include "xic_peak.h"
#include "../xic/xic_base.h"
#include <math.h>

/// il faut les anciens rt pour le calcul de l'aire et 
/// les alignés pour le contains_rt

xicPeak::xicPeak(const QuantiItemBase * xic_quanti_item, const Msrun * xic_msrun) 
	:
	_xic_quanti_item(xic_quanti_item),
	_xic_msrun(xic_msrun)
{
	_previous_rtime = 0;
	_previous_intensity = 0;
	_first_rtime = 0;
	_first_intensity = 0;
	_area = 0;
	_rtime_max = 0;
	_max_intensity = 0;
}

xicPeak::~xicPeak() {
}

const Msrun * 
xicPeak::getMsrun() const {
	return _xic_msrun;
}

/// anciens rt
void
xicPeak::add_measure(mcq_double rtime, mcq_double intensity) {
	if (intensity >= _max_intensity) {
		_max_intensity = intensity;
		_rtime_max = rtime;
	}
	if (_previous_rtime != 0) {
		_area += ( (fabs((double) rtime - _previous_rtime)) * 
				   (intensity + _previous_intensity) ) / 2;
	} else {
		_first_rtime = rtime;
		_first_intensity = intensity;
	}
	_previous_rtime = rtime;
	_previous_intensity = intensity;
}

void 
xicPeak::print(std::ostream & out) const {
	mcq_double aligned_rtime_max = this->get_aligned_max_rt();
	out << "rtime max\tmax intensity\tarea\tmz_start\tmz_stop" << std::endl;
	out << aligned_rtime_max << "\t" << _max_intensity << "\t" << _area << "\t"
		<< _xic_quanti_item->get_min_mz() << "\t"
		<< _xic_quanti_item->get_max_mz() << std::endl;
}

void 
xicPeak::printXmlResults(std::ostream & out) const {
	mcq_double aligned_rtime_max = this->get_aligned_max_rt();
	out << "<peak rtime_max=\"" << aligned_rtime_max << "\" max_intensity=\""
		<< _max_intensity << "\" area=\"" << _area << "\"/>" << std::endl;
}

bool 
xicPeak::contains_rt(mcq_double rt, mcq_double coeff) const {
	mcq_double aligned_first_rtime = this->get_aligned_first_rtime();
	mcq_double aligned_last_rtime = this->get_aligned_last_rtime();
  
	if ( (rt >= aligned_first_rtime) && 
		 (rt <= aligned_last_rtime) ) {
		return (true);
	}
  
	if (coeff > 0) {
		mcq_double plus_length = 
			coeff * ( (aligned_last_rtime - aligned_first_rtime) / 2 );
		if ( (rt >= (aligned_first_rtime - plus_length)) && 
			 (rt <= (aligned_last_rtime + plus_length)) ) {
			return (true);
		}
	}
	return (false);
}

