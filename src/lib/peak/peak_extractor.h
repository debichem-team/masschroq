/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file peak_extractor.h
 * \date September 21, 2009
 * \author Olivier Langella
 */


#ifndef PEAK_EXTRACTOR_H_
#define PEAK_EXTRACTOR_H_

#include <vector>
//#include <iostream>

#include "xic_peak.h"
#include "../detections/peak_detection_base.h"
#include "../xic/xic_base.h"

//using namespace std;

/**
 * \class PeakExtractor
 * \brief A class that extracts the peaks from a Xic
 *
 * The extraction is performed based on :
 * - a xic type (_xic_type member)
 *  
 */

class PeakExtractor {
  
public:
  
	PeakExtractor(const PeakDetectionBase * p_peak_detection);
  
	virtual ~PeakExtractor();
  
	std::vector<xicPeak *> * newAllPeaksList(const xicBase * xic) const;
	std::vector<xicPeak *> * newAllPeaksList(const QuantiItemBase * quanti_item,
										const Msrun * msrun,
										const std::vector<mcq_double> * v_rt,
										const std::vector<mcq_double> * v_intensity) const;
	
protected:
  
	const PeakDetectionBase * _p_peak_detection;
  
};

#endif /*PEAK_EXTRACTOR_H_*/
