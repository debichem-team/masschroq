/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xic_peak.h
 * \date September 21, 2009
 * \author Olivier Langella
 */


#ifndef XIC_PEAK_H_
#define XIC_PEAK_H_

//#include <iostream>
#include "../quanti_items/quantiItemBase.h"
#include "../msrun/msrun.h"

class xicBase;
/**
 * \class xicPeak
 * \brief Represents a peak in a Xic. A peak is 
 */

class xicPeak {

 public:
  
  xicPeak(const QuantiItemBase * xic_quanti_item, const Msrun * xic_msrun);
  virtual ~xicPeak();
  
  const Msrun * getMsrun() const;

  void print(std::ostream & out) const;
  void printXmlResults(std::ostream & out) const;
  
  void add_measure(mcq_double rtime, mcq_double intensity);  

  bool contains_rt(mcq_double rt, mcq_double coeff = 0) const;
  
  mcq_double get_first_rtime() const {
    return (_first_rtime);
  };
  
  mcq_double get_first_intensity() const {
    return (_first_intensity);
  };
  
  mcq_double get_last_rtime() const {
    return (_previous_rtime);
  };
  
  mcq_double get_last_intensity() const {
    return (_previous_intensity);
  };
  
  mcq_double get_max_intensity() const {
    return (_max_intensity);
  };
  
  mcq_double get_max_rt() const {
    return (_rtime_max);
  };
  
  mcq_double get_area() const {
    return (_area);
  };
 
  mcq_double get_aligned_first_rtime() const {
    return (_xic_msrun->getAlignedRtByOriginalRt(_first_rtime));
  };
   
  mcq_double get_aligned_last_rtime() const {
    return (_xic_msrun->getAlignedRtByOriginalRt(_previous_rtime));
  };

  mcq_double get_aligned_max_rt() const {
    return (_xic_msrun->getAlignedRtByOriginalRt(_rtime_max));
  };
  

 private:
  
  mcq_double _first_rtime;
  mcq_double _first_intensity;
  mcq_double _previous_rtime;
  mcq_double _previous_intensity;
  
  mcq_double _area;
  mcq_double _rtime_max;
  mcq_double _max_intensity;
  
  const QuantiItemBase * _xic_quanti_item;
  const Msrun * _xic_msrun;

};

#endif /*XIC_PEAK_H_*/
