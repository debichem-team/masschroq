/**
 * \file peak_extractor.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include "peak_extractor.h"

PeakExtractor::PeakExtractor(const PeakDetectionBase * p_peak_detection)
	:
	_p_peak_detection(p_peak_detection)
{
}

PeakExtractor::~PeakExtractor()
{
}

std::vector<xicPeak *> * 
PeakExtractor::newAllPeaksList(const xicBase * xic) const
{
  
	// qDebug() << "peakExtractor::newPeakList begin";
  
	std::vector<xicPeak *> * p_peak_list = 
		_p_peak_detection->newDetectedPeaks(*xic);
  
	//qDebug() << "peakExtractor::newPeakList end";
  
	return (p_peak_list);
}

std::vector<xicPeak *> * 
PeakExtractor::newAllPeaksList(const QuantiItemBase * quanti_item,
							   const Msrun * msrun,
							   const std::vector<mcq_double> * v_rt,
							   const std::vector<mcq_double> * v_intensity) const
{
 	std::vector<xicPeak *> * p_peak_list = 
		_p_peak_detection->newDetectedPeaks(quanti_item,
											msrun,
											v_rt,
											v_intensity);
	
	return (p_peak_list);
}
