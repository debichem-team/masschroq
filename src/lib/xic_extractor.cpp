#include <fstream>

#include "xic_extractor.h"
#include "../saxhandlers/xicextractorparamhandler.h"

#include "qms_error.h"
#include "xic/xic.h"
#include "xic/xic_sum.h"

xicExtractor::xicExtractor()
{
	//_filter_background = true;
	_xic_type = "max";
	_p_background_filter = NULL;
	_p_xic_peak_detection = NULL;
}

xicExtractor::~xicExtractor()
{
	if (_p_background_filter != NULL) {
		delete _p_background_filter;
	}
	if (_p_xic_peak_detection != NULL) {
		delete _p_xic_peak_detection;
	}	
}

void xicExtractor::push_mass(float mass) {
	peakSearchMz mz_search;
	mz_search.set_mz(mass);
	peakExtractor * p_peak_extractor(mz_search.newPeakExtractor(_ms_run));
	p_peak_extractor->set_p_xic_peak_detection(_p_xic_peak_detection);
	p_peak_extractor->set_p_background_filter(_p_background_filter);
	p_peak_extractor->set_xic_type(_xic_type);
	p_peak_extractor->set_p_xic_mz_selection(_xic_mz_selection);
	
	_peak_extractor_list.push_back(p_peak_extractor);
}

void xicExtractor::push_mass(const peakSearchMzRt & search_mzrt) {
	//cerr << "begin xicExtractor::push_mass(const peakSearchMzRt & search_mzrt)" << endl;
	//search_mzrt.print(cerr);
	
	peakExtractorMzRt * p_peak_extractor(search_mzrt.newPeakExtractor(_ms_run));
	p_peak_extractor->set_p_xic_peak_detection(_p_xic_peak_detection);
	p_peak_extractor->set_p_background_filter(_p_background_filter);
	p_peak_extractor->set_xic_type(_xic_type);
	p_peak_extractor->set_p_xic_mz_selection(_xic_mz_selection);
	
	_peak_extractor_list.push_back(p_peak_extractor);
}


void xicExtractor::set_result_file_info(const QFileInfo & result_file) {
	_result_file_info = result_file;
}


void xicExtractor::set_msrun(msRun * the_ms_run)
{
	_ms_run = the_ms_run;
}


void xicExtractor::setParam(QString fileName)
{
	xicExtractorParamHandler handler(this);
	QXmlSimpleReader reader;
	reader.setContentHandler(&handler);
	reader.setErrorHandler(&handler);
	
	QFile file(fileName);
	_param_file_info.setFile(file);


	QXmlInputSource xmlInputSource(&file);
	if (reader.parse(xmlInputSource)) {
		//cout << "parsing OK" << endl;
	}
	else {
		throw qmsError("error reading xicextractor param file :\n" + handler.errorString().toStdString());
	}
}

void xicExtractor::detectAndQuantify() {
	
	std::vector<peakExtractor *>::const_iterator it;
	for (it = _peak_extractor_list.begin (); it != _peak_extractor_list.end (); ++it) {
		
		//detectAndQuantifyPeaksOnMass(*it);
		(*it)->run();
	}
	
}


void xicExtractor::printExtractorResults(std::ostream & out) const {
	
	out << endl;
	out << "peak detection on file : " << endl << _ms_run->getFileInfo().absoluteFilePath ().toStdString () << endl << endl;
	out << "list of mass to analyze : " << endl;
	std::vector<peakExtractor *>::const_iterator it;
	for (it = _peak_extractor_list.begin (); it != _peak_extractor_list.end (); ++it) {
		out << (*it)->get_mz() << " ";
	}	
	out << endl << endl;
	out << "peaks : " << endl;
	for (it = _peak_extractor_list.begin (); it != _peak_extractor_list.end (); ++it) {
		(*it)->print(out);
	}	
}

void xicExtractor::printXmlResults() const {
	
	if (_result_file_info.fileName () == "") {
		printXmlResults(cout);
	}
	else {
		//try to write in file
		std::ofstream file_out;
		file_out.open(_result_file_info.absoluteFilePath().toStdString().c_str());
		printXmlResults(file_out);
		file_out.close();
		
	}
	
}


void xicExtractor::printXmlResults(std::ostream & out) const {
	out << "<xicextractor>" << endl;
	out << "<results>" << endl;
	out << "<param_file path=\"" << _param_file_info.absoluteFilePath().toStdString () << "\"/>" << endl;
	out << "<msrun_file path=\"" << _ms_run->getFileInfo().absoluteFilePath ().toStdString ()  << "\"/>" << endl;
	
	out.precision(10);
	
	std::vector<peakExtractor *>::const_iterator it;
	for (it = _peak_extractor_list.begin (); it != _peak_extractor_list.end (); ++it) {
		(*it)->printXmlResults(out);
	}	
	out << "</results>" << endl;
	out << "</xicextractor>" << endl;
}
