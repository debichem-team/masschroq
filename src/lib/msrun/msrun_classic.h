/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file msrun_classic.h
 * \date 18 sept. 2009
 * \author Olivier Langella
 */

#ifndef MSRUN_CLASSIC_H_
#define MSRUN_CLASSIC_H_ 1

#include "msrun.h"



/**
 * \class MsrunClassic
 * \brief Class representing an MS-run that stores spectra in a temporary file,
 * puts them on RAM when needed, and restores them on disk when done.
 */

class MsrunClassic : public Msrun {
 
 public:
  
  MsrunClassic(const QString & msrunid);
  virtual ~MsrunClassic();
  
  virtual void prepareSpectraForQuantification(const XicExtractionMethodBase *);
  
  virtual void doneWithSpectra();
  
  virtual xicBase * extractXic(const mcq_xic_type & xic_type, 
			       QuantiItemBase * look_for_mz) const;
  
 protected:

  virtual void setSpectrum(const mcq_double retention_time, spectrum * the_spectrum);

  /* virtual void priv_quantify(QuantificationMethod * quantification_method, */
  /* 			     PeakMatcher * peak_matcher, */
  /* 			     const vector<const QuantiItemBase *> * quantification_items_vector, */
  /* 			     MonitorBase & monitor_list); */
 
 private:  
  
  /// hash map : original retention time -> corresponding spectrum
  /// (as parsed in the mzXml file)
  std::map<mcq_double, spectrum *> _map_lc;
  
  void setXicVectors(xicBase * ptr_on_xic) const;
  
  /// delete the spectra parse, they are not needed anymore
  virtual void deleteSpectra();
  
  /// begin iterator for _map_lc
  std::map<mcq_double, spectrum *>::const_iterator begin_map_lc() const {
    return (_map_lc.begin());
  }
  
  /// end iterator for _map_lc
  std::map<mcq_double, spectrum *>::const_iterator end_map_lc() const {
    return (_map_lc.end());
  }
  
};

#endif /*MSRUN_CLASSIC_H_*/
