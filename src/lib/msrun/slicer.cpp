/**
 * \file slicer.cpp
 * \date June 17, 2011
 * \author Edlira Nano
 */


#include "slicer.h"
#include "spectrum.h"
#include "../../lib/consoleout.h"
#include "../mcq_error.h"
#include "../quanti_items/quantiItemBase.h"
#include <math.h>

Slicer::Slicer(const int nb_of_slices, 
	       const mcq_double min_mz_range, 
	       const mcq_double max_mz_range,
	       const mcq_double overlap_range)
  :
  _nb_of_slices(nb_of_slices)
{
  // qDebug() << "Slicer created: nb of slices " << _nb_of_slices;
  set_min_mz_range(min_mz_range);
  set_max_mz_range(max_mz_range);
  set_overlap_mz_range(overlap_range);
  compute_mz_range_per_slice();
  _current_waked_slice = NULL_SLICE_ID;
  setSlices();
}

Slicer::~Slicer(){
  std::vector<Slice * >::iterator it_slice;
  for (it_slice = _slices.begin();
       it_slice != _slices.end();
       ++it_slice) {
    if (*it_slice != 0) {
      delete (*it_slice);
      *it_slice = 0;
    }
  }
  _slices.clear();
  _current_waked_slice = NULL_SLICE_ID;
}

void
Slicer::setSlices() {
  if (_nb_of_slices <= 0) {
    throw mcqError(QObject::tr("error in msrun slicer: nonsense, the number of slices is set to %1, but it should be a positive non zero integer :)\n").arg(_nb_of_slices));
  }
  
  _slices.resize(_nb_of_slices);
  mcq_double slice_begin_mz(_min_mz_range);
  mcq_double slice_end_mz(_max_mz_range);
  for (int i = 0; i < _nb_of_slices; ++i) {
    slice_begin_mz = _min_mz_range + (_mz_range_per_slice * i) - _overlap_mz_range;
    slice_end_mz = slice_begin_mz + _mz_range_per_slice + (2 * _overlap_mz_range);
    mcq_slice_index_id slice_index_id = i;
    Slice * current_slice = new Slice(slice_index_id, slice_begin_mz, slice_end_mz);
    _slices[i] = current_slice;
  }
}

void
Slicer::set_min_mz_range(const mcq_double min_mz_range) {
  _min_mz_range = min_mz_range;
}

void
Slicer::set_max_mz_range(const mcq_double max_mz_range) {
   if (max_mz_range > 0) {
     _max_mz_range = max_mz_range;
   } else {
     _max_mz_range = 2500;
   }
}

void
Slicer::set_overlap_mz_range(const mcq_double overlap_range) {
  qDebug() << "Computed overlap is: " << overlap_range;
  if (overlap_range > 2) {
    const mcq_double overlap_security_margin = 0.1;
    _overlap_mz_range = overlap_range + overlap_security_margin;
  } else {
    _overlap_mz_range = 2;
  }
}
  

void 
Slicer::compute_mz_range_per_slice() {
  mcq_double diff_mz_range = _max_mz_range - _min_mz_range;
  if (diff_mz_range <= 0) {
    throw mcqError(QObject::tr("error in msrun slicer: preposterous, the max_mz_range = %1 and the _min_mz_range = %2 are not set correctly :)\n").arg(_max_mz_range, _min_mz_range));
  }
  /// range per slice rounded up (ceil(2.3) = 3)
  _mz_range_per_slice = ceil(diff_mz_range/_nb_of_slices);

  //qDebug() << "Slicer : the computed mz range per slice is " << _mz_range_per_slice; 
}

void 
Slicer::setSpectrumInSlices(const mcq_double rt, spectrum * the_spectrum) {
  /// create nb_of_slices empty new spectra, one for each slice
  std::vector<Slice * >::iterator it_slice;
  for (it_slice = _slices.begin();
       it_slice != _slices.end();
       ++it_slice) {
    (*it_slice)->initialize_current_spectrum();
  }
  
  /// put the (mz,int) values in the spectrum of the corresponding slice 
  std::vector<mcq_double> * v_int = the_spectrum->getIntensityVector(); 
  std::vector<mcq_double> * v_mz = the_spectrum->getMzVector();
  std::vector<mcq_double>::const_iterator it_mz;
  std::vector<mcq_double>::const_iterator it_int;
  
  for (it_mz = v_mz->begin(), it_int = v_int->begin(); 
       it_mz != v_mz->end(); 
       ++it_mz, ++it_int) {
    for (it_slice = _slices.begin();
	 it_slice != _slices.end();
	 ++it_slice) {
      (*it_slice)->putMzIntInSpectrum(*it_mz, *it_int);
    }
  }
  
  /// sleep the newly created spectra on disk 
  for (it_slice = _slices.begin();
       it_slice != _slices.end();
       ++it_slice) {
    (*it_slice)->sleep_current_spectrum(rt);
  }
}

void
Slicer::closeCacheInSlices() {
  std::vector<Slice * >::iterator it_slice;
  for (it_slice = _slices.begin();
       it_slice != _slices.end();
       ++it_slice) {
    (*it_slice)->closeCache();
  }
}

const mcq_slice_index_id
Slicer::getSliceIdForMz(const QuantiItemBase * quanti_item) const {
  const mcq_double high_mz = quanti_item->get_max_mz();
  const mcq_double low_mz = quanti_item->get_min_mz();
 
  std::vector<Slice * >::const_iterator it_slice;
  for (it_slice = _slices.begin();
       it_slice != _slices.end(); 
       ++it_slice) {
    
    if ( (high_mz <= ((*it_slice)->get_max_mz()))
	 && 
	 (low_mz >= ((*it_slice)->get_min_mz())) ) {
      return ((*it_slice)->get_index_id());
    }
  }
  return NULL_SLICE_ID;
}


void 
Slicer::wakeUpSliceForMz(const QuantiItemBase * quanti_item) {
  const mcq_slice_index_id slice_index = getSliceIdForMz(quanti_item);
  
  /// if no slice corresponds, return
  if (slice_index == NULL_SLICE_ID) {
    mcqout() << "WARNING: mz = "
  	 << quanti_item->get_mz()
  	 << ": not quantified, no slice was waked."
  	 << endl;
    return;
  }
  
  /// if the corresponding slice is already the currently waked one, nothing to do
  if (slice_index == _current_waked_slice) {
    return;
  } 
  /// else we have found a new corresponding slice, but before waking it up
  /// we have to sleep the old one on disk
  sleepSlice(_current_waked_slice);
  /// finally, wake the new slice
  _current_waked_slice = slice_index;
  qDebug() << "WakeUpSlice:Waking slice "<< slice_index << endl;
  wakeUpSlice(_current_waked_slice);
}

void
Slicer::sleepSlice(const mcq_slice_index_id index_id) {
  if (index_id < _nb_of_slices) {
    if (index_id != NULL_SLICE_ID) {
      _slices[index_id]->sleepSpectraOnDisk();
    } 
  } else {
    throw mcqError(QObject::tr("error in msrun slicer: pfoui, this is a bug, you are trying to sleep an unexisting slice index_id '%1'.\n").arg(index_id));
  }
} 

void
Slicer::wakeUpSlice(const mcq_slice_index_id index_id) {
  if (index_id < _nb_of_slices) {
    if (index_id != NULL_SLICE_ID) {
      _slices[index_id]->wakeUpSpectra();
    } 
  } else {
    throw mcqError(QObject::tr("error in msrun slicer: pfoui, this is a bug, you are trying to wake up an unexisting slice index_id '%1'.\n").arg(index_id));
  }
}

void
Slicer::setXicVectors(xicBase * ptr_on_xic) const {
  if ( (_current_waked_slice != NULL_SLICE_ID) 
       &&
       (_current_waked_slice < _nb_of_slices) ) {
    _slices[_current_waked_slice]->setXicVectors(ptr_on_xic);
  } else {
    throw mcqError(QObject::tr("error in msrun slicer: pfoui, this is a bug, the current waked slice index is -1.\n"));
  }
}

void 
Slicer::clearSlices() {
  qDebug() << "Clearing all slices"<< endl;
  std::vector<Slice * >::iterator it_slice;
  for (it_slice = _slices.begin();
       it_slice != _slices.end();
       ++it_slice) {
    (*it_slice)->clear();
  }
  _current_waked_slice = NULL_SLICE_ID;
}
