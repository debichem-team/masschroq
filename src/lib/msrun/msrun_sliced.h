/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file msrun_sliced.h
 * \date June 14, 2011
 * \author Edlira Nano
 */

#ifndef MSRUN_SLICED_H_
#define MSRUN_SLICED_H_ 1

#include "msrun.h"
#include "slicer.h"

/**
 * \class MsrunSliced
 * \brief Class representing a sliced LC-MS run.
 */

class MsrunSliced : public Msrun {
 
 public:
  
  MsrunSliced(const QString & msrunid, const int nb_of_slices);

  virtual ~MsrunSliced();

  virtual void prepareSpectraForQuantification(const XicExtractionMethodBase *);
  
  virtual void doneWithSpectra();

  virtual xicBase * extractXic(const mcq_xic_type & xic_type, 
			       QuantiItemBase * look_for_mz) const;
  
 protected:

  virtual void setSpectrum(const mcq_double retention_time, spectrum * the_spectrum);
  
 private:

  /// full (with path) template filename for the temporary file
  const QString _fullTmpFilename;
  
  const int _nb_of_slices;
  
  Slicer * _slicer;
  
  void setSlicer(const XicExtractionMethodBase * xic_extraction_method);

  void deleteSlicer();
  
};

#endif /*MSRUN_SLICED_H_*/
