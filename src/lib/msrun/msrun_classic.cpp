/**
 * \file msrun_classi.cpp
 * \date 18 sept. 2009
 * \author Olivier Langella
 */


#include "msrun_classic.h"
#include "../xic/xic_max.h"
#include "../xic/xic_sum.h"


#include <math.h>


MsrunClassic::MsrunClassic(const QString & msrunid) 
  :
  Msrun(msrunid)
{
}

MsrunClassic::~MsrunClassic() {
  this->deleteSpectra();
}

/// delete the spectra of this msrun, we do not need it anymore
void 
MsrunClassic::deleteSpectra() {
  std::map<mcq_double, spectrum *>::iterator it;
  for (it = _map_lc.begin(); it != _map_lc.end(); ++it) {
    if (it->second != 0) {
      delete (it->second);
      (it->second) = 0;
    }
  }
  _map_lc.clear();
}

/// set spectrum (parsed from the mzxml file)
void 
MsrunClassic::setSpectrum(const mcq_double retention_time, spectrum * the_spectrum) {
  _map_lc[retention_time] = the_spectrum;
}

void
MsrunClassic::prepareSpectraForQuantification(const XicExtractionMethodBase * 
					      xic_extraction_method) {
  this->parseSpectra();
}

void
MsrunClassic::doneWithSpectra() {
  deleteSpectra();
}

xicBase *
MsrunClassic::extractXic(const mcq_xic_type & xic_type, 
			 QuantiItemBase * look_for_mz) const {
  /// extract XIC
  xicBase * ptr_on_xic;
  if (xic_type == MAX_XIC_TYPE) {
    ptr_on_xic = new xicMax(this, look_for_mz);
  } else if (xic_type == SUM_XIC_TYPE) {
    ptr_on_xic = new xicSum(this, look_for_mz);
  } else {
    throw mcqError(QObject::tr("error extracting xic :\n xic type can be 'sum' or 'max', but not %1").arg(xic_type));
  }
  
  setXicVectors(ptr_on_xic);
  
  if (ptr_on_xic->size() == 0) {
    delete ptr_on_xic;
    ptr_on_xic = 0;
  }
  return (ptr_on_xic);
}

void
MsrunClassic::setXicVectors(xicBase * ptr_on_xic) const {
  std::vector<mcq_double> * rt_vector = ptr_on_xic->getRetentionTimes(); 
  std::vector<mcq_double> * intensity_vector = ptr_on_xic->getIntensities(); 
  std::map<mcq_double, spectrum *>::const_iterator it_map_lc;
  for (it_map_lc = this->begin_map_lc(); 
       it_map_lc != this->end_map_lc(); 
       ++it_map_lc) {
    rt_vector->push_back(it_map_lc->first);
    mcq_double sum_or_max = ptr_on_xic->getIntensityFromSpectrum(*(it_map_lc->second));
	intensity_vector->push_back(sum_or_max);
  }
}
