/**
 * \file  precursor.cpp
 * \date 8 juin 2010
 * \author Edlira Nano
 */


#include "precursor.h"


Precursor::Precursor(const int scan_num, const mcq_double rt,
		     const mcq_double intensity, const mcq_double mz) 
  : 
  _scan_num(scan_num), _rt(rt), 
  _intensity(intensity), _mz(mz) {}

Precursor::~Precursor() {}

mcq_double Precursor::getIntensity() const {
  return _intensity; 
}

mcq_double Precursor::getMz() const {
  return _mz; 
}

mcq_double Precursor::getRt() const {
  return _rt; 
}

int Precursor::getScanNum() const {
  return _scan_num; 
}





 
