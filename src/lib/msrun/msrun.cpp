/**
 * \file msrun.cpp
 * \date June 14, 2011
 * \author Edlira Nano
 */


#include "msrun.h"
#include "../../lib/consoleout.h"
#include "../mcq_error.h"
#include "../../saxparsers/xmlParserFactory.h"
#include "../peptides/peptide_list.h"

#include <math.h>
//#include <iostream>
//#include <fstream>
#include <QTime>
#include <QDebug>

Msrun::Msrun(QString msrunid) :
	_xml_id(msrunid)
{
	_peptide_list = NULL;
	_low_mz = 0;
	_high_mz = 0;
}

Msrun::~Msrun()
{
  
	delete (_peptide_list);
	std::map<int, Precursor *>::iterator it2;
	for (it2 = _map_scan_precursor.begin(); 
		 it2 != _map_scan_precursor.end();
		 ++it2)
	{
		delete it2->second;
	}
	_map_scan_precursor.clear();
	_map_aligned_rt.clear();
	_original_rt_set.clear();
}
 
/// set the list of peptides observed in this msrun
void 
Msrun::setPeptideList(PeptideList * pep_list)
{
	_peptide_list = pep_list;
}

/// get the list of peptides observed in this msrun
PeptideList * 
Msrun::getPeptideList() const
{
	return _peptide_list;
}
 
/// adds an element to the hash map scan_num -> precursor
void 
Msrun::mapPrecursor(int scan_num, Precursor * precursor)
{
	_map_scan_precursor[scan_num] = precursor;
}

/** 
 * \fn mcq_double getPrecursorIntensity(const int scan_num) 
 * \brief searches the _map_scan_precursor map for the Precursor 
 * object of scan number scan_num and returns its intensity.
 */
mcq_double 
Msrun::getPrecursorIntensity(const int scan_num) const
{
	std::map<int, Precursor *>::const_iterator it;
	it = _map_scan_precursor.find(scan_num);

	if (it != _map_scan_precursor.end())
		return ( (it->second)->getIntensity() );
	else
		throw 
			mcqError(QObject::tr("error in msrun '%1' (in getPrecursorIntensity method): scan number %2 does not exist\n").arg(this->getXmlId()).arg(scan_num));
}

/** 
 * \fn mcq_double get_precursor_rt(const int scan_num) 
 * \brief searches the _map_scan_precursor map for the Precursor 
 * object of scan number scan_num and returns its retention time. 
 */
mcq_double 
Msrun::get_precursor_rt(const int scan_num) const
{
	std::map<int, Precursor *>::const_iterator it;
	it = _map_scan_precursor.find(scan_num);
  
	if (it != _map_scan_precursor.end())
		return ( (it->second)->getRt() );
	else
		throw mcqError(QObject::tr("error in msrun '%1' (in get_precursor_rt method): scan number %2 does not exist\n").arg(this->getXmlId()).arg(scan_num));
}

void
Msrun::addOriginalRetentionTime(const mcq_double retention_time)
{
	_original_rt_set.insert(retention_time);
}

/** 
 * \fn void setNewTimeValues(const std::vector<mcq_double> & new_time_values)
 * \brief sets the new retention times in the hash map _map_aligned_rt 
 * containing [original rt -> aligned rt].
 * This method is called twice : once in the read_time_values method called 
 * after the parsing of the mzXml files and once after an alignment of this 
 * msrun has been performed. In the first case the new_time_values vector 
 * parameter contains both original ang aligned times (read from the .time 
 * file), in the second case (for example after an MS2 alignment) it may
 * contain only the aligned time values in the order of the corresponding 
 * original values (as in _map_lc).
 */
void 
Msrun::setNewTimeValues(const std::vector<mcq_double> & new_time_values)
{
  
	qDebug() << "Msrun::setNewTimeValues begin";

	std::set<mcq_double>::const_iterator it_original_rt;
	std::vector<mcq_double>::const_iterator it_new_tv;
	mcq_double old_rt(-1);
	mcq_double new_rt(-1);
  
	// If the new rt values are twice the old original rt values,
	// (the new_time_values vector is supposed to contain
	// original_rt1 aligned_rt1 original_rt2 aligned_rt2 ...)
	if (new_time_values.size() == (_original_rt_set.size() * 2))
	{
    
		qDebug()
			<< "Msrun::setNewTimeValues input size is twice the map_lc size : the vector contains : original and aligned times";
    
		for (it_original_rt = _original_rt_set.begin(), it_new_tv = new_time_values.begin(); 
			 it_original_rt != _original_rt_set.end(); 
			 ++it_original_rt, it_new_tv += 2) 
		{
			old_rt = *it_new_tv;
			new_rt = *(it_new_tv + 1);
			// check that original time values are the same in _original_rt_set 
			// and in new_time_values
			if (*it_original_rt == old_rt)
				_map_aligned_rt[old_rt] = new_rt;
			else
				throw 
					mcqError("error in Msrun::setNewTimeValues :\n the original retention time does not correspond to the one given");
		}
	}
 
	// else (the new_rt values are not twice the old_rt values)
	else
	{       
		qDebug()
			<< "Msrun::setNewTimeValues input size == _original_rt_set size : the vector contains : aligned times only";
		// and they're neither the same size, so throw exception
		if (new_time_values.size() != _original_rt_set.size())
		{
			qDebug()<<"Original number of time "<<_original_rt_set.size()<<" is different of "<<new_time_values.size();
			throw 
				mcqError("error in Msrun::setNewTimeValues :\n wrong number of time values");
		}
    
		// if they have the same size, copy the aligned values in _map_aligned_rt
		for (it_original_rt = _original_rt_set.begin(), it_new_tv = new_time_values.begin(); 
			 it_original_rt != _original_rt_set.end(); 
			 ++it_original_rt, ++it_new_tv) 
		{
			old_rt = *it_original_rt;
			new_rt = *(it_new_tv);
			_map_aligned_rt[old_rt] = new_rt;
		} 
	}

	// check that after copy the two maps have the same size
	if (_map_aligned_rt.size() != _original_rt_set.size())
	{
		throw 
			mcqError("error in Msrun::setNewTimeValues :\n original rt table size and new rt table size does not match");
	}
  
	qDebug() << "Msrun::setNewTimeValues end";
}

/**
 * read_time_values reads the time values in the .time file in the 
 * given directory time_dir if it exists and calls setNewTimeValues with them.
 */
bool 
Msrun::read_time_values(const QString & time_dir)
{
	QString dir = time_dir;
	const QFileInfo & fileInfo = this->getXmlFileInfo();
	if (time_dir.isEmpty()) {
		dir = fileInfo.absolutePath();
	}
	QFile file(dir + "/" + fileInfo.completeBaseName() + ".time");
	
	if (file.exists()) {
		mcqout() << "MS run '"
				 << (this->getXmlId())
				 << "' : reading time values file '"
				 << file.fileName()
				 << "'"
				 << endl;
		//try to read file
		std::vector<mcq_double> retention_times;
		mcq_double time;
		std::ifstream file_in;
		file_in.open(file.fileName().toStdString().c_str());
		while (file_in.good())
		{
			file_in >> time;
			if (file_in.good())
			{
				retention_times.push_back(time);
			}
		}
		file_in.close();
		setNewTimeValues(retention_times);
	} else {
		mcqout() << "WARNING : alignment time file '" << file.fileName()
				 << "' : does not exist, no time values to read." << endl;
	}
	return true;
}
	
/** 
 * \fn void printRetentionTimes(std::ostream & out) 
 * \brief prints to the output stream out (ex. on file .time) 
 * the retention times : (original_rt aligned_rt) if there are aligned times,
 * otherwise it prints the retention times per spectrum
 */
void 
Msrun::printRetentionTimes(QTextStream & out) const
{
	if (this->hasBeenAligned())
	{
		std::map<mcq_double, mcq_double>::const_iterator it;
		for (it = _map_aligned_rt.begin(); it != _map_aligned_rt.end(); ++it)
		{
			out << it->first << "\t" << it->second << endl;
		}
	} else
	{
		//no aligned retention times to write, only the original ones
		std::set<mcq_double>::const_iterator it;
		for (it = _original_rt_set.begin(); it != _original_rt_set.end(); ++it)
		{
			out << *it << endl;
		}
	}
}


/**
 * \fn bool write_time_values() 
 * \brief calls printRetentionTimes to print the rt in the  
 * _xml_file.time file in the current directory. If the 
 * file already exists it will be overwrited.  
 */
bool 
Msrun::write_time_values() const
{
	/// look for the .time file
	QFile file(_xml_file.absolutePath() + 
			   "/" +
			   _xml_file.completeBaseName() + 
			   ".time");
	QFileInfo fileInfo(file);
	
	/// if it exists it will be overwrited,
	if (file.exists())
	{
		mcqout() << "WARNING : MS run '"
				 << (this->getXmlId())
				 << "' :  time values file '"
				 << fileInfo.fileName()
				 << "' : already exists, it will be overwrited"
				 << endl;
	} else
	{
		mcqout() << "MS run '" << (this->getXmlId()) 
				 << "' : writing time values : file '"
				 << fileInfo.fileName()
				 << "' : does not exist, it will be created"
				 << endl;
	}
	 /// try to write in file
	QFile file_out(file.fileName());
	file_out.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file_out);
	//std::ofstream file_out;
	//file_out.open(file.fileName().toStdString().c_str());
	printRetentionTimes(out);
	file_out.close();
	
	return (true);
}

/**
   Returns true if this msrun has been aligned. This happens in two cases :
   if a .time file has been read in the beginning of the MassChroQ process
   (i.e. an old alignment has been charged) or if an alignment has occurred 
   during this process.
*/
const bool
Msrun::hasBeenAligned() const
{
	return ( ! _map_aligned_rt.empty() );
}

/**
   clears the alignment effects on this Ms-run. We need to call this method 
   when we begin a new alignment (which will in any case overwrite the 
   previous one). See alignment classes for more information.  
*/
void
Msrun::resetAlignmentTimeValues()
{
	if (this->hasBeenAligned())
		_map_aligned_rt.clear();
}

/**
   Get the aligned rt corresponding to a given orginal rt in the 
   original rt -> aligned rt map _map_aligned_rt. If the given 
   original rt does not correspond exactly to a value in the map, 
   which is often the case, we make a linear extrapolation of the 
   aligned rt-s to find the nearest rt corresponding to it. 
*/
mcq_double 
Msrun::getAlignedRtByOriginalRt(mcq_double original_rt) const
{
     
	/// if the hash table of "originaRt -> alignedRt" is empty, 
	/// no alignment has occurred so return original_rt
	if (_map_aligned_rt.empty())
	{
		return (original_rt); 
	}
	/// else
	/// get the alignedRt in the table if the corresponding originalRt exists
	std::map<mcq_double, mcq_double>::const_iterator 
		it(_map_aligned_rt.find(original_rt));
	if (it != _map_aligned_rt.end())
	{
		return (it->second);
    
		/// else (if the originalRt is not in the table) make a linear 
		/// extrapolation of the two nearest surrounding aligned rt values
	} else
	{
    
		it = _map_aligned_rt.begin();
		std::map<mcq_double, mcq_double>::const_iterator itbefore(it);
    
		/// if the original rt is smaller than the ones in the map
		/// get the first value in the map 
		if ( (it->first) > original_rt )
		{
			return (it->second);
		}
 
		it++;
		while ((it != _map_aligned_rt.end()) && ((it->first) < original_rt))
		{
			it++;
			itbefore++;
		}
    
		/// it original_rt is biger than the ones in the map get the last value
		if (it == _map_aligned_rt.end())
		{
			return (itbefore->second);
		} else
		{
			/// linear extrapolation
			mcq_double time_min = itbefore->first;
			mcq_double time_max = it->first;
			mcq_double time_max_minus_min = time_max - time_min;
			mcq_double ratio;
			if (time_max_minus_min != 0)
				ratio = (original_rt - time_min) / time_max_minus_min;
			else 
				ratio = 1;
      
			mcq_double t_min = itbefore->second;
			mcq_double t_max = it->second;
			return (itbefore->second + (ratio * (t_max - t_min)));
		}
	}
}

const std::vector<mcq_double>
Msrun::getOriginalRetentionTimes() const
{
	std::vector<mcq_double> v_rt;
	std::set<mcq_double>::const_iterator it_original_rt;
	for (it_original_rt = this->begin_original_rt(); 
		 it_original_rt != this->end_original_rt(); 
		 ++it_original_rt)
	{
		v_rt.push_back(*it_original_rt);
	}
	return v_rt;  
}

const std::vector<mcq_double>
Msrun::getAlignedRetentionTimes() const
{
	if (this->hasBeenAligned())
	{
		std::vector<mcq_double> v_rt;
		std::map<mcq_double, mcq_double>::const_iterator it_map_aligned;
		for (it_map_aligned = _map_aligned_rt.begin(); 
			 it_map_aligned != _map_aligned_rt.end(); 
			 ++it_map_aligned)
		{
			v_rt.push_back(it_map_aligned->second);
		}
		return v_rt;
	} else
		return this->getOriginalRetentionTimes();
}

const std::vector<mcq_double>
Msrun::getVectorOfTimeValues() const
{
	std::vector<mcq_double> v_time;
	std::map<mcq_double, mcq_double>::const_iterator it_map_aligned;
	for (it_map_aligned = _map_aligned_rt.begin(); 
		 it_map_aligned != _map_aligned_rt.end(); 
		 ++it_map_aligned)
	{
		v_time.push_back(it_map_aligned->first);
		v_time.push_back(it_map_aligned->second);
		
	}
	return v_time;
}


/// simple parsing of this msrun's xml file
bool 
Msrun::set_from_xml(const QString & fileName, const mcq_xml_format & format,
					const bool read_time, const QString & time_dir)
{
	qDebug() << "Msrun::set_from_xml begin " << fileName;
	XmlParserFactory xml_factory;
  
	QFile file(fileName);
	setXmlFile(fileName);
	setXmlFileFormat(format);
  
	XmlSimpleParser * xml_parser = xml_factory.newXmlSimpleParser(format, this);
	QXmlSimpleReader reader;
	QXmlInputSource xmlInputSource(&file);
	reader.setContentHandler(xml_parser);
	reader.setErrorHandler(xml_parser);
  
	QTime tparsing;
	tparsing.start();
  
	if (reader.parse(xmlInputSource))
	{
		mcqout() << "MS run '"
			 << (this->getXmlId())			 << "' : xml file '"
			 << fileName 
			 << "' :  parsing OK" << endl;
    
		qDebug() << "Total simple parsing time of msrun "
				 << this->getXmlId() << endl;
		qDebug("is %d ms", tparsing.elapsed()); 
  
	} else
	{
		throw mcqError(QObject::tr("error reading xml input file '%1' :\n").arg(fileName).append(xml_parser->errorString()));
    
		mcqerr() << "error reading msrun xml input file " << endl;
		mcqerr() << xml_parser->errorString() << endl;
		return (false);
	}

	//read time values if asked so by the user
 	if (read_time) {
		read_time_values(time_dir);
	}
	
	delete (xml_parser);
	qDebug() << "Msrun::set_from_xml end " << fileName;
	return (true);
}

/// spectra parsing of this msrun's xml file
bool
Msrun::parseSpectra()
{
  
	QFile file(_xml_file.absoluteFilePath());
	XmlParserFactory xml_factory;
  
	XmlSpectrumParser * xml_parser = xml_factory.newXmlSpectrumParser(_xml_file_format, this);
	QXmlSimpleReader reader;
	QXmlInputSource xmlInputSource(&file);
	reader.setContentHandler(xml_parser);
	reader.setErrorHandler(xml_parser);
  
	// QTime tparsing;
	//tparsing.start();
  
	if (reader.parse(xmlInputSource))
	{
		mcqout() << "MS run '"
			 << (this->getXmlId())
			 << "' : xml file '"
			 << (file.fileName()) 
			 << "' :  parsing OK" << endl;
    
		// qDebug() << "Total spectra parsing time of msrun sliced"
		// 	     << this->getXmlId() << endl;
		// qDebug("is %d ms", tparsing.elapsed());
  
	} else
	{
		throw mcqError(QObject::tr("error reading xml input file '%1' :\n").arg(file.fileName()).append(xml_parser->errorString()));
		return (false);
	}
  
	delete (xml_parser);
	return (true);
}

void
Msrun::set_low_mz(const mcq_double low_mz)
{
}

void
Msrun::set_high_mz(const mcq_double high_mz)
{
	mcq_double abs_high_mz = fabs(high_mz);
	if (abs_high_mz > _high_mz)
	{
		_high_mz = abs_high_mz;
	}
}

const mcq_double
Msrun::get_low_mz() const
{
	return _low_mz;
}

const mcq_double
Msrun::get_high_mz() const
{
	return _high_mz;
}
