/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file slicer.h
 * \date June 17, 2011
 * \author Edlira Nano
 */

#ifndef SLICER_H_
#define SLICER_H_ 1

#include <map>
#include <vector>
#include <QString>
#include <QTemporaryFile>

#include "../../mcq_types.h"
#include "slice.h"


class spectrum;
class xicBase;
class QuantiItemBase;

/**
 * \class Slicer
 * \brief Class representing a slicer of msrun spectra.
 *
 */


class Slicer {
 
 public:
  
  Slicer(const int nb_of_slices, 
	 const mcq_double min_mz_range, 
	 const mcq_double max_mz_range,
	 const mcq_double overlap_mz_range);

  ~Slicer();
 
  /// Each spectrum decoded in the msrun file is divided into nb_of_slices slices of spectra
  /// containing the (mz, int) portion of values allowed for each slice.  
  void setSpectrumInSlices(const mcq_double rt, spectrum * the_spectrum);
 
  /// Once all the spectra decoded from the msrun file are divided into nb_of_slices 
  /// portion of spectra, we need to close the cache file/stream corresponding to each slice
  void closeCacheInSlices();
  
  /// Given a quanti_item, find the slice that contains teh right portion of spectra and wake up
  /// its cache file (from disk to RAM)
  void wakeUpSliceForMz(const QuantiItemBase * quanti_item); 
    
  /// Given an empty xic, set its data from the current waked slice
  void setXicVectors(xicBase * ptr_on_xic) const;
  
  /// Empty, clear all cache and everything in all slices, done with this slicer
  void clearSlices();

 private :

  void set_min_mz_range(const mcq_double min_mz_range);
  void set_max_mz_range(const mcq_double max_mz_range);
  void set_overlap_mz_range(const mcq_double overlap_mz_range); 
  void compute_mz_range_per_slice(); 

  void setSlices();
  const mcq_slice_index_id getSliceIdForMz(const QuantiItemBase * quanti_item) const;
  void wakeUpSlice(const mcq_slice_index_id index_id);
  void sleepSlice(const mcq_slice_index_id index_id);
  

  const int _nb_of_slices;
  mcq_double _min_mz_range;
  mcq_double _max_mz_range;
  mcq_double _overlap_mz_range;
  mcq_double _mz_range_per_slice;
    
  
  /// vector containing the slices
  std::vector<Slice * > _slices;
  
  /// unique index_id of the currently waked slice 
  /// (the one being charged in memory and being worked with)   
  mcq_slice_index_id _current_waked_slice;

};
#endif /*SLICER_H_*/
