/**
 * \file ms_run_hash_group.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include "ms_run_hash_group.h"
#include "../consoleout.h"
#include "../peak/peak_extractor.h"
#include "../xic/xic_factory.h"
#include "../filters/filter_base.h"
#include "../quantifications/quantificationMethod.h"
#include "../monitors/monitorBase.h"

msRunHashGroup::msRunHashGroup() {
  _p_msrun_ref = NULL;
  _group_xml_id = "root";
}

msRunHashGroup::msRunHashGroup(const QString group_xml_id) {
  _p_msrun_ref = NULL;
  _group_xml_id = group_xml_id;
}

msRunHashGroup::~msRunHashGroup() {
}

/// delete msruns and clear container
void 
msRunHashGroup::free() {
  msRunHashGroup::iterator it;
  for (it = this->begin(); it != this->end(); it++) {
    delete (it->second);
  }
  this->clear();
}


/// returns true if this msrun group contains the given msrun
bool 
msRunHashGroup::containsMsRun(const Msrun * p_msrun) const {
  msRunHashGroup::const_iterator it;
  for (it = this->begin(); it != this->end(); it++) {
    if (it->second == p_msrun) {
      return (true);
    }
  }
  return (false);
}

/// returns a pointer to the msrun with the given id if it is part of 
/// this group, otherwise NULL is returned 
Msrun * 
msRunHashGroup::getMsRun(const QString & idname) const {
  msRunHashGroup::const_iterator it;
  it = find(idname);
  if (it == end()) {
    return (NULL);
  } else {
    return (it->second);
  }
}

/// adds the given msrun to this group (if it is not already in) 
/// the first msrun setted here will be the reference one 
void 
msRunHashGroup::setMsRun( Msrun * p_msrun) {
  const QString idname(p_msrun->getXmlId());
  msRunHashGroup::iterator it;
  it = find(idname);
  if (it == end()) {
    //OK this key does not exist
    this->operator[](idname) = p_msrun;
  } else {
    throw mcqError(QObject::tr("error in msRunHashGroup::setMsRun :\nthe msrun with id %1 already exists").arg(idname));
  }
}

void 
msRunHashGroup::setReferenceMsrun(const QString & ref_msrun_id) {
  Msrun * ref_msrun = this->getMsRun(ref_msrun_id);
  if (ref_msrun == NULL) {
    /// we have a problem
    throw 
      mcqError(QObject::tr("error in msRunHashGroup::setReferenceMsrun :\nthe msrun with id %1 does not exist or is not part of group %2").arg(ref_msrun_id, _group_xml_id));
  } else {
    _p_msrun_ref = ref_msrun;
  }
}

const Msrun * 
msRunHashGroup::getReferenceMsrun() const {
  return (_p_msrun_ref);
} 

/// aligns the msruns of this group (one by one, towards the reference msrun)
void 
msRunHashGroup::alignMsRuns(AlignmentBase & alignment_method) {
  if (this->size() > 1) {
       
    //warperize_now.setRef(*p_ref_msrun);
    //p_ref_msrun->sleepOnDisk();
    
    alignment_method.printInfos(mcqout());
    msRunHashGroup::iterator it;
    Msrun * p_current_ms_run;
    
    for (it = this->begin(); it != this->end(); ++it) 
      {
	p_current_ms_run = it->second;
	alignment_method.alignTwoMsRuns(_p_msrun_ref, 
					p_current_ms_run, 
					this);
      }
    
    alignment_method.clean();
  } else {
    mcqout() << "WARNING : Skipping alignment in group '" 
	 << (this->_group_xml_id) 
	 <<"' : not enough msruns in this group.";
  }
}

/// detects peaks and quantifies
// void 
// msRunHashGroup::detectAndQuantify(QuantificationMethod * quanti_method,
// 				  const vector<const QuantiItemBase *> * quanti_items,
// 				  MonitorBase & monitor) const {
  
//   qDebug() << "msRunHashGroup::detectAndQuantify begin on "
// 	   << this->_group_xml_id;
  
//   monitor.setCurrentGroup(this);
  
//   Msrun * current_msrun;
//   msRunHashGroup::const_iterator itmsrun;
//   unsigned int count_msruns(1);
  
//   for (itmsrun = this->begin(); 
//        itmsrun != this->end(); 
//        ++itmsrun, ++count_msruns) 
//     {
//       current_msrun = itmsrun->second;
     
//       cout << "\tQuantifying in MS run '"
// 	   << (current_msrun->getXmlId()).toStdString() 
// 	   << "' : " << count_msruns << "/" << this->size() 
// 	   << " in group '" << _group_xml_id.toStdString() 
// 	   << "'" << endl;
      
//       monitor.setCurrentMsrun(current_msrun);

//       QString quanti_type;
//       current_msrun->quantify(quanti_method, 
// 			      quanti_items,
// 			      monitor);
      
//       monitor.setEndCurrentMsrun();
//     }
//   monitor.setEndCurrentGroup();

//   qDebug() << "void msRunHashGroup::detectAndQuantify end on "
// 	   << this->_group_xml_id;
// }
