/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file  precursor.h
 * \date 8 juin 2010
 * \author Edlira Nano
 */

#ifndef PRECURSOR_H_
#define PRECURSOR_H_ 1

#include "../../config.h"

/**
 * \class Precursor
 * \brief A Precursor object represents the parent/precursor of the MS level 2
 * scan with number _scan_num in the mzxml file.

 * In mzxml files, MS scans of level 2 have each a precursor object
 * representing their MS level 1 parent/precursor. A Precursor object has :
 * - a scan number
 * - a retention time value
 * - an intensity value
 * - an mz value
 */

class Precursor {
 
 public : 
  
  Precursor(const int scan_num, const mcq_double rt,
	    const mcq_double intensity, const mcq_double mz);
  
  virtual ~Precursor();
  
  mcq_double getIntensity() const ;
  
  mcq_double getMz() const;
  
  mcq_double getRt() const;
  
  int getScanNum() const;
  
 private :
  ///< MS level 2 scan number whome this Precursor is parent 
  const int _scan_num; 
  /// retention time of this precursor
  const mcq_double _rt; 
  /// intensity of this precursor
  const mcq_double _intensity;
  /// mz of this precursor
  const mcq_double _mz;

};

#endif /* PRECURSOR_H_ */
