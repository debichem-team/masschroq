/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file slice.h
 * \date June 29, 2011
 * \author Edlira Nano
 */

#ifndef SLICE_H_
#define SLICE_H_ 1


#include <vector>
#include <QTemporaryFile>

#include "../../mcq_types.h"


class spectrum;
class xicBase;

/**
 * \class Slice
 * \brief Class representing a slice of msrun spectra.
 *
 */


class Slice {
 
 public:
  
  Slice(const mcq_slice_index_id index_id, 
	const mcq_double min_mz,
	const mcq_double max_mz);

  ~Slice();
  
  const mcq_slice_index_id get_index_id() const;
  
  const mcq_double get_min_mz() const;
  
  const mcq_double get_max_mz() const;
  
  void initialize_current_spectrum();

  void setSpectrum(const mcq_double rt, spectrum * the_spectrum);

  void putMzIntInSpectrum(const mcq_double mz, const mcq_double intensity);
  
  void sleep_current_spectrum(const mcq_double rt);
  
  void wakeUpSpectra();
  
  void sleepSpectraOnDisk();

  void setCache();

  void closeCache();
  
  void clear();
  
  void setXicVectors(xicBase * ptr_on_xic);

 private :
  
  bool isSleeping() const;
  
  void initSlice();
  
  void deleteCache();
  void deleteSpectra();

  /// unique id (unsigned int) for this slice
  const mcq_slice_index_id _index_id;
  
  mcq_double _min_mz;
  mcq_double _max_mz;

  std::vector<mcq_double> _rt_v;
  std::vector<spectrum *> _spectra_v;
  
  spectrum * _current_spectrum;

  const QString _fullTmpFilename;
  QTemporaryFile * _current_spectrum_cache_file;
  QDataStream * _current_spectrum_out_stream; 
  
  bool _is_sleeping;
};
#endif /*SLICE_H_*/
