/**
 * \file spectrum.cpp
 * \date 18 janv. 2010
 * \author Olivier Langella
 */

#include "spectrum.h"
#include "../mcq_error.h"

#include <math.h>
#include <limits>
#include <vector>
#include <QDebug>


spectrum::spectrum() {
	_p_v_intensity = new std::vector<mcq_double>;
	_p_v_mz = new std::vector<mcq_double>;
}

spectrum::~spectrum() {
	if (_p_v_intensity != 0) {
		delete _p_v_intensity;
		_p_v_intensity = 0;
	}
	if (_p_v_mz != 0) {
		delete _p_v_mz;
		_p_v_mz = 0;
	}
}

void
spectrum::debugPrintValues() {
	for (unsigned int i = 0; i < _p_v_intensity->size(); ++i) {
		qDebug() << "mz = " << _p_v_mz->at(i) 
				 << ", int = " << _p_v_intensity->at(i);
	}
}
 
const unsigned int 
spectrum::getSpectrumSize() const {
	checkAwakeOrDie();
	return (_p_v_intensity->size());
}

void 
spectrum::checkAwakeOrDie() const {
	if (_p_v_intensity == 0) {
		throw mcqError("error in spectrum::checkAwakeOrDie :\n spectrum is sleeping on disk, and it should not");
	}
}

void 
spectrum::sleepOnDisk(QDataStream & out) {
  
	out << (quint32) _p_v_intensity->size();
	std::vector<mcq_double>::const_iterator it;
	for (it = _p_v_intensity->begin(); it != _p_v_intensity->end(); ++it) {
		out << *it;
	}
	delete _p_v_intensity;
  
	for (it = _p_v_mz->begin(); it != _p_v_mz->end(); ++it) {
		out << *it;
	}
	delete _p_v_mz;

	_p_v_intensity = 0;
	_p_v_mz = 0;
}

void 
spectrum::wakeUp(QDataStream & in) {
	if (_p_v_intensity != 0) {
		return;
	}
	_p_v_intensity = new std::vector<mcq_double>;
	_p_v_mz = new std::vector<mcq_double>;
  
	quint32 size;
	mcq_double temp;
	in >> size;
	reserve(size);
	for (quint32 i = 0; i < size; i++) {
		in >> temp;
		_p_v_intensity->push_back(temp);
	}
  
	for (quint32 i = 0; i < size; i++) {
		in >> temp;
		_p_v_mz->push_back(temp);
	}
}

void 
spectrum::clear() {
	checkAwakeOrDie();
	_p_v_intensity->clear();
	_p_v_mz->clear();
}

void 
spectrum::reserve(const unsigned int size) {
	checkAwakeOrDie();
	_p_v_intensity->reserve(size);
	_p_v_mz->reserve(size);
}

void 
spectrum::reserve_mz(const unsigned int size) {
	checkAwakeOrDie();
	_p_v_mz->reserve(size);
}

void 
spectrum::reserve_intensity(const unsigned int size) {
	checkAwakeOrDie();
	_p_v_intensity->reserve(size);
}

void 
spectrum::setPeak(const mcq_double mz, const mcq_double intensity) {
	checkAwakeOrDie();
	_p_v_mz->push_back(mz);
	_p_v_intensity->push_back(intensity);
}

void 
spectrum::setPeakMz(const mcq_double mz) {
	checkAwakeOrDie();
	_p_v_mz->push_back(mz);
}

void 
spectrum::setPeakIntensity(const mcq_double intensity) {
	checkAwakeOrDie();
	_p_v_intensity->push_back(intensity);
}


// method called by the obiwarp alignment, that is why we have mcq_floats
void 
spectrum::integrate_mass(mcq_float * mass_list,
						 std::map<int, mcq_float> & mass_integrator, 
						 mcq_float precision) const {
	checkAwakeOrDie();
	//reset mass integrator :
	std::map<int, mcq_float>::iterator it;
	for (it = mass_integrator.begin(); it != mass_integrator.end(); ++it) {
		(*it).second = 0;
	}
  
	mcq_float coeff = 1 / precision;
	mcq_float half = (precision / 2);

	unsigned int i(0);
  
	std::vector<mcq_double> & v_mz(*_p_v_mz);
	std::vector<mcq_double> & v_intensity(*_p_v_intensity);
  
	for (i = 0; i < v_mz.size(); i++) {
		unsigned int mass_index = (unsigned int) ((v_mz[i] - half) * coeff);
		it = mass_integrator.find(mass_index);
		if (it == mass_integrator.end()) {
			//mass index not found
		} else {
			it->second += (mcq_float)v_intensity[i];
		}
	}
  
	i = 0;
	for (it = mass_integrator.begin(); it != mass_integrator.end(); ++it, i++) {
		mass_list[i] = it->second;
	}
}

mcq_double 
spectrum::getSumIntensity(const mcq_double mz_start, const mcq_double mz_stop) const {
	checkAwakeOrDie();
	mcq_double sum(0);
	std::vector<mcq_double> & v_mz(*_p_v_mz);
	std::vector<mcq_double> & v_intensity(*_p_v_intensity);
  
	for (unsigned int i = 0; ((i < v_mz.size()) && (mz_stop > v_mz[i])); ++i) {
		if (mz_start < v_mz[i]) {
			sum += v_intensity[i];
		}
	}
	return (sum);
}

mcq_double 
spectrum::getMaxIntensity(const mcq_double mz_start, 
						  const mcq_double mz_stop) const {
	checkAwakeOrDie();
	//mcq_double max(numeric_limits<mcq_double>::quiet_NaN());
	mcq_double max(0);
	std::vector<mcq_double> & v_mz(*_p_v_mz);
	std::vector<mcq_double> & v_intensity(*_p_v_intensity);

	for (unsigned int i = 0; ((i < v_mz.size()) && (mz_stop > v_mz[i])); ++i) {
		if (mz_start < v_mz[i]) {
			if (max < v_intensity[i]) {
				max = v_intensity[i];
			}
		}
	}
	return (max);
}

std::vector<mcq_double> *
spectrum::getIntensityVector() {
	return _p_v_intensity;
}

std::vector<mcq_double> *
spectrum::getMzVector() {
	return _p_v_mz;
}

