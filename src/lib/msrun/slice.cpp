/**
 * \file slice.cpp
 * \date June 29, 2011
 * \author Edlira Nano
 */


#include "slice.h"
#include "spectrum.h"
#include "../mass_chroq.h"
#include "../share/utilities.h"
#include "../xic/xic_base.h"
#include <math.h>

Slice::Slice(const mcq_slice_index_id index_id, 
			 const mcq_double min_mz, 
			 const mcq_double max_mz)
	:
	_index_id(index_id),
	_min_mz(min_mz),
	_max_mz(max_mz),
	_fullTmpFilename(MassChroq::tmp_dir.absolutePath() + "/" + TMP_FILENAME_PREFIX)
{
	initSlice();
	setCache();
}

void
Slice::initSlice() {
	_current_spectrum_cache_file = 0;
	_current_spectrum_out_stream = 0;
	_current_spectrum = 0;
	_is_sleeping = false;
}

void
Slice::setCache() {
	_current_spectrum_cache_file = new QTemporaryFile(_fullTmpFilename);
	_current_spectrum_cache_file->open();
	_current_spectrum_out_stream = new QDataStream(_current_spectrum_cache_file);
}

const mcq_slice_index_id
Slice::get_index_id() const {
	return _index_id;
}


const mcq_double 
Slice::get_min_mz() const {
	return _min_mz;
}

const mcq_double 
Slice::get_max_mz() const {
	return _max_mz;
}

bool 
Slice::isSleeping() const {
	return _is_sleeping;
}

Slice::~Slice() {
	clear();
}

void
Slice::clear() {
	deleteSpectra();
	deleteCache();
	_rt_v.clear();
	_rt_v.resize(0);
	_is_sleeping = true;
}

void
Slice::deleteCache() {
	if (_current_spectrum_out_stream != 0) {
		qDebug() << "Deleting output stream in slice " << _index_id << endl;
		delete _current_spectrum_out_stream;
		_current_spectrum_out_stream = 0;
	}
 
	if (_current_spectrum_cache_file != 0) {
		qDebug() << "Deleting cache in slice " << _index_id << endl;
		delete (_current_spectrum_cache_file);
		_current_spectrum_cache_file = 0;
	}
}

void
Slice::deleteSpectra() {
	qDebug() << "Deleting spectra in slice " << _index_id << endl;
	std::vector<spectrum *>::iterator it;
	for (it = _spectra_v.begin(); it != _spectra_v.end(); ++it) {
		if ( (*it) != 0) {
			delete (*it);
			(*it) = 0;
		}
	}
	_spectra_v.clear();
	_spectra_v.resize(0);
}

void 
Slice::initialize_current_spectrum() {
	_current_spectrum = new spectrum();
}

void
Slice::putMzIntInSpectrum(const mcq_double mz, const mcq_double intensity) { 
	if ( (mz <= _max_mz) && (mz >= _min_mz) ) {
		_current_spectrum->setPeak(mz, intensity);
	}
}

void
Slice::sleep_current_spectrum(const mcq_double rt) {
	_rt_v.push_back(rt);
	_spectra_v.push_back(_current_spectrum);
	_current_spectrum->sleepOnDisk(*_current_spectrum_out_stream);
}

void
Slice::closeCache() {
	if (this->isSleeping()) {
		// already sleeping
	} else {
		_current_spectrum_cache_file->close();
		if (_current_spectrum_out_stream != 0) {
			delete _current_spectrum_out_stream;
			_current_spectrum_out_stream = 0;
		}
		_is_sleeping = true;
	}
}


void
Slice::wakeUpSpectra() {
	if (this->isSleeping()) {
		_is_sleeping = false;
		_current_spectrum_cache_file->open();
		QDataStream in(_current_spectrum_cache_file);
		std::vector<spectrum *>::iterator it;
		for (it = _spectra_v.begin(); it != _spectra_v.end(); ++it) {
			(*it)->wakeUp(in);
		}   
		_current_spectrum_cache_file->close();
		deleteCache();
		_current_spectrum_cache_file = new QTemporaryFile(_fullTmpFilename);
	}
}

void
Slice::sleepSpectraOnDisk() {
	if (this->isSleeping()) {
		//already sleeping
	} else {
		_is_sleeping = true;
		_current_spectrum_cache_file->open();
		_current_spectrum_out_stream = new QDataStream(_current_spectrum_cache_file);
		QDataStream out (_current_spectrum_cache_file);
		std::vector<spectrum *>::iterator it;
		for (it = _spectra_v.begin(); it != _spectra_v.end(); ++it) {
			(*it)->sleepOnDisk(out);
		}
		_current_spectrum_cache_file->close();
	}
}

void
Slice::setXicVectors(xicBase * ptr_on_xic) {
	/// the rt in xics have to be in ascending order, but as we have slept spectra 
	/// in the order of mzxml parsing into simple vectors, no ordering is guaranteed
	/// in our _rt_v vector. Before setting the xic vectors we sort if necessary, 
	/// but we have to sort without loosing the matching rt-> spectra.
	std::vector<mcq_double> * rt_vector = ptr_on_xic->getRetentionTimes(); 
	std::vector<mcq_double> * intensity_vector = ptr_on_xic->getIntensities(); 
  
	if ( !(Utilities::isSorted(_rt_v)) ) {
		int size = _rt_v.size();
		Utilities::double_quicksort(_rt_v, _spectra_v, 0, (size - 1));
	}
  
	for (unsigned int i = 0; i < _spectra_v.size(); ++i) {
		rt_vector->push_back(_rt_v.at(i));
		mcq_double sum_or_max = ptr_on_xic->getIntensityFromSpectrum(*(_spectra_v.at(i)));
		intensity_vector->push_back(sum_or_max);
	}
} 
