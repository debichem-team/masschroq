/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file msrun.h
 * \date June 14, 2011
 * \author Edlira Nano
 */

#ifndef MSRUN_H_
#define MSRUN_H_ 1

#include "spectrum.h"
#include "precursor.h"

#include <set>
#include <QString>
#include <QFileInfo>
#include <QTemporaryFile>


class PeptideList;
class QuantiItemBase;
class xicBase;
class XicExtractionMethodBase;

/**
 * \class Msrun
 * \brief Interface class representing an LC-MS run.
 
 * - An LC-MS run is constructed with an unique id : _xml_id.
 * - It corresponds to a raw data file  _original_file in the system.
 * - It keeps a set member _original_rt_set containing the original retention times. 
 * This set is never modified.
 * - It keeps a hash map _map_aligned_rt. This map is filled after an 
 * alignment with original observed rt -> corresponding 
 * aligned rt. If no alignment has occurred this map stays empty.
 * - It keeps a hash map _map_scan_precursor : scan number -> precursor. 
 * This map is filled during the parsing of the raw data file : the scans of 
 * MS level >=2 have a precursor ion choosed for the fragmentation.
 * - It keeps a list of all the peptides observed in this msrun.
 *
 */

class Msrun {
 
 public:
  
  Msrun(QString msrunid);
  virtual ~Msrun();
    
  /// unique xml id for this msrun
  const QString _xml_id;
  
  /// .mzxml or .mzml data filename corresponding to this msrun
  QFileInfo _xml_file;
  
  /// format for the _xml_file
  mcq_xml_format _xml_file_format;
  
  void setXmlFile(const QString & filename) {
    _xml_file.setFile(filename);
  };
  
  void setXmlFileFormat(const mcq_xml_format & format) {
    _xml_file_format = format;
  };
 
  const QString & getXmlId() const {
    return _xml_id;
  }
  
  /// get the QFileInfo of the .mzxml or .mzml file for this msrun
  const QFileInfo & getXmlFileInfo() const {
    return (_xml_file);
  }
  
  /// get format of the xml file for this msrun
  const mcq_xml_format & getXmlFileFormat() const {
    return (_xml_file_format);
  }
  
  /// set the list of peptides observed in this msrun
  void setPeptideList(PeptideList * pep_list);
  
  PeptideList * getPeptideList() const;

  /// map precursor to its scan number as parsed in the xml file of this msrun
  void mapPrecursor(int scan_num, Precursor * precursor);
  
  /** 
   * \fn mcq_double getPrecursorIntensity(const int scan_num) 
   * \brief searches the _map_scan_precursor for the Precursor 
   * with scan number scan_num and returns its intensity.
   */
  mcq_double getPrecursorIntensity(const int scan_num) const;
  
  /** 
   * \fn mcq_double get_precursor_rt(const int scan_num) 
   * \brief searches the _map_scan_precursor map for the Precursor 
   * with scan number scan_num and returns its retention time. 
   */
  mcq_double get_precursor_rt(const int scan_num) const;
   
  /** 
   * \fn void setNewTimeValues(const std::vector<mcq_double> & new_time_values)
   * \brief sets the new retention times after alignment in map _map_aligned_rt 
   * (containing [original rt -> aligned rt]).
   * This method is called twice : once in the read_time_values method called 
   * after the parsing of the mzXml files and once after an alignment of this 
   * msrun has been performed. In the first case the new_time_values vector 
   * parameter contains both original ang aligned times (read from the .time 
   * file), in the second case (for example after an MS2 alignment) it may
   * contain only the aligned time values in the order of the corresponding 
   * original values.
   */ 
  void setNewTimeValues(const std::vector<mcq_double> & new_time_values);
  
  /// inserts an original retention time in set _original_rt_set
  void addOriginalRetentionTime(const mcq_double retention_time);

  /// read the retention times for this msrun (original or aligned)
  bool read_time_values(const QString & time_dir);
  /**
   * \fn bool write_time_values() 
   * \brief calls printRetentionTimes to print the rt in the  
   * _xml_file.time file in the current directory. If the 
   * file already exists it will be overwrited.  
   */
  bool write_time_values() const;

  /// print the retention times for this msrun (two columns original rt - aligned rt)
  void printRetentionTimes(QTextStream & out) const;
  
  /**
   Returns true if this msrun has been aligned. This happens in two cases :
   if a .time file has been read in the beginning of the MassChroQ process
   (i.e. an old alignment has been charged) or if an alignment has occurred 
   during this process.
  */
  const bool hasBeenAligned() const;

  /**
   clears the alignment effects on this Ms-run. We need to call this method 
   when we begin a new alignment (which will in any case overwrite the 
   previous one). See alignment classes for more information.  
  */
  void resetAlignmentTimeValues();

  /**
   Get the aligned rt corresponding to a given orginal rt in the 
   original rt -> aligned rt map _map_aligned_rt. If the given 
   original rt does not correspond exactly to a value in the map, 
   which is often the case, we make a linear extrapolation of the 
   aligned rt-s to find the nearest rt corresponding to it. 
 */
  mcq_double getAlignedRtByOriginalRt(mcq_double rt) const;

  const std::vector<mcq_double> getOriginalRetentionTimes() const;

  const std::vector<mcq_double> getAlignedRetentionTimes() const;

  const std::vector<mcq_double> getVectorOfTimeValues() const;
  
   /// begin iterator for _original_rt_set
  std::set<mcq_double>::const_iterator begin_original_rt() const {
    return (_original_rt_set.begin());
  }
  
  /// end iterator for _original_rt_set
  std::set<mcq_double>::const_iterator end_original_rt() const {
    return (_original_rt_set.end());
  }
 
  /// begin iterator for _map_aligned_rt
  std::map<mcq_double, mcq_double>::const_iterator begin_map_aligned() const {
    return (_map_aligned_rt.begin());
  }
  
  /// end iterator for _map_aligned_rt
  std::map<mcq_double, mcq_double>::const_iterator end_map_aligned() const {
    return (_map_aligned_rt.end());
  }
 
  /**
   * \fn bool set_from_xml(const QString & fileName, const mcq_xml_format & format)
   * \brief parses (simple)the .mzxml/.mzml file corresponding to this msrun and sets
   * all information apart from spectra
   */
  bool set_from_xml(const QString & fileName, const mcq_xml_format & format,
					const bool read_time_values, const QString & time_dir);

  virtual void setSpectrum(const mcq_double retention_time, spectrum * the_spectrum) = 0;
  
  void set_low_mz(const mcq_double low_mz);

  void set_high_mz(const mcq_double high_mz);

  const mcq_double get_low_mz() const;

  const mcq_double get_high_mz() const;

  /// quantification methods

  virtual void prepareSpectraForQuantification(const XicExtractionMethodBase *) = 0;
  
  virtual void doneWithSpectra() = 0;
  
  virtual xicBase * extractXic(const mcq_xic_type & xic_type, 
			       QuantiItemBase * look_for_mz) const = 0;
  
  /* void quantify(QuantificationMethod * quantification_method, */
  /* 		PeakMatcher * peak_matcher, */
  /* 		const vector<const QuantiItemBase * > * quantification_items_vector, */
  /* 		MonitorBase & monitor_list); */
  
 protected : 
  
  /* virtual void priv_quantify(QuantificationMethod * quantification_method, */
  /* 			     PeakMatcher * peak_matcher, */
  /* 			     const vector<const QuantiItemBase *> * quantification_items_vector, */
  /* 			     MonitorBase & monitor_list) = 0; */
  
  /// hash set containing the original retention times -> corresponding spectrum
  /// (as parsed in the mzXml file) this map never changes after simple parsing
  
  std::set<mcq_double> _original_rt_set;
  
  /// hash map : (original rt  -> aligned rt)
  std::map<mcq_double, mcq_double> _map_aligned_rt;
  
  /// hash map : scan_number -> precursor, as parsed in the mzXML file
  std::map<int, Precursor *> _map_scan_precursor;
  
  /// list of the peptides observed in this msrun
  PeptideList * _peptide_list;
  
  mcq_double _low_mz;
  mcq_double _high_mz;

  bool parseSpectra();
  
};

#endif /*MSRUN_H_*/
