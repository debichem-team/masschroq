/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file ms_run_hash_group.h
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#ifndef MS_RUN_HASH_GROUP_H_
#define MS_RUN_HASH_GROUP_H_ 1

#include "msrun.h"
#include <map>
#include <QString>

#include "../alignments/alignment_base.h"

class QuantiItemBase;
class MonitorBase;
//using namespace std;

/**
 * \class msRunHashGroup
 * \brief Class representing a group of msruns in the form of a hash map
 * of msrun id -> msrun *

 * An msRunHashGroup has a unique xml group id and a reference msrun.
 */

class msRunHashGroup : public std::map<QString, Msrun *> {

 public:

  msRunHashGroup();
  msRunHashGroup(const QString group_id);
  virtual ~msRunHashGroup();

  void free();

  void setMsRun(Msrun * p_msrun);
  
  Msrun * getMsRun(const QString & idname) const;
  
  const QString & getXmlId() const {
    return _group_xml_id;
  }

  void setReferenceMsrun(const QString & ref_msrun_id);

  const Msrun * getReferenceMsrun() const;
    

  void alignMsRuns(AlignmentBase & alignment_method);

  /* void detectAndQuantify(QuantificationMethod * quanti_method, */
  /* 			 const vector<const QuantiItemBase *> * */
  /* 			 quanti_items, */
  /* 			 MonitorBase & monitor) const ; */
  
  bool containsMsRun(const Msrun * p_msrun) const;
  
 private:

  Msrun * _p_msrun_ref;
  QString _group_xml_id;

};

#endif /* MS_RUN_HASH_GROUP_H_ */
