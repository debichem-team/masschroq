/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file spectrum.h
 * \date 18 janv. 2010
 * \author Olivier Langella
 */


#ifndef SPECTRUM_H_
#define SPECTRUM_H_ 1

//#include <iostream>

#include <QDataStream>
#include <map>
#include <vector>

#include "../../config.h"

//using namespace std;

/**
   \class spectrum
   \brief Represents a MS spectrum (intensity/mz)
*/
class spectrum {
 
 public:
  spectrum();
  virtual ~spectrum();
  
  void setPeak(const mcq_double mz, const mcq_double intensity);
  
  // void insertPeak(const mcq_double mz, const mcq_double intensity);
  
  void setPeakMz(const mcq_double mz);
  
  void setPeakIntensity(const mcq_double intensity);
  
  mcq_double getSumIntensity(const mcq_double mz_start, const mcq_double mz_stop) const;
  
  mcq_double getMaxIntensity(const mcq_double mz_start, const mcq_double mz_stop) const;
  
  const unsigned int getSpectrumSize() const;

  void debugPrintValues();
  void integrate_mass(mcq_float * mass_list, 
		      std::map<int, mcq_float> & mass_integrator, 
		      mcq_float precision) const;
  
  void reserve(const unsigned int size);
  void reserve_mz(const unsigned int size);
  void reserve_intensity(const unsigned int size);
  
  void sleepOnDisk(QDataStream & out);
  void wakeUp(QDataStream & in);

  std::vector<mcq_double> * getIntensityVector();

  std::vector<mcq_double> * getMzVector();
  
 private :

  void checkAwakeOrDie() const;
  
  void clear();
  
  std::vector<mcq_double> * _p_v_intensity;
  
  std::vector<mcq_double> * _p_v_mz;
  
};

#endif /*SPECTRUM_H_*/
