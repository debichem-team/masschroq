/**
 * \file msrun_sliced.cpp
 * \date June 14, 2011
 * \author Edlira Nano
 */


#include "msrun_sliced.h"
#include "../mass_chroq.h"
#include "../xic/xic_max.h"
#include "../xic/xic_sum.h"


MsrunSliced::MsrunSliced(const QString & msrunid, const int nb_of_slices) 
  :
  Msrun(msrunid),
  _nb_of_slices(nb_of_slices)
{
  _slicer = 0;
}

MsrunSliced::~MsrunSliced() {
  deleteSlicer();
}

void
MsrunSliced::deleteSlicer() {
  if (_slicer != 0) {  
    delete (_slicer);
    _slicer = 0;
  }
}
  

void 
MsrunSliced::setSpectrum(const mcq_double retention_time, spectrum * the_spectrum) {
  _slicer->setSpectrumInSlices(retention_time, the_spectrum);
  /// very important to delete the old spectrum here, we do not need it anymore
  if (the_spectrum != 0) {
    delete (the_spectrum);   
    the_spectrum = 0; 
  }
}

void
MsrunSliced::prepareSpectraForQuantification(const XicExtractionMethodBase * 
					     xic_extraction_method) {
    this->setSlicer(xic_extraction_method);
    this->parseSpectra();
    _slicer->closeCacheInSlices();
}

void
MsrunSliced::doneWithSpectra() {
  _slicer->clearSlices();
  deleteSlicer();
 }
 
// set the slicing for this msrun 
void
MsrunSliced::setSlicer(const XicExtractionMethodBase * xic_extraction_method) {
  deleteSlicer();
  const mcq_double min_mz = this->get_low_mz();
  const mcq_double max_mz = this->get_high_mz();
  mcq_double overlap_range;
  if (xic_extraction_method != NULL) {
    overlap_range = xic_extraction_method->getMzRangeForMz(max_mz);
  } else {
    overlap_range = 5;
  }
  qDebug() << "initializing slicer with _nb_of_slices, low_mz, high_mz " << _nb_of_slices
	   << ", " << min_mz << ", " << max_mz;
  _slicer = new Slicer(_nb_of_slices, min_mz, max_mz, overlap_range);
}

xicBase *
MsrunSliced::extractXic(const mcq_xic_type & xic_type, QuantiItemBase * quanti_item) const {
  /// wake the right slicer
  _slicer->wakeUpSliceForMz(quanti_item);
  
  /// extract XIC
  xicBase * ptr_on_xic;
  if (xic_type == MAX_XIC_TYPE) {
    ptr_on_xic = new xicMax(this, quanti_item);
  } else if (xic_type == SUM_XIC_TYPE) {
    ptr_on_xic = new xicSum(this, quanti_item);
  } else {
    throw mcqError(QObject::tr("error extracting xic :\n xic type can be 'sum' or 'max', but not %1").arg(xic_type));
  }
  
  /// setXicVectors
  _slicer->setXicVectors(ptr_on_xic);
  
  if (ptr_on_xic->size() == 0) {
    qDebug() << "Null Xic" <<endl;
    delete ptr_on_xic;
    ptr_on_xic = 0;
  }
  return (ptr_on_xic);
}
