/**
 * \file alignment_base.cpp
 * \date 23 sept. 2009
 * \author: Olivier Langella
 */

#include "alignment_base.h"
#include "../msrun/ms_run_hash_group.h"

AlignmentBase::AlignmentBase(MonitorAlignmentBase * monitor) {
	_monitorAlignment = monitor;
}

AlignmentBase::~AlignmentBase() {
}

/** method that aligns two msruns (the first one, p_msrun_ref, 
 being the reference msrun, group being the group they belong to)
 */
void AlignmentBase::alignTwoMsRuns(Msrun * p_msrun_ref, Msrun * p_msrun,
								   msRunHashGroup * group) {
	qDebug() << "Alignment of two MsRuns";
	/// don't align the reference msrun
	if (p_msrun_ref == p_msrun) {
		qDebug() << "Alignment of two MsRuns : don't align the reference msrun";
		return;
	}
	/// set the current alignment group
	_current_group = group;
	const Msrun * msrun_ref = group->getReferenceMsrun();
	if (p_msrun_ref != msrun_ref) {
		qDebug() << "Alignment of two MsRuns : different input reference";
		throw mcqError(
				QObject::tr(
						"error in AlignmentBase::alignTwoMsRuns :\nthe input reference msrun %1 is not the actual reference msrun (%2) for group %3").arg(
						p_msrun_ref->getXmlId(), msrun_ref->getXmlId(),
						group->getXmlId()));
	}

	/// If this msrun is being realigned the .time values eventually read before
	/// have no importance (they will be overwrited after the alignment).
	/// But in order to consider the original rt times for the alignment, we
	/// clear the effects of the reading of this obsolete .time (we clear
	/// _map_aligned_rt).
	p_msrun->resetAlignmentTimeValues();
	/// virtual method that aligns the two msruns
	this->privAlignTwoMsRuns(p_msrun_ref, p_msrun);
	
    /// write the aligned values in a .time file for the aligned msrun if asked by the user
	_monitorAlignment->setTimeValues(p_msrun);
    //		p_msrun->write_time_values();

}
