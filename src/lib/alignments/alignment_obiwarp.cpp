/**
 * \file alignment_obiwarp.cpp
 * \date 23 sept. 2009
 * \author Olivier Langella
 */

#include "alignment_obiwarp.h"

#include "../../libobiwarp/dynprog.h"

AlignmentObiwarp::AlignmentObiwarp(MonitorAlignmentBase * monitor)
	:
	AlignmentBase(monitor)
{
	_p_msrun_ref = 0;
	_ref_lmat = 0;
	_to_obiwarp_lmat = 0;
	_mass_start = 500;
	_mass_end = 1200;
	_lmat_precision = 1;
}

AlignmentObiwarp::~AlignmentObiwarp() {
}

void AlignmentObiwarp::printInfos(QTextStream & out) const {
	out << "\t Obiwarp alignment parameters :" << endl;
	out << "\t _mass_start = " << _mass_start << endl;
	out << "\t _mass_end = " << _mass_end << endl;
	out << "\t _lmat_precision = " << _lmat_precision << endl;
}

void AlignmentObiwarp::setLmatPrecision(mcq_double lmat_precision) {
	_lmat_precision = lmat_precision;
}

void AlignmentObiwarp::setMassStart(mcq_double mass_start) {
	_mass_start = mass_start;
}

void AlignmentObiwarp::setMassEnd(mcq_double mass_end) {
	_mass_end = mass_end;
}

void AlignmentObiwarp::privAlignTwoMsRuns(Msrun * p_msrun_ref,
		Msrun * p_msrun) {
	qDebug() << "AlignmentObiwarp::privAlignTwoMsRuns begin";
	if (_p_msrun_ref != p_msrun_ref) {
		_p_msrun_ref = p_msrun_ref;
		delete _ref_lmat;
		_ref_lmat = 0;
	}
	// set lmat for reference msrun
	// Attention! wake up reference ms_run and set reference lmat
	updateRefLmat();

	if (_to_obiwarp_lmat != 0) {
		delete (_to_obiwarp_lmat);
	}
	// set lmat for current msrun being aligned
	_to_obiwarp_lmat = new lmatReader();

	if (_to_obiwarp_lmat->set_from_xml(*p_msrun, _mass_start, _mass_end,
			_lmat_precision)) {

		//lmat OK
	} else {
		throw mcqError("ERROR reading the MsRun to obiwarp (creating lmat)");
	}
	/*********************************************************
	 // end set lmat to OBIWARP
	 *********************************************************/
	obiWarperize();

	p_msrun->setNewTimeValues(_time_shift);

	//qDebug() << "AlignmentObiwarp::privAlignTwoMsRuns end";
}

void AlignmentObiwarp::updateRefLmat() {
	if (_p_msrun_ref == 0) {
		throw mcqError(
				"Error in AlignmentObiwarp::updateRefLmat(), _p_msrun_ref is NULL");
	}
	if (_ref_lmat == 0) {
		_ref_lmat = new lmatReader();

		if (_ref_lmat->set_from_xml(*_p_msrun_ref, _mass_start, _mass_end,
				_lmat_precision)) {
			//lmat OK
		} else {
			throw mcqError("ERROR reading the reference msrun");
		}
	} else {
		return;
	}
}

void AlignmentObiwarp::obiWarperize() {
	MatF smat;
	DynProg dyn;
	bool opts_nostdnrm(0);
	std::string opts_score("cor");
	//mcq_double opts_gap_extend(2.4);
	mcq_double opts_gap_extend(0);
	mcq_double opts_gap_init(0);
	mcq_double opts_factor_diag(2);
	mcq_double opts_factor_gap(1);
	bool opts_local(0);
	mcq_double opts_init_penalty(0);
	mcq_double opts_response(1);

	//_to_obiwarp_lmat->print("/tmp/mat22.txt");

	// ************************************************************
	// * SCORE THE MATRICES
	// ************************************************************
	/*
	 if (opts.smat_in != NULL) {
	 smat.set_from_binary(opts.smat_in);
	 dyn._smat = &smat;
	 }
	 else {
	 */
	//std::cout << " opts_score.c_str() " << endl;
	//std::cout <<  opts_score.c_str() << endl;
	dyn.score(*(_ref_lmat->mat()), *(_to_obiwarp_lmat->mat()), smat,
			opts_score.c_str());
	// SETTING THE SMAT TO BE std normal
	if (!opts_nostdnrm) {
		if (!smat.all_equal()) {
			smat.std_normal();
		}
	}
	if (opts_score == "euc") {
		smat *= -1; // inverting euclidean
	}

	// ************************************************************
	// * PREPARE GAP PENALTY ARRAY
	// ************************************************************

	MatF time_tester;
	MatF time_tester_trans;
	VecF mpt;
	VecF npt;
	VecF mOut_tm;
	VecF nOut_tm;

	int gp_length = smat.rows() + smat.cols();

	//std::cout << "opts.gap_extend,opts.gap_init,gp_length " << endl;
	//std::cout << opts_gap_extend << " " << opts_gap_init << " " << gp_length << endl;
	VecF gp_array;
	dyn.linear_less_before(opts_gap_extend, opts_gap_init, gp_length, gp_array);

	// ************************************************************
	// * DYNAMIC PROGRAM
	// ************************************************************
	int minimize = 0;

	//std::cout << "minimize, opts.factor_diag, opts.factor_gap, opts.local, opts.init_penalty " << endl;
	//std::cout << minimize << " " << opts_factor_diag << " " << opts_factor_gap << " " << opts_local << " " <<  opts_init_penalty << endl;
	dyn.find_path(smat, gp_array, minimize, opts_factor_diag, opts_factor_gap,
			opts_local, opts_init_penalty);

	VecI mOut;
	VecI nOut;
	dyn.warp_map(mOut, nOut, opts_response, minimize);
	//puts("mOUT"); mOut.print(); nOut.print();

	// Major output unless its the only case where we don't need warped time
	// values
	//if (!(outfile_is_stdout && format_is_labelless(opts.format))) {
	// MAJOR OUTPUT:
	VecF nOutF;
	VecF mOutF;
	_ref_lmat->tm_axis_vals(mOut, mOutF);
	_to_obiwarp_lmat->tm_axis_vals(nOut, nOutF); //
	_to_obiwarp_lmat->warp_tm(nOutF, mOutF);
	//_to_obiwarp_lmat->tm()->print(1);

	unsigned int i, n;
	n = _to_obiwarp_lmat->tm()->length();
	_time_shift.resize(n);
	for (i = 0; i < n; ++i) {
		_time_shift[i] = _to_obiwarp_lmat->tm()->at(i);
	}

	/*
	 delete (_ref_lmat);
	 _ref_lmat = NULL;
	 delete (_to_obiwarp_lmat);
	 _to_obiwarp_lmat = NULL;
	 */

	//}
	// No labels on matrix and we have an outfile to produce
	// Needs to be after MAJOR OUTPUT since it warps the data!
	// if (format_is_labelless(opts.format) && outfile) {
	// @TODO: implement data warping here
	//}
}

void AlignmentObiwarp::correct_new_time_values() {
	// no correction implemented here
}

void AlignmentObiwarp::clean() {
	//qDebug() << "void AlignmentObiwarp::clean() begin";
	if (_ref_lmat != 0) {
		delete _ref_lmat;
		_ref_lmat = 0;
	}
	if (_to_obiwarp_lmat != 0) {
		delete _to_obiwarp_lmat;
		_to_obiwarp_lmat = 0;
	}
	//qDebug() << "void AlignmentObiwarp::clean() end";
}
