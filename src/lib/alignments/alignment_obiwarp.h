/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file alignment_obiwarp.h
 * \date 23 sept. 2009
 * \author Olivier Langella
 */

#ifndef ALIGNMENT_OBIWARP_H_
#define ALIGNMENT_OBIWARP_H_ 1

#include "alignment_base.h"

#include "lmatreader.h"

/**
 * \class AlignmentObiwarp
 * \brief Class implementing the OBI-Warp alignment 
 * 
 * Ordered Bijective Interpolated Warping (OBI-Warp) : 
 * http://obi-warp.sourceforge.net/ aligns two msruns by
 * aligning the XICs using the DTW (Dynamic Time Warping) 
 * algorithm.   
 */

class AlignmentObiwarp : public AlignmentBase {

 public:
  
  AlignmentObiwarp(MonitorAlignmentBase * monitor);
  virtual ~AlignmentObiwarp();
  
  virtual void printInfos(QTextStream & out) const;
  
  void setLmatPrecision(mcq_double lmat_precision);
  
  void setMassStart(mcq_double mass_start);
  
  void setMassEnd(mcq_double mass_end);

  mcq_double getLmatPrecision() const{
	  return(_lmat_precision);
  }

  mcq_double getMassStart() const{
	  return(_mass_start);
  }

  mcq_double getMassEnd() const{
	  return(_mass_end);
  }

  virtual void clean();

 protected:
  
  /// virtual method aligning two msruns
  virtual void privAlignTwoMsRuns(Msrun * p_msrun_ref, 
				  Msrun * p_msrun);

  virtual void correct_new_time_values();

 private:
	
  void updateRefLmat();
  
  void obiWarperize();
  
  Msrun * _p_msrun_ref;
  
  lmatReader * _ref_lmat;
  
  lmatReader * _to_obiwarp_lmat;
  
  std::vector<mcq_double> _time_shift;
  
  mcq_double _mass_start;
  mcq_double _mass_end;
  mcq_double _lmat_precision;

};

#endif /* ALIGNMENT_OBIWARP_H_ */
