/**
 * \file alignment_ms2.cpp
 * \date 9 juillet 2010
 * \author: Edlira Nano
 */
#include <QDebug>
#include "alignment_ms2.h"
#include <algorithm>
#include "../share/utilities.h"
#include "../consoleout.h"

AlignmentMs2::AlignmentMs2(MonitorAlignmentBase * monitor) :
		AlignmentBase(monitor) {
	_p_msrun_ref = 0;
	_ms2_tendency_halfwindow = 10;
	_ms2_smoothing_halfwindow = 10;
	_ms1_smoothing_halfwindow = 10;
}

AlignmentMs2::~AlignmentMs2() {
	_shared_peptide_list.clear();
	_new_time_values.clear();
}

/// sets the _ms2_tendency_halfwindow
void AlignmentMs2::setMs2TendencyWindow(mcq_double ms2_tendency_halfwindow) {
	_ms2_tendency_halfwindow = ms2_tendency_halfwindow;
}

///sets the _ms2_smoothing_halfwindow
void AlignmentMs2::setMs2SmoothingWindow(mcq_double ms2_smoothing_halfwindow) {
	_ms2_smoothing_halfwindow = ms2_smoothing_halfwindow;
}

///sets the _ms1_smoothing_halfwindow
void AlignmentMs2::setMs1SmoothingWindow(mcq_double ms1_smoothing_halfwindow) {
	_ms1_smoothing_halfwindow = ms1_smoothing_halfwindow;
}

/// sets the _correction_parameter
void AlignmentMs2::set_correction_parameter(
		const mcq_double correction_parameter) {
	_correction_parameter = correction_parameter;
}

void AlignmentMs2::printInfos(QTextStream & out) const {
	out << "\t MS2 alignment parameters :" << endl;
	out << "\t ms2_tendency_halfwindow = " << _ms2_tendency_halfwindow << endl;
	out << "\t ms2_smoothing_halfwindow = " << _ms2_smoothing_halfwindow
			<< endl;
	out << "\t ms1_smoothing_halfwindow = " << _ms1_smoothing_halfwindow
			<< endl;
}

/// set the _shared_peptide_list for the two msruns being aligned
void AlignmentMs2::setSharedPeptides(const Msrun * p_msrun_ref,
		const Msrun * p_msrun) {

	PeptideList * pep_list_ref = p_msrun_ref->getPeptideList();
	if (pep_list_ref == NULL)
		throw mcqError(
				QObject::tr("No peptide defined for msrun: %1\n").arg(
						p_msrun_ref->getXmlId()));

	std::vector<const Peptide *>::const_iterator itpeplist_ref;
	for (itpeplist_ref = pep_list_ref->begin();
			itpeplist_ref != pep_list_ref->end(); ++itpeplist_ref) {
		if ((*itpeplist_ref)->isObservedIn(p_msrun)) {
			_shared_peptide_list.push_back(*itpeplist_ref);
		}
	}
}

// test if there are not less shared peptides than the 
// _ms2_tendency_halfwindow or the _ms2_smoothing_halfwindow
// in which case alignment is impossible
bool AlignmentMs2::isAlignmentPossible(const QString p_msrunId,
		const unsigned int map_size) {

	unsigned int tendency_window_size = 1 + 2 * _ms2_tendency_halfwindow;
	unsigned int smoothing_window_size = 1 + 2 * _ms2_smoothing_halfwindow;

	mcqout() << "Aligning MS run '" << p_msrunId << "' : shared peptides "
			<< map_size << endl;

	if (tendency_window_size > map_size) {
		mcqout() << "WARNING : MS2 alignment of MS run '" << p_msrunId
				<< "' not possible : \nthe shared peptides number : "
				<< map_size << " is smaller than the ms2 tendency window : "
				<< tendency_window_size << endl;
		return false;
	}

	if (smoothing_window_size > map_size) {
		mcqout() << "WARNING : MS2 alignment of MS run '" << p_msrunId
				<< "' not possible : \nthe shared peptides number : "
				<< map_size << " is smaller than the ms2 smoothing window : "
				<< smoothing_window_size << endl;
		return false;
	}
	return true;
}

/// get the map : [bestRt = RT_MS2(p_msrun)] -> [deltaRt=RT_MS2(p_msrun) -
/// RT_MS2(p_msrun_ref)] 
std::map<mcq_double, mcq_double> *
AlignmentMs2::get_map_bestRt_deltaRt(const Msrun * p_msrun_ref,
		const Msrun * p_msrun) const {

	std::map<mcq_double, mcq_double> * map_bestRt_deltaRt = new std::map<
			mcq_double, mcq_double>;

	PeptideList::const_iterator it;
	for (it = _shared_peptide_list.begin(); it != _shared_peptide_list.end();
			++it) {
		mcq_double pepRefRt = (*it)->getObservedBestRtForMsRun(p_msrun_ref,
				*_current_group);
		mcq_double pepRt = (*it)->getObservedBestRtForMsRun(p_msrun,
				*_current_group);

		if ((pepRefRt != -1) && (pepRt != -1)) {
			mcq_double deltaRt = pepRt - pepRefRt;
			std::pair<mcq_double, mcq_double> elmt(pepRt, deltaRt);
			map_bestRt_deltaRt->insert(elmt);
		}
	}
	return map_bestRt_deltaRt;
}

/// correct the deltaRt-s (applying median with half_window steps on a map
std::map<mcq_double, mcq_double> *
AlignmentMs2::correct_deltaRt(
		std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
		const unsigned int correction_halfwindow) const {

	// create the new map to return
	std::map<mcq_double, mcq_double> * map_bestRt_correctedDeltaRt =
			new std::map<mcq_double, mcq_double>;

	// test if there is no smoothing to be done
	mcq_double zero(0);
	if (correction_halfwindow == zero) {
		*map_bestRt_correctedDeltaRt = *map_bestRt_deltaRt;
		return (map_bestRt_correctedDeltaRt);
	}

	//go on with the smoothing

	// copy the deltaRt-s in a vector
	std::vector<mcq_double> tmp_v_deltaRt;
	std::map<mcq_double, mcq_double>::const_iterator itmap;
	for (itmap = map_bestRt_deltaRt->begin();
			itmap != map_bestRt_deltaRt->end(); ++itmap) {
		tmp_v_deltaRt.push_back((*itmap).second);
	}

	// get a new corrected delta_Rt vector by applying median
	std::vector<mcq_double> * tmp_v_corrected_deltaRt =
			Utilities::newVectorByApplyingWindowsOperation(&tmp_v_deltaRt,
					correction_halfwindow, &get_median);

	// copy the corrected deltaRt in the result map bestRt -> correctedDeltaRt

	if (tmp_v_corrected_deltaRt->size() == map_bestRt_deltaRt->size()) {
		std::vector<mcq_double>::const_iterator itv;
		for (itmap = map_bestRt_deltaRt->begin(), itv =
				tmp_v_corrected_deltaRt->begin();
				itmap != map_bestRt_deltaRt->end(); ++itmap, ++itv) {
			std::pair<mcq_double, mcq_double> elmt(itmap->first, *itv);
			map_bestRt_correctedDeltaRt->insert(elmt);
		}

	} else {
		delete (tmp_v_corrected_deltaRt);
		delete (map_bestRt_correctedDeltaRt);
		throw mcqError(
				QObject::tr(
						"Error in AlignmentMs2::correct_deltaRt : not the same number of deltaRt-s after correction, this is a Bug.\n"));
	}
	delete (tmp_v_corrected_deltaRt);
	return (map_bestRt_correctedDeltaRt);
}

/// method that adds a first element to a map
void AlignmentMs2::addFirstElmtToMap(
		std::map<mcq_double, mcq_double> * map_bestRt_correctedDeltaRt,
		mcq_double first_key, mcq_double first_value) const {
	std::pair<mcq_double, mcq_double> firstElmt(first_key, first_value);
	map_bestRt_correctedDeltaRt->insert(firstElmt);
}

/// method that adds a last element to a map (for map bestRt->correctedDeltaRt)
void AlignmentMs2::addLastElmtToMap(
		std::map<mcq_double, mcq_double> * map_bestRt_correctedDeltaRt,
		mcq_double last_key, mcq_double last_value) const {
	std::pair<mcq_double, mcq_double> lastElmt(last_key, last_value);
	map_bestRt_correctedDeltaRt->insert(lastElmt);
}

/// Get the rt-s of MS level 1 for a given msrun. If an alignment 
/// has already occurred for this msrun we get the aligned rt-s 
/// (from _map_aligned_rt) in order to allow cascade alignments
/// (msrun A -> msrunB -> msrun C). If no alignment has occurred
/// we get the original rt-s (from _original_rt_set)
const std::vector<mcq_double> *
AlignmentMs2::getOldRtSetForMsrun(const Msrun * p_msrun) const {
	std::vector<mcq_double> * oldRtSet = new std::vector<mcq_double>;
	if (p_msrun->hasBeenAligned()) {
		std::map<mcq_double, mcq_double>::const_iterator it_map_aligned;
		for (it_map_aligned = p_msrun->begin_map_aligned();
				it_map_aligned != p_msrun->end_map_aligned();
				++it_map_aligned) {
			oldRtSet->push_back(it_map_aligned->second);
		}
	} else {
		std::set<mcq_double>::const_iterator it_original_rt;
		for (it_original_rt = p_msrun->begin_original_rt();
				it_original_rt != p_msrun->end_original_rt();
				++it_original_rt) {
			oldRtSet->push_back(*it_original_rt);
		}
	}
	return oldRtSet;
}

/// given an ms-level 1 rt (oldRt), by linear interpolation on the given 
/// deltaRt points, calculate the best corresponding deltaRt
const mcq_double AlignmentMs2::getDeltaRtForOldRt(mcq_double oldRt,
		std::map<mcq_double, mcq_double> bestRt_deltaRt) const {

	// get the deltaRt in the map if the corresponding bestRt=oldRt
	std::map<mcq_double, mcq_double>::const_iterator it(
			bestRt_deltaRt.find(oldRt));
	if (it != bestRt_deltaRt.end()) {
		return (it->second);
	} else {
		it = bestRt_deltaRt.begin();
		std::map<mcq_double, mcq_double>::const_iterator itbefore(it);
		if ((it->first) > oldRt) {
			return (it->second);
		}
		it++;
		while ((it != bestRt_deltaRt.end()) && ((it->first) < oldRt)) {
			it++;
			itbefore++;
		}
		if (it == bestRt_deltaRt.end()) {
			return (itbefore->second);
		} else {
			//règle de trois :
			mcq_double bestRt_min = itbefore->first;
			mcq_double bestRt_max = it->first;
			mcq_double bestRt_max_minus_min = bestRt_max - bestRt_min;
			mcq_double deltaRt_min = itbefore->second;
			mcq_double deltaRt_max = it->second;
			mcq_double ratio;

			if (bestRt_max_minus_min != 0) {
				ratio = (oldRt - bestRt_min) / bestRt_max_minus_min;
			} else {
				ratio = 1;
			}

			return (itbefore->second + (ratio * (deltaRt_max - deltaRt_min)));
		}
	}
}

/// smooth the deltaRt-s (apply mean with smoothing_halfwindows steps on a map)
std::map<mcq_double, mcq_double> *
AlignmentMs2::smooth_deltaRt(
		std::map<mcq_double, mcq_double> * map_oldRt_deltaRt,
		const unsigned int smoothing_halfwindow) const {
	// create the new map to return
	std::map<mcq_double, mcq_double> * map_oldRt_smoothedDeltaRt = new std::map<
			mcq_double, mcq_double>;

	// test if there is no smoothing to be done
	mcq_double zero(0);
	if (smoothing_halfwindow == zero) {
		*map_oldRt_smoothedDeltaRt = *map_oldRt_deltaRt;
		return (map_oldRt_smoothedDeltaRt);
	}

	//go on with the smoothing

	// copy the deltaRt-s in a vector
	std::vector<mcq_double> tmp_v_deltaRt;
	std::map<mcq_double, mcq_double>::const_iterator itmap;
	for (itmap = map_oldRt_deltaRt->begin(); itmap != map_oldRt_deltaRt->end();
			++itmap) {
		tmp_v_deltaRt.push_back((*itmap).second);
	}

	// get a new smoothed delta_Rt vector by applying average
	std::vector<mcq_double> * tmp_v_smoothed_deltaRt =
			Utilities::newVectorByApplyingWindowsOperation(&tmp_v_deltaRt,
					smoothing_halfwindow, &get_average);

	// copy the smoothed deltaRt in a new map bestRt -> smoothedDeltaRt
	if (tmp_v_smoothed_deltaRt->size() == map_oldRt_deltaRt->size()) {
		std::vector<mcq_double>::const_iterator itv;
		for (itmap = map_oldRt_deltaRt->begin(), itv =
				tmp_v_smoothed_deltaRt->begin();
				itmap != map_oldRt_deltaRt->end(); ++itmap, ++itv) {
			std::pair<mcq_double, mcq_double> elmt(itmap->first, *itv);
			map_oldRt_smoothedDeltaRt->insert(elmt);
		}
	} else {
		delete (tmp_v_smoothed_deltaRt);
		delete (map_oldRt_smoothedDeltaRt);
		throw mcqError(
				QObject::tr(
						"Error in AlignmentMs2::smooth_deltaRt : not the same number of deltaRt-s after smoothing, this is a Bug.\n"));
	}
	delete (tmp_v_smoothed_deltaRt);
	return (map_oldRt_smoothedDeltaRt);
}

/// virtual method aligning two msruns
void AlignmentMs2::privAlignTwoMsRuns(Msrun * p_msrun_ref, Msrun * p_msrun) {

	if (_p_msrun_ref != p_msrun_ref) {
		_p_msrun_ref = p_msrun_ref;
	}

	/************* Begin alignment algorithm *****************/
	qDebug() << "Begin alignment";

	// set list of peptides common to both msruns
	setSharedPeptides(p_msrun_ref, p_msrun);

	qDebug() << "Ludo debug : shared peptides = "
			<< _shared_peptide_list.size();

	// for each shared peptide construct the map :
	// bestRt_Ms2(p_msrun) -> deltaRt (= bestRt_Ms2(p_msrun) - bestRt_Ms2(p_msrun_ref))
	std::map<mcq_double, mcq_double> * map_bestRt_deltaRt =
			get_map_bestRt_deltaRt(p_msrun_ref, p_msrun);

	// test if there are not less shared peptides than the
	// _ms2_tendency_halfwindow or the _ms2_smoothing_halfwindow
	// in which case alignment is impossible

	if (!isAlignmentPossible(p_msrun->getXmlId(), map_bestRt_deltaRt->size())) {
		_shared_peptide_list.clear();
		return;
	}

	// the map is already ordered in ascending bestRt order
	// (otherwise we should have ordered it)

	// get the corrected set of deltaRt-s by applying a moving median
	// by steps of +-_ms2_tendency_halfwindow on them

	qDebug() << "Ludo debug : map_bestRt_deltaRt before median on ms2 = "
			<< map_bestRt_deltaRt->size();
	std::map<mcq_double, mcq_double> * corrected_map_bestRt_deltaRt =
			correct_deltaRt(map_bestRt_deltaRt, _ms2_tendency_halfwindow);

	qDebug() << "Ludo debug : map_bestRT_deltaRt before mean on ms2 = "
			<< corrected_map_bestRt_deltaRt->size();

	// smooth the deltaRt-s by applying a mean by steps of
	// +-_ms2_smoothing_halfwindow on them
	std::map<mcq_double, mcq_double> * corrected_mean_map_bestRt_deltaRt =
			smooth_deltaRt(corrected_map_bestRt_deltaRt,
					_ms2_smoothing_halfwindow);

	/*********** add first and last element to this map ********************/

	// get the vectors of the ms-level 1 retention times of p_msrun
	// and p_msrun_ref
	const std::vector<mcq_double> * oldRtSet = getOldRtSetForMsrun(p_msrun);
	const std::vector<mcq_double> * oldRtRefSet = getOldRtSetForMsrun(
			p_msrun_ref);

	// add first element to the corrected_map_bestRt_deltaRt
	std::vector<mcq_double>::const_iterator it_firstRt(oldRtSet->begin());
	std::vector<mcq_double>::const_iterator it_firstRt_ref(
			oldRtRefSet->begin());
	mcq_double firstOldRt(*it_firstRt);
	mcq_double firstRtToAdd(firstOldRt - 1);
	if (firstRtToAdd < 0) {
		firstRtToAdd = 0;
	}
	mcq_double first_deltaRt = firstOldRt - *it_firstRt_ref;
	addFirstElmtToMap(corrected_mean_map_bestRt_deltaRt, firstRtToAdd,
			first_deltaRt);
	// adding it here too, only for the traces file
	addFirstElmtToMap(map_bestRt_deltaRt, firstRtToAdd, first_deltaRt);
	addFirstElmtToMap(corrected_map_bestRt_deltaRt, firstRtToAdd,
			first_deltaRt);

	// add last element to corrected_map_bestRt_deltaRt
	std::vector<mcq_double>::const_reverse_iterator it_lastRt(
			oldRtSet->rbegin());
	std::vector<mcq_double>::const_reverse_iterator it_lastRt_ref(
			oldRtRefSet->rbegin());
	mcq_double lastOldRt(*it_lastRt);
	mcq_double lastRtToAdd(lastOldRt + 1);
	mcq_double last_deltaRt = lastOldRt - *it_lastRt_ref;
	addLastElmtToMap(corrected_mean_map_bestRt_deltaRt, lastRtToAdd,
			last_deltaRt);
	// adding it here too, only for the traces file
	addLastElmtToMap(map_bestRt_deltaRt, lastRtToAdd, last_deltaRt);
	addLastElmtToMap(corrected_map_bestRt_deltaRt, lastRtToAdd, last_deltaRt);
	// we do not need oldRtRefSet anymore
	delete (oldRtRefSet);

	/********* end adding first and last element to map ***********************/

	// for each oldRt, calculate the best corresponding deltaRt by linear
	// extrapolation and put it in a new map oldRt -> bestDeltaRt
	std::map<mcq_double, mcq_double> map_oldRt_bestDeltaRt;
	std::vector<mcq_double>::const_iterator itOld;
	for (itOld = oldRtSet->begin(); itOld != oldRtSet->end(); ++itOld) {
		const mcq_double bestDeltaRt = getDeltaRtForOldRt(*itOld,
				*corrected_mean_map_bestRt_deltaRt);
		std::pair<mcq_double, mcq_double> elmt(*itOld, bestDeltaRt);
		map_oldRt_bestDeltaRt.insert(elmt);
	}

	// Smooth the bestDeltaRt-s (with mean on a correction )
	// but before test if the window size _ms1_smoothing_halfwindow is not
	// greater than the deltaRt-s size

	unsigned int ms1_window_size = 1 + 2 * _ms1_smoothing_halfwindow;
	unsigned int map_size = map_oldRt_bestDeltaRt.size();

	if (ms1_window_size > map_size) {
		mcqout() << "WARNING : MS2 alignment of MS run '"
				<< (p_msrun->getXmlId())
				<< "' not possible : \nthe MS1 rt-s number : " << map_size
				<< " is smaller than the ms1 smoothing window :  "
				<< ms1_window_size << endl;
		_shared_peptide_list.clear();
		delete (oldRtSet);
		delete (corrected_map_bestRt_deltaRt);
		delete (corrected_mean_map_bestRt_deltaRt);
		delete (map_bestRt_deltaRt);
		return;
	}

	std::map<mcq_double, mcq_double> * map_oldRt_smoothedDelta = smooth_deltaRt(
			&map_oldRt_bestDeltaRt, _ms1_smoothing_halfwindow);

	// Calculate the aligned Rt for each oldRt, put it in a vector
	std::map<mcq_double, mcq_double>::const_iterator itmap;
	for (itmap = map_oldRt_smoothedDelta->begin();
			itmap != map_oldRt_smoothedDelta->end(); ++itmap) {
		mcq_double aligned_value = itmap->first - itmap->second;
		_new_time_values.push_back(aligned_value);
	}

	//correct the non-increasing consequent values

	// Here, the correction parameter is the slope of old rt points curve
	// (divided by 4 to get a finer correction).
	const mcq_double correction_parameter = (lastOldRt - firstOldRt)
			/ (map_size);
	set_correction_parameter(correction_parameter / 4);
	correct_new_time_values();

	// add data to monitor
	if (_monitorAlignment != 0) {
		_monitorAlignment->setTraceValues(p_msrun, map_bestRt_deltaRt,
				corrected_map_bestRt_deltaRt, corrected_mean_map_bestRt_deltaRt,
				map_oldRt_smoothedDelta);
	}
	// set the new time-values
	p_msrun->setNewTimeValues(_new_time_values);

	/*********** Finished alignment algorithm ******************************/

	/// clean_up
	delete (map_bestRt_deltaRt);
	delete (corrected_map_bestRt_deltaRt);
	delete (corrected_mean_map_bestRt_deltaRt);
	delete (oldRtSet);
	delete (map_oldRt_smoothedDelta);
	_shared_peptide_list.clear();
	_new_time_values.clear();
	qDebug() << "void AlignmentMs2::privAlignTwoMsRuns() end ";
}

/// method correcting the final aligned values if necessary. Sometimes,
/// big changing delta-rt-s produce aligned values that are not always in 
/// an increasing order. In those cases this method adds a correction
/// parameter to the decreasing rt values and puts them in increasing order.
/// As the correction parameter gets smaller, the aligned values get 
/// modified less. It's up to you to determine a correction parameter 
/// that is sufficient to make values increase without becoming aberrant.
/// A good sign is teh number of values having been corrected : the smaller 
/// this number gets, the better your correction is. 
void AlignmentMs2::correct_new_time_values() {
	unsigned int values_corrected(0);
	std::vector<mcq_double>::iterator new_it(_new_time_values.begin());
	std::vector<mcq_double>::iterator new_nextit(_new_time_values.begin());
	new_nextit++;
	for (; new_nextit != _new_time_values.end(); ++new_nextit, ++new_it) {
		if (*new_nextit < *new_it) {
			++values_corrected;
			qDebug() << "Correction param = " << _correction_parameter
					<< "previous_rt = " << *new_it << ", rt = " << *new_nextit;
			*new_nextit = *new_it + _correction_parameter;
			qDebug() << "After : rt = " << *new_nextit;
		}
	}
	if (values_corrected != 0) {
		mcqout() << "WARNING : MS2 alignment needed correction : \n"
				<< " correction parameter : " << _correction_parameter << ", "
				<< values_corrected << "/" << _new_time_values.size()
				<< " values corrected." << endl;
	}
}

void AlignmentMs2::clean() {
}
