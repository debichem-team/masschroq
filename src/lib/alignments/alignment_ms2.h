/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file alignment_ms2.h
 * \date 9 juillet 2010
 * \author Edlira Nano
 */

#ifndef ALIGNMENT_MS2_H_
#define ALIGNMENT_MS2_H_ 1

#include "alignment_base.h"
#include "../peptides/peptide_list.h"

/**
 * \class AlignmentMs2
 * \brief Alignment method based on rt-s of MS2 runs

 The MS2 alignment aligns the retention times of peptides (rt_MS1) in 
 a msrun based on the retention times of the ms level 2 (rt_MS2) peptides
 having been identified in the two msruns (teh reference msrun and the one
 being aligned to it).
 Algorithm description (aligning msrun_ref and msrun) :
 - get the list of shared peptides between the two msruns;
 - for each peptide in this list :
 - construct the map : Rt_MS2(msrun)->deltaRt(=Rt_MS2(msrun)-RT_MS2(msrun_ref));
 - sort this map in ascending Rt_MS2 order;
 - correct the deltaRt-s in this map (apply a median to them) (optional)
 - smooth the deltaRt-s (apply a mean) (optional)
 - get the list of MS1 retention times (Rt_MS1) for msrun (these are the rt-s
 to be aligned)
 - add a first and last element to this map corresponding to the first and 
 last MS1 retention times - 1, in order to extrapolate the alignment curve for
 the beginning and end of the runs (the peptides beginning fragmentation after
 a while, there is ususally no MS2 point in the beginning and end of the run).
 - for each rt in the Rt_MS1 list, by linear interpolation calculate 
 the nearest existing deltaRt from the map; we get map2 : Rt_MS1(msrun) -> 
 deltaRt
 - smooth the deltaRt-s of map2 (by applying a mean) (optional)
 - from map2, calculate the map : Rt_MS1(msrun) -> aligned Rt_MS1(msrun)
 with aligned_Rt_MS1(msrun) =  Rt_MS1(msrun) - deltaRt
 - apply a correction if needed on the aligned_Rt_MS1. Sometimes, 
 difficult alignments that have big changing deltaRt-s produce aligned rt-s 
 that are not in an increasing order. In these cases a correction is applied.
 For more details see the comments in the code.
 */

class AlignmentMs2  : public AlignmentBase {
 
 public :
  
  AlignmentMs2(MonitorAlignmentBase * monitor);

  virtual ~AlignmentMs2();
  
  virtual void printInfos(QTextStream & out) const ;
  
  void setMs2TendencyWindow(mcq_double ms2_tendency_halfwindow);
  
  void setMs2SmoothingWindow(mcq_double ms2_smoothing_halfwindow);

  void setMs1SmoothingWindow(mcq_double ms1_smoothing_halfwindow);

  int getMs2TendencyWindow() const{
	  return(_ms2_tendency_halfwindow);
  }

  int getMs2SmoothingWindow() const{
	  return(_ms2_smoothing_halfwindow);
  }

  int getMs1SmoothingWindow() const{
	  return(_ms1_smoothing_halfwindow);
  }

  void clean();

 protected : 
  
  void setSharedPeptides(const Msrun * p_msrun_ref, const Msrun * p_msrun);
  
  bool isAlignmentPossible(const QString p_msrunId, 
			   const unsigned int map_size);
      
			   std::map<mcq_double, mcq_double> * get_map_bestRt_deltaRt(const Msrun * p_msrun_ref, 
						       const Msrun * p_msrun) const;
  
						       const std::vector<mcq_double> * getOldRtSetForMsrun(const Msrun * p_msrun) const;
  
						       std::map<mcq_double, mcq_double> * 
						       correct_deltaRt(std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
		    const unsigned int correction_half_window) const;
  
		    std::map<mcq_double, mcq_double> *
		    smooth_deltaRt(std::map<mcq_double, mcq_double> * map_oldRt_deltaRt, 
		   const unsigned int smoothing_half_window) const;
  
		   void addFirstElmtToMap(std::map<mcq_double, mcq_double> * map_bestRt_correctedDeltaRt,
			mcq_double first_key, mcq_double first_value) const;
    
			void addLastElmtToMap(std::map<mcq_double, mcq_double> * map_bestRt_correctedDeltaRt, 
		       mcq_double last_key, mcq_double last_value) const;
  
  const mcq_double getDeltaRtForOldRt(mcq_double oldRt, 
				      std::map<mcq_double, mcq_double> bestRt_deltaRt) const;
  
  virtual void privAlignTwoMsRuns(Msrun * p_msrun_ref, Msrun * p_msrun);
 
  void set_correction_parameter(const mcq_double correction_parameter);
  
  virtual void correct_new_time_values();
 
 private :
  
  /// Msrun of reference for the alignment
  Msrun * _p_msrun_ref; 
  
  /// List of the peptides shared by the two msruns being aligned 
  PeptideList _shared_peptide_list;
  
  /// correction tendency parameter (half window) for the deltaRt-ms2 points
  unsigned int _ms2_tendency_halfwindow;

  /// smoothing parameter (half window) for the ms2 rt points
  unsigned int _ms2_smoothing_halfwindow;
  
  /// smoothing parameter (half window) for the ms1 rt points
  unsigned int _ms1_smoothing_halfwindow;
  
  /// correction parameter used to correct the final alignment
  /// if  necessary
  mcq_double _correction_parameter;
  
  std::vector<mcq_double> _new_time_values;

};




#endif /* ALIGNMENT_MS2_H_ */
