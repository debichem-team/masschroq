/**
 * \file lmatreader.cpp
 * \author Olivier Langella
 */

#include "lmatreader.h"
#include "../../saxparsers/xmlParserFactory.h"
#include "../mcq_error.h"
#include "../consoleout.h"

#include <iostream>
#include <QTime>

lmatReader::lmatReader() :
  LMat() {
}

lmatReader::~lmatReader() {
}

bool 
lmatReader::set_from_xml(Msrun & msrun, mcq_float mass_start,
			 mcq_float mass_end, mcq_float precision) {
  
  QFile file((msrun.getXmlFileInfo()).absoluteFilePath());
  qDebug() << "Obiwarp align, begin set from xml : " << file.fileName() << endl;
  // QTime tmzxml;
  // tmzxml.start();
  
  
  QString xml_file_format = msrun.getXmlFileFormat();
  XmlParserFactory xml_factory;
  XmlToLmatParser * xml_parser = 
    xml_factory.newXmlToLmatParser(xml_file_format, this, mass_start, mass_end, precision);
  QXmlSimpleReader reader;
  QXmlInputSource xmlInputSource(&file);
  reader.setContentHandler(xml_parser);
  reader.setErrorHandler(xml_parser);
  
  if (reader.parse(xmlInputSource)) {
    mcqout() << "MS run '"
	 << (msrun.getXmlId())
	 << "' : xml file '"
	 << (file.fileName()) 
	 << "' :  obiwarp parsing for lmat: OK" << endl;
    
    // qDebug("Obiwarp align set from xml parsing time was %d ms", tmzxml.elapsed()); 
 
  } else {
    throw mcqError(QObject::tr("error reading xml input file '%1' :\n").arg(file.fileName()).append(xml_parser->errorString()));
    return (false);
  }

  delete (xml_parser);
  return (true);
}

