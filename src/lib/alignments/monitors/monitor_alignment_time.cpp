/**
 * \file monitor_alignement_time.cpp
 * \date September 12, 2012
 * \author Benoit Valot
 */

#include "monitor_alignment_time.h"
#include "../../consoleout.h"

MonitorAlignmentTime::MonitorAlignmentTime() :
		_outDir("") {

}

MonitorAlignmentTime::~MonitorAlignmentTime() {
}

void MonitorAlignmentTime::setOutputDirectory(const QString & dir) {
	_outDir = dir;
}

void MonitorAlignmentTime::setTimeValues(const Msrun * pmsrun) {
	const QFileInfo & fileInfo = pmsrun->getXmlFileInfo();
	if (_outDir.isEmpty()) {
		_outDir = fileInfo.absolutePath();
	}
	QFile file(_outDir + "/" + fileInfo.completeBaseName() + ".time");

	if (file.exists()) {
		mcqout() << "WARNING : alignment time file '" << file.fileName()
				<< "' : already exists, it will be overwrited" << endl;
	}

	if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
	} else {
		mcqerr() << "WARNING : alignment time file '" << file.fileName()
				<< "' : failed to open, no time file for this alignment\n"
				<< file.errorString() << endl;
		return;
	}

	QTextStream out(&file);
	pmsrun->printRetentionTimes(out);
	file.close();
}

void MonitorAlignmentTime::setTraceValues(const Msrun * pmsrun,
		const std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
		const std::map<mcq_double, mcq_double> * corrected_map_bestRt_deltaRt,
		const std::map<mcq_double, mcq_double> * corrected_mean_map_bestRt_deltaRt,
		const std::map<mcq_double, mcq_double> * map_oldRt_smoothedDelta) {

	const QFileInfo & fileInfo = pmsrun->getXmlFileInfo();
	if (_outDir.isEmpty()) {
		_outDir = fileInfo.absolutePath();
	}
	QFile file(_outDir + "/" + fileInfo.completeBaseName() + ".trace");

	if (file.exists()) {
		mcqout() << "WARNING : alignment trace file '" << file.fileName()
				<< "' : already exists, it will be overwrited" << endl;
	}

	if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
	} else {
		mcqerr() << "WARNING : alignment trace file '" << file.fileName()
				<< "' : failed to open, no traces for this alignment\n"
				<< file.errorString() << endl;
		return;
	}

	QTextStream out(&file);

	// printing header
	out << "rt_MS2 " << "\t" << "deltaRt_MS2" << "\t"
			<< "post-median-deltaRt_MS2" << "\t" << "post-mean-deltaRt_MS2"
			<< "\t" << "rt_MS1" << "\t" << "smoothed-deltaRt_MS1 " << endl;

	// Begin printing the values

	// printing bestRt deltaRt correctedDeltaRt
	std::map<mcq_double, mcq_double>::const_iterator itdeltaRt;
	std::map<mcq_double, mcq_double>::const_iterator itcorrected;
	std::map<mcq_double, mcq_double>::const_iterator itcorrectedmean;
	std::map<mcq_double, mcq_double>::const_iterator itsmoothed;

	const unsigned int sizeDelta = map_bestRt_deltaRt->size();
	const unsigned int sizeSmoothed = map_oldRt_smoothedDelta->size();
	unsigned int position;
	// if we have less elements in map_bestRt_deltaRt than
	// in map_oldRt_smoothedDeltaRt
	if (sizeDelta < sizeSmoothed) {
		qDebug()
				<< "AlignmentMs2::write_traces begin sizeDelta < sizeSmoothed ";

		for (itdeltaRt = map_bestRt_deltaRt->begin(), itcorrected =
				corrected_map_bestRt_deltaRt->begin(), itcorrectedmean =
				corrected_mean_map_bestRt_deltaRt->begin(), itsmoothed =
				map_oldRt_smoothedDelta->begin(), position = 0;
				itsmoothed != map_oldRt_smoothedDelta->end();
				++itsmoothed, ++position) {
			// we print all elements till the end of the smaller map
			if (position < sizeDelta) {
				out << itcorrected->first << "\t" << itdeltaRt->second << "\t"
						<< itcorrected->second << "\t"
						<< itcorrectedmean->second << "\t" << itsmoothed->first
						<< "\t" << itsmoothed->second << endl;

				++itdeltaRt, ++itcorrected, ++itcorrectedmean;

			} else {
				// we fill the rest with tabs and finish printing the longest map
				out << "\t\t\t\t" << itsmoothed->first << "\t"
						<< itsmoothed->second << endl;
			}
		}

		// if we have less elements in map_oldRt_smoothedDeltaRt than
		// in map_bestRt_deltaRt
	} else if (sizeDelta > sizeSmoothed) {

		for (itdeltaRt = map_bestRt_deltaRt->begin(), itcorrected =
				corrected_map_bestRt_deltaRt->begin(), itcorrectedmean =
				corrected_mean_map_bestRt_deltaRt->begin(), itsmoothed =
				map_oldRt_smoothedDelta->begin(), position = 0;
				itdeltaRt != map_bestRt_deltaRt->end();
				++position, ++itdeltaRt) {
			// we print all elements till the end of the smaller map
			if (position < sizeSmoothed) {
				out << itcorrected->first << "\t" << itdeltaRt->second << "\t"
						<< itcorrected->second << "\t"
						<< itcorrectedmean->second << "\t" << itsmoothed->first
						<< "\t" << itsmoothed->second << endl;

				++itcorrected, ++itcorrectedmean, ++itsmoothed;

			} else {
				// we fill the rest with tabs
				out << itcorrected->first << "\t" << itdeltaRt->second << "\t"
						<< itcorrected->second << "\t"
						<< itcorrectedmean->second << "\t\t" << endl;

				++itcorrected, ++itcorrectedmean;
			}
		}
	} else {
		// if the two maps have same size, we print all elements till the end
		qDebug()
				<< "AlignmentMs2::write_traces begin sizeDelta == sizeSmoothed ";
		for (itdeltaRt = map_bestRt_deltaRt->begin(), itcorrected =
				corrected_map_bestRt_deltaRt->begin(), itcorrectedmean =
				corrected_mean_map_bestRt_deltaRt->begin(), itsmoothed =
				map_oldRt_smoothedDelta->begin();
				itdeltaRt != map_bestRt_deltaRt->end(); ++itdeltaRt) {
			out << itcorrected->first << "\t" << itdeltaRt->second << "\t"
					<< itcorrected->second << "\t" << itcorrectedmean->second
					<< "\t" << itsmoothed->first << "\t" << itsmoothed->second
					<< endl;

			++itcorrected, ++itcorrectedmean, ++itsmoothed;
		}
	}

	file.close();
}

