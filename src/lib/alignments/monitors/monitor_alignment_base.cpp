/**
 * \file monitor_alignement_base.cpp
 * \date September 12, 2012
 * \author Benoit Valot
 */


#include "monitor_alignment_base.h"

MonitorAlignmentBase::MonitorAlignmentBase() {
	// TODO Auto-generated constructor stub

}

MonitorAlignmentBase::~MonitorAlignmentBase() {
	// TODO Auto-generated destructor stub
}

void MonitorAlignmentBase::setTimeValues(const Msrun * pmsrun){
	//Do nothing by default
}
void MonitorAlignmentBase::setTraceValues(const Msrun * pmsrun,
							const std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
							const std::map<mcq_double, mcq_double> * corrected_map_bestRt_deltaRt,
							const std::map<mcq_double, mcq_double> * corrected_mean_map_bestRt_deltaRt,
							const std::map<mcq_double, mcq_double> * map_oldRt_smoothedDelta){
	//Do nothing by default
}
