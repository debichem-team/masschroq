/**
 * \file monitor_alignement_time.h
 * \date September 12, 2012
 * \author Benoit Valot
 */

#ifndef MONITOR_ALIGNMENT_TIME_H_
#define MONITOR_ALIGNMENT_TIME_H_

#include "monitor_alignment_base.h"

class MonitorAlignmentTime : public MonitorAlignmentBase {

public:

	MonitorAlignmentTime();
	virtual ~MonitorAlignmentTime();

	void setOutputDirectory(const QString & dir);
	
	virtual void setTimeValues(const Msrun * pmsrun);
	
	virtual void setTraceValues(const Msrun * pmsrun,
						   const std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
						   const std::map<mcq_double, mcq_double> * corrected_map_bestRt_deltaRt,
						   const std::map<mcq_double, mcq_double> * corrected_mean_map_bestRt_deltaRt,
						   const std::map<mcq_double, mcq_double> * map_oldRt_smoothedDelta);
	
private:

	QString _outDir;

};

#endif /* MONITOR_ALIGNMENT_TIME_H_ */
