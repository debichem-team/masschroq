/*
 * \file monitor_alignment_plot.h
 * \date September 14, 2012
 * \author: valot
 */

#ifndef MONITOR_ALIGNMENT_PLOT_H_
#define MONITOR_ALIGNMENT_PLOT_H_

#include "monitor_alignment_base.h"

class MonitorAlignmentPlot: public MonitorAlignmentBase {

public:
	
	MonitorAlignmentPlot();
	virtual ~MonitorAlignmentPlot();

	virtual void setTimeValues(const Msrun * pmsrun);
	
	virtual void setTraceValues(const Msrun * pmsrun,
			const std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
			const std::map<mcq_double, mcq_double> * corrected_map_bestRt_deltaRt,
			const std::map<mcq_double, mcq_double> * corrected_mean_map_bestRt_deltaRt,
			const std::map<mcq_double, mcq_double> * map_oldRt_smoothedDelta);

	const std::map<mcq_double, mcq_double>& getMs2CommonPeak()const{
		return _map_bestRt_deltaRt;
	}

protected:
	std::map<mcq_double, mcq_double> _map_bestRt_deltaRt;

};

#endif /* MONITOR_ALIGNMENT_PLOT_H_ */

