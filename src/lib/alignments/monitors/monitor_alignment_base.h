/**
 * \file monitor_alignement_base.h
 * \date September 12, 2012
 * \author Benoit Valot
 */

#ifndef MONITOR_ALIGNMENT_BASE_H_
#define MONITOR_ALIGNMENT_BASE_H_

#include "../../msrun/msrun.h"

class MonitorAlignmentBase {
public:
	MonitorAlignmentBase();
	virtual ~MonitorAlignmentBase();

	virtual void setTimeValues(const Msrun * pmsrun);
	virtual void setTraceValues(const Msrun * pmsrun,
								const std::map<mcq_double, mcq_double> * map_bestRt_deltaRt,
								const std::map<mcq_double, mcq_double> * corrected_map_bestRt_deltaRt,
								const std::map<mcq_double, mcq_double> * corrected_mean_map_bestRt_deltaRt,
								const std::map<mcq_double, mcq_double> * map_oldRt_smoothedDelta);

};

#endif /* MONITOR_ALIGNMENT_BASE_H_ */
