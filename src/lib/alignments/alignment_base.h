/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file alignment_base.h
 * \date September 23, 2009
 * \author Olivier Langella
 */

#ifndef ALIGNMENT_BASE_H_
#define ALIGNMENT_BASE_H_ 1

#include "../mcq_error.h"
#include "../msrun/msrun.h"
#include "monitors/monitor_alignment_base.h"
#include <iostream>

class msRunHashGroup;

/**
 * \class AlignmentBase
 * \brief Virtual base class representing the alignment of the msruns of a group
 * in QuantiMSCpp
 *
 * The msrun with xml id "samp0" is always the reference msrun for 
 * the alignment of the group. The other msruns of the group are each 
 * aligned, sequentially, with this reference msrun (in method 
 * alignTwoMsRuns).    
 */

class AlignmentBase {
  
 public:
  
  AlignmentBase(MonitorAlignmentBase * monitor);
  
  virtual ~AlignmentBase();
  
  virtual void printInfos(QTextStream & out) const = 0;

  
  /// general method for the alignment of two msruns (calls virtual method
  /// privAlignTwoMsRuns below)
  void alignTwoMsRuns(Msrun * p_msrun_ref, 
		      Msrun * p_msrun, 
		      msRunHashGroup * group);

  virtual void clean() = 0;
  
 protected:
  
  /// virtual method for the alignment of two msruns
  virtual void privAlignTwoMsRuns(Msrun * p_msrun_ref, Msrun * p_msrun) = 0;
  
  /// virtual method for correction of the aligned _new_time_values
  /// if needed (generally if two consequent new time values are not
  /// in an increasing order)
  virtual void correct_new_time_values() = 0;

  /// current group of msruns being aligned
  const msRunHashGroup * _current_group;

  MonitorAlignmentBase * _monitorAlignment;

};

#endif /* ALIGNMENT_BASE_H_ */
