/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file lmatreader.h
 * \author Olivier Langella
 */


#ifndef LMATREADER_H_
#define LMATREADER_H_ 1

#include "../../libobiwarp/lmat.h"
#include "../msrun/msrun.h"
#include <QString>

//using namespace std;

class lmatReader : public LMat {
 
 public:
  
  lmatReader();
  virtual ~lmatReader();
  
  bool set_from_xml(Msrun & msrun, mcq_float mass_start, 
		    mcq_float mass_end, mcq_float precision);
  
};

#endif /*LMATREADER_H_*/
