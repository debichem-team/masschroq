/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file utilities.h
 * \date November 29, 2010
 * \author Edlira Nano
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_ 1

#include "../mcq_error.h"
#include "../../config.h"
#include <QDateTime>
#include <vector>
#include <map>

class spectrum;

typedef std::vector<mcq_double>::const_iterator constvdoubleit; 

typedef std::vector<int> Duration;

mcq_double get_max_element(constvdoubleit beginit, constvdoubleit endit);

mcq_double get_min_element(constvdoubleit beginit, constvdoubleit endit);

mcq_double get_average(constvdoubleit beginit, constvdoubleit endit);

mcq_double get_median(constvdoubleit beginit, constvdoubleit endit);

mcq_double get_spike_to_zero(constvdoubleit beginit, constvdoubleit endit);

/**
   \class Utilities
   \brief This class implements some calculation algorithms needed by 
   MassChroQ
*/

class Utilities {
 private :
 
  Utilities();
  
  virtual ~Utilities();
  
 public :
  
  /**
   * \brief Creates a new vector by applying a given function to a given vector.
   *
   * The given function calculates the value of an element of the vector by
   * applying operations (like mean, median) to the elements located in a given
   * window on the vector.
   */
  
  static std::vector<mcq_double> * newVectorByApplyingWindowsOperation
    (const std::vector<mcq_double> * p_v_intensity,
     unsigned int half_edge_size, 
     mcq_double(* p_sub_function)(constvdoubleit begin, constvdoubleit end));
  
  /// verify if a given amino-acid sequence is valid
  
  static const bool isValidSequence(const QString & seq, 
				    const Qt::CaseSensitivity cs);

  /// Replace Leucine with Isoleucine amino-acid in a sequence
  /// Not used in MassChroQ v 1.0.1 anymore
  static void replaceLwithI(QString & pep_seq);
  
  /// given a mz and a z, calculate the mh
  static const mcq_double mz2mh(const mcq_double mz, const unsigned int charge);

  /// given a mh and a z, calculate the mz
  static const mcq_double mh2mz(const mcq_double mh, const unsigned int charge);

  static const Duration getDurationFromDates(const QDateTime & date_begin, const QDateTime & date_end);

  static const int getDaysFromDuration(const Duration & d);
  
  static const int getHoursFromDuration(const Duration & d);

  static const int getMinutesFromDuration(const Duration & d);

  static const int getSecondsFromDuration(const Duration & d);

  static const bool isSorted(const std::vector<mcq_double> & v);

  /// given two integers first and last, returns a random integer between first and last 
  static const int random_pivot(const int first, const int last);

  /// double_quicksort sorts vector v1 using a quicksort algorithm, but also sorts vector v2
  /// in order to keep the correspondence with the vector one items, i.e. keeps matched up the 
  /// pairs of items of v1 and v2, while sorting v1. 
  static void double_quicksort(std::vector<mcq_double> & v1, std::vector<spectrum *> & v2, 
			       int first, int last);

};  


#endif /* UTILITIES_H_ */
