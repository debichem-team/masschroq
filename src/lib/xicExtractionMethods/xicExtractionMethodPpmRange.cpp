/**
 * \file xicExtractionMethodPpmRange.cpp
 * \date September 21, 2009
 * \author Olivier Langella
 */

#include "xicExtractionMethodPpmRange.h"

XicExtractionMethodPpmRange::XicExtractionMethodPpmRange() {
}

XicExtractionMethodPpmRange::~XicExtractionMethodPpmRange() {
}

void 
XicExtractionMethodPpmRange::computeMzRange() {
  mcq_double interval;
  interval = (_mz * _min_mz_ppm_range) / ONEMILLION;
  _min_mz = _mz - interval;
  interval = (_mz * _max_mz_ppm_range) / ONEMILLION;
  _max_mz = _mz + interval;
}

const mcq_double
XicExtractionMethodPpmRange::getMzRangeForMz(const mcq_double mz) const {
  mcq_double min, max;
  min = (mz * _min_mz_ppm_range) / ONEMILLION;
  max = (mz * _max_mz_ppm_range) / ONEMILLION;
  return (min + max);
}

void 
XicExtractionMethodPpmRange::printInfos(QTextStream & out) const {
	out << "\t xic extraction method 'ppm range' parameters :" << endl;
	out << "\t _min_mz_ppm_range = " << _min_mz_ppm_range << endl;
	out << "\t _max_mz_ppm_range = " << _max_mz_ppm_range << endl;
}
