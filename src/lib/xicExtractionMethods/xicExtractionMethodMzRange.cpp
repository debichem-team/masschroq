#include "xicExtractionMethodMzRange.h"

XicExtractionMethodMzRange::XicExtractionMethodMzRange() {
}

XicExtractionMethodMzRange::~XicExtractionMethodMzRange() {
}

void 
XicExtractionMethodMzRange::computeMzRange() {
  _min_mz = _mz - _min_mz_range;
  _max_mz = _mz + _max_mz_range;
}

const mcq_double 
XicExtractionMethodMzRange::getMzRangeForMz(const mcq_double mz) const {
  return (_min_mz_range + _max_mz_range);
}

void 
XicExtractionMethodMzRange::printInfos(QTextStream & out) const {
	out << "\t xic extraction method 'mz range' parameters :" << endl;
	out << "\t _min_mz_range = " << _min_mz_range << endl;
	out << "\t _max_mz_range = " << _max_mz_range << endl;
}
