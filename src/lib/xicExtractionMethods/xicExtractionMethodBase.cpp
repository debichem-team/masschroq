/**
 * \file xicExtractionMethodBase.h
 * \date September 21, 2009
 * \author Olivier Langella
 */


#include "xicExtractionMethodBase.h"

XicExtractionMethodBase::XicExtractionMethodBase() {
}

XicExtractionMethodBase::~XicExtractionMethodBase() {
}

const QString 
XicExtractionMethodBase::printInfos() const {
  QString infos;
  QTextStream info_stream(&infos, QIODevice::WriteOnly);
  printInfos(info_stream);
  return (infos);
}
