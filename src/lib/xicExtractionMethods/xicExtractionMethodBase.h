/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xicExtractionMethodBase.h
 * \date September 21, 2009
 * \author Olivier Langella
 */

#ifndef XIC_EXTRACTION_METHOD_BASE_H_
#define XIC_EXTRACTION_METHOD_BASE_H_ 1

#include <QDebug>
#include <QString>
#include <QTextStream>

#include "../../config.h"

//using namespace std;

/**
 * \class XicExtractionMethodBase
 * \brief The base class representing the method of the xic extraction (by mass
 * range or by ppm)
 * 
 * The _low_mz and the _high_mz variables determine the mz range for 
 * this extraction method. 
 */

class XicExtractionMethodBase {

 public:
  
  XicExtractionMethodBase();
  virtual ~XicExtractionMethodBase();
  
  const QString printInfos() const;
  virtual void printInfos(QTextStream & out) const = 0;
  
  virtual void set_min_range(mcq_double min) = 0;
  virtual void set_max_range(mcq_double max) = 0;
  
  void setMz(mcq_double mz) {
    _mz = mz;
    computeMzRange();
  }
  
  mcq_double get_min_mz() const {
    return (_min_mz);
  }

  mcq_double get_max_mz() const {
    return (_max_mz);
  }

  mcq_double get_mz() const {
    return (_mz);
  }

  virtual const mcq_double getMzRangeForMz(const mcq_double mz) const = 0;

 protected:
 
  /// virtual method computing the range of mz to be selected by the method
  virtual void computeMzRange() = 0;
  
  mcq_double _mz;
  mcq_double _min_mz;
  mcq_double _max_mz;

};

#endif /*XIC_EXTRACTION_METHOD_BASE_H_*/
