/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xicExtractionMethodPpmRange.h
 * \date September 21, 2009
 * \author Olivier Langella
 */


#ifndef XIC_EXTRACTION_METHOD_PPM_RANGE_H_
#define XIC_EXTRACTION_METHOD_PPM_RANGE_H_ 1

#include "xicExtractionMethodBase.h"

/**
 * \class XicExtractionMethodPpmRange
 * \brief represents the xic extraction method by ppm range 
 * (<ppm_range max min>)
 */


class XicExtractionMethodPpmRange : public XicExtractionMethodBase {
 
 public:
  
  XicExtractionMethodPpmRange();
  virtual ~XicExtractionMethodPpmRange();
  
  virtual void printInfos(QTextStream & out) const;
  
  virtual void set_min_range(mcq_double mass_min) {
    _min_mz_ppm_range = mass_min;
  }
  
  virtual void set_max_range(mcq_double mass_max) {
    _max_mz_ppm_range = mass_max;
  }
  
  virtual const mcq_double getMzRangeForMz(const mcq_double mz) const;

 private:
  
  virtual void computeMzRange();
 
  mcq_double _min_mz_ppm_range;
  mcq_double _max_mz_ppm_range;

};

#endif /*XIC_EXTRACTION_METHOD_PPM_RANGE_H_*/
