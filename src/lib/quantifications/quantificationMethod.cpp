#include "quantificationMethod.h"

QuantificationMethod::QuantificationMethod(const QString & xml_id) 
	:
	_xml_id(xml_id)
{
}

QuantificationMethod::~QuantificationMethod()
{
  
	delete _p_xic_extraction_method;
	delete _p_detection_method;
	// delete xic filters
	//_v_p_xic_filters.clear();
	std::vector<const FilterBase *>::iterator it_filters;
	for (it_filters = _v_p_xic_filters.begin();
		 it_filters != _v_p_xic_filters.end();
		 ++it_filters)
	{
		delete (*it_filters);
	}
}

const QString &
QuantificationMethod::getXmlId() const
{
	return _xml_id;
}

void 
QuantificationMethod::printInfos(QTextStream & out) const
{
	out << "\t Quantification method '" << _xml_id
		<< "' parameters : " << endl;
	out << "\t xic type : '" << _xic_type << "'" << endl;
	out << (_p_xic_extraction_method->printInfos());
	std::vector<const FilterBase *>::const_iterator it_f;
	for (it_f = _v_p_xic_filters.begin(); 
		 it_f != _v_p_xic_filters.end(); 
		 ++it_f) {
		out << ( (*it_f)->printInfos() );
	}
	out << (_p_detection_method->printInfos());
}

void
QuantificationMethod::setDetectionMethod(PeakDetectionBase * detection_method)
{
	_p_detection_method = detection_method;
}

PeakDetectionBase *
QuantificationMethod::getDetectionMethod() const
{
	return _p_detection_method;
}

void
QuantificationMethod::setXicExtractionMethod(XicExtractionMethodBase * xic_extraction_method)
{
	_p_xic_extraction_method = xic_extraction_method;
}

XicExtractionMethodBase * 
QuantificationMethod::getXicExtractionMethod()
{
	return _p_xic_extraction_method;
}

void
QuantificationMethod::setXicType(const QString & xic_type)
{
	if ((xic_type == "sum") || (xic_type == "max"))
	{
		_xic_type = xic_type;
	}
	else
	{
		throw mcqError(QObject::tr("error in QuantificationMethod::setXicType :\n _xic_type must be 'sum' or 'max' and not %1.\n").arg(_xic_type));
	}
}

const QString &
QuantificationMethod::getXicType() const
{
	return _xic_type;
}

void
QuantificationMethod::addFilter(const FilterBase * filter)
{
	_v_p_xic_filters.push_back(filter);
}

const std::vector<const FilterBase *> 
QuantificationMethod::getXicFilters() const
{
	return _v_p_xic_filters;
}
