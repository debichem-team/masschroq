/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantificationMethod.h
 * \date October 26, 2010
 * \author Edlira Nano
 */

#ifndef QUANTIFICATION_METHOD_H_
#define QUANTIFICATION_METHOD_H_ 1

#include "../xicExtractionMethods/xicExtractionMethodBase.h"
#include "../detections/peak_detection_base.h"

/**
   \class QuantificationMethod
   \brief Represents a method of quantification
*/
class QuantificationMethod
{

public :
  
	QuantificationMethod(const QString & xml_id);
	~QuantificationMethod();

	const QString & getXmlId() const ;

	virtual void printInfos(QTextStream & out) const;
  
	void setDetectionMethod(PeakDetectionBase * detection_method);
  
	PeakDetectionBase * getDetectionMethod() const;
	
	void setXicExtractionMethod(XicExtractionMethodBase * xic_extraction_method);
  
	XicExtractionMethodBase * getXicExtractionMethod();
  
	void setXicType(const QString & xic_type);
  
	const QString & getXicType() const ;
  
	void addFilter(const FilterBase * filter);
    
	const std::vector<const FilterBase *> getXicFilters() const; 

private :
  
	/// unique xml id for this method
	const QString _xml_id;
  
	/// this method's xic_type of extraction 
	QString _xic_type;
  
	/// the xic filters
	std::vector<const FilterBase *> _v_p_xic_filters;
  
	/// the xic extraction method for this quantification
	XicExtractionMethodBase * _p_xic_extraction_method;
 
	/// the peak detection method for this quantification
	PeakDetectionBase * _p_detection_method;

 
};


#endif /* QUANTIFICATION_METHOD_H_ */
