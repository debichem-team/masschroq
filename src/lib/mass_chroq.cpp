/**
 * \file mass_chroq.cpp
 * \date September 18, 2009
 * \author Olivier Langella
 */

#include "mass_chroq.h"
#include "mcq_error.h"
#include "msrun/msrun_classic.h"
#include "msrun/msrun_sliced.h"
#include "../saxparsers/masschroqmlParser.h"
#include "../pepfileparser/masschroq_dom_engine.h"
#include "../lib/peptides/peptide_isotope.h"
#include "./consoleout.h"

#include <math.h>
#include <QFileInfo>



#if QT_VERSION >= QT_V_4_6
#include <QUrl>
#include <QXmlSchemaValidator>
#include <QXmlSchema>
#include "messageHandler.h"
#endif

//static variable
QDir MassChroq::tmp_dir;  

/** 
 * We set the temporary working directory to the one precised by the
 * user if he did so (second parameter of masschroq); if not we set
 * it to the system's temporary directory
 */


void
MassChroq::setTmpDir(QString dirName)
{
	tmp_dir.setPath(dirName);
	if (!tmp_dir.exists())
	{
		throw mcqError(QObject::tr("error : cannot find the temporary directory : %1 \n").arg(dirName));
	}
	else
	{
		QFileInfo tmpDirInfo(tmp_dir.absolutePath());
		if (!tmpDirInfo.isWritable())
		{
			throw mcqError(QObject::tr("error : cannot write to the temporary directory : %1 \n").arg(dirName));
		}
	}
}

const QString
MassChroq::getTmpDirName()
{
	return tmp_dir.absolutePath();
}

QDateTime MassChroq::begin_date_time;

/// set the beginning of execution date and time member _begin_date_time
/// the main method sets it
void 
MassChroq::setBeginDateTime(const QDateTime & dt_begin)
{
	begin_date_time = dt_begin;
}

QDateTime 
MassChroq::getBeginDateTime()
{
	return begin_date_time;
}

MassChroq::MassChroq()
{
	qDebug() << "MassChroq::MassChroq begin";
	//_output_stream = new QTextStream(stdout, QIODevice::WriteOnly);
}

MassChroq::~MassChroq()
{
	qDebug() << "MassChroQ engine DESTRUCTOR";
  
	//delete (_output_stream);

	//delete isotopes
	std::map<QString, const IsotopeLabel *>::iterator iti;
	for (iti = _map_p_isotope_labels.begin();
		 iti != _map_p_isotope_labels.end();
		 ++iti)
	{
		delete (iti->second);
	}
	qDebug() << "MassChroQ DESTRUCTOR isotopes";
	
	//delete peptides
	_peptide_list.free();

	qDebug() << "MassChroQ DESTRUCTOR peptides";

	//delete proteins
	std::map<QString, const Protein *>::iterator itp;
	for (itp = _p_proteins.begin();
		 itp != _p_proteins.end();
		 ++itp)
	{
		delete (itp->second);
	}
   
	qDebug() << "MassChroQ DESTRUCTOR proteins";

	//delete alignment methods
	std::map<const QString, AlignmentBase *>::iterator ita;
	for (ita = _alignment_methods.begin();
		 ita != _alignment_methods.end();
		 ++ita)
	{
		delete (ita->second);
	}
   
	qDebug() << "MassChroQ DESTRUCTOR alignement";

	// //delete quantification methods
	std::map<const QString, QuantificationMethod *>::iterator itq;
	for (itq = _quantification_methods.begin();
		 itq != _quantification_methods.end();
		 ++itq)
	{
		delete (itq->second);
	}

	qDebug() << "MassChroQ DESTRUCTOR quantification";

	//delete all msruns
	_ms_run_collection.free();

	qDebug() << "MassChroQ DESTRUCTOR msrun";

	//delete groups
	std::map<QString, msRunHashGroup *>::iterator itg;
	for (itg = _groups.begin();
		 itg != _groups.end();
		 ++itg)
	{
		delete (itg->second);
	} 
	qDebug() << "MassChroq::~MassChroq end";
}

/// Set the XML input filename and verify if it exists and if its readable
void 
MassChroq::setXmlFilename(QString & fileName)
{
	_param_file_info.setFile(fileName);
	if (!_param_file_info.exists())
	{
		throw mcqError(QObject::tr("error : cannot find the XML input file : '%1' \n").arg(fileName));
	}
	else if (!_param_file_info.isReadable())
	{
		throw mcqError(QObject::tr("error : cannot read the XML input file : '%1' \n").arg(fileName));
	}
	//Defined current directory to be compatible with relative path on MassChroqML
	QDir::setCurrent(_param_file_info.absolutePath());
	qDebug() << "Current Directory is " + _param_file_info.absolutePath();
}

void
MassChroq::setMasschroqDir(const QString & masschroq_dir_path)
{
	_masschroq_dir_path = masschroq_dir_path;
}

/**
 * This method validates both XSD schema and XML input file against it
 */

void
MassChroq::validateXmlFile()
{
	qDebug() << "MassChroq::validateXmlFile begin : Qt local version "
			 << QT_VERSION_STR << ", must be at least 4.6";
	
#if QT_VERSION >= QT_V_4_6
  
	QFile schema_file(MASSCHROQ_XSD);
	if ( !QFile::exists(MASSCHROQ_XSD) )
	{
		QString schema_current(_masschroq_dir_path);
		schema_current.append("/").append(MASSCHROQ_SCHEMA_FILE);
		if (QFile::exists(schema_current))
		{ 
			schema_file.setFileName(schema_current);
		} else
			{
				qDebug() << "MassChroQ XSD schema file '%1' does not exist.";
				throw mcqError(QObject::tr("MassChroQ XSD schema is not located in '%1' or '%2'.\n").arg(MASSCHROQ_XSD, schema_current));
			}
	}
	if (!schema_file.open(QIODevice::ReadOnly))
	{
		throw
			mcqError(QObject::tr("error opening the file of the XSD schema : %1 \n Try make install to install the schema file. \n").arg(schema_file.fileName()));
	}
	qDebug() << "MassChroq::validateXmlFile enter >= 4.6";
	QXmlSchema schema;
	MessageHandler messHandler;
	schema.setMessageHandler(&messHandler);
	/// load verifies if the schema is valid before loading it
	// attention : if schema is invalid the behaviour is undefined (Qt)
	if ( schema.load(&schema_file, QUrl::fromLocalFile(schema_file.fileName())) )
	{
		QFile file(_param_file_info.filePath());
		file.open(QIODevice::ReadOnly);
		QXmlSchemaValidator validator(schema);
		if (validator.validate(&file, QUrl::fromLocalFile(file.fileName())))
		{
			qDebug() << "MassChroq::validateXmlFile instance document is valid";
			file.close();
			schema_file.close();
		}
		else
		{
			qDebug() << "MassChroq::validateXmlFile instance document is invalid";
			QString err_text(messHandler.description());
			QString err_line, err_column;
			err_line.setNum(messHandler.line());
			err_column.setNum(messHandler.column());
			file.close();
			schema_file.close();
			throw
				mcqError(QObject::tr("error validating against XSD schema in file %1\n at line %2, column %3 : %4\n").arg(_param_file_info.filePath(), err_line, err_column, err_text));
		}
	}
	else
	{
		schema_file.close();
		throw
			mcqError(QObject::tr("error validating XSD schema : %1 : is invalid \n").arg(schema_file.fileName()));
	}
#endif
}

/** Method that parses the XML input file (using DOM) looking for 
 * peptide text files (containing peptide identification information).
 * If there are such files it launches their parsing and then writes parsed information
 * in the XML input file (in peptide_list form). It then sets this modified XML
 * file as the new input file to MassChroQ. If no peptide files are present, 
 * the input file and its filename are not modified.  
 **/
void 
MassChroq::runDomParser()
{
	QString filepath = _param_file_info.filePath();
	mcqout() << "Running MassChroQ " << MASSCHROQ_VERSION
					<< " with XML file '" << filepath 
					<< "'" << endl;
  
	QFile file(filepath);
	MassChroqDomEngine dom_parser(file);
	QString new_filename = dom_parser.parse();
	this->setXmlFilename(new_filename);
}

/**
 * this important method is the first one to be called in this class : it
 * launches the parsing of the masschroqML input file and it handles this
 * parsing till it ends
 */
void 
MassChroq::runXmlFile()
{
	validateXmlFile();
	QFile file(_param_file_info.filePath());
	MasschroqmlParser parser(this);
  
	QXmlSimpleReader reader;
	reader.setContentHandler(&parser);
	reader.setErrorHandler(&parser);
  
	mcqout() << "Parsing XML input file '" << _param_file_info.filePath()
					<< "'" << endl;
  
  
	QXmlInputSource xmlInputSource(&file);
  
	if (reader.parse(xmlInputSource))
	{
	}
	else
	{
		throw mcqError(QObject::tr("error reading masschroqML input file :\n").append(parser.errorString()));
	}
	mcqout() << "MassChroQ : DONE on file '"
					<< _param_file_info.filePath() 
					<< "'" << endl;
}

/// create the msrun, launch its simple parsing, initialize its slicer if possible
void
MassChroq::setMsRun(const QString & idname, const QFileInfo & fileInfo, const QString & format,
					const bool read_time_values, const QString & time_dir)
{
	mcq_double size_in_bytes = fileInfo.size();
	qDebug() << "Msrun '" << idname <<"' file size is : " << size_in_bytes << " bytes" << endl;
	QString filename = fileInfo.absoluteFilePath();
	if (size_in_bytes < MAX_SLICE_SIZE )
	{ 
		try
		{
			MsrunClassic * msrun = new MsrunClassic(idname);
			mcqout() << "MS run classic'" << idname
				 << "' : " << format << " file '" 
				 << filename
				 << "' : parsing begin" << endl;
			msrun->set_from_xml(filename, format, read_time_values, time_dir);
			this->addMsRun(msrun);
		} catch (mcqError& error)
		{
			throw mcqError(QObject::tr("problem creating msrun %1:\n%2").arg(idname, error.what()));
		}
	} else
	{ 
		mcq_double nb_of_slices_double = ceil(size_in_bytes/MAX_SLICE_SIZE); 
		int nb_of_slices = static_cast<int> (nb_of_slices_double);
		try
		{
			MsrunSliced * msrun = new MsrunSliced(idname, nb_of_slices);
			mcqout() << "MS run sliced '" << idname				 << "' : " << format << " file '" 
				 << filename
				 << "', number of slices " << nb_of_slices 
				 <<", : parsing begin" << endl;
      
			msrun->set_from_xml(filename, format, read_time_values, time_dir);
			this->addMsRun(msrun);
		} catch (mcqError& error)
		{
			throw mcqError(QObject::tr("problem creating msrun %1:\n%2").arg(idname, error.what()));
		}  
	} 
}

void 
MassChroq::addMsRun(Msrun * p_msrun)
{
	/// sleeping spectra on disk
	/// attention p_msrun->doneWithSpectra();
	_ms_run_collection.setMsRun(p_msrun);
	mcqout() << "MS run '" << p_msrun->getXmlId() << "' : added" << endl;
}

void 
MassChroq::addAlignmentMethod(const QString & method_id, AlignmentBase * alignment_method)
{
	_alignment_methods[method_id] = alignment_method;
	mcqout() << "Alignment method '" << method_id << "' : added" << endl;
}

void 
MassChroq::addQuantificationMethod(QuantificationMethod * quantification_method)
{
	QString quantification_id = quantification_method->getXmlId(); 
	_quantification_methods[quantification_id] = quantification_method;
	mcqout() << "Quantification method '" << quantification_id << "' : added" << endl;
}

void 
MassChroq::addQuantificator(Quantificator * quantificator)
{
	QString quantify_id = quantificator->getXmlId();
	_quantificators[quantify_id] = quantificator;
}

void
MassChroq::deleteQuantificator(const QString & quantify_id)
{
	_quantificators.erase(quantify_id);
}

void
MassChroq::setQuantificatorMatchMode(
	const QString & quantify_id,
	const mcq_matching_mode & match_mode)
{
	
	Quantificator * quantificator = findQuantificator(quantify_id); 
	if (quantificator == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::setQuantificatorMatchMode :\nthe quantificator with id '%1' does not exist").arg(quantify_id));
	}
	quantificator->set_matching_mode(match_mode);
}

void
MassChroq::addQuantificatorPeptideItems(
	const QString & quantify_id,
	const QStringList & isotope_labels_list)
{

	Quantificator * quantificator = findQuantificator(quantify_id); 
	if (quantificator == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::addQuantificatorPeptideItems:\nthe quantificator with id '%1' does not exist").arg(quantify_id));
	}

	const QString group_id = quantificator->get_quanti_group_id();

    /// get the list of the peptides observed in the current msrun group
	PeptideList * p_peptide_list = newPeptideListInGroup(group_id);

	qDebug() << "Yo2 peptide list size " << p_peptide_list->size();
  
	/// in this vector we will put both peptides and isotopes
	std::vector<const Peptide *> isotope_peptides;
	const IsotopeLabel * isotope_label;
	PeptideList::const_iterator it;
	std::vector<const Peptide *>::const_iterator itpep;
	for (it = p_peptide_list->begin(); it != p_peptide_list->end(); ++it)
	{
		//isotope_peptides.resize(0);
		// if no isotopes put only the peptides
		if (isotope_labels_list.size() == 0)
		{
			isotope_peptides.push_back(*it);
		} 
		// if isotopes put only isotopes
		else
		{
			for (QStringList::const_iterator itstr = isotope_labels_list.begin(); 
				 itstr != isotope_labels_list.end(); 
				 ++itstr)
			{
				isotope_label = getIsotopeLabel(*itstr);
				PeptideIsotope * isotope = new PeptideIsotope(*it, isotope_label);
				if (isotope->getMass() != (*it)->getMass())
				{
					isotope_peptides.push_back(isotope);
				} else
				{
					delete (isotope);
				}
			}
		}
	}

	qDebug() << "Yo 3 isotope peptides list size " << isotope_peptides.size();
	
    
	quantificator->add_peptide_quanti_items(isotope_peptides);
	delete (p_peptide_list); 
}

void
MassChroq::addQuantificatorMzrtItem(
	const QString & quantify_id,
	const QString & mz, const QString & rt)
{

	Quantificator * quantificator = findQuantificator(quantify_id); 
	if (quantificator == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::addQuantificatorMzrtItem:\nthe quantificator with id '%1' does not exist").arg(quantify_id));
	}
  
	quantificator->add_mzrt_quanti_item(mz, rt);
}

void
MassChroq::addQuantificatorMzItems(
	const QString & quantify_id,
	const QStringList & mz_list)
{

	Quantificator * quantificator = findQuantificator(quantify_id); 
	if (quantificator == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::addQuantificatorMzItems:\nthe quantificator with id '%1' does not exist").arg(quantify_id));
	}

	quantificator->add_mz_quanti_items(mz_list);
}

void 
MassChroq::addIsotopeLabel(const IsotopeLabel * p_isotope_label)
{
	_map_p_isotope_labels[p_isotope_label->getXmlId()] = p_isotope_label;
}

const IsotopeLabel * 
MassChroq::getIsotopeLabel(const QString & label_id)
{
	return (_map_p_isotope_labels[label_id]);
}

void 
MassChroq::newMsRunGroup(const QString & group_id,
						 const QStringList & list_msrun_ids)
{
  
	std::map<QString, msRunHashGroup *>::iterator it;
	it = _groups.find(group_id);
	if (it == _groups.end())
	{
		/// OK this key does not exist, we can create it
		msRunHashGroup * newgroup = new msRunHashGroup(group_id);
		_groups[group_id] = newgroup;
		Msrun * p_msrun;
		
		QStringListIterator list_it(list_msrun_ids);
		QString msrun_id;
		while (list_it.hasNext())
		{
			msrun_id = list_it.next();
			p_msrun = _ms_run_collection.getMsRun(msrun_id);
			if (p_msrun == NULL)
			{
				/// we have a problem
				throw mcqError(QObject::tr("error in MassChroQ::newMsRunGroup :\nthe msrun with id %1 does not exist").arg(msrun_id));
			}
			else
			{
				/// verify that this msrun is not already part of an existing group
				std::map<QString, msRunHashGroup *>::iterator itverif;
				for (itverif = _groups.begin();
					 itverif != _groups.end();
					 ++itverif)
				{ 
					if ( (itverif->second)->containsMsRun(p_msrun) )
					{
						mcqout() << "WARNING : MS run group '"
										<< group_id << "' : contains MS run '"
										<< p_msrun->getXmlId()
										<< "' which is already part of '" 
										<< itverif->first
										<< "' group"
										<< endl;
					}
				}
				/// add this msrun to the new group 
				newgroup->setMsRun(p_msrun);
			}
		}
		
	} else
	{
		throw mcqError(QObject::tr("error in MassChroQ::newMsRunGroup :\nthe group with id %1 already exists").arg(group_id));
	}
	QString coutList = list_msrun_ids.join(", ");
	mcqout() << "MS run group '" << group_id
					<< "' = ("
					<< coutList
					<<") : defined" << endl;
}

Msrun * 
MassChroq::findMsRun(const QString & msrun_id) const
{
	return _ms_run_collection.getMsRun(msrun_id);
}

msRunHashGroup * 
MassChroq::findGroup(const QString & group_id) const
{
	std::map<QString, msRunHashGroup *>::const_iterator it;
	it = _groups.find(group_id);
	if (it == _groups.end())
	{
		return (NULL);
	}
	return (it->second);
}

const Protein * 
MassChroq::findProtein(const QString & protein_id) const
{
	std::map<QString, const Protein *>::const_iterator it;
	it = _p_proteins.find(protein_id);
	if (it == _p_proteins.end())
	{
		return (NULL);
	}
	return (it->second);
}

AlignmentBase * 
MassChroq::findAlignmentMethod(const QString & method_id) const
{
	std::map<const QString, AlignmentBase *>::const_iterator it;
	it = _alignment_methods.find(method_id);
	if (it == _alignment_methods.end())
	{
		return (NULL);
	}
	return (it->second);
}

QuantificationMethod * 
MassChroq::findQuantificationMethod(const QString & method_id) const
{
	std::map<const QString, QuantificationMethod *>::const_iterator it;
	it = _quantification_methods.find(method_id);
	if (it == _quantification_methods.end())
	{
		return (NULL);
	}
	return (it->second);
}

Quantificator * 
MassChroq::findQuantificator(const QString & quantify_id) const
{
	std::map<const QString, Quantificator *>::const_iterator it;
	it = _quantificators.find(quantify_id);
	if (it == _quantificators.end())
	{
		return (NULL);
	}
	return (it->second);
}


void 
MassChroq::alignGroup(const QString & group_id,
					  const QString & method_id,
					  const QString & ref_msrun_id)
{
	mcqout() << "MS run group '" << group_id
					<< "': alignment method '" << method_id
					<< "', reference msrun '" << ref_msrun_id 
					<< "': alignment begin" << endl;
  
	msRunHashGroup * p_group;
	AlignmentBase * p_method;
	//find group
	p_group = findGroup(group_id);
	if (p_group == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::alignGroup :\nthe group with id %1 does not exist").arg(group_id));
	}
  
	p_method = findAlignmentMethod(method_id);
	if (p_method == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::alignGroup :\nthe alignment method with id %1 does not exist").arg(method_id));
	}
	p_group->setReferenceMsrun(ref_msrun_id);
	p_group->alignMsRuns(*p_method);
	mcqout() << "MS run group '" << group_id
					<< "' : alignment method '" << method_id
					<< "', reference msrun '" << ref_msrun_id 
					<< "' : alignment finished" << endl;
}

void 
MassChroq::executeQuantification(const QString & quantify_id)
{

	Quantificator * quantificator = findQuantificator(quantify_id); 
	if (quantificator == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::executeQuantification :\nthe quantificator with id '%1' does not exist").arg(quantify_id));
	}

	quantificator->setMonitor(_p_quantif_results);
	quantificator->quantify();
    
}

void 
MassChroq::addProtein(const Protein * p_protein)
{
	_p_proteins[p_protein->getXmlId()] = p_protein;
}

void 
MassChroq::addPeptideInPeptideList(Peptide * p_peptide)
{
	_peptide_list.push_back(p_peptide);
}

const Peptide *
MassChroq::getPeptide(const QString pepid) const
{
	return _peptide_list.getPeptide(pepid);
}

PeptideList * 
MassChroq::newPeptideListInGroup(const QString & group_xml_id) const
{
 
	const msRunHashGroup * group = findGroup(group_xml_id);
	if (group == NULL)
	{
		throw mcqError(QObject::tr("error in MassChroQ::alignGroup :\nthe group with id %1 does not exist").arg(group_xml_id));
	}
	return _peptide_list.newPeptideListObservedInMsGroup(*group);
}

void 
MassChroq::setPeptideListInMsruns() const
{
  
	msRunHashGroup::const_iterator it;
	for (it = _ms_run_collection.begin();
		 it != _ms_run_collection.end();
		 ++it)
	{
		Msrun * msrun = it->second;
		msrun->setPeptideList(_peptide_list.newPeptideListObservedInMsrun(msrun) );
	}
}

void 
MassChroq::setResults(MonitorList * p_results)
{
	_p_quantif_results = p_results;
}

void 
MassChroq::debriefing()
{
	qDebug() << "MassChroq::debriefing begin " ;
	_p_quantif_results->debriefing();
	qDebug() << "MassChroq::debriefing end " ;
}
