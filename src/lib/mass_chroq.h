/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope thatx it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file mass_chroq.h
 * \date September 18, 2009
 * \author Olivier Langella
 */

#ifndef MASS_CHROQ_H_
#define MASS_CHROQ_H_ 1

#include <QDir>
#include <QTextStream>
#include <QDateTime>

#include "../config.h"
#include "quantificator.h"
#include "monitors/monitorList.h"
#include "msrun/msrun.h"
#include "msrun/ms_run_hash_group.h"
#include "alignments/alignment_base.h"
#include "quantifications/quantificationMethod.h"
#include "peptides/peptide_list.h"
#include "peptides/protein.h"
#include "peptides/isotope_label.h"

/**
 * \class MassChroq
 * \brief The super class in MassChroq : it knows and coordinates everything
 * else

 * This class launches the parsing of the XML input file and keeps track of
 all of the elements parsed : msruns, groups, peptides, and of all of the 
 parameters on them : alignment methods, quantification methods.
 */

class MassChroq {
public:
  
  /// Temporary working directory static variable
  static QDir tmp_dir;  
  /// set the temporary working directory
  static void setTmpDir(QString tmpDirName);
 
  /// get the temporary working directory's absolute path name
  static const QString getTmpDirName();
 
  /// masschroq's beginning of execution date and time static variable
  static QDateTime begin_date_time;  
 
  /// set the masschroq's beginning date and time static variable
  static void setBeginDateTime(const QDateTime & dt_begin);
  static QDateTime getBeginDateTime();

  MassChroq();
  virtual ~MassChroq();
  
  /// set the input XML filename
  void setXmlFilename(QString &);
  
  const QString getXmlFilename() {
    return _param_file_info.filePath();
  }; 
  
    
/// validation of the XSD schema and of the XML input file against it
  void validateXmlFile();
  
  /// parse the XML input file with Dom to look for peptide_files_list tag
  void runDomParser();
  
  /// parsing of the XML input file
  void runXmlFile();
  
  void setResults(MonitorList * p_results);
  
  void setPeptideListInMsruns() const;

  const IsotopeLabel * getIsotopeLabel(const QString & label_id);
 
  /// creates and sets an msrun
  void setMsRun(const QString & idname, const QFileInfo & fielInfo, const QString & format,
				const bool read_time_values, const QString & time_dir);
 
  void addMsRun(Msrun * p_msrun);
  
  void addAlignmentMethod(const QString & method_id,
			  AlignmentBase * alignment_method);
  
  void addQuantificationMethod(QuantificationMethod * quantification_method);

  void addQuantificator(Quantificator * quantificator);
  
  void deleteQuantificator(const QString & quantify_id);

  Quantificator * findQuantificator(const QString & quantify_id) const;

  void setQuantificatorMatchMode(const QString & quantify_id, const mcq_matching_mode & match_mode);

  void addQuantificatorPeptideItems(const QString & quantify_id, const QStringList & isotope_labels_list);

  void addQuantificatorMzrtItem(const QString & quantify_id, const QString & mz, const QString & rt);

  void addQuantificatorMzItems(const QString & quantify_id, const QStringList & mz_list);


  void addIsotopeLabel(const IsotopeLabel * p_isotope_label);
  
  void addPeptideInPeptideList(Peptide * p_peptide);

  const Peptide * getPeptide(const QString pepid) const;
  
  void addProtein(const Protein * p_protein);

  void newMsRunGroup(const QString & idname,
		     const QStringList & list_msrun_ids);
  
  PeptideList * newPeptideListInGroup(const QString & group_xml_id) const;
  
  Msrun * findMsRun(const QString & msrun_id) const;
  
  msRunHashGroup * findGroup(const QString & group_id) const;
  
  const Protein * findProtein(const QString & protein_id) const;
  
  void alignGroup(const QString & group_id, 
		  const QString & method_id,
		  const QString & ref_msrun_id);
  
  std::vector<const FilterBase *> getXicFilters() const;
  
  void executeQuantification(const QString & quantify_id);
  
  AlignmentBase * findAlignmentMethod(const QString & method_id) const;
  
  QuantificationMethod * findQuantificationMethod(const QString & method_id) const;

  void setMasschroqDir(const QString & masschroq_dir_path);

  
  void debriefing();
  
 protected:
   
  QFileInfo _param_file_info;
  
  msRunHashGroup _ms_run_collection;
  
  //QTextStream * _output_stream;
  
  /// 
  std::map<QString, msRunHashGroup *> _groups;
  
  std::map<const QString, AlignmentBase *> _alignment_methods;
  
  std::map<const QString, QuantificationMethod *> _quantification_methods;
  
  std::map<const QString, Quantificator *> _quantificators;
  
  MonitorList * _p_quantif_results;
  
  PeptideList _peptide_list;
  
  std::map<QString, const IsotopeLabel *> _map_p_isotope_labels;
  
  std::map<QString, const Protein *> _p_proteins;

  QString _masschroq_dir_path;

};

#endif /* MASS_CHROQ_H_ */
