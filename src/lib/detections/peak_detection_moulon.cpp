/**
 * \file peak_detection_moulon.cpp
 * \date 24 sept. 2009
 * \author Olivier Langella
 */

#include "peak_detection_moulon.h"
#include "../share/utilities.h"

#include <deque>
#include <map>

PeakDetectionMoulon::PeakDetectionMoulon()
{
	coeff_detect_range = 1;
}

PeakDetectionMoulon::~PeakDetectionMoulon()
{
}

void
PeakDetectionMoulon::set_half_smoothing_window_size(unsigned int length)
{
	_half_smoothing_window_size = length;
}
  
void
PeakDetectionMoulon::set_tic_start(mcq_double tic_start)
{
	_tic_start = tic_start;
}
  
void
PeakDetectionMoulon::set_tic_stop(mcq_double tic_stop)
{
	_tic_stop = tic_stop;
}

unsigned int
PeakDetectionMoulon::get_half_smoothing_window_size() const
{
	return (_half_smoothing_window_size);
}

mcq_double
PeakDetectionMoulon::get_tic_start() const
{
	return (_tic_start);
}

mcq_double
PeakDetectionMoulon::get_tic_stop() const
{
	return (_tic_stop);
}

void 
PeakDetectionMoulon::printInfos(QTextStream & out) const
{
	out << "\t detection of type 'moulon' with parameters :" << endl;
	out << "\t _half_smoothing_window_size = " 
		<< _half_smoothing_window_size << endl;
	out << "\t _tic_start = " << _tic_start << endl;
	out << "\t _tic_stop = " << _tic_stop << endl;
}

std::vector<xicPeak *> *
PeakDetectionMoulon::privNewDetectedPeaks(const QuantiItemBase * quanti_item,
										  const Msrun * p_msrun,
										  const std::vector<mcq_double> * p_v_rt,
										  const std::vector<mcq_double> * p_v_intensity) const
{
  	std::vector<xicPeak *> * result = new std::vector<xicPeak *>;
	/// smoothing 
	std::vector<mcq_double> * p_v_smoothing = Utilities::newVectorByApplyingWindowsOperation
		(p_v_intensity, _half_smoothing_window_size, &get_average);
  
	// detect peaks :
	mcq_double rtime;
	mcq_double intensity;
	bool banked(false);
	xicPeak * current_peak(0);
	unsigned int nb_tic_start(0);
	current_peak = NULL;
	constvdoubleit it;
	unsigned int i;
	for (i = 0, it = p_v_smoothing->begin(); 
		 it != p_v_smoothing->end(); 
		 ++it, ++i) 
    {
		rtime = (*p_v_rt)[i];
		intensity = (*p_v_intensity)[i];
		
		if ((nb_tic_start == 0) 
			&& (current_peak != NULL)
			&& (banked == false))
		{
			delete (current_peak);
			current_peak = NULL;
		}
      
		if (*it >= _tic_start)
		{
			nb_tic_start++;
			if (current_peak == NULL) {
				current_peak = new xicPeak(quanti_item, p_msrun);
				banked = false;
			}
			if ((nb_tic_start == 2) && (banked == false))
			{
				result->push_back(current_peak);
				banked = true;
			}
		} else
		{
			nb_tic_start = 0;
		}
		if (current_peak != NULL)
		{
			current_peak->add_measure(rtime, intensity);
		}
		if (*it <= _tic_stop)
		{
			if (current_peak != NULL)
			{
				current_peak = NULL;
				banked = false;
			}
		}
    }
	delete p_v_smoothing;
	return (result);
}
