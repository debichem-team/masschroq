/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file peak_detection_moulon.h
 * \date 24 sept. 2009
 * \author Olivier Langella
 */

#ifndef PEAK_DETECTION_MOULON_H_
#define PEAK_DETECTION_MOULON_H_ 1

#include "peak_detection_base.h"

/**
 * \class PeakDetectionMoulon
 * \brief A classical peak detection method 
 */

class PeakDetectionMoulon : public PeakDetectionBase {

public:
  
	PeakDetectionMoulon();
	virtual ~PeakDetectionMoulon();

	virtual void printInfos(QTextStream & out) const;
  
	void set_half_smoothing_window_size(unsigned int length);
  
	void set_tic_start(mcq_double tic_start);
  
	void set_tic_stop(mcq_double tic_stop);

	unsigned int get_half_smoothing_window_size() const;

	mcq_double get_tic_start() const;

	mcq_double get_tic_stop() const;
  
protected:

	virtual std::vector<xicPeak *> *
		privNewDetectedPeaks(const QuantiItemBase * quanti_item,
							 const Msrun * p_msrun,
							 const std::vector<mcq_double> * p_v_rt,
							 const std::vector<mcq_double> * p_v_intensity) const;

private:

	unsigned int _half_smoothing_window_size;
	mcq_double _tic_start;
	mcq_double _tic_stop;

};

#endif /* PEAK_DETECTION_MOULON_H_ */
