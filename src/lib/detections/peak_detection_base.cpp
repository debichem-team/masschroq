/**
 * \file peak_detection_base.cpp
 * \date September 24, 2009
 * \author Olivier Langella
 */

#include "peak_detection_base.h"
#include "../xic/xic_base.h"
#include "../share/utilities.h"

PeakDetectionBase::PeakDetectionBase()
{
	coeff_detect_range = 0;
}

PeakDetectionBase::~PeakDetectionBase()
{
}

const mcq_double 
PeakDetectionBase::get_coeff_detect_range() const
{
	return coeff_detect_range;
}

const QString 
PeakDetectionBase::printInfos() const
{
	QString infos;
	QTextStream info_stream(&infos, QIODevice::WriteOnly);
	printInfos(info_stream);
	return (infos);
}

std::vector<xicPeak *> * 
PeakDetectionBase::newDetectedPeaks(const xicBase & xic) const
{
	const Msrun * p_msrun(xic.getMsRun());
	QuantiItemBase * quanti_item(xic.getQuantiItem());
	const std::vector<mcq_double> * p_v_rt(xic.getConstRetentionTimes());
	const std::vector<mcq_double> * p_v_intensity(xic.getConstIntensities());

	std::vector<xicPeak *> * v_peaks = this->newDetectedPeaks(quanti_item,
														 p_msrun,
														 p_v_rt,
														 p_v_intensity);
	return v_peaks;
}

std::vector<xicPeak *> * 
PeakDetectionBase::newDetectedPeaks(const QuantiItemBase * quanti_item,
									const Msrun * p_msrun,
									const std::vector<mcq_double> * p_v_rt,
									const std::vector<mcq_double> * p_v_intensity) const
{
	if (p_v_rt->size() != p_v_intensity->size())
	{
		throw mcqError("Error in peakDetectionBase::newDetectedPeaks :\n rt and signal intensity vectors must have the same size.");
	}
	// detect all peaks
	std::vector<xicPeak *> * v_peaks = this->privNewDetectedPeaks(quanti_item,
															 p_msrun,
															 p_v_rt, 
															 p_v_intensity);
	
	return v_peaks;
}

void 
PeakDetectionBase::findPeakRangeWalkingFromMax(unsigned int * begin,
											   unsigned int * end, 
											   unsigned int initial_pos,
											   const std::vector<mcq_double> curve) const
{
	//find the begining of the peak (where minmax > 0 and don't go up)
	*begin = initial_pos;
	while (((*begin) > 0) && (curve[(*begin)] > 0)
		   &&
		   (curve[(*begin) - 1] <= curve[(*begin)]))
	{
		(*begin)--;
	}
	//find the end of the peak 
	*end = initial_pos;
	while (((*end) + 1 < curve.size()) && (curve[(*end)] > 0)
		   &&
		   (curve[(*end)] >= curve[(*end) + 1]))
	{
		(*end)++;
	}
  
	//walk back on plates
	while ((curve[(*begin) + 1] == curve[(*begin)]))
	{
		(*begin)++;
	}
	while ((curve[(*end) - 1] == curve[(*end)]))
	{
		(*end)--;
	}
}

