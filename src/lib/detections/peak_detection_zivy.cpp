/**
 * \file peak_detection_zivy.cpp
 * \date 23 oct. 2009
 * \author Olivier Langella
 */

#include "peak_detection_zivy.h"
#include "../filters/filter_min_max.h"
#include "../filters/filter_max_min.h"
#include "../filters/filter_smoothing.h"

PeakDetectionZivy::PeakDetectionZivy()
{
	_minmax_half_edge = 3;
	_maxmin_half_edge = 2;
	_detection_threshold_on_minmax = 2000;
	_detection_threshold_on_maxmin = 1000;
	_mean_filter_half_edge = 1;
}

PeakDetectionZivy::~PeakDetectionZivy()
{
}

void
PeakDetectionZivy::set_mean_filter_half_edge(unsigned int mean_filter_half_edge)
{
	_mean_filter_half_edge = mean_filter_half_edge;
}
  
void
PeakDetectionZivy::set_minmax_half_edge(unsigned int minmax)
{
	_minmax_half_edge = minmax;
}
  
void
PeakDetectionZivy::set_maxmin_half_edge(unsigned int maxmin)
{
	_maxmin_half_edge = maxmin;
}
  
void
PeakDetectionZivy::set_detection_threshold_on_max(mcq_double max)
{
	_detection_threshold_on_minmax = max;
}
  
void
PeakDetectionZivy::set_detection_threshold_on_min(mcq_double min)
{
	_detection_threshold_on_maxmin = min;
}


void
PeakDetectionZivy::printInfos(QTextStream & out) const
{
	out << "\t detection of type 'zivy' with parameters :\n"
		<< "\t _minmax_half_edge = " << _minmax_half_edge << endl;
	out << "\t _maxmin_half_edge = " << _maxmin_half_edge << endl;
	out << "\t _detection_threshold_on_minmax = "
		<< _detection_threshold_on_minmax << endl;
	out << "\t _detection_threshold_on_maxmin = "
		<< _detection_threshold_on_maxmin << endl;
}

std::vector<xicPeak *> *
PeakDetectionZivy::privNewDetectedPeaks(const QuantiItemBase * quanti_item,
										const Msrun * p_msrun, 
										const std::vector<mcq_double> * p_v_rt,
										const std::vector<mcq_double> * p_v_intensity) const 
{
	
	std::vector<xicPeak *> * result = new std::vector<xicPeak *>;
	std::vector<mcq_double> * p_v_rtbis = new std::vector<mcq_double>(*p_v_rt);
	std::vector<mcq_double> * p_v_minmax = new std::vector<mcq_double>(*p_v_intensity);
	std::vector<mcq_double> * p_v_maxmin = NULL;
	
	if (_mean_filter_half_edge != 0)
	{
		// Smooth signal with a smoothing (moving-average) filter before 
		// performing detection. The original signal is not altered.
		FilterSmoothing f_mean_smoothing;
		f_mean_smoothing.set_smoothing_half_window_length(_mean_filter_half_edge);
		f_mean_smoothing.treatSignal(p_v_rtbis, &p_v_minmax);
		p_v_maxmin = new std::vector<mcq_double>(*p_v_minmax);
	}
	else
	{
		p_v_maxmin = new std::vector<mcq_double>(*p_v_intensity);
	}
	
	// Close transform 
	FilterMinMax f_minmax;
	f_minmax.set_min_max_half_window_length(_minmax_half_edge);
	f_minmax.treatSignal(p_v_rtbis, & p_v_minmax);
	
	// Open transform
	FilterMaxMin f_maxmin;
	f_maxmin.set_max_min_half_window_length(_minmax_half_edge);
	f_maxmin.treatSignal(p_v_rtbis, & p_v_maxmin);
	
	//detect peak positions on close curve : a peak is an intensity value 
	//strictly greater than the two surrounding values. In case of 
	//equality (very rare, can happen with some old old spectrometers) we 
	//take the last equal point to be the peak
	
	std::vector<mcq_double> & v_minmax = *p_v_minmax; //"close" courbe du haut
	std::vector<mcq_double> & v_maxmin = *p_v_maxmin; //"open" courbe du bas
	std::vector<mcq_double> peak_rt_position;
	for (unsigned int i = 1, count = 0; i < p_v_minmax->size() - 1; )
	{
		// conditions to have a peak
		if ( (v_minmax[i - 1 - count] < v_minmax[i]) && 
			 (v_minmax[i] > _detection_threshold_on_minmax) &&
			 (v_maxmin[i] > _detection_threshold_on_maxmin) ) 
		{
			// here we test the last condition to have a peak
			
			// no peak case
			if (v_minmax[i] < v_minmax[i + 1])
			{
				++i;
				count = 0;
			} 
			// there is a peak here ! case
			else if (v_minmax[i] > v_minmax[i + 1])
			{
				xicPeak * tmp_peak = new xicPeak(quanti_item, p_msrun);
				unsigned int begin_peak;
				unsigned int end_peak;
				findPeakRangeWalkingFromMax(&begin_peak, &end_peak, i, v_minmax);
				
				// integrate peak surface :
				for (unsigned int j = begin_peak; j <= end_peak; j++)
				{
					tmp_peak->add_measure(p_v_rt->at(j), p_v_intensity->at(j));
				}
				// put peak in the results vector
				if (begin_peak == end_peak)
				{
					delete tmp_peak;
				}
				else
				{
					result->push_back(tmp_peak);
				}
				++i;
				count = 0;
			}
			// equality case, skipping equal points
			else
			{
				// while (v_minmax[i] == v_minmax[i + 1]) {
				++i, ++count;
			}
		} 
		// no chance to have a peak at all, continue looping
		else
			++i;
	} //end loop for peaks
	
	// clean_up
	delete p_v_minmax;
	delete p_v_maxmin;
	delete p_v_rtbis;
	return (result);
}
