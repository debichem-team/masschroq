/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file peak_detection_zivy.h
 * \date 23 oct. 2009
 * \author Olivier Langella
 */


#ifndef PEAK_DETECTION_ZIVY_H_
#define PEAK_DETECTION_ZIVY_H_ 1

#include "peak_detection_base.h"

/**
 * \class PeakDetectionZivy
 * \brief The famous Zivy peak detection method
 */

class PeakDetectionZivy : public PeakDetectionBase {

public:
  
	PeakDetectionZivy();
	virtual ~PeakDetectionZivy();
  
	virtual void printInfos(QTextStream & out) const;
  
	void set_mean_filter_half_edge(unsigned int mean_filter_half_edge);
  
	void set_minmax_half_edge(unsigned int minmax);
  
	void set_maxmin_half_edge(unsigned int maxmin);
  
	void set_detection_threshold_on_max(mcq_double max);
  
	void set_detection_threshold_on_min(mcq_double min);

	int get_mean_filter_half_edge() const {
		return(_mean_filter_half_edge);
	}

	int get_minmax_half_edge() const {
		return(_minmax_half_edge);
	}

	int get_maxmin_half_edge() const {
		return(_maxmin_half_edge);
	}

	mcq_double get_detection_threshold_on_max() const {
		return(_detection_threshold_on_minmax);
	}

	mcq_double get_detection_threshold_on_min() const {
		return(_detection_threshold_on_maxmin);
	}

protected:
	
	virtual std::vector<xicPeak *> *
		privNewDetectedPeaks(const QuantiItemBase * quanti_item,
							 const Msrun * p_msrun,
							 const std::vector<mcq_double> * p_v_rt,
							 const std::vector<mcq_double> * p_v_intensity) const;
	
private:
	
	unsigned int _mean_filter_half_edge;
	unsigned int _minmax_half_edge;
	unsigned int _maxmin_half_edge;
	mcq_double _detection_threshold_on_minmax;
	mcq_double _detection_threshold_on_maxmin;

};

#endif /* PEAK_DETECTION_ZIVY_H_ */
