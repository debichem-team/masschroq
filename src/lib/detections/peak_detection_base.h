/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file peak_detection_base.h
 * \date September 24, 2009
 * \author Olivier Langella
 */

#ifndef PEAK_DETECTION_BASE_H_
#define PEAK_DETECTION_BASE_H_ 1

#include <QDebug>
#include <QString>
#include <QTextStream>

#include "../peak/xic_peak.h"
#include "../mcq_error.h"
#include "../filters/filter_base.h"

//using namespace std;

/**
 * \class PeakDetectionBase
 * \brief A virtual class representing a method of peak detection
 */

class PeakDetectionBase {

public:
	PeakDetectionBase();
	virtual ~PeakDetectionBase();
  
	const QString printInfos() const;
  
	virtual void printInfos(QTextStream & out) const=0;
  
	std::vector<xicPeak *> * newDetectedPeaks(const xicBase & xic) const;
	
	std::vector<xicPeak *> * newDetectedPeaks(const QuantiItemBase * mz_quanti_item,
											  const Msrun * p_msrun,
											  const std::vector<mcq_double> * p_v_rt,
											  const std::vector<mcq_double> * p_v_intensity) const;
	
	const mcq_double get_coeff_detect_range() const;

protected:
  
	/// pure virtual method that detects and returns a vector of xicPeak-s
	virtual std::vector<xicPeak *> *
		privNewDetectedPeaks(const QuantiItemBase * mz_quanti_item,
							 const Msrun * p_msrun,
							 const std::vector<mcq_double> * p_v_rt,
							 const std::vector<mcq_double> * p_v_intensity) const = 0;
  
	void findPeakRangeWalkingFromMax(unsigned int * begin, 
									 unsigned int * end,
									 unsigned int initial_pos, 
									 const std::vector<mcq_double> curve) const;
  
	mcq_double coeff_detect_range;

};

#endif /* PEAK_DETECTION_BASE_H_ */
