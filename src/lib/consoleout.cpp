/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "consoleout.h"

//#include <iostream>



QTextStream * ConsoleOut::_p_out = NULL;
QTextStream * ConsoleOut::_p_err = NULL;


QTextStream & mcqout() {
  return ConsoleOut::mcq_cout();
}

QTextStream & mcqerr() {
  return ConsoleOut::mcq_cerr();
}

void ConsoleOut::setCout(QTextStream * out) {
  ConsoleOut::_p_out = out;
}
void ConsoleOut::setCerr(QTextStream * out) {
  ConsoleOut::_p_err = out;
}

QTextStream & ConsoleOut::mcq_cout() {
  if (ConsoleOut::_p_out == NULL) {
    ConsoleOut::_p_out = new QTextStream(stdout, QIODevice::WriteOnly);
  }
  return * ConsoleOut::_p_out;
}

QTextStream & ConsoleOut::mcq_cerr() {
  if (ConsoleOut::_p_err == NULL) {
    ConsoleOut::_p_err = new QTextStream(stderr, QIODevice::WriteOnly);
  }
  return * ConsoleOut::_p_err;
}

