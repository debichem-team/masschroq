/**
 * \file peptide.cpp
 *
 * \date Created on: 27 oct. 2009
 * \author langella
 */

#include "peptide.h"
#include "../msrun/ms_run_hash_group.h"
#include "../share/utilities.h"

#include <QRegExp>
#include <set>


Peptide::Peptide(const QString & peptide_xml_id) :
  _peptide_xml_id(peptide_xml_id)
{
}

Peptide::~Peptide() {
  std::vector<PeptideObservedIn *>::iterator it;
  for (it = _observed_in_list.begin(); 
       it != _observed_in_list.end(); 
       it++) 
    {
      delete (*it);
    }
  _observed_in_list.clear();
}

mcq_double 
Peptide::getMass() const {
  return (_mass);
}

mcq_double 
Peptide::getMz(unsigned int z) const {
	return ((_mass + (MHPLUS * z)) / z);
}

const QString & 
Peptide::getMods() const {
  return (_mods);
}

void 
Peptide::printInfos(QTextStream & out) const {
  out << "peptide : " << _peptide_xml_id << endl;
  out << "_sequence = " << _sequence << endl;
  out << "_mass = " << _mass << endl;
}

void 
Peptide::addProtein(const Protein * p_protein) {
  _v_p_protein.push_back(p_protein);
}

void 
Peptide::setSequence(const QString & sequence) {
  if (Utilities::isValidSequence(sequence, Qt::CaseSensitive)) 
    _sequence = sequence;
  else 
    throw mcqError(QObject::tr("Error in Peptide::setSequence :\n %1 is not a valid amino acid sequence").arg(sequence));
}

void 
Peptide::setMods(const QString & mods) {
  _mods = mods;
}

void 
Peptide::setMh(mcq_double mh) {
  _mass = mh - MHPLUS;
}

/// creates a new PeptideObservedIn object ans adds it to the 
///_observed_in_list vector
void 
Peptide::observed_in(const Msrun * p_msrun, 
		     const int scan_num, 
		     unsigned int z) {
  PeptideObservedIn * p_obs = 
    new PeptideObservedIn(this, p_msrun, scan_num, z);
  _observed_in_list.push_back(p_obs);
}

/// creates a new vector containing the PeptideObservedIn objects
/// observed in the given group
// Attention : returns a pointer to a new vector. When this function 
// is called be careful to delete it after use
std::vector<PeptideObservedIn *> *
Peptide::getObservedInGroup(const msRunHashGroup & group) const {
  
  std::vector<PeptideObservedIn *> * v_observed_in_group =
    new std::vector<PeptideObservedIn *>();
  
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  
  for (itobslist = _observed_in_list.begin(); 
       itobslist != _observed_in_list.end(); 
       ++itobslist) {
    if (group.containsMsRun((*itobslist)->getPmsRun())) {
      v_observed_in_group->push_back(*itobslist);
    }
  }
  return v_observed_in_group;
}


/// returns true if this Peptide is observed in the given group
bool 
Peptide::isObservedIn(const msRunHashGroup & group) const {
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  for (itobslist = _observed_in_list.begin(); 
       itobslist != _observed_in_list.end(); 
       ++itobslist) {
    if (group.containsMsRun((*itobslist)->getPmsRun())) {
      return (true);
    }
  }
  return (false);
}

/// returns true if this Peptide is observed in the given msRun
bool 
Peptide::isObservedIn(const Msrun * msrun) const {
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  for (itobslist = _observed_in_list.begin(); 
       itobslist != _observed_in_list.end(); 
       ++itobslist) {
    if ((*itobslist)->getPmsRun() == msrun) {
      return (true);
    }
  }
  return (false);
}

/// returns true if this Peptide is observed in the given msRun ID
bool
Peptide::isObservedIn(const QString & msRunID) const {
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  for (itobslist = _observed_in_list.begin();
       itobslist != _observed_in_list.end();
       ++itobslist) {
    if ((*itobslist)->getPmsRun()->getXmlId() == msRunID) {
      return (true);
    }
  }
  return (false);
}


/**
   calculates the best rt (the one corresponding to the biggest intensity) 
   really observed in the given p_msrun for this peptide. If the peptide 
   is not observed in p_msrun, returns -1   
*/
mcq_double 
Peptide::getObservedBestRtForMsRun(const Msrun * p_msrun, 
				   const msRunHashGroup & group) const {
  // If peptide not observed at all in msrun, bestRt stays at -1
  mcq_double bestRt(-1);
  mcq_double bestIntensity(-1);
  mcq_double tmpIntensity(-1);

  std::vector<PeptideObservedIn *> * observed_in_group = 
    getObservedInGroup(group);
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  for (itobslist = observed_in_group->begin(); 
       itobslist != observed_in_group->end(); 
       ++itobslist) 
    { 
      if ( (*itobslist)->getPmsRun() == p_msrun ) {
	tmpIntensity = (*itobslist)->getIntensity();
	if (tmpIntensity >= bestIntensity)
	  {
	    bestIntensity = tmpIntensity; 
	    bestRt = (*itobslist)->getAlignedRt();
	  }
      }
    }
   delete observed_in_group;
  return bestRt;
}

/**
   calculates the mean of the observed best rt-s of this peptide
   overall msruns of the given group.
   If the peptide is not observed in one of the msruns of the group, 
   (so bestRt of this msrun is -1) it is not considered.   
*/

mcq_double 
Peptide::getMeanBestRt(const msRunHashGroup & group) const {
  // stock all the msrun_ids this peptide is observed in in a set 
  std::set<const Msrun *> s_msrun;
  std::vector<PeptideObservedIn *> * observed_in_group = 
    getObservedInGroup(group);
  std::vector<PeptideObservedIn *>::const_iterator itobslist;  
  
  for (itobslist = observed_in_group->begin(); 
       itobslist != observed_in_group->end(); 
       ++itobslist) 
    { 
      const Msrun *  c_msrun =  (*itobslist)->getPmsRun();
      s_msrun.insert(c_msrun);
    }
  
  // for each msrunId in s_msrunId getObservedBestRtForMsRun(msrunId)
  // and put them in a hash map msrunId -> bestRt
  std::map<QString, mcq_double> map_msrunId_bestRt;
  std::set<const Msrun *>::const_iterator itset;
  
  for (itset = s_msrun.begin(); 
       itset != s_msrun.end(); 
       ++itset) 
    { 
      const QString msrunId = (*itset)->getXmlId();
      mcq_double bestRt = getObservedBestRtForMsRun(*itset, group);
      map_msrunId_bestRt[msrunId] = bestRt;
    }  
  
  // calculate the mean of the bestRt-s in this hash map  
  mcq_double sumRt(0);
  mcq_double nbRt(0);
  
  std::map<QString, mcq_double>::iterator itmap;
  for (itmap = map_msrunId_bestRt.begin();
       itmap != map_msrunId_bestRt.end();
       ++itmap)
    {
      mcq_double bestRt = itmap->second;
      if (bestRt != -1) {
	sumRt += bestRt;
	nbRt++;
      }
    }
  mcq_double meanRt;
  if (nbRt != 0) {
    meanRt = sumRt / nbRt;
  } else {
    meanRt = -1;
  }
  delete observed_in_group;
  return meanRt;
}

///gets the list of the charges this Peptide has been observed in a group
// Attention : returns a pointer to a new set. When this function is called,
// be careful to delete the set. 
std::set<unsigned int> *
Peptide::getCharges(const msRunHashGroup & group) const {
  std::set<unsigned int> * s_charges = new std::set<unsigned int>;  
  std::vector<PeptideObservedIn *> * observed_in_group = 
    getObservedInGroup(group);
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  
  for (itobslist = observed_in_group->begin(); 
       itobslist != observed_in_group->end(); 
       ++itobslist) 
    {
      unsigned int z = (*itobslist)->getZ();
      s_charges->insert(z);
    }
  
  delete (observed_in_group);
  return s_charges;
}
