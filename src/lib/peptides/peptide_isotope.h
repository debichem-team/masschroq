/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file peptide_isotope.h
 * \date February 12, 2010
 * \author Olivier Langella
 */

#ifndef PEPTIDE_ISOTOPE_H_
#define PEPTIDE_ISOTOPE_H_ 1

#include "peptide.h"
#include "isotope_label.h"

/**
 * \class PeptideIsotope
 * \brief Class representing an isotope of a peptide
 */

class PeptideIsotope : public Peptide {

 public:
  
  PeptideIsotope(const Peptide * p_peptide_original,
		 const IsotopeLabel * p_label);
  
  virtual ~PeptideIsotope();
  
  virtual const IsotopeLabel * getIsotopeLabel() const;
  
  virtual void observed_in(const Msrun * p_msrun, 
			   const int scan_num,
			   unsigned int z);
  
  virtual bool isObservedIn(const msRunHashGroup & group) const;
  virtual bool isObservedIn(const QString & msRunID) const;

  virtual const QString & getMods() const;
  
  virtual std::vector<PeptideObservedIn *> *
    getObservedInGroup(const msRunHashGroup & group) const;
  
  virtual std::set<unsigned int> *
    getCharges(const msRunHashGroup & group) const;
  
  virtual mcq_double getObservedBestRtForMsRun(const Msrun * p_msrun,
					  const msRunHashGroup & group) const;
  
  virtual mcq_double getMeanBestRt(const msRunHashGroup & group) const;
  
  virtual const std::vector<const Protein *> & getProteinList() const;
  
  virtual void printInfos(QTextStream & out) const;
  
 private:
  
  const Peptide * _p_peptide_original;
  
  const IsotopeLabel * _p_label;
  
};

#endif /* PEPTIDE_ISOTOPE_H_ */
