/**
 * \file peptide_observed_in.c xpp
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#include "peptide_observed_in.h"
#include "peptide.h"
#include "../mcq_error.h"

PeptideObservedIn::PeptideObservedIn(const Peptide * p_peptide,
				     const Msrun * p_msrun, 
				     const int scan_num,
				     unsigned int z) 
  :
  _p_peptide(p_peptide), 
  _p_msrun(p_msrun), 
  _scan_num(scan_num), 
  _z(z)
{ 
	_rt = -1;
	//_p_msrun->get_precursor_rt(scan_num)
}

PeptideObservedIn::~PeptideObservedIn() {
}

/// get the intensity of this observation by the scan_number 
/// (the Precursor object with this scan number has it) 
mcq_double 
PeptideObservedIn::getIntensity() const {
    return (_p_msrun->getPrecursorIntensity(_scan_num));
}

mcq_double 
PeptideObservedIn::getAlignedRt()  {
	if(_rt==-1){
		_rt = _p_msrun->get_precursor_rt(_scan_num);
	}
  return (_p_msrun->getAlignedRtByOriginalRt(_rt));
}
