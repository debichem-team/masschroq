/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file isotope_label.h
 * \date 12 févr. 2010
 * \author Olivier langella
 */

#ifndef ISOTOPE_LABEL_H_
#define ISOTOPE_LABEL_H_ 1

#include <vector>
#include "../../config.h"
#include "QString"

/**
 * \class IsotopeLabelModification
 * \brief Class representing the label modifications of an isotopo

 */

class IsotopeLabelModification {
 
 public:
  
  IsotopeLabelModification();
  virtual ~IsotopeLabelModification();
  
  void setAA(const QString & aa);
  
  const QString & getAA() const {
    return (_at);
  }
	
  void setMass(mcq_double mass) {
    _mass = mass;
  }

  mcq_double getMass() const {
    return (_mass);
  }
  
 private:
  
  mcq_double _mass;
  QString _at;
};

/**
 * \class IsotopeLabel
 * \brief Class representing an isotope

 */
class IsotopeLabel {

 public:
	
  IsotopeLabel();
  virtual ~IsotopeLabel();
  
  void setXmlId(const QString & id);
  const QString & getXmlId() const;
  
  void addIsotopeLabelModification(const IsotopeLabelModification * p_mod);
  
  mcq_double getMassDelta(const QString & sequence) const;

 private:
  
  QString _id;
  
  std::vector<const IsotopeLabelModification *> _v_p_mod;
};

#endif /* ISOTOPE_LABEL_H_ */
