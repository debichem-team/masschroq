/**
 * \file isotope_label.cpp
 * \date 12 févr. 2010
 * \author Olivier langella
 */

#include "isotope_label.h"

#include <QRegExp>
#include "../mcq_error.h"

IsotopeLabelModification::IsotopeLabelModification() {
}

IsotopeLabelModification::~IsotopeLabelModification() {
}

void 
IsotopeLabelModification::setAA(const QString & aa) {
  //[A,R,N,D,C,E,Q,G,H,I,L,K,M,F,P,S,T,W,Y,V]
  //Nter, Cter
  QRegExp match_aa("^[A,R,N,D,C,E,Q,G,H,I,L,K,M,F,P,S,T,W,Y,V]|Nter|Cter$");
  
  if (!aa.contains(match_aa)) {
    throw mcqError(QObject::tr("ERROR in IsotopeLabelModification::setAA :\n %1 is not a valid amino acid or Nter, Cter position").arg(aa));
  }
  _at = aa;
}


IsotopeLabel::IsotopeLabel() {
}

IsotopeLabel::~IsotopeLabel() {
  std::vector<const IsotopeLabelModification *>::const_iterator itmod;
  for (itmod = _v_p_mod.begin(); itmod != _v_p_mod.end(); ++itmod) {
    delete (*itmod);
  }
}

void 
IsotopeLabel::setXmlId(const QString & id) {
  _id = id;
}

const QString & 
IsotopeLabel::getXmlId() const {
  return (_id);
}

mcq_double 
IsotopeLabel::getMassDelta(const QString & sequence) const {
  mcq_double mass = 0;
  std::vector<const IsotopeLabelModification *>::const_iterator itmod;
  for (itmod = _v_p_mod.begin(); 
       itmod != _v_p_mod.end(); 
       ++itmod) 
    {
      if ((*itmod)->getAA() == "Cter") {
	mass += (*itmod)->getMass();
      }
      if ((*itmod)->getAA() == "Nter") {
	mass += (*itmod)->getMass();
      } else {
	mass += (((*itmod)->getMass()) * sequence.count((*itmod)->getAA()));
      }
    }
  return (mass);
}

void 
IsotopeLabel::addIsotopeLabelModification(const IsotopeLabelModification * p_mod) {
  _v_p_mod.push_back(p_mod);
}
