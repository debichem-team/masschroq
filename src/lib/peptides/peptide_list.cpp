/**
 * \file peptide_list.cpp
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#include "peptide_list.h"

PeptideList::PeptideList() {
  // TODO Auto-generated constructor stub
}

PeptideList::~PeptideList() {
}


/// delete peptides and clear container
void 
PeptideList::free() {
  std::vector<const Peptide *>::const_iterator it;
  for (it = this->begin(); it != this->end(); ++it) {
    delete (*it);
  }
  this->clear();
}

/// given e peptide id, try to get it from the PeptideList if it is in,
/// return a NULL pointer otherwise
const Peptide * 
PeptideList::getPeptide(const QString & pepid) const {
  std::vector<const Peptide *>::const_iterator it;
  for (it = this->begin(); it != this->end(); ++it) {
    if ( (*it)->getXmlId() == pepid )
      return *it;
  }
  return (NULL);
}

/// tests if the PeptideList contains a given peptide or not
bool 
PeptideList::containsPeptide( const Peptide * pep) const {
  std::vector<const Peptide *>::const_iterator it;
  for (it = this->begin(); it != this->end(); ++it) {
    if ( *it == pep )
      return true;
  }
  return false;
}

/// creates a new PeptideList containing only the peptides observed in the
/// given msrun group 
PeptideList * 
PeptideList::newPeptideListObservedInMsGroup(const msRunHashGroup & group) const {
  PeptideList * p_list_pep = new PeptideList();
  std::vector<const Peptide *>::const_iterator itpeplist;
  for (itpeplist = this->begin(); 
       itpeplist != this->end(); 
       ++itpeplist) 
    {
      if ((*itpeplist)->isObservedIn(group)) {
	p_list_pep->push_back(*itpeplist);
      }
    }
  return (p_list_pep);
}

/// creates a new PeptideList containing only the peptides observed in the
/// given msrun
PeptideList * 
PeptideList::newPeptideListObservedInMsrun(const Msrun * msrun) const {
  
  PeptideList * p_list_pep = new PeptideList();
  std::vector<const Peptide *>::const_iterator itpeplist;
  for (itpeplist = this->begin(); 
       itpeplist != this->end(); 
       ++itpeplist) 
    {
      if ( (*itpeplist)->isObservedIn(msrun) ) {
	p_list_pep->push_back(*itpeplist);
      }
    }
  return (p_list_pep);
}
