/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file protein.h
 * \date November 10, 2009
 * \author Olivier Langella
 */

#ifndef PROTEIN_H_
#define PROTEIN_H_ 1

#include <QString>

/**
   \class Protein
   \brief Represents a protein

   Each protein has : 
   - an unique identifier (_protein_xml_id)
   - a description (QString) of its functions
*/
class Protein {

 public:
  
  Protein(const QString & protein_xml_id);
  virtual ~Protein();
  
  const QString & getXmlId() const {
    return _protein_xml_id;
  }
  
  void setDescription(const QString & description);
  
  const QString & getDescription() const {
    return _description;
  }
  
 private:
  
  const QString _protein_xml_id;
  QString _description;
  
};

#endif /* PROTEIN_H_ */
