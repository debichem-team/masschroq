/**
 * \file protein.cpp
 * \date 10 nov. 2009
 * \author Olivier Langella
 */

#include "protein.h"

Protein::Protein(const QString & protein_xml_id) :
  _protein_xml_id(protein_xml_id) {
}

Protein::~Protein() {
}

void 
Protein::setDescription(const QString & description) {
  _description = description;
}
