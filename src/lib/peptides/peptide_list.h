/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file peptide_list.h
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#ifndef PEPTIDE_LIST_H_
#define PEPTIDE_LIST_H_ 1

#include "peptide.h"
#include "../msrun/ms_run_hash_group.h"
#include <vector>

/** 
 * \class PeptideList
 * \brief This class is a vector of Peptide pointers (inherits from std::vector)
 */

class PeptideList : public std::vector<const Peptide *> {
 public:
  
  PeptideList();
  virtual ~PeptideList();

  const Peptide * getPeptide(const QString & idname) const;

  bool containsPeptide( const Peptide * pep) const;

  void free();

  PeptideList * 
    newPeptideListObservedInMsGroup(const msRunHashGroup & group) const;
  
  PeptideList * newPeptideListObservedInMsrun(const Msrun * msrun) const;
  
};
#endif /* PEPTIDE_LIST_H_ */
