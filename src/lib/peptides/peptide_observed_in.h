/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file peptide_observed_in.h
 * \date 27 oct. 2009
 * \author: Olivier Langella
 */

#ifndef PEPTIDE_OBSERVED_IN_H_
#define PEPTIDE_OBSERVED_IN_H_ 1

#include "../../config.h"

class Msrun;

class Peptide;

/**
 * \class PeptideObservedIn
 * \brief A PeptideObservedIn object is associated to a Peptide object.
 * It represents one state of this Peptide observed in a particular msrun.
 * 
 * Object PeptideObservedIn is constructed when the 
 * <observed_in data="samp1" scan="531" z="2"/> tag of the parameter XML file
 * is parsed. Therefore it contains essentially all the information given in
 * this tag.
 */

class PeptideObservedIn {

 public:
  PeptideObservedIn(const Peptide * p_peptide, 
		    const Msrun * p_msrun,
		    const int scan_num, 
		    unsigned int z);
  virtual ~PeptideObservedIn();

  const Msrun * getPmsRun() const {
    return _p_msrun;
  }
  
  unsigned int getZ() const {
    return _z;
  }
  
  mcq_double getIntensity() const ;
  
  int getScan() const {
    return _scan_num;
  }
  
  mcq_double getAlignedRt();
  
 private:
  
  /// The Peptide parent
  const Peptide * _p_peptide;
  /// The msrun it has been observed in
  const Msrun * _p_msrun;
  ///The MS scan_number(mzxml) it has been observed in 
  const int _scan_num; 
  ///The charge it has been observed in  
  const unsigned int _z;
  ///The retention time it has been observed in
  mcq_double _rt;
};

#endif /* PEPTIDE_OBSERVED_IN_H_ */
