/**
 * \file peptide_isotope.cpp
 * \date February 12, 2010
 * \author Olivier Langella
 */

#include <QDebug>
#include "peptide_isotope.h"
#include "../mcq_error.h"

PeptideIsotope::PeptideIsotope(const Peptide * p_peptide_original,
		const IsotopeLabel * p_label) :
	Peptide(p_peptide_original->getXmlId()), _p_peptide_original(
			p_peptide_original), _p_label(p_label) {
	_sequence = p_peptide_original->getSequence();
	_mass = p_peptide_original->getMass() + p_label->getMassDelta(_sequence);
}

PeptideIsotope::~PeptideIsotope() {
}

const IsotopeLabel *
PeptideIsotope::getIsotopeLabel() const {
	return _p_label;
}

void PeptideIsotope::observed_in(const Msrun * p_msrun, const int scan_num,
		unsigned int z) {
	throw mcqError(
			QObject::tr(
					"ERROR in PeptideIsotope::observed_in :\nobserved_in cannot be used for isotope peptide"));
}

bool PeptideIsotope::isObservedIn(const msRunHashGroup & group) const {
	//	qDebug() << "PeptideIsotope::isObservedIn()"
	//		<< _p_peptide_original->isObservedIn(group);
	return (_p_peptide_original->isObservedIn(group));
}

/// returns true if this Peptide is observed in the given msRun ID
bool PeptideIsotope::isObservedIn(const QString & msRunID) const {
	return (_p_peptide_original->isObservedIn(msRunID));
}

const QString &
PeptideIsotope::getMods() const {
	return (_p_peptide_original->getMods());
}

std::vector<PeptideObservedIn *> *
PeptideIsotope::getObservedInGroup(const msRunHashGroup & group) const {
	return (_p_peptide_original->getObservedInGroup(group));
}

std::set<unsigned int> *
PeptideIsotope::getCharges(const msRunHashGroup & group) const {
	return (_p_peptide_original->getCharges(group));
}

mcq_double PeptideIsotope::getObservedBestRtForMsRun(const Msrun * p_msrun,
		const msRunHashGroup & group) const {
	return (_p_peptide_original->getObservedBestRtForMsRun(p_msrun, group));
}

mcq_double PeptideIsotope::getMeanBestRt(const msRunHashGroup & group) const {
	return (_p_peptide_original->getMeanBestRt(group));
}

const std::vector<const Protein *> &
PeptideIsotope::getProteinList() const {
	return (_p_peptide_original->getProteinList());
}

void PeptideIsotope::printInfos(QTextStream & out) const {
	out << "peptide isotope : " << getXmlId() << endl;
	out << "_sequence = " << _sequence << endl;
	out << "_mass = " << _mass << endl;
}
