/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file mzmlSimpleParser.h
 * \date November 15, 2010
 * \author Edlira Nano
 */

#ifndef MZML_SIMPLE_PARSER_H_
#define MZML_SIMPLE_PARSER_H_ 1

#include "xmlSimpleParser.h"

/**
 * \class MzmlSimpleParser
 * \brief XmlSimpleParser derived class for the simple SAX parsing (simple = no spectra parsing)
 * of the mzml file corresponding to a given msrun.
 */

class MzmlSimpleParser : public XmlSimpleParser {
  
 public :
  
  MzmlSimpleParser(Msrun * ms_run);
  virtual ~MzmlSimpleParser();
  
  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes);
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName);

 private : 

  bool startElement_spectrum(const QXmlAttributes &attributes);

  bool startElement_cvParam(const QXmlAttributes &attributes);
  
  bool startElement_precursorList(const QXmlAttributes &attributes);
  
  bool startElement_precursor(const QXmlAttributes &attributes);
  
  const int get_scan_number(const QString & spectrum_id) const;

   
};

#endif /* MZML_SIMPLE_HANDLER_H_*/
