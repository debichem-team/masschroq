/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file mzxmlToLmatParser.h
 * \date July 26, 2011
 * \author Edlira Nano
 */

#ifndef MZXML_TO_LMAT_PARSER_H_
#define MZXML_TO_LMAT_PARSER_H_ 1

#include "xmlToLmatParser.h"

/**
 * \class MzxmlToLmatParser
 * \brief XmlToLmatParser derived class that performs SAX parsing of an mzxml msrun
 * file corresponding and creates the corresponding lmat object. 
 */

class MzxmlToLmatParser: public XmlToLmatParser {

public:

	MzxmlToLmatParser(LMat *lmat, mcq_float mass_start, mcq_float mass_end,
			mcq_float precision);

	virtual ~MzxmlToLmatParser();

	virtual bool startElement(const QString & namespaceURI,
			const QString & localName, const QString & qName,
			const QXmlAttributes & attributes);

	virtual bool endElement(const QString & namespaceURI,
			const QString & localName, const QString & qName);

private:

	bool startElement_scan(const QXmlAttributes & attributes);

	bool startElement_peaks(const QXmlAttributes & attributes);

	/// number of encoded peaks
	unsigned int _peakscount;
	// true if peaks are compressed using zlib, false ohterwise
	bool _zlib_compression;
  // very usefull : important to make one more test in case of compression :
  // after decoding it from base64, it gives the expected length of compressed data (interesting to check !!!!)
	unsigned int _compressed_len;
	bool _is_bigendian;

};

#endif /*MZXML_TO_LMAT_PARSER_H_*/
