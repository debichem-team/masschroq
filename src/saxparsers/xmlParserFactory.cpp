/**
 * \file xmlParserFactory.cpp
 * \date May 31, 2011
 * \author Edlira Nano
 */

#include "xmlParserFactory.h"
#include "mzxmlSimpleParser.h"
#include "mzmlSimpleParser.h"
#include "mzxmlSpectrumParser.h"
#include "mzmlSpectrumParser.h"
#include "mzxmlToLmatParser.h"
#include "mzmlToLmatParser.h"
#include "../lib/mcq_error.h"

XmlParserFactory::XmlParserFactory() {
}

XmlParserFactory::~XmlParserFactory() {
}

XmlSimpleParser * 
XmlParserFactory::newXmlSimpleParser(const mcq_xml_format & xml_format, 
						       Msrun * msrun) const {

  XmlSimpleParser * ptr_on_parser;
  if (xml_format == MZXML) {
    ptr_on_parser = new MzxmlSimpleParser(msrun);
  } else if (xml_format == MZML) {
    ptr_on_parser = new MzmlSimpleParser(msrun);
  } else {
    throw mcqError(QObject::tr("error in XmlParserFactory: msrun xml file format can be '%1' or '%2', but not '%3'.").arg(MZXML, MZML, xml_format));
  }

  return (ptr_on_parser);
}


XmlSpectrumParser * 
XmlParserFactory::newXmlSpectrumParser(const mcq_xml_format & xml_format, 
				       Msrun * msrun) const {
  
  XmlSpectrumParser * ptr_on_parser;
  if (xml_format == MZXML) {
    ptr_on_parser = new MzxmlSpectrumParser(msrun);
  } else if (xml_format == MZML) {
    ptr_on_parser = new MzmlSpectrumParser(msrun);
  } else {
    throw mcqError(QObject::tr("error in XmlParserFactory: msrun xml file format can be '%1' or '%2', but not '%3'.").arg(MZXML, MZML, xml_format));
  }

  return (ptr_on_parser);
}

XmlToLmatParser * 
XmlParserFactory::newXmlToLmatParser(const mcq_xml_format & xml_format,
				     LMat *lmat,
				     mcq_float mass_start,
				     mcq_float mass_end, 
				     mcq_float precision) const {
  
  XmlToLmatParser * ptr_on_parser;
  if (xml_format == MZXML) {
    ptr_on_parser = new MzxmlToLmatParser(lmat, mass_start, mass_end, precision);
  } else if (xml_format == MZML) {
    ptr_on_parser = new MzmlToLmatParser(lmat, mass_start, mass_end, precision);
  } else {
    throw mcqError(QObject::tr("error in XmlParserFactory: msrun xml file format can be '%1' or '%2', but not '%3'.").arg(MZXML, MZML, xml_format));
  }

  return (ptr_on_parser);
}


