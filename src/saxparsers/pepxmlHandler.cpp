/**
 * \file pepxmlHandler.cpp
 * \date November 25, 2010
 * \author Edlira Nano
 */

#include "pepxmlHandler.h"

PepxmlHandler::PepxmlHandler() {
}

PepxmlHandler::~PepxmlHandler() {
}


bool 
PepxmlHandler::characters(const QString &str) {
  _currentText += str;
  return true;
}

bool 
PepxmlHandler::fatalError(const QXmlParseException &exception) {
  _errorStr = QObject::tr("Parse error at line %1, column %2:\n"
			  "%3").arg(exception.lineNumber()).arg(exception.columnNumber()) .arg(exception.message());
  return false;
}

QString 
PepxmlHandler::errorString() const {
  return _errorStr;
}

bool 
PepxmlHandler::startElement(const QString & /* namespaceURI */,
			    const QString & /* localName */, 
			    const QString &qname,
			    const QXmlAttributes &attributes) {
  
  bool is_ok = true;
  
  if (qname == "spectrum_query") {
    is_ok = ;
  }
  else


    

  _currentText.clear();
  return is_ok;
}


bool 
PepxmlHandler::endElement(const QString & /* namespaceURI */,
			const QString & /* localName */, 
			const QString &qname) {
  
  bool is_ok = true;
  
  if (qname == "") {
    is_ok = ;
  }
  else if 

  _currentText.clear();
  return is_ok;
}
