/**
 * \file xmlSpectrumParser.cpp
 * \date November 23, 2010
 * \author Edlira Nano
 */

#include "xmlSpectrumParser.h"

XmlSpectrumParser::XmlSpectrumParser(Msrun * msrun)  
  : _msrun(msrun)
{
}

XmlSpectrumParser::~XmlSpectrumParser() {
}

bool 
XmlSpectrumParser::characters(const QString & str) {
  _currentText += str;
  return true;
}

bool 
XmlSpectrumParser::fatalError(const QXmlParseException & exception) {
  _errorStr = QObject::tr("Parse error at line %1, column %2:\n"
			  "%3").arg(exception.lineNumber()).arg(exception.columnNumber()).arg(exception.message());
  return false;
}

QString 
XmlSpectrumParser::errorString() const {
  return _errorStr;
}

const Msrun * 
XmlSpectrumParser::getMsRun() const {
  return (_msrun);
}

bool 
XmlSpectrumParser::setSpectrum() const {
  _msrun->setSpectrum(_retention_time, _current_spectrum);
  return true;
}

bool 
XmlSpectrumParser::endDocument() {
  return true;
}
