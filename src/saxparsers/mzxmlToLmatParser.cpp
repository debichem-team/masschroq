/**
 * \file mzxmlToLmatParser.cpp
 * \date July, 26 2011
 * \author Edlira Nano
 */

#include "mzxmlToLmatParser.h"
#include "../encode/decodeBinary.h"
#include "../lib/consoleout.h"

//#include <iostream>

MzxmlToLmatParser::MzxmlToLmatParser(LMat *lmat, mcq_float mass_start,
		mcq_float mass_end, mcq_float precision) :
		XmlToLmatParser(lmat, mass_start, mass_end, precision) {
	_zlib_compression = false;
	_compressed_len = 0;
	_peakscount = 0;
}

MzxmlToLmatParser::~MzxmlToLmatParser() {
	_zlib_compression = false;
	_compressed_len = 0;
	_peakscount = 0;
}

bool MzxmlToLmatParser::startElement(const QString & /* namespaceURI */,
		const QString & /* localName */, const QString & qName,
		const QXmlAttributes & attributes) {

	bool is_ok = true;
	if (qName == "scan") {
		is_ok = startElement_scan(attributes);
	}

	else if (qName == "peaks" && _ms_level == 1) {
		is_ok = startElement_peaks(attributes);
	}

	_currentText.clear();
	return is_ok;
}

bool MzxmlToLmatParser::endElement(const QString & /* namespaceURI */,
		const QString & /* localName */, const QString & qName)
{

	if (qName == "peaks" && _ms_level == 1) {
		
		if (_scan_type == "Full" && _peakscount != 0) {
			//qDebug() << "Starting decode" << endl;
			//QTime tdecode;
			//tdecode.start();
			const QByteArray encoded_data = _currentText.toAscii();

			std::vector<mcq_double> decode = DecodeBinary::base64_decode_peaks(
					encoded_data, _current_binary_precision, _is_bigendian,
					this->_peakscount, this->_zlib_compression,
					this->_compressed_len);
			//qDebug("Decode time is %d ms", tdecode.elapsed());

			// verify that _peakscount is the same as the size of the decoded
			// peaks vector
			unsigned int decoded_pairs = decode.size() / 2;
			if (_peakscount == decoded_pairs) {
				return (this->setSpectrum(decode));
			} else {
				// skip this scan
				mcqout() << "mzXmlHandlerBase : problem in scan number "
						<< _current_scan_number << " attribute peaksCount is "
						<< _peakscount << ", but we decoded " << decoded_pairs
						<< " peaks. Skipping scan.\n";
			}
		} else {
			// skip this scan
			qDebug() << "scan number " << _current_scan_number
					<< " is not a 'Full' scan type, so it is not handled;"
					<< " or peakscount = 0 so we skipped the scan";
		}
	}
	return true;
}

bool MzxmlToLmatParser::startElement_scan(const QXmlAttributes & attributes) {

	QString ms_level = attributes.value("msLevel");
	if (ms_level.isEmpty()) {
		_errorStr = QObject::tr(
				"the msLevel attribute in mzXML file must not be empty\n");
		return false;
	}
	_ms_level = ms_level.toInt();

	if (_ms_level == 1) {

		_current_scan_number = attributes.value("num").toInt();

		QString temp = attributes.value("retentionTime").mid(2,
				(attributes.value("retentionTime").size() - 3));
		_retention_time = temp.toDouble();

		QString peaks_count = attributes.value("peaksCount");
		if (peaks_count.isEmpty()) {
			_errorStr =
					QObject::tr(
							"the peaksCount attribute in mzXML file must not be empty\n");
			return false;
		}
		_peakscount = peaks_count.toInt();

		QString lowMz = attributes.value("lowMz");
		QString highMz = attributes.value("highMz");

		if (lowMz.isEmpty() || highMz.isEmpty()) {
			_scan_type = "Full";
		} else {
			mcq_double low_mz = lowMz.toDouble();
			mcq_double high_mz = highMz.toDouble();

			if ((high_mz - low_mz) > 100)
				_scan_type = "Full";
			else
				_scan_type = "Zoom";
		}
	}
	return true;
}

bool MzxmlToLmatParser::startElement_peaks(const QXmlAttributes & attributes) {
	QString precision = attributes.value("precision");
	if (precision.isEmpty())
		_current_binary_precision = 32;
	else
		_current_binary_precision = precision.toInt();

	//byteOrder="network"
	_is_bigendian = true;
	QString byteOrder = attributes.value("byteOrder");
	if (byteOrder.isEmpty()) {
		_is_bigendian = true;
	} else {
		if (byteOrder != "network") {
			_is_bigendian = false;
		}
	}
	//compressionType="zlib"
	QString compressionType = attributes.value("compressionType");
	if ( compressionType.isEmpty() || 
	     compressionType != "zlib") {
		_zlib_compression = false;
	} else {
		_zlib_compression = true;
	}
	//    compressedLen="339"
	QString compressedLen = attributes.value("compressedLen");
	if (compressedLen.isEmpty()) {
		this->_compressed_len = 0;
	} else {
		_compressed_len = compressedLen.toInt();
	}

	return(true);
}

