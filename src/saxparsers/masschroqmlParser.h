/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file masschroqmlParser.h
 * \date September 21, 2009
 * \author Olivier Langella
 */

#ifndef MASSCHROQML_PARSER_H_
#define MASSCHROQML_PARSER_H_ 1

#include <QXmlDefaultHandler>
#include <QString>
#include <vector>

#include "../lib/mass_chroq.h"
#include "../lib/alignments/alignment_base.h"
#include "../lib/xicExtractionMethods/xicExtractionMethodBase.h"
#include "../lib/detections/peak_detection_base.h"
#include "../lib/peptides/peptide.h"
#include "../lib/peptides/isotope_label.h"
#include "../lib/monitors/monitorList.h"
#include "../lib/monitors/tsv_mzrt_xics.h"

//using namespace std;

/**
 * \class MasschroqmlParser
 * \brief Sax parser for the parsing of the masschroqML (XML format) input file to masschroq.
 */

class MasschroqmlParser : public QXmlDefaultHandler {
 
 public:

  MasschroqmlParser(MassChroq * p_my_chroq);
  
  virtual ~MasschroqmlParser();

  bool startElement(const QString &namespaceURI, const QString &localName,
		    const QString &qName, const QXmlAttributes &attributes);
  
  bool endElement(const QString &namespaceURI, const QString &localName,
		  const QString &qName);
  
  bool endDocument();
  
  bool characters(const QString &str);
 
  bool fatalError(const QXmlParseException &exception);

  QString errorString() const;

 protected:

  /// <rawdata time_values_dir=.../>
  bool startElement_rawdata(const QXmlAttributes &attributes);
  
  /// <data_file id=.../>
  bool startElement_data_file(const QXmlAttributes &attributes);

  /// <group data_ids="delumeau1 delumeau2" id="G1"></group>
  bool startElement_group(const QXmlAttributes &attributes);

  /// <protein id="P1" desc=""/>
  bool startElement_protein(const QXmlAttributes &attributes);

  /// <peptide id="pep1" seq="" mh="856.23" prot_ids="P1 P2">
  bool startElement_peptide(const QXmlAttributes &attributes);

  /// <observed_in data="delumeau1" rt="645" z="2">
  bool startElement_observed_in(const QXmlAttributes &attributes);

  /// isotope_label
  bool startElement_isotope_label(const QXmlAttributes &attributes);

  /// isotope_label modification
  bool startElement_mod(const QXmlAttributes &attributes);

  /// <alignment_method id="obiwarp1"/"ms2_1">
  bool startElement_alignment_method(const QXmlAttributes &attributes);
  bool startElement_obiwarp(const QXmlAttributes &attributes);
  bool startElement_ms2(const QXmlAttributes &attributes);

  /// <align group_id="G1" method_id="obiwarp1">
  bool startElement_align(const QXmlAttributes &attributes);

  /// <quantification_method id="q1">
  bool startElement_quantification_method(const QXmlAttributes &attributes);

  /// <xic_extraction xic_type="sum">
  bool startElement_xic_extraction(const QXmlAttributes &attributes);

  /// <mz_range min="0.5" max="1.5"/>
  bool startElement_mz_range(const QXmlAttributes &attributes);
  
  /// <ppm_range min="" max=""/>
  bool startElement_ppm_range(const QXmlAttributes &attributes);

   /// <anti_spike half="8">
  bool startElement_anti_spike(const QXmlAttributes &attributes);

  /// <background>
  bool startElement_background(const QXmlAttributes &attributes);

  /// <smoothing half="5"></smoothing>
  bool startElement_smoothing(const QXmlAttributes &attributes);

  /// <detection_moulon>
  bool startElement_detection_moulon(const QXmlAttributes &attributes);
  
  /// <detection_zivy>
  bool startElement_detection_zivy(const QXmlAttributes &attributes);
 
  /// <quantification_results>
  bool startElement_quantification_results(const QXmlAttributes &attributes);
  
  /// <quantification_result output_file="bla" format="X">
  bool startElement_quantification_result(const QXmlAttributes &attributes);
  
  /// <quantification_result output_file="bla" format="xhtmltable">
  bool startXhtmltableResult(const QString output_file);

  /// <quantification_result output_file="bla" format="tsv">
  bool startTsvResult(const QString output_file);
  
  /// <quantification_result output_file="bla" format="gnumeric">
  bool startGnumericResult(const QString output_file);

  /// <quantification_result output_file="bla" format="xml">
  bool startXmlResult(const QString output_file, const bool with_traces);

  /// <all_xics_traces output_dir="xics_traces" format="tsv"/>
  bool startElement_all_xics_traces(const QXmlAttributes &attributes);
  
  /// <peptide_traces peptide_ids="pep0 pep6" output_dir="bla" format="tsv"/>
  bool startElement_peptide_traces(const QXmlAttributes &attributes);

  /// <mz_traces mz_values="449.754" output_dir="mzlist_xics_traces"/>
  bool startElement_mz_traces(const QXmlAttributes & attributes);
  
  /// <mzrt_traces output_dir="bla" format="tsv"/>  
  /// <mzrt_values>
  /// <mzrt_value mz="1" rt="2">
  /// <mzrt_value mz="3" rt="4">
  /// </mzrt_values>
  /// </mzrt_traces
  bool startElement_mzrt_traces(const QXmlAttributes & attributes);
  bool startElement_mzrt_value(const QXmlAttributes & attributes);

  /// <quantify withingroup="G1" detection_method_id="q1">
  bool startElement_quantify(const QXmlAttributes &attributes);
  
  /// <peptides_in_peptide_list>
  bool startElement_peptides_in_peptide_list(const QXmlAttributes &attributes);

  /// <mzrt mz="354.45" rt="7549.25"/>
  bool startElement_mzrt(const QXmlAttributes &attributes);
  
  
  /// </peptide_list>
  bool endElement_peptide_list();
  
  /// </isotope_label>
  bool endElement_isotope_label();
  
  /* <alignment_method id="obiwarp1">
     <obiwarp>
     <lmat_precision>1</lmat_precision>
     <mz_start>500</mz_start>
     <mz_stop>1200</mz_stop>
     </alignment_method>
  */
  bool endElement_lmat_precision();
  bool endElement_mz_start();
  bool endElement_mz_stop();

  /* <alignment_method id="ms2">
     <ms2>
     <ms2_tendency_halfwindow>5</ms2_tendency_halfwindow>
     <ms2_smoothing_halfwindow>15</ms2_smoothing_halfwindow>
     <ms1_smoothing_halfwindow>10</ms1_smoothing_halfwindow>
     </alignment_method>
  */
  bool endElement_ms2_tendency_halfwindow();
  bool endElement_ms2_smoothing_halfwindow();
  bool endElement_ms1_smoothing_halfwindow();

  /// </alignment_method>
  bool endElement_alignment_method();
     
  /// <smoothing_point>3</smoothing_point>
  /// <TIC_start>30000</TIC_start>  
  /// <TIC_stop>15000</TIC_stop>
  /// </detection_moulon>
  bool endElement_smoothing_point();
  bool endElement_TIC_start();
  bool endElement_TIC_stop();
  
  /// </detection_zivy>
  bool endElement_mean_filter_half_edge();
  bool endElement_minmax_half_edge();
  bool endElement_maxmin_half_edge();
  bool endElement_detection_threshold_on_max();
  bool endElement_detection_threshold_on_min();
  
  /// </peak_detection>
  bool endElement_peak_detection();

  bool endElement_quantification_method();

  /// </mzrt_traces>
  bool endElement_mzrt_traces();
  
  /// </quantification_traces>
  bool endElement_quantification_traces();

  /// <mz_list>45.154 56.15465 0354.45</mass_list>
  bool endElement_mz_list();

  /// </quantify>
  bool endElement_quantify();
  
  /// </quantification>
  bool endElement_quantification();
   
  /// </masschroq>
  bool endElement_masschroq();


 private:
  
  /// the error description variable
  QString _errorStr;
  
  QString _currentText;
  
  std::vector<QString> _tag_stack;

  Peptide * _p_current_peptide;
  IsotopeLabel * _p_isotope_label;
  
  MassChroq * _p_my_chroq;
  AlignmentBase * _p_current_alignment_method;
  QuantificationMethod * _p_current_quantification_method;
  PeakDetectionBase * _p_current_detection_method;
  MonitorList * _p_current_monitor_list;
  TsvMzRtXics * _p_current_mzrt_traces;
  QString _current_alignment_method_id;
  QString _current_quantify_id;
  bool _read_time_values;
  QString _time_directory;

};

#endif /* MASSCHROQML_PARSER_H_ */
