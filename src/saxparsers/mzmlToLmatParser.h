/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/**
 * \file mzmlToLmatParser.h
 * \date July 26, 2011
 * \author Edlira Nano
 */

#ifndef MZML_TO_LMAT_PARSER_H_
#define MZML_TO_LMAT_PARSER_H_ 1

#include "xmlToLmatParser.h"

/**
 * \class MzmlToLmatParser
 * \brief XmlToLmatParser derived class that performs SAX parsing of an mzml msrun
 * file and sets the lmat object from it. Used for the obiwarp alignment. 
 */

class MzmlToLmatParser : public XmlToLmatParser {
  
 public :
  
  MzmlToLmatParser(LMat *lmat, 
		   mcq_float mass_start,
		   mcq_float mass_end, 
		   mcq_float precision);
  
  virtual ~MzmlToLmatParser();
  
  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes);
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName);
 
 private : 
  
  bool startElement_spectrum(const QXmlAttributes &attributes);
  
  bool startElement_cvParam(const QXmlAttributes &attributes);
  
  bool endElement_binary();
  
  bool endElement_spectrum();

  /// current binary's type : containing mz or intensity data
  QString _current_binary_type;

  /// number of encoded peaks (defaultArrayLength attribute)
  unsigned int _peakscount;
  
    /// true if zlib compression of binary, false if no compression 
  bool _current_zlib_compression;
  
  /// key-names for the type of binaries (mz, intensity or retention time
  /// binary)
  const QString MZ_BINARY;
  const QString INTENSITY_BINARY;
  
  // vectors containing the decoded data
  std::vector<mcq_double> _current_decoded_mz;
  std::vector<mcq_double> _current_decoded_int;
};

#endif /* MZML_TO_LMAT_PARSER_H_*/
