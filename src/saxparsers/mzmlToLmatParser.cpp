/**
 * \file mzmlToLmatParser.cpp
 * \date July 26, 2011
 * \author Edlira Nano
 */

#include "mzmlToLmatParser.h"
#include "../lib/mcq_error.h"
#include "../encode/decodeBinary.h"

MzmlToLmatParser::MzmlToLmatParser(LMat *lmat, 
				   mcq_float mass_start,
				   mcq_float mass_end, 
				   mcq_float precision)
  : 
  XmlToLmatParser(lmat, mass_start, mass_end, precision),
  MZ_BINARY("MZ"),
  INTENSITY_BINARY("INT")
{
	_current_zlib_compression = false;
  _peakscount = 0;
}

MzmlToLmatParser::~MzmlToLmatParser()
{
	_current_zlib_compression = false;
  _peakscount = 0;
}

bool 
MzmlToLmatParser::startElement(const QString & /* namespaceURI */,
				 const QString & /* localName */, 
				 const QString & qname,
				 const QXmlAttributes & attributes) {
  
  bool is_ok = true;
  if (qname == "spectrum") {
	  is_ok = startElement_spectrum(attributes);  
	}
  else if (qname == "cvParam") {
    is_ok = startElement_cvParam(attributes);  
  }
  _currentText.clear();
  return is_ok;
}

bool 
MzmlToLmatParser::endElement(const QString & /* namespaceURI */,
			       const QString & /* localName */, 
			       const QString & qname) {
  
  bool is_ok = true;
  
  if (qname == "binary" && _ms_level == 1) {
    is_ok = endElement_binary();
  }
  else if (qname == "spectrum" && _ms_level == 1) {
    is_ok = endElement_spectrum();  
  }  
  _currentText.clear();
  return is_ok;
}  

bool
MzmlToLmatParser::startElement_spectrum(const QXmlAttributes &attributes) 
{
  QString peakscount = attributes.value("defaultArrayLength");
  if (peakscount.isEmpty()) {
    _errorStr = QObject::tr("The spectrum defaultArrayLength attribute must not be empty\n");
    return false;
  }
  _peakscount = peakscount.toInt();
  return true;
}

bool
MzmlToLmatParser::startElement_cvParam(const QXmlAttributes & attributes) {

  const QString ms_level_key("MS:1000511"); 
  const QString rt_key("MS:1000016");
  const QString rt_in_minutes_key("UO:0000031");
  const QString rt_in_secs_key("UO:0000010");
  const QString total_ion_current("MS:1000285");
  const QString mz_binary_key("MS:1000514");
  const QString intensity_binary_key("MS:1000515"); 
  const QString binary_32bit_mcq_double_key("MS:1000521");
  const QString binary_64bit_mcq_double_key("MS:1000523");
  const QString binary_nocompression_key("MS:1000576");
  const QString binary_zlibcompression_key("MS:1000574");
  
  QString ref = attributes.value("cvRef");
  QString accession = attributes.value("accession");

  // get ms_level
  
  if (accession.contains(ms_level_key)) {
    _ms_level = (attributes.value("value")).toInt();
  }
  
  if (_ms_level == 1) {
    
    if (accession.contains(rt_key)) {
      _retention_time = (attributes.value("value")).toDouble();
      QString unitAccession = attributes.value("unitAccession");
      if (unitAccession.contains(rt_in_minutes_key)) {
	_retention_time *= 60.;
      }
    }
    
    if (accession.contains(binary_64bit_mcq_double_key)) {
      _current_binary_precision = 64;
    }
    
    if (accession.contains(binary_32bit_mcq_double_key)) {
      _current_binary_precision = 32;
    }
    
    if (accession.contains(binary_nocompression_key)) {
      _current_zlib_compression = false;
    }
    
    if (accession.contains(binary_zlibcompression_key)) {
	  _current_zlib_compression = true;
    }
    
    if (accession.contains(mz_binary_key)) {
      _current_binary_type = MZ_BINARY;
    }
    
    if (accession.contains(intensity_binary_key)) {
      _current_binary_type = INTENSITY_BINARY;
    }
    
  }
  
  _currentText.clear();
  return true;
}

bool
MzmlToLmatParser::endElement_binary() {
 
  // decoding of binary data arrays 
  const QByteArray encoded_data = _currentText.toAscii();
  if (encoded_data.isEmpty()) {
    qDebug() << "binary empty";
    return true;
  }

  if (_current_binary_type == MZ_BINARY) {
    _current_decoded_mz = DecodeBinary::base64_decode_peaks_without_compressed_length_check(encoded_data,
							     _current_binary_precision, 
															false, _peakscount, _current_zlib_compression);
    return true;
  }
  
  else if (_current_binary_type == INTENSITY_BINARY) {
    
    _current_decoded_int = DecodeBinary::base64_decode_peaks_without_compressed_length_check(encoded_data,
							      _current_binary_precision, 
															 false, _peakscount, _current_zlib_compression);
    return true;
  }
  
  _currentText.clear();
  return true;
}

bool
MzmlToLmatParser::endElement_spectrum() {
  std::vector<mcq_double>::const_iterator it_mz, it_int;
  std::vector<mcq_double> decoded_data;
  unsigned int mz_size = _current_decoded_mz.size();
  unsigned int int_size = _current_decoded_int.size();
  if ( (mz_size ==  0) || (int_size == 0) || (mz_size != int_size) ) {
    _errorStr = QObject::tr("the decoded mz and intensity data are not of equal size or are of size 0\n");
    return false;
  }
  
  decoded_data.reserve(mz_size + int_size);
  for (it_mz = _current_decoded_mz.begin(), it_int = _current_decoded_int.begin();
       it_mz != _current_decoded_mz.end();
       ++it_mz, ++it_int) {
    decoded_data.push_back(*it_mz);
    decoded_data.push_back(*it_int);
  }
  _current_decoded_mz.clear();
  _current_decoded_mz.resize(0);
  _current_decoded_int.clear();
  _current_decoded_int.resize(0);
  return (this->setSpectrum(decoded_data));
}
