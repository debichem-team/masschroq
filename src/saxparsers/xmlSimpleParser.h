/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xmlSimpleParser.h
 * \date November 23, 2010
 * \author Edlira Nano
 */

#ifndef XML_SIMPLE_PARSER_H_
#define XML_SIMPLE_PARSER_H_ 1

#include <QXmlDefaultHandler>
#include "../lib/msrun/msrun.h"

/**
 * \class XmlSimpleParser
 * \brief Abstract base class for the simple SAX parsing (simple = no spectra decoding) 
 * of the xml LC-MS data files (mzxml or mzml).
 * 
 */

class XmlSimpleParser : public QXmlDefaultHandler {
  
 public :

  XmlSimpleParser(Msrun * msrun);
  virtual ~XmlSimpleParser();
  
  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes) = 0;
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName) = 0;
  
  virtual bool endDocument();

  bool characters(const QString &str);
  
  bool fatalError(const QXmlParseException &exception);
  
  QString errorString() const;

  const Msrun * getMsRun() const;
  
 protected :

   /**
    * virtual method that creates a Precursor object and sets its parameters
    from the mzxml/mzml file (one Precursor object per level >=2 scan).
   */
  virtual bool setPrecursor(const int scan_num, const mcq_double rt, 
			    const mcq_double intensity, const mcq_double mz);


  void set_low_mz();
  void set_high_mz();
  
  /// the msrun object corresponding to the unique run in this xml file
  Msrun * _msrun;

  /// current parsed text
  QString _currentText;
  
  /// error message during parsing
  QString _errorStr;
   
  /// current parsed retention time
  mcq_double _retention_time;
 
  /// current parsed scan number
  int _current_scan_number;

  /// current precursor's scan number (in mslevel >=2 only)
  int _precursor_scan_num;
  
  /// current precursor's rt (in mslevel >=2 only)
  mcq_double _precursor_rt;
  
  /// current precursor's mz (in mslevel >=2 only)
  mcq_double _precursor_mz;
  
  /// current precursor's intensity level (in mslevel = 2 only, 
  /// because for bigger levels, the precursor is the same)
  mcq_double _precursor_intensity_level2;

  mcq_double _tic_level2;

  /// type of scan("Full" or "Zoom")  
  QString _scan_type;
  
  /// current ms-level
  int _ms_level;
  
  mcq_double _low_mz;
  mcq_double _high_mz;
  
};
#endif /* XML_SIMPLE_PARSER_H_ */
