/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file mzxmlSpectrumParser.h
 * \date June 14, 2011
 * \author Edlira Nano
 */

#ifndef MZXML_SPECTRUM_PARSER_H_
#define MZXML_SPECTRUM_PARSER_H_ 1

#include "xmlSpectrumParser.h"

/**
 * \class mzxmlSpectrumParser
 * \brief XmlSpectrumParser derived class that performs SAX parsing of the spectra
 * in the mzXML file corresponding to a given msrun. 
 */

class MzxmlSpectrumParser : public XmlSpectrumParser {
 
 public:
  
  MzxmlSpectrumParser(Msrun * ms_run);
  virtual ~MzxmlSpectrumParser();
  
  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes);
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName);

 private:
  
  bool startElement_scan(const QXmlAttributes & attributes);
  
  bool startElement_peaks(const QXmlAttributes & attributes);
  
  /// number of encoded peaks
  unsigned int _peakscount;

  // true if peaks are compressed using zlib, false otherwise
  bool _zlib_compression ;
  // very usefull : important to make one more test in case of compression :
  // after decoding it from base64, it gives the expected length of compressed data (interesting to check !!!!)
  unsigned int _compressed_len ;
  bool _is_bigendian;

};

#endif /*MZXML_SPECTRUM_PARSER_H_*/
