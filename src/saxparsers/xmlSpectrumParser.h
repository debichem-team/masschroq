/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xmlSpectrumParser.h
 * \date June 14, 2011
 * \author Edlira Nano
 */

#ifndef XML_SPECTRUM_PARSER_H_
#define XML_SPECTRUM_PARSER_H_ 1

#include <QXmlDefaultHandler>
#include "../lib/msrun/msrun.h"

/**
 * \class XmlSpectrumParser
 * \brief Abstract base class for the SAX parsing of the spectra in the xml 
 * LC-MS data files (mzxml or mzml).
 * 
 */

class XmlSpectrumParser : public QXmlDefaultHandler {
  
 public :

  XmlSpectrumParser(Msrun * msrun);
  virtual ~XmlSpectrumParser();
  
  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes) = 0;
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName) = 0;
  
  virtual bool endDocument();

  bool characters(const QString &str);
  
  bool fatalError(const QXmlParseException &exception);
  
  QString errorString() const;

  const Msrun * getMsRun() const;
  
 protected :
 
  /// virtual method that sets the spectra after decoding 
  virtual bool setSpectrum() const;
  
  /// the msrun object corresponding to the unique run in this xml file
  Msrun * _msrun;

  /// current parsed text
  QString _currentText;
  
  /// error message during parsing
  QString _errorStr;
  
  /// current parsed retention time
  mcq_double _retention_time;
  
  /// current parsed scan number
  int _current_scan_number;

  mcq_double _low_mz;
  mcq_double _high_mz;
  
  /// type of scan("Full" or "Zoom")  
  QString _scan_type;
  
  /// current ms-level
  int _ms_level;
    
  /// 32-bit mcq_double or 64-bit mcq_double binary
  unsigned int _current_binary_precision;
  
  /// the current spectrum 
  spectrum * _current_spectrum;
    
};
#endif /* XML_SPECTRUM_PARSER_H_ */
