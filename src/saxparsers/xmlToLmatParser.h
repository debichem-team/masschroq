/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/** 
 * \file xmlToLmatParser.h
 * \date July 25, 2011
 * \author Edlira Nano
 */


#ifndef XML_TO_LMAT_PARSER_H_
#define XML_TO_LMAT_PARSER_H_ 1

#include <vector>
#include <map>
#include <QXmlDefaultHandler>
#include "../mcq_types.h"
#include "../libobiwarp/vec.h"
#include "../libobiwarp/mat.h"
#include "../libobiwarp/lmat.h"


/**
 * \class XmlToLmatParser
 * \brief This class parses mzxml/mzml files and sets LMat objects
 from it. Lmat objects are used by the obiwarp alignment.
 */

class XmlToLmatParser : public  QXmlDefaultHandler {
 
 public:
  
  XmlToLmatParser(LMat *lmat, 
		  mcq_float mass_start,
		  mcq_float mass_end, 
		  mcq_float precision);
  
  virtual ~XmlToLmatParser();

  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes) = 0;
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName) = 0;
  
  virtual bool endDocument();

  bool characters(const QString &str);
  
  bool fatalError(const QXmlParseException &exception);
  
  QString errorString() const;

 protected :
 
  /// current parsed text
  QString _currentText;
  
  /// error message during parsing
  QString _errorStr;
  
  /// current parsed retention time
  mcq_double _retention_time;
  
  /// current parsed scan number
  int _current_scan_number;

  /// type of scan("Full" or "Zoom")  
  QString _scan_type;
  
  /// current ms-level
  int _ms_level;
    
  /// 32-bit mcq_double or 64-bit mcq_double binary
  unsigned int _current_binary_precision;

  /// virtual method that sets the lmat after decoding 
  virtual bool setSpectrum(const std::vector<mcq_double> & decoded_data);
 
 private :
   
  LMat * _lmat;
  
  bool mzXmlTag;
  
  mcq_float _precision;
  
  std::map<int, mcq_float> _mass_integrator;
  
  std::map<mcq_float, std::vector<mcq_float> > _spectrum_map;

};

#endif /* XML_TO_LMAT_PARSER_H_ */
