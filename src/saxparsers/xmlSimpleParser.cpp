/**
 * \file xmlSimpleParser.cpp
 * \date November 23, 2010
 * \author Edlira Nano
 */

#include "xmlSimpleParser.h"

XmlSimpleParser::XmlSimpleParser(Msrun * msrun)  
  : _msrun(msrun)
{
}


XmlSimpleParser::~XmlSimpleParser() {
}

bool 
XmlSimpleParser::characters(const QString & str) {
  _currentText += str;
  return true;
}

bool 
XmlSimpleParser::fatalError(const QXmlParseException & exception) {
  _errorStr = QObject::tr("Parse error at line %1, column %2:\n"
			  "%3").arg(exception.lineNumber()).arg(exception.columnNumber()).arg(exception.message());
  return false;
}

QString 
XmlSimpleParser::errorString() const {
  return _errorStr;
}

const Msrun * 
XmlSimpleParser::getMsRun() const {
  return (_msrun);
}

bool 
XmlSimpleParser::setPrecursor(const int scan_num, const mcq_double rt, 
			       const mcq_double intensity, const mcq_double mz) {
  //qDebug() << "setPrecursor Xml begin : ";
  //qDebug() << "scan_num rt int mz:" << scan_num << "," 
  //	 << rt << "," << intensity << "," << mz;
  
  /// creating the new precursor object and setting its parameters
  Precursor * precursor = new Precursor(scan_num, rt, intensity, mz);
  
  /// adding this new precursor to the msrun's hash map of scan_num -> precursor
  _msrun->mapPrecursor(scan_num, precursor);
  return true;
}

void 
XmlSimpleParser::set_low_mz() {
  _msrun->set_low_mz(_low_mz);
}

void 
XmlSimpleParser::set_high_mz() {
  _msrun->set_high_mz(_high_mz);
}

bool 
XmlSimpleParser::endDocument() {
  return true;
}
