 /**
 * \file mzxmlSimpleParser.cpp
 * \date May, 22 2011
 * \author Olivier Langella, Edlira Nano
 */

#include "mzxmlSimpleParser.h"


MzxmlSimpleParser::MzxmlSimpleParser(Msrun * ms_run)  :
  XmlSimpleParser(ms_run)
{
  
}

MzxmlSimpleParser::~MzxmlSimpleParser() {
}


bool 
MzxmlSimpleParser::startElement(const QString & /* namespaceURI */,
				const QString & /* localName */, 
				const QString & qName,
				const QXmlAttributes & attributes) {
 
  bool is_ok = true;
  if (qName == "scan") {
    is_ok = startElement_scan(attributes);  
  }  
 
  else if (qName == "precursorMz") {
    is_ok = startElement_precursorMz(attributes);  
  }
  
  _currentText.clear();
  return is_ok;
}
 
bool
MzxmlSimpleParser::startElement_scan(const QXmlAttributes & attributes) {
  QString scan_number = attributes.value("num");
  QString ms_level = attributes.value("msLevel");
  
  if (scan_number.isEmpty()) {
    _errorStr = QObject::tr("the scan number attribute in mzXML file must not be empty\n");
    return false;
  }
  if (ms_level.isEmpty()) {
    _errorStr = QObject::tr("the msLevel attribute in mzXML file must not be empty\n");
    return false;
  }
  
  _current_scan_number = scan_number.toInt();
  _ms_level = ms_level.toInt(); 
  
  /// for all ms-levels
  QString temp = attributes.value("retentionTime").mid
    (2, (attributes.value("retentionTime").size() - 3));
  _retention_time = temp.toDouble();
  
  if ( _ms_level == 1 ) {
    QString lowMz = attributes.value("lowMz");
    QString highMz = attributes.value("highMz");
    if ( lowMz.isEmpty() || highMz.isEmpty() ) {
      _scan_type = "Full"; 
    } else {
      _low_mz = lowMz.toDouble();
      _high_mz = highMz.toDouble();
      this->set_low_mz();
      this->set_high_mz();
      if ( (_high_mz - _low_mz) > 100 )
	_scan_type = "Full";
      else
	_scan_type = "Zoom";
    }
  }
  
  // for ms_levels >= 2
  if ( _ms_level >= 2 ) {
    _precursor_scan_num = _current_scan_number;
    
    QString temp = attributes.value("retentionTime").mid
      (2, (attributes.value("retentionTime").size() - 3));
    _precursor_rt = temp.toDouble();
    
    if (_ms_level == 2)
      _tic_level2 = (attributes.value("totIonCurrent")).toDouble();
  }
  return true;
}

bool
MzxmlSimpleParser::startElement_precursorMz(const QXmlAttributes & attributes) {
  /// only for ms-level >= 2 
  /* in ms-levels >= 2 we have a new tag : precursorMz
     witch has an attribute precursorIntensity. This attribute is 
     set to 0 in ms-level >= 3 and in those levels 
     we have to take the one corresponding to the corresponding 
     ms-level 2*/
  if (_ms_level == 2) {
    QString prec_int = attributes.value("precursorIntensity");
    if (prec_int.isEmpty() ||
	prec_int.toDouble() == 0) {
      _precursor_intensity_level2 = _tic_level2;
    } else {
      _precursor_intensity_level2 = prec_int.toDouble();
    }
  }
  return true;
}

bool 
MzxmlSimpleParser::endElement(const QString & /* namespaceURI */,
			       const QString & /* localName */, 
			       const QString & qName) {
  bool is_ok = true;
  
  if (qName == "precursorMz") {
      _precursor_mz = _currentText.toDouble();
      is_ok = setPrecursor(_precursor_scan_num, _precursor_rt, 
			   _precursor_intensity_level2, _precursor_mz); 
  }
  else if (qName == "peaks" && _ms_level == 1 && _scan_type == "Full") {
    _msrun->addOriginalRetentionTime(_retention_time);
  }
  _currentText.clear();
  return is_ok;
}

