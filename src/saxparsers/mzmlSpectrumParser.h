/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file mzmlSpectrumParser.h
 * \date June 14, 2011
 * \author Edlira Nano
 */

#ifndef MZML_SPECTRUM_PARSER_H_
#define MZML_SPECTRUM_PARSER_H_ 1

#include "xmlSpectrumParser.h"

/**
 * \class MzmlSpectrumParser
 * \brief XmlSpectrumParser derived class that performs SAX parsing of the spectra
 * in the mzml file corresponding to a given msrun.
 */

class MzmlSpectrumParser : public XmlSpectrumParser {
  
 public :
  
  MzmlSpectrumParser(Msrun * ms_run);
  virtual ~MzmlSpectrumParser();
  
  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes);
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName);

  // virtual bool endDocument();
 
 private : 
  
  bool startElement_spectrum(const QXmlAttributes &attributes);

  bool startElement_cvParam(const QXmlAttributes &attributes);
  
  bool startElement_binaryDataArrayList(const QXmlAttributes &attributes);
  
  bool startElement_binaryDataArray(const QXmlAttributes &attributes);
  
  bool endElement_binary();
  
  bool endElement_spectrum();

  /// current binary's type : containing mz or intensity data
  QString _current_binary_type;
  
  unsigned int _current_binary_length;

  /// number of encoded peaks (defaultArrayLength attribute)
  unsigned int _peakscount;

  /// true if zlib compression of binary peaks, false otherwise
  bool _current_zlib_compression;

  /// key-names for the type of binaries (mz, intensity or retention time
  /// binary)
  const QString MZ_BINARY;
  const QString INTENSITY_BINARY;
  
};

#endif /* MZML_SPECTRUM_PARSER_H_*/
