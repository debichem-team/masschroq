/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file mzxmlSimpleParser.h
 * \date May 30, 2011
 * \author Olivier Langella, Edlira Nano
 */

#ifndef MZXML_SIMPLE_PARSER_H_
#define MZXML_SIMPLE_PARSER_H_ 1

#include "xmlSimpleParser.h"

/**
 * \class mzxmlSimpleParser
 * \brief XmlSimpleParser derived class that performs simple SAX parsing (no spectra decoding)
 *  of the mzXML file corresponding to a given msrun. 

 * An mzxml file is a suite of uniquely numbered scans, each corresponding
 * to a signal acquired by the MS at a retention time. 
 * The scans differ upon the MS-level they are taken: MS1 level 
 * gives us the spectra, MS2 and greater levels give us precise information 
 * about the peptides obtained by fragmenting a precursor.
 * The read precursor mode corresponds to MS acquisition levels >=2. 
 * In these levels the precursor's information are parsed and put
 * in a map of Precursors. Why? Because, a peptide acquired in MS levels >=2 can have several 
 * precursors durng the same run. For this peptide we will compute (later in MassChroQ) the
 * best retention time, i.e. the RT corresponding to the most intense precursor. 
 */

class MzxmlSimpleParser : public XmlSimpleParser {
 
 public:
  
  MzxmlSimpleParser(Msrun * ms_run);
  virtual ~MzxmlSimpleParser();

  virtual bool startElement(const QString & namespaceURI, 
			    const QString & localName,
			    const QString & qName, 
			    const QXmlAttributes & attributes);
  
  virtual bool endElement(const QString & namespaceURI,
			  const QString & localName,
			  const QString & qName);
  

 private:
  
  bool startElement_scan(const QXmlAttributes & attributes);
  
  bool startElement_precursorMz(const QXmlAttributes & attributes);
  
};

#endif /*MZXML_SIMPLE_PARSER_H_*/
