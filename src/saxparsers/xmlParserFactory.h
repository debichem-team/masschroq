/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file xmlParserFactory.h
 * \date May 31, 2011
 * \author Edlira Nano
 */

#ifndef XML_PARSER_FACTORY_H_
#define XML_PARSER_FACTORY_H_ 1

#include "xmlSimpleParser.h"
#include "xmlSpectrumParser.h"
#include "xmlToLmatParser.h"

class LMat;

/**
 * \class XmlParserFactory
 * \brief A class that, given an msrun and the type of the xml (mzxml or mzml) format of the 
 * file corresponding to this run, fabricates an XmlSimpleParser of the appropriate type to 
 * parse the msrun file.
 */
class XmlParserFactory {

 public:
  
  XmlParserFactory();
  virtual ~XmlParserFactory();

  XmlSimpleParser * newXmlSimpleParser(const mcq_xml_format & xml_format, 
				       Msrun * ms_run) const;

  XmlSpectrumParser * newXmlSpectrumParser(const mcq_xml_format & xml_format, 
					 Msrun * ms_run) const;
  

  XmlToLmatParser * newXmlToLmatParser(const mcq_xml_format & xml_format,
					 LMat *lmat,
					 mcq_float mass_start,
					 mcq_float mass_end, 
					 mcq_float precision) const;
};

#endif /* XML_PARSER_FACTORY_H_ */
