/**
 * \file mzmlSpectrumParser.cpp
 * \date June 15, 2011
 * \author Edlira Nano
 */

#include "mzmlSpectrumParser.h"
#include "../lib/mcq_error.h"
#include "../encode/decodeBinary.h"

MzmlSpectrumParser::MzmlSpectrumParser(Msrun * ms_run) 
		:
  XmlSpectrumParser(ms_run), 
  MZ_BINARY("MZ"), 
  INTENSITY_BINARY("INT") 
{
  _current_zlib_compression = false;
  _peakscount = 0;
  _current_binary_length = 0;
}

MzmlSpectrumParser::~MzmlSpectrumParser() 
{
  _current_zlib_compression = false;
  _peakscount = 0;
  _current_binary_length = 0;
}

bool 
MzmlSpectrumParser::startElement(const QString & /* namespaceURI */,
				 const QString & /* localName */, 
				 const QString & qname,
				 const QXmlAttributes & attributes) {
  
        bool is_ok = true;
	if (qname == "spectrum") {
	  is_ok = startElement_spectrum(attributes);  
	}
	else if (qname == "cvParam") {
	  is_ok = startElement_cvParam(attributes);
	} 
	else if (qname == "binaryDataArrayList" && _ms_level == 1) {
	  is_ok = startElement_binaryDataArrayList(attributes);
	} 
	else if (qname == "binaryDataArray" && _ms_level == 1) {
	  is_ok = startElement_binaryDataArray(attributes);
	}
	
	_currentText.clear();
	return is_ok;
}

bool MzmlSpectrumParser::endElement(const QString & /* namespaceURI */,
		const QString & /* localName */, const QString & qname) {

	bool is_ok = true;

	if (qname == "binary" && _ms_level == 1) {
		is_ok = endElement_binary();
	} else if (qname == "spectrum" && _ms_level == 1) {
		is_ok = endElement_spectrum();
	}
	_currentText.clear();
	return is_ok;
}

bool
MzmlSpectrumParser::startElement_spectrum(const QXmlAttributes &attributes) 
{
  QString peakscount = attributes.value("defaultArrayLength");
  if (peakscount.isEmpty()) {
    _errorStr = QObject::tr("The spectrum defaultArrayLength attribute must not be empty\n");
    return false;
  }
  _peakscount = peakscount.toInt();
  return true;
}

  
  
bool 
MzmlSpectrumParser::startElement_cvParam(const QXmlAttributes & attributes) {

	const QString ms_level_key("MS:1000511");
	const QString rt_key("MS:1000016");
	const QString rt_in_minutes_key("UO:0000031");
	const QString rt_in_secs_key("UO:0000010");
	const QString total_ion_current("MS:1000285");
	const QString mz_binary_key("MS:1000514");
	const QString intensity_binary_key("MS:1000515");
	const QString binary_32bit_mcq_double_key("MS:1000521");
	const QString binary_64bit_mcq_double_key("MS:1000523");
	const QString binary_nocompression_key("MS:1000576");
	const QString binary_zlibcompression_key("MS:1000574");

	QString ref = attributes.value("cvRef");
	QString accession = attributes.value("accession");

	// get ms_level

	if (accession.contains(ms_level_key)) {
		_ms_level = (attributes.value("value")).toInt();
	}

	if (_ms_level == 1) {

		if (accession.contains(rt_key)) {
			_retention_time = (attributes.value("value")).toDouble();
			QString unitAccession = attributes.value("unitAccession");
			if (unitAccession.contains(rt_in_minutes_key)) {
				_retention_time *= 60.;
			}
		}

		if (accession.contains(binary_64bit_mcq_double_key)) {
			_current_binary_precision = 64;
		}

		if (accession.contains(binary_32bit_mcq_double_key)) {
			_current_binary_precision = 32;
		}

		if (accession.contains(binary_nocompression_key)) {
		  _current_zlib_compression = false;
}

		if (accession.contains(binary_zlibcompression_key)) {
		  _current_zlib_compression = true;

		}

		if (accession.contains(mz_binary_key)) {			_current_binary_type = MZ_BINARY;
		}

		if (accession.contains(intensity_binary_key)) {
			_current_binary_type = INTENSITY_BINARY;
		}

	}

	_currentText.clear();
	return true;
}

bool MzmlSpectrumParser::startElement_binaryDataArrayList(
		const QXmlAttributes & attributes) {
	_current_spectrum = new spectrum();
	return true;
}

bool 
MzmlSpectrumParser::startElement_binaryDataArray(
		const QXmlAttributes & attributes) {
	QString encodedLength = attributes.value("encodedLength");
	if (encodedLength.isEmpty()) {
		throw mcqError(
				QObject::tr(
						"error : encodedLength attribute must not be empty"));
		return false;
	}
	_current_binary_length = encodedLength.toInt();
	return true;
}

bool MzmlSpectrumParser::endElement_binary() {

	// decoding of binary data arrays
	std::vector<mcq_double> decode;
	std::vector<mcq_double>::const_iterator it;

	const QByteArray encoded_data = _currentText.toAscii();
	if (encoded_data.isEmpty()) {
		qDebug() << "binary empty";
		return true;
	}
	if (_current_binary_type == MZ_BINARY) {
	 
	  decode = DecodeBinary::base64_decode_peaks_without_compressed_length_check
	    (encoded_data,
	     _current_binary_precision, 
	     false, 
	     _peakscount, 
	     _current_zlib_compression);
	  
	  _current_spectrum->reserve_mz(decode.size());
	  for (it = decode.begin(); it != decode.end(); ++it) {
	    _current_spectrum->setPeakMz(*it);
	  }
	  return true;
	}
	
	else if (_current_binary_type == INTENSITY_BINARY) {
	  
	  decode = DecodeBinary::base64_decode_peaks_without_compressed_length_check
	    (encoded_data, 
	     _current_binary_precision, 
	     false, 
	     _peakscount, 
	     _current_zlib_compression);
	  _current_spectrum->reserve_intensity(decode.size());
	  for (it = decode.begin(); it != decode.end(); ++it) {
	    _current_spectrum->setPeakIntensity(*it);
	  }
	  return true;
	}
	
	_currentText.clear();
	return true;
}

bool MzmlSpectrumParser::endElement_spectrum() {
	return (this->setSpectrum());
}
