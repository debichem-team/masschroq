/**
 * \file mzxmlSpectrumParser.cpp
 * \date June, 14 2011
 * \author Edlira Nano
 */

#include "mzxmlSpectrumParser.h"
#include "../encode/decodeBinary.h"
#include "../lib/consoleout.h"

MzxmlSpectrumParser::MzxmlSpectrumParser(Msrun * ms_run) :
		XmlSpectrumParser(ms_run) {
	_zlib_compression = false;
	_compressed_len = 0;
	_peakscount = 0;
}

MzxmlSpectrumParser::~MzxmlSpectrumParser() {
	_zlib_compression = false;
	_compressed_len = 0;
	_peakscount = 0;
}

bool 
MzxmlSpectrumParser::startElement(const QString & /* namespaceURI */,
				  const QString & /* localName */, 
				  const QString & qName,
				  const QXmlAttributes & attributes) {
	bool is_ok = true;
	if (qName == "scan") {
		is_ok = startElement_scan(attributes);
	}

	else if (qName == "peaks" && _ms_level == 1) {
		is_ok = startElement_peaks(attributes);
	}

	_currentText.clear();
	return is_ok;
}

bool MzxmlSpectrumParser::endElement(const QString & /* namespaceURI */,
				     const QString & /* localName */, 
				     const QString & qName) {
	if (qName == "peaks" && _ms_level == 1) {

		if (_scan_type == "Full" && _peakscount != 0) {
			//qDebug() << "Starting decode" << endl;
			//QTime tdecode;
			//tdecode.start();
			const QByteArray encoded_data = _currentText.toAscii();

			//compressionType="zlib"
			//    compressedLen="339"

			std::vector<mcq_double> decode = DecodeBinary::base64_decode_peaks(
					encoded_data,
					//_current_binary_length,
					//_current_binary_compression,
					_current_binary_precision, _is_bigendian, this->_peakscount,
					this->_zlib_compression, this->_compressed_len);
			//qDebug("Decode time is %d ms", tdecode.elapsed()); 
			// verify that _peakscount is the same as the size of the decoded
			// peaks vector

			unsigned int decoded_pairs = decode.size() / 2;
			if (_peakscount == decoded_pairs) {
				_current_spectrum = new spectrum();
				_current_spectrum->reserve(decoded_pairs);

				//qDebug() << "Spectrum decoded values (pairs of mz int)\n";
				//qDebug() << "Spectrum mz count = " << decoded_data.size() / 2; 

				for (unsigned int i = 0; i < decode.size(); i += 2) {
					// i = mz, i+1 = intensity  
					//qDebug() << "mz = " << decoded_data[i];
					_current_spectrum->setPeak(decode[i], decode[i + 1]);
				}
				return (this->setSpectrum());
			} else {
				// skip this scan 
				mcqout() << "MzxmlSpectrumParser : problem in scan number "
						<< _current_scan_number << " attribute peaksCount is "
						<< _peakscount << ", but we decoded " << decoded_pairs
						<< " peaks. Skipping scan.\n";
			}
		} else {
			// skip this scan
			qDebug() << "scan number " << _current_scan_number
					<< " is not a 'Full' scan type, so it is not handled;"
					<< " or peakscount = 0 so we skipped the scan";
		}
	}
	return true;
}

bool MzxmlSpectrumParser::startElement_scan(const QXmlAttributes & attributes) {
	QString ms_level = attributes.value("msLevel");
	if (ms_level.isEmpty()) {
		_errorStr = QObject::tr(
				"the msLevel attribute in mzXML file must not be empty\n");
		return false;
	}
	_ms_level = ms_level.toInt();

	if (_ms_level == 1) {

		_current_scan_number = attributes.value("num").toInt();

		QString temp = attributes.value("retentionTime").mid(2,
				(attributes.value("retentionTime").size() - 3));
		_retention_time = temp.toDouble();

		QString peaks_count = attributes.value("peaksCount");
		if (peaks_count.isEmpty()) {
			_errorStr =
					QObject::tr(
							"the peaksCount attribute in mzXML file must not be empty\n");
			return false;
		}
		_peakscount = peaks_count.toInt();

		QString lowMz = attributes.value("lowMz");
		QString highMz = attributes.value("highMz");

		if (lowMz.isEmpty() || highMz.isEmpty()) {
			_scan_type = "Full";
		} else {
			_low_mz = lowMz.toDouble();
			_high_mz = highMz.toDouble();
			if ((_high_mz - _low_mz) > 100)
				_scan_type = "Full";
			else
				_scan_type = "Zoom";
		}
	}
	return true;
}

bool MzxmlSpectrumParser::startElement_peaks(
		const QXmlAttributes & attributes) {
	QString precision = attributes.value("precision");
	if (precision.isEmpty())
		_current_binary_precision = 32;
	else
		_current_binary_precision = precision.toInt();

	//byteOrder="network"
	_is_bigendian = true;
	QString byteOrder = attributes.value("byteOrder");
	if (byteOrder.isEmpty()) {
		_is_bigendian = true;
	} else {
		if (byteOrder != "network") {
			_is_bigendian = false;
		}
	}

	//compressionType="zlib"
	QString compressionType = attributes.value("compressionType");
	if ( compressionType.isEmpty() || 
	     compressionType != "zlib") {
		_zlib_compression = false;
	} else {
		_zlib_compression = true;
	}
	//    compressedLen="339"
	QString compressedLen = attributes.value("compressedLen");
	if (compressedLen.isEmpty()) {
		this->_compressed_len = 0;
	} else {
		_compressed_len = compressedLen.toInt();
	}
	return true;
}

