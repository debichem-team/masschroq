/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file pepxmlHandler.h
 * \date November 25, 2010
 * \author Edlira Nano
 */

#ifndef PEPXML_HANDLER_H_
#define PEPXML_HANDLER_H_ 1
/**
 * \class PepxmlHandler
 * \brief QXmlDefaultHandler derived class for the parsing of 
 * the pepXML input files (SAX parsing)
 */

class PepxmlHandler : public QXmlDefaultHandler {

 public :
  
  PepxmlHandler(msRun * ms_run);
  virtual ~PepxmlHandler();
  
  virtual bool startElement(const QString &namespaceURI, 
			    const QString &localName,
			    const QString &qName, 
			    const QXmlAttributes &attributes);
  
  virtual bool endElement(const QString &namespaceURI,
			  const QString &localName,
			  const QString &qName);

 protected :
   
 private :
  

};

#endif /* PEPXML_HANDLER_H_*/
