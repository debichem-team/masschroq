/**
 * \file mzmlSimpleParser.cpp
 * \date November 15, 2010
 * \author Edlira Nano
 */

#include "mzmlSimpleParser.h"
#include "../lib/mcq_error.h"

MzmlSimpleParser::MzmlSimpleParser(Msrun * ms_run) : 
  XmlSimpleParser(ms_run)
{
}

MzmlSimpleParser::~MzmlSimpleParser() {
}

bool 
MzmlSimpleParser::startElement(const QString & /* namespaceURI */,
				const QString & /* localName */, 
				const QString & qname,
				const QXmlAttributes & attributes) {
  
  bool is_ok = true;
  
  if (qname == "spectrum") {
    is_ok = startElement_spectrum(attributes);  
  }
  else if (qname == "cvParam") {
    is_ok = startElement_cvParam(attributes);  
  }
  else if (qname == "precursorList") {
    is_ok = startElement_precursorList(attributes);  
  }
  else if (qname == "precursor") {
    is_ok = startElement_precursor(attributes);  
  }
  
  _currentText.clear();
  return is_ok;
}

bool
MzmlSimpleParser::startElement_spectrum(const QXmlAttributes &attributes) {
  QString spectrumId = attributes.value("id");
  if (spectrumId.isEmpty()) {
    _errorStr = QObject::tr("The spectrum id attribute must not be empty\n");
    return false;
  }
  try  {
    _current_scan_number = get_scan_number(spectrumId);
  } catch (mcqError error) {
    _errorStr = QObject::tr("mzml parsing scan number problem :\n%1").arg(error.what());
    return false;
  }

  return true;
}

const int
MzmlSimpleParser::get_scan_number(const QString & spectrum_id) const {
  QRegExp scan_exp("scan=(\\d+)");
  int pos = scan_exp.indexIn(spectrum_id);
  if (pos > -1) {
    QString value = scan_exp.cap(1);
    return value.toInt();
  }
  else 
    throw mcqError(QObject::tr("error : cannot find the scan number in attribute: %1 \n").arg(spectrum_id));
}

bool
MzmlSimpleParser::startElement_precursorList(const QXmlAttributes &attributes) {
  QString count = attributes.value("count");
  if (count.isEmpty()) {
    _errorStr = QObject::tr("The count attribute of precursorList is empty");
    return false;
  }
  
  int prec_nb = count.toInt();
  
  if (prec_nb > 1) {
    _errorStr = QObject::tr("More than one precursor runs are not supported");
    return false;
  }
  
  return true;
}

bool
MzmlSimpleParser::startElement_precursor(const QXmlAttributes &attributes) {
  _precursor_scan_num = _current_scan_number;
  return true;
}

bool
MzmlSimpleParser::startElement_cvParam(const QXmlAttributes &attributes) {

  const QString ms_level_key("MS:1000511"); 
  const QString rt_key("MS:1000016");
  const QString rt_in_minutes_key("UO:0000031");
  const QString rt_in_secs_key("UO:0000010");
  const QString low_mz_key("MS:1000528");
  const QString high_mz_key("MS:1000527");
  const QString precursor_mz_key("MS:1000744");
  const QString precursor_intensity_key("MS:1000042");
  const QString total_ion_current("MS:1000285");

  // const QString mz_binary_key("MS:1000514");
  // const QString intensity_binary_key("MS:1000515"); 
  // const QString rt_binary_key("MS:1000595");
  // const QString binary_32bit_mcq_double_key("MS:1000521");
  // const QString binary_64bit_mcq_double_key("MS:1000523");
  // const QString binary_nocompression_key("MS:1000576");
  // const QString binary_zlibcompression_key("MS:1000574");
  
  QString ref = attributes.value("cvRef");
  QString accession = attributes.value("accession");

  // get ms_level
  
  if (accession.contains(ms_level_key)) {
    _ms_level = (attributes.value("value")).toInt();
  }
  
  // for all ms_levels get rt
  
  if (accession.contains(rt_key)) {
    _retention_time = (attributes.value("value")).toDouble();
    QString unitAccession = attributes.value("unitAccession");
    if (unitAccession.contains(rt_in_minutes_key)) {
      _retention_time *= 60.;
    }
  }
  
  if (_ms_level == 1) {
    if (accession.contains(low_mz_key)) {
      _low_mz = (attributes.value("value")).toDouble();
      this->set_low_mz();
    }
    if (accession.contains(high_mz_key)) {
      _high_mz = (attributes.value("value")).toDouble();
      this->set_high_mz();
    }
  }
  
  // for ms_level >= 2  get precursor_mz and precursor_rt
  if (_ms_level >= 2) {
    if (accession.contains(precursor_mz_key)) {
      _precursor_mz = (attributes.value("value")).toDouble();
      _precursor_rt = _retention_time;
    }
  }
  
  // only for ms level 2 get precursor _intensity_level1 (if 0 = ticlevel)
  if (_ms_level == 2) { 
    if (accession.contains(precursor_intensity_key)) {
      _precursor_intensity_level2 = (attributes.value("value")).toDouble();
      if (_precursor_intensity_level2 == 0) {
	_precursor_intensity_level2 = _tic_level2;
      }
    }
    
    if (accession.contains(total_ion_current)) {
      _tic_level2 = (attributes.value("value")).toDouble();
      if (_precursor_intensity_level2 == 0) {
	_precursor_intensity_level2 = _tic_level2;
      }
    }
  }
  
  _currentText.clear();
  return true;
}


bool 
MzmlSimpleParser::endElement(const QString & /* namespaceURI */,
			      const QString & /* localName */, 
			      const QString & qname) {
  
  bool is_ok = true;
  
  if (qname == "precursor") {
    is_ok = setPrecursor(_precursor_scan_num, _precursor_rt,
			 _precursor_intensity_level2, _precursor_mz);
  }
  
  else if (qname == "spectrum" && _ms_level == 1) {
    _msrun->addOriginalRetentionTime(_retention_time);
  }  
  
  _currentText.clear();
  return is_ok;
}  
