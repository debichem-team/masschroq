#include <qpid/messaging/Connection.h>
#include <qpid/messaging/Message.h>
#include <qpid/messaging/Receiver.h>
#include <qpid/messaging/Sender.h>
#include <qpid/messaging/Session.h>

#include <iostream>

#include "../../lib/mass_chroq.h"
#include "../../lib/share/utilities.h"
#include "../../reporters/ReporterConsole.h"
#include <QString>
#include <QLocale>
#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QDebug>

using namespace std;

int main(int argc, char** argv) {
  QCoreApplication app(argc, argv);
  QLocale::setDefault(QLocale::system());
  QStringList arguments_total(app.arguments());
  QStringList arguments;
  
  QString name="";
  QStringList::const_iterator iter_args;
  for (iter_args = arguments_total.begin(); 
       iter_args != arguments_total.end(); 
       ++iter_args) {
    if (*iter_args == "-n") {
      ++iter_args;
      name = *iter_args;
    } else {
      arguments.append(*iter_args);
    }
  }
  if (name == "") {
    std::cout << "please specify listener name with -n option" << std::endl;
    return 1;
  }
  
  QString broker = "localhost:5672";
  if (arguments.size() > 1) {
    broker = arguments.at(1);
  }
    
  QString connectionOptions = "";
  if (arguments.size() > 4) {
    connectionOptions = arguments.at(4);
  }

  qpid::messaging::Connection connection(broker.toStdString(), 
					 connectionOptions.toStdString());
  try {
 
    connection.open();
    qpid::messaging::Session session = connection.createSession();
    qpid::messaging::Receiver receiver = session.createReceiver("masschroq_request_queue; {create:always, delete:always}");
    
    while (true) {
      qpid::messaging::Message message = receiver.fetch();
      const qpid::messaging::Address& response_address = message.getReplyTo();
      
      std::cout << message.getContent() << std::endl;
      
      MassChroq mass_chroq_engine;

      QString tmpDirName = QDir::currentPath();
      const QString masschroq_dir_path(QCoreApplication::applicationDirPath());
      const QDateTime dt_begin = QDateTime::currentDateTime();
      mass_chroq_engine.setMasschroqDir(masschroq_dir_path);
      mass_chroq_engine.setTmpDir(tmpDirName);
      mass_chroq_engine.setBeginDateTime(dt_begin);
      QString xmlContent(message.getContent().c_str());
      
      mass_chroq_engine.runXmlString(xmlContent);
      
      const QDateTime dt_end = QDateTime::currentDateTime();
      
      const Duration dur = Utilities::getDurationFromDates(dt_begin,
							   dt_end);
      
      cout << "MassChroQ's execution time was : "
	   << Utilities::getDaysFromDuration(dur) << " days, "
	   << Utilities::getHoursFromDuration(dur) << " hours, "
	   << Utilities::getMinutesFromDuration(dur)
	   << " minutes, " << Utilities::getSecondsFromDuration(dur) 
	   << " seconds." << endl;
      
      qpid::messaging::Sender sender = 
	session.createSender(response_address);
      QString response(name);
      response.append(": Miam! the masschroq message was tasty! I want more!");
      sender.send(response.toStdString());
      session.acknowledge();  
    }
    
    connection.close();
    return 0;
  } catch (mcqError error) {
    cerr << "Oops! an error occurred in MassChroQ. Dont Panic :"
	 << endl;
    cerr << error.qwhat().toStdString() << endl;
    } catch (const std::exception& error) {
    std::cerr << error.what() << std::endl;
    connection.close();
    return 1;
  }
}
