/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file pepParser.h
 * \date December 03, 2010
 * \author Edlira Nano
 */

#ifndef PEP_PARSER_H_
#define PEP_PARSER_H_ 1

#include <QFileInfo>
#include <QTextStream>

class MassChroqDomEngine;

/**
 * \class PepParser
 * \brief Parser of a peptide identification text file. 

 * The peptide text file is organised into rows and columns. Columns 
 * are seperated by a tabulation, a comma ',' or a semi-colon ';'. Once a separation 
 * character is used in the header, every separator in the file has to be the same, 
 * otherwise an error is thrown. The text file must contain the following header : 
 * scan[sep]sequence[sep]mh[sep]z[sep]protein
 * and an optional 'mods' 6th column can be added.   
 * 'sequence' is the sequence of a peptide, 
 * 'mh' is its mass + mass of an H+ ion set to 1.007825 in MassChroQ, 
 * 'protein' is the description of a single protein the peptide is identified in, 
 * 'scan' is the scan number of the spectrum the peptide is observed in the mzXML/mzML file, 
 * 'z' is its charge,
 * 'mods' is an optional column containing free text and/or numbers that the user chooses. 
 * If the same peptide is identified in more than one protein, one 
 * line per protein must be filled (same peptide, scan, mh ...)
 */


class PepParser {
  
 public : 
  
  /// Constructor that takes the name of the peptide file to parse  
  PepParser(const QString & filename);
  
  virtual ~PepParser();
  
  /// main method doing all the parsing and passing proteins and
  /// peptides information to the dom reader/writer class
  void parse(MassChroqDomEngine * m_engine);

 protected :
 
  /// method that takes a line and parses it
  void processLine(QString & line);

  /// verifies if the given line is a header line or not
  const bool isHeader(const QString & header_line);

  /// get the separator for the peptide file using header line;
  /// the other lines will be expected to use the same separator.
  const QString getSeparator(const QString & header_line);

  /// parses a peptide sequence 
  const QString processSequence(QString & seq_token);

  /// parses a peptide mh
  const QString processMh(QString & mh_token);

  /// parses a protein description field field
  const QString processProtOrMods(QString & prot_token);

  /// parses a scan number field
  const int processScan(QString & scan_token);

  /// parses a peptide charge  field
  const int processCharge(QString & charge_token);
 
 private :
  
  const QString whatSeparator() const;

  /// peptide's text file information
  QFile * _pep_file;
  
  /// peptide's text file input stream
  QTextStream * _pep_txt_stream;

  /// separator used for this file
  QString _sep;
  
  /// line number being currently parsed
  unsigned int _line_number;
  
  /// current parsed data (in the current line)
  QString _current_sequence;
  QString _current_prot_desc;
  QString _current_mh;
  int _current_scan;
  int _current_z;
  QString _current_mods;
  
   
};
#endif /* PEP_PARSER_H_ */
