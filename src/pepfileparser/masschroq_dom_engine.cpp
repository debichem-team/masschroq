/**
 * \file masschroq_dom_engine.cpp
 * \date December 8, 2010
 * \author Edlira Nano
 */


#include "masschroq_dom_engine.h"
#include "../lib/mcq_error.h"
#include "pepParser.h"

#include <QTextStream>  
#include <QDebug>  
#include <iostream>

using namespace std;

MassChroqDomEngine::MassChroqDomEngine(QFile & file) 
  : 
  _filename(file)
{

  _prot_counter = 0;
  _pep_counter = 0;
  
  _domDoc = QDomDocument("MasschroqXML");
  if (!file.open(QIODevice::ReadOnly)) {
    throw mcqError(QObject::tr("Cannot read MassChroq param file : %1").arg(_filename.filePath()));
  }
  
  QString errorMsg;
  int lineNum;
  int colNum;
  
  // parse the file and construct the tree
  if (!_domDoc.setContent(&file, &errorMsg, &lineNum, &colNum)) {
    file.close();
    QString ln, cn;
    ln.setNum(lineNum);
    cn.setNum(colNum);
    throw mcqError(QObject::tr("Cannot read MassChroq param file %1 : \n %2 at line %3, column %4.").arg(_filename.filePath(), errorMsg, ln, cn));  
  }
 
  // we do not need the file anymore
  file.close();
}

const QString
MassChroqDomEngine::parse() {
  // look for the peptide_files_list tag presence
  QDomElement root = _domDoc.documentElement();
  // if(root.tagName() != "masschroq") 
  QDomNodeList pep_files_tag = root.elementsByTagName("peptide_files_list");
  
  if (pep_files_tag.isEmpty()) {
    return _filename.filePath();
  } else {
    std::cout << "Found peptide text files to parse " << std::endl;  
    QDomNodeList pepList = (pep_files_tag.item(0)).childNodes();
    // parse pep files and get all information from them in the maps
    parsePepFiles(pepList);
    
    // write info to a new XML input file
    writeXmlFile(pep_files_tag, root);
    
    // create a new file to put the info
    QString new_filename(_filename.path());
    new_filename.append("/parsed-peptides_");
    new_filename.append(_filename.fileName());
    QFile newfile(new_filename);
    std::cout << "Creating new XML input file with the parsed peptides '"
	      << new_filename.toStdString() << "'" << std::endl;
    
    if ( !newfile.open(QIODevice::WriteOnly) )
      throw mcqError(QObject::tr("Cannot create new XML input file with parsed peptides : '%1'\nCheck the current working directory's permissions.").arg(new_filename));
    QTextStream ts(&newfile);
    ts << _domDoc.toString();
    newfile.close();
    return new_filename; 
  }
}
 
MassChroqDomEngine::~MassChroqDomEngine() {
  
}

void
MassChroqDomEngine::parsePepFiles(const QDomNodeList & pepList) {
  /// <peptide_file data="samp0" path="peptide0.txt"/>
  for (unsigned int i = 0; i < pepList.length(); ++i) {
    QDomElement pepfile_el = pepList.item(i).toElement();
    QString data = pepfile_el.attribute("data");
    QString pepfile = pepfile_el.attribute("path"); 
    if (data.isEmpty()) {
      throw mcqError(QObject::tr("Error in MassChroQ param file %1 : \n the peptide_file tag must have a data attribute.").arg(_filename.filePath()));
    }
       
    if (pepfile.isEmpty()) {
      throw mcqError(QObject::tr("Error in MassChroQ param file %1 : \n the peptide_file tag must have a path attribute.").arg(_filename.filePath()));
    }
    
    _current_data = data;
    PepParser pep_parser(pepfile);
    pep_parser.parse(this);
  } // parsing finished
  // we can now write protein_list and peptide_list to the XML file
}

void
MassChroqDomEngine::updateMaps(const QString & sequence, 
			       const QString & mh, 
			       const QString & prot_desc, 
			       const int scan, 
			       const int z,
			       const QString & mods) {


  // update _proteins_map : if prot_desc is a new protein add it in the
  // map and generate a new id for this protein, else find the id of this
  // in the map
 
  map<const QString, const QString>::const_iterator itprot
    = _proteins_map.find(prot_desc);
  
  if (itprot == _proteins_map.end()) {
    generate_prot_xmlId();
    _proteins_map.insert(_proteins_map.begin(),
			 pair<QString, QString>(prot_desc,
						_current_prot_xmlId));
  } else {
    _current_prot_xmlId = itprot->second;
  }
  
  // update _peptides_map : if new peptide add a new QDomElement, else : 
  // update the existing one

  const PepId cur_pep = pair<const QString, const QString>(sequence, mh);
  
  map<const PepId, QDomElement>::const_iterator itpep 
    = _peptides_map.find(cur_pep);
  
  // if it is a new peptide
  if (itpep == _peptides_map.end()) {
    generate_pep_xmlId();
    // create new entry
    QDomElement new_pep = _domDoc.createElement("peptide");
    new_pep.setAttribute("prot_ids", _current_prot_xmlId);
    new_pep.setAttribute("mh", mh);
    new_pep.setAttribute("seq", sequence);
    new_pep.setAttribute("id", _current_pep_xmlId);
    if (!mods.isEmpty()) {
      new_pep.setAttribute("mods", mods);
    }
    
    QDomElement new_obs = _domDoc.createElement("observed_in");
    new_obs.setAttribute("data", _current_data);
    new_obs.setAttribute("z", z);
    new_obs.setAttribute("scan", scan);

    new_pep.appendChild(new_obs);
    
    _peptides_map[cur_pep] = new_pep;
    QString prots = new_pep.attribute("prot_ids");
  }
  else { // if the peptide already exists, update domchildelements + 
    // domelement, scan, z, prots and mods 
    

    // update the prot_ids attribute if neccessary
    QDomElement pep_el = itpep->second;
    QString prots = pep_el.attribute("prot_ids");
    //if (prots.isEmpty())
    //  throw mcqError(QObject::tr("This is a bug in MassChroqDomEngine::updateMaps : \n the prot_ids attribute is empty and it should not."));
    
    if ( !prots.contains(_current_prot_xmlId) ) {
      prots.append(" ").append(_current_prot_xmlId);
      pep_el.setAttribute("prot_ids", prots);
    }
    
    // update the mods attribute if neccessary
    
    QString mods_attr = pep_el.attribute("mods");
    if ( !mods.isEmpty() &&
	 !mods_attr.contains(mods) ) {
      mods_attr.append(" ").append(mods);
      pep_el.setAttribute("mods", mods_attr);
    }
    
    // update the observed_in elements if necessary
    QDomNodeList pep_obs = pep_el.elementsByTagName("observed_in");
    bool is_present = false;
    for (unsigned int i = 0; i < pep_obs.length(); ++i) {
      QDomElement obs = (pep_obs.item(i)).toElement();
      QString prev_data = obs.attribute("data");
      int prev_scan = (obs.attribute("scan")).toInt();
      int prev_z = (obs.attribute("z")).toInt();
      if ( prev_data == _current_data && 
	   prev_scan == scan &&
	   prev_z == z ) {
	is_present = true;
	break;
      }
    }
    if (!is_present) {
      QDomElement new_obs = _domDoc.createElement("observed_in"); 
      new_obs.setAttribute("data", _current_data);      
      new_obs.setAttribute("z", z);
      new_obs.setAttribute("scan", scan);
      
      pep_el.appendChild(new_obs);
    }
  }
}


void
MassChroqDomEngine::generate_prot_xmlId() {
  QString protId("P"), tmp;
  tmp.setNum(_prot_counter);
  protId.append(tmp);
  _current_prot_xmlId = protId;
  _prot_counter++;
}

void
MassChroqDomEngine::generate_pep_xmlId() {
  QString pepId("pep"), tmp;
  tmp.setNum(_pep_counter);
  pepId.append(tmp);
  _current_pep_xmlId = pepId;
  _pep_counter++;
}


void
MassChroqDomEngine::writeXmlFile(QDomNodeList & pep_files_tag,
				 QDomElement & root) {
  // write info to XML input file
  // but first delete the peptide_file nodes
  root.removeChild(pep_files_tag.item(0));
  
  // write proteins
  QDomNodeList groups_list = root.elementsByTagName("groups");
  QDomElement groups = (groups_list.item(0)).toElement();
  QDomElement protein_list = _domDoc.createElement("protein_list");
  map<const QString, const QString>::const_iterator itprots;
  for (itprots = _proteins_map.begin();
       itprots != _proteins_map.end();
       ++itprots) {
    
    QDomElement new_prot = _domDoc.createElement("protein");
    new_prot.setAttribute("desc", itprots->first);
    new_prot.setAttribute("id", itprots->second);
    protein_list.appendChild(new_prot);
  }
  root.insertAfter(protein_list, groups);
  // write peptides
  QDomElement peptide_list = _domDoc.createElement("peptide_list");

  map<const PepId, QDomElement>::const_iterator itpeps;
  for (itpeps = _peptides_map.begin();
       itpeps != _peptides_map.end();
       ++itpeps) {
    peptide_list.appendChild(itpeps->second);
  }
  root.insertAfter(peptide_list, protein_list);
}

