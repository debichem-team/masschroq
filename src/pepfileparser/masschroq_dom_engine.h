/*
*
* MassChroQ: Mass Chromatogram Quantification software.
* Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
 * \file masschroq_dom_engine.h
 * \date December 8, 2010
 * \author Edlira Nano
 */

#ifndef MASSCHROQ_DOM_ENGINE_H_
#define MASSCHROQ_DOM_ENGINE_H_ 1

#include <utility>
#include <map>
#include <QString>
#include <QDomDocument>
#include <QFileInfo>
#include <QPair>


/// type corresponding to a unique peptide identification : 
/// pair of an amino-acid sequence (I translated to L) and an mh value.
typedef std::pair<const QString, const QString> PepId;


/**
 * \class MassChroqDomEngine
 * \brief Dom handler that reads the XML input file and if it finds a 
 * "peptide_files" tag it launches the parsing of the peptide text files.
 * It then writes into the XML input file the corresponding peptide_list
 * and protein_list blocks parsed.
 */

class MassChroqDomEngine {
 
 public:
  
  /// Constructor that takes the XML input file of MassChroQ
  MassChroqDomEngine(QFile & file);
  
  ~MassChroqDomEngine();

  const QString parse();
  
  /// method called by the peptide text parsers to put peptides
  /// and proteins in the member hash maps
  void updateMaps(const QString & sequence, const QString & mh, 
		  const QString & prot_desc, const int scan, 
		  const int z, const QString & mods);
  
 protected :
  
  /// method that parses the peptide text files one by one
  void parsePepFiles(const QDomNodeList & pepList);
  
   
  /// method that writes the peptide_list and protein_list blocks into
  /// the XML input file
  void writeXmlFile(QDomNodeList & pep_files_tag,
		    QDomElement & root);
  
 private :

  /// method that generates a unique protein xml id
  void generate_prot_xmlId();
  
  /// method that generates a unique peptide xml id
  void generate_pep_xmlId();
  
  /// name of the XML input file
  const QFileInfo _filename;
  
  /// Dom document corresponding to the XML input file
  QDomDocument _domDoc;
  
  /// current sample data corresponding to the current peptide 
  /// text file being parsed
  QString _current_data;
  
  /// counter for proteins (used to generate xml id)
  int _prot_counter;
  /// counter for peptidess (used to generate xml id)
  int _pep_counter;

  /// current protein (resp. peptide) xml id
  QString _current_prot_xmlId;
  QString _current_pep_xmlId;
  
  /// map where each peptide and its data is stocked
  /// with peptide unicity : two peptides are identified if
  /// they have the same sequence (where I is translated to L)
  /// and the same mh, i.e. the same PepId.
  std::map<const PepId, QDomElement> _peptides_map;
  
  /// map protein description -> unique protein xml id
  std::map<const QString, const QString> _proteins_map;
  
};


#endif /* MASSCHROQ_DOM_ENGINE_H_ */
