[Desktop Entry]
Name=MassChroQ GUI ${MASSCHROQ_VERSION}
Categories=Education;Science;Math;
Comment=MassChroQ GUI launcher
Exec=masschroq_gui %U
Icon=${CMAKE_INSTALL_PREFIX}/share/masschroq/masschroq.svg
Terminal=false
Type=Application
StartupNotify=true